<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Addresses extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address_type', 'linked_id', 'address_title', 'address_line1', 'address_line2', 'contact_person_name', 'contact_person_mobile', 'city', 'state', 'country', 'pincode','latitude','longitude'
    ];
	
	protected $table= 'addresses';
}
