<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
  protected $fillable = ['parent_id','title','photo','status','sort_index'];
  protected $table = 'categories';
  
  
  
  
 
    public function getParentCategoryTitleAttribute($value) {
          return  @\App\Categories::where('id',$this->parent_id)->first(['title'])->title;
   }
   
}
