<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturesSettings extends Model
{
  protected $fillable = ['title','status','type'];
  protected $table = 'features_settings'; 
}
