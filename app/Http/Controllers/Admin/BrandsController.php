<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Permission;
use App\ProductVariants;
use App\ProductVariantTypes;
use App\Brands;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class BrandsController extends Controller
{
    
    public function index(Request $request)
    {
	 
	 
		//filters
		$request_data = @$request[0];
		
	 
		$parent_id = @$request_data['parent_id'];
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
	 
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Brands List';
		$model_name = 'brands';
	    $modelName = "App\Brands";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
		
		       $data_array = new \App\Brands;
 
	   
 
	   
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
		   
	 
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		   
	   }
	   
 
	   
	   
	      if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->paginate($limit);
 
 foreach($data_array as $d)
 {
	 $d->value = $d->id;
 }
				
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					return $data;
    }
	
 
 
    public function store_brand(Request $request)
    {
    $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = Brands::create($request->toArray()))  {
 
           flash('Brand has been created.');

        } else {
            flash()->error('Unable to create Brand.');
        }

        return back();
    }

 
    public function update_brand(Request $request, $id)
    {
 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = Brands::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Brand has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
 
		if( \App\Brands::findOrFail($id)->delete() ) {
            flash()->success('Brand has been deleted');
        } else {
            flash()->success('Brand not deleted');
        }

        return redirect()->back();
    }

 
}
