<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Permission;
use App\Categories;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class CategoriesController extends Controller
{
    
	
		public function get_increment_category_title($category_id)
	{
		$title = @\App\Categories::where('id',$category_id)->first(['title'])->title;
		$parent_id = @\App\Categories::where('id',$category_id)->first(['parent_id'])->parent_id;
       
	    if($parent_id == '' || $parent_id == null )
		{
			$next_parent_available = '1'; 
	   }
	   else
	   {
		   $next_parent_available = '0'; 
	   }
    		$d['parent_id'] = $parent_id;			
			$d['title'] = $title;			
			$d['next_parent_available'] = $next_parent_available;	
 		return $d;	
		
	}
	
		public function get_category_title($category_id,$title )
	{
		 
		 	$d= $this->get_increment_category_title($category_id);
	 
	 
		if($d['next_parent_available'] == 1 or $d['next_parent_available'] == '1' )
		{
			$title = @$title."->".$d['title'];
			
			return $title ;
		    $this->get_category_title($d['parent_id'],$title);
		}
		else
		{
			return $title;
		}
		
	}
	
	
	
    public function index(Request $request)
    {	 
		//filters
		$request_data = @$request[0];
		
	 
		$parent_id = @$request_data['parent_id'];
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
	 
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Categories List';
		$model_name = 'categories';
	    $modelName = "App\Categories";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
		
		       $data_array = new \App\Categories;
 
	   
 
	   
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
		   
	 
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		   
	   }
	   
	   	 if(isset($request_data['parent_id']) && $request_data['parent_id'] != '' && $request_data['parent_id'] != null)
	   {
	     $data_array = $data_array->where('parent_id',$request_data['parent_id']);
	   }
	   
	   
	      if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->paginate(1000000);
 
 
 
 foreach($data_array as $d)
 {
	 
	  
	 //return $this->get_category_title($d->id,$d->title );
	 
	// $d->title = $this->get_category_title($d->id,$d->title );
 }
 
 
 
 foreach($data_array as $d)
 {
	 $d->value = $d->id;
 }
				
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					return $data;
    }
	
 
 
 
  public function view_categories(Request $request)
    {
		
	 
		//filters
		$request_data = @$request[0];
		
	 
		$parent_id = @$request_data['parent_id'];
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
	 
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Categories List';
		$model_name = 'categories';
	    $modelName = "App\Categories";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
		
		       $data_array = new \App\Categories;
 
	   
 
	   
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
		   
	 
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		   
	   }
	   
	   	 if(isset($request_data['parent_id']) && $request_data['parent_id'] != '' && $request_data['parent_id'] != null)
	   {
	     $data_array = $data_array->where('parent_id',$request_data['parent_id']);
	   }
	   
	   
	      if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->orderBy('sort_index', 'ASC')->paginate(10000);
				
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					
					if(isset($_GET["request_type"]) && $_GET["request_type"] = 'ajax')
					{
						return $data;
					}
					$result = $data_array;
					return view('categories.index', compact('result','model_name','display_modal_name'));
					
    }
	
	
	
 
    public function store(Request $request)
    {
		$flight = new Categories;
        $flight->title =  $request[0]['title'];
		$flight->photo =  $request[0]['photo'];
		
		if($request[0]['parent_id'] == '' or $request[0]['parent_id'] == null or $request[0]['parent_id'] ==' ')
		{
			$flight->parent_id =  'jj';
		}
		else
		{
			$flight->parent_id =  $request[0]['parent_id'];
		}
		
        $flight->save();

		
		           if($flight != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $flight ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
	
	
	public function edit($id)
    {
      $categories = \App\Categories::find($id);
	  return $categories;
		
	}

 
    public function update(Request $request)
    {
 
		// Get the user
        $user = Categories::findOrFail($request[0]["category_id"]);
        // Update user
        $user->title =  $request[0]['title'];
		$user->photo =  $request[0]['photo'];
		$user->parent_id =  $request[0]['parent_id'];
        $user->save();
        if($user != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $user ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
 
 
 
 
    public function destroy($id)
    {
        if( \App\Categories::findOrFail($id)->delete() ) {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Deleted Successfully';
                    $data['data'] = [] ;
        } else {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Error Occurred';
                    $data['data'] = [] ;
        }

        return $data;
    }

 
}
