<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...

class DashboardController extends Controller
{
     use feature;   
     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    

	//================================================================== USERS CRUD FUNCTION STARTS ==============================================================================//
	
   // 1. =================== Get all users ====================	
   public function get_dashboard_data(Request $request)
    {
		
		
	  $login_id = @Auth::id();
	  $user_type = @Auth::user()->user_type;
      $role_id = @\App\Role::where('name','Vendor')->first(['id'])->id;
	  
	  
		
    // graph 1 starts here
    $main_array = array();
	
	$d['type'] ='recentOrders';
	$orders = \App\Orders::orderBy('created_at','desc')->get()->take(5);
	
	
			 	   if($user_type ==$role_id )
	   {
	     $orders = \App\Orders::join('orders_meta_values', 'orders_meta_values.order_id', '=', 'orders.id')->where('orders_meta_values.order_meta_type_id',8)->where('orders_meta_values.value',$login_id)->orderBy('orders.id','DESC')->get()->take(5);
	   }
	   else
	   {
		    $orders = \App\Orders::orderBy('created_at','desc')->get()->take(5);
	   }
	   
	  
	   
 
	foreach($orders as $order)
	{
        $order->created_at_formatted = $order->created_at->diffForHumans();
	   $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	   $cust_id =@\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where("order_id",$order->id)->first(['value'])->value;
	   $order->custId = $cust_id; 
	   $order->cust = @\App\User::where("id",$cust_id)->first(['first_name'])->first_name." ".@\App\User::where("id",$cust_id)->first(['last_name'])->last_name;
	   $order->color = @\App\OrdersStatus::where("identifier",$order->order_status)->first(['label_colors'])->label_colors;
	   $order->total = @\App\OrdersPayments::where("order_id",$order->id)->first(['total'])->total.", Cash";
	  
	}	
	$d['data'] = $orders;
  
    $main_array[] = $d;

 /** 
 //graph 2 starts here
  $d2_data = array();
  $d2["type"] = 'sales'; 
       for($t = 0; $t <= 12; $t++) 
     {
       $dates[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
     }

	 
 
$users_graph_data = array();
$total = 0; 
foreach(array_reverse($dates)  as $date)
{
	     $ud['label'] = @\Carbon\Carbon::parse($date)->format('M');
		 $ud['date'] = $date;
		 $orders = @\App\Orders::whereDate('created_at', 'like', "%".$date."%")->get();
		 $t_sum = 0;
		 foreach($orders as $o)
		 {
			 $t = @\App\OrdersPayments::where('order_id',@$o->id)->first(['total'])->total;
			 $t = intval($t);
			 $total = @$total + $t;
			 $t_sum = @\App\OrdersPayments::where('order_id',@$o->id)->sum('total');
			  
		 }
		 $ud['y'] = @$t_sum;
		 $d2_data[] = @$ud;
}
$d2["total"] = $total;
$d2["desc"] = "Including all Active & Completed Orders";
$d2["data"] = $d2_data;
$main_array[] = $d2;

**/

 //graph 2 starts here
  $d2_data = array();
  $d2["type"] = 'sales'; 
       for($t = 0; $t <= 12; $t++) 
     {
       $dates[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
     }

	 
 
$users_graph_data = array();
$total = 0; 
foreach(array_reverse($dates)  as $date)
{
	     $ud['label'] = @\Carbon\Carbon::parse($date)->format('M');
		 $ud['date'] = $date;
		 $orders = @\App\OrdersTransactions::whereDate('created_at', 'like', "%".$date."%")->get();
		 
		 $orders_count = @\App\OrdersTransactions::whereDate('created_at', 'like', "%".$date."%")->sum("amount");
 
		 $ud['y'] = @$orders_count;
		 $d2_data[] = @$ud;
		 @$total = @$total + $orders_count;
}
$d2["total"] = $orders_count;
$d2["desc"] = "Including all Active & Completed Orders";
$d2["data"] = $d2_data;
$main_array[] = $d2;

 
 




 //graph 3 starts here
  $d3_data = array();
  $d3["type"] = 'orders'; 
  $dates3 = array();
       for($t = 0; $t <= 12; $t++) 
     {
       $dates3[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
     }

	  
 
$users_graph_data = array();
$total3 = 0;
foreach(array_reverse($dates3)  as $date)
{
	     $ud3['label'] = \Carbon\Carbon::parse($date)->format('M');
		 $ud3['date'] = $date;
		
		 
		 
		 
		 
		 	   if($user_type ==$role_id )
	   {
	     $orders3 = \App\Orders::join('orders_meta_values', 'orders_meta_values.order_id', '=', 'orders.id')->where('orders_meta_values.order_meta_type_id',8)->where('orders_meta_values.value',$login_id)->orderBy('orders.id','DESC')->paginate(1000);
	   }
	   else
	   {
		    $orders3 = \App\Orders::whereDate('created_at', 'like', "%".$date."%")->get();
	   }
		   
		   
		 
		   
		 foreach($orders3 as $o)
		 {
			 $t = @\App\OrdersPayments::where('order_id',@$o->id)->first(['total'])->total;
			 $t = intval($t);
			 $total3 = $total3 + $t;
		 }
		 $ud3['y'] = \App\Orders::whereDate('created_at', 'like', "%".$date."%")->count();
		 $d3_data[] = $ud3;
}
$d3["total"] = $total3;
$d3["desc"] = "Total Order of Last 12 Months";
$d3["data"] = $d3_data;


 

$main_array[] = $d3;

















 //graph 4 starts here NEW USERS
  $d4_data = array();
  $d4["type"] = 'newUsers'; 
  $users = \App\User::orderBy("id","desc")->groupBy('user_type')->get();
 
   foreach($users as $u)
  {
	  $u->user_type_title = @\App\Role::where('id',$u->user_type)->first(['name'])->name;
  }
  
  
  $d4["data"] = $users;
  $main_array[] = $d4;
  
   //graph 4 starts here NEW ITEMS
  $d5_data = array();
  $d5["type"] = 'newItems'; 
    $items = \App\Products::orderBy("id","desc")->get()->take(5);
  
  foreach($items as $it)
  {
	  $meta_id = @\App\ProductMetaTypes::where('title','unit')->first(['id'])->id;
	  $it->created_at_formatted = $it->created_at;
	 
	  $unit = @\App\ProductMetaValues::where('product_meta_type_id',$meta_id)->where('product_id',$it->id)->first(['value'])->value;
	  
	  if(  $unit !='' && $unit != ' ')
	  {
		  $it->priceDesc = $it->base_price."/ ".$unit;
	  }
	  else{
		  $it->priceDesc = $it->base_price;
	  }
	    
  }
  $d5["data"] = $items;
  $main_array[] = $d5;
  $result = $main_array;

  
  
  
	  return view('dashboard.index', compact('result'));





 $type = $request->type;
 if($type =='monthly')
 {
     $no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
     $total_users = User::count();
	 $total_brands = \App\Admin::where('user_type','brand')->count();
	 $total_stores = \App\Admin::where('user_type','store')->count();
	 $total_offers = \App\Posts::count();
	 
     $current_month = date("Y-m");     
     $previous_month = date("Y-m",strtotime("-1 month"));  
 
	 
	 $users_graph_data = array();
	 for($t=1;$t <= $no_of_days_current_month;$t++)
	 {
	     $current_day = date("Y-m-".$t); 
		 $ud['label'] = $t;
		 $ud['date'] = $current_day;
		 $ud['y'] = User::whereDate('created_at', '=', date('Y-m-'.$t))->count();
		 $users_graph_data[] = $ud;
	 }
	 return $users_graph_data;
 }
	
 
 if($type =='weekly')
 {
$start = Carbon::now()->subDays(30);
$m= date("m");
$de= date("d");
$y= date("Y");

for($i=14; $i>=0; $i--)
{
 $dates[] = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y)); 
}
$users_graph_data = array();
foreach($dates as $date)
{
 
		 $ud['label'] = Carbon::parse($date)->format('d M');
		 $ud['date'] = $date;
		 $ud['y'] = User::whereDate('created_at', '=', $date)->count();
		 $users_graph_data[] = $ud;
}
return $users_graph_data;
 }

 
if($type =='yearly')
{
     for($t = 0; $t <= 12; $t++) 
     {
       $dates[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
     }

$users_graph_data = array();
foreach(array_reverse($dates)  as $date)
{
 
		 $ud['label'] = Carbon::parse($date)->format('M');
		 $ud['date'] = $date;
		 $ud['y'] = User::whereDate('created_at', 'like', "%".$date."%")->count();
		 $users_graph_data[] = $ud;
}
return $users_graph_data;
}
    }
 
 

 public function upload_app_logo(Request $request)
 {
	  @\App\Settings::where('key_title','app_logo')->update(['key_value' => $request->photo ]);

	  $d['status_code'] = 1;
	  $d['message'] = 'App logo updated successfully';
	  $d['data'] = [];
	  return $d;
 }
 
 
 

 
}
