<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Permission;
use App\Faqs;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class FaqsController extends Controller
{
    
	
  
	
    public function index(Request $request)
    {	 
		//filters
		$request_data = @$request[0];
		 
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Faqs List';
		$model_name = 'faqs';
	    $modelName = "App\Faqs";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
	    $data_array = new \App\Faqs;
 
	 
	    if($date_filter == 'lastAdded')
	    {
		   $data_array = $data_array->orderBy('created_at','DESC');
	    }
	    else if($date_filter == 'firstAdded')
	    {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		}
	   
 
	   if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->paginate(1000000);
 
 
 
 
 
 foreach($data_array as $d)
 {
   //return $this->get_category_title($d->id,$d->title );
   // $d->title = $this->get_category_title($d->id,$d->title );
 }
 
 
 
 foreach($data_array as $d)
 {
	 $d->value = $d->id;
 }
				$result = $data_array;
				
				
	 
				return view('faqs.index',compact('result','model_name'));
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					return $data;
    }
	
 
 
    public function store(Request $request)
    {
		$flight = new Categories;
        $flight->title =  $request[0]['title'];
		$flight->photo =  $request[0]['photo'];
		
		if($request[0]['parent_id'] == '' or $request[0]['parent_id'] == null or $request[0]['parent_id'] ==' ')
		{
			$flight->parent_id =  'jj';
		}
		else
		{
			$flight->parent_id =  $request[0]['parent_id'];
		}
		
        $flight->save();

		
		           if($flight != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $flight ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
	
	
	public function edit($id)
    {
      $categories = \App\Categories::find($id);
	  return $categories;
		
	}

 
    public function update(Request $request)
    {
 
		// Get the user
        $user = Faqs::findOrFail($request[0]["category_id"]);
        // Update user
        $user->title =  $request[0]['title'];
		$user->photo =  $request[0]['photo'];
		$user->parent_id =  $request[0]['parent_id'];
        $user->save();
        if($user != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $user ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
	
	
	
	    public function get_faqs(Request $request)
    {
		
		
		/**
		
		$input['useragent'] = $request->server('HTTP_USER_AGENT');

	$input['ip'] = $request->ip();

	echo json_encode($input);
	
	
		return \Request::getClientIp(true);
		return $request;
 **/
		// Get the user
        $faqs = \App\Faqs::get();
 
        if(count($faqs) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $faqs ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
 
 
 
 
    public function destroy($id)
    {
        if( \App\Faqs::findOrFail($id)->delete() ) {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Deleted Successfully';
                    $data['data'] = [] ;
        } else {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Error Occurred';
                    $data['data'] = [] ;
        }

        return $data;
    }

 
}
