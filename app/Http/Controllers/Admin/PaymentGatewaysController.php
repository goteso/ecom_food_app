<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Permission;
use App\ProductVariants;
use App\ProductVariantTypes;
use App\Brands;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class PaymentGatewaysController extends Controller
{
    
    public function index(Request $request)
    {
	   $result = \App\PaymentGateways::paginate(50);
	   
	   foreach($result as $t)
	   {
		    $order_id = $t->order_id;
		   	 $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		     $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
			 $t->customer_id =$customer_id;
			 
			 $t->customer_name = @\App\User::where('id',$customer_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$customer_id)->first(['last_name'])->last_name;
	   }
	   
	  
	   return view('transactions.index',compact('result'));
    }
	
	
	
	    public function get_payment_gateways(Request $request)
    {
	   $result = \App\PaymentGateways::paginate(50);
	   
	   return $result;
	   
    }
	
	
	
	    public function add_transactions(Request $request)
    {
		
		
	    $t = new \App\OrdersTransactions;

        $t->order_id = $request->order_id;
		$t->amount = $request->amount;
		$t->payment_gateway = $request->payment_gateway;
		$t->transaction_id = $request->transaction_id;
 
        $t->save();
		
		return 1;
    }
	
 
 
    public function store_brand(Request $request)
    {
    $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = Brands::create($request->toArray()))  {
 
           flash('Brand has been created.');

        } else {
            flash()->error('Unable to create Brand.');
        }

        return back();
    }

 
    public function update_brand(Request $request, $id)
    {
 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = Brands::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Brand has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
 
		if( \App\Brands::findOrFail($id)->delete() ) {
            flash()->success('Brand has been deleted');
        } else {
            flash()->success('Brand not deleted');
        }

        return redirect()->back();
    }

 
}
