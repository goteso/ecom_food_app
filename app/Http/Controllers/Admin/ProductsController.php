<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
 
use Session;
 
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...

class ProductsController extends Controller
{
   use feature;   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    

// 1.  this function is used to show add product basic form --- DOCUMENTATION DONE
  public function add( $product_id = '' , $ajax = 'no')
  {
	  $main_array = array();
	  
	  // 1. Basic Informations
	  $data_array1['title'] = 'Basic Information';
 	  $fields_array = array();
	  
	  $fields_keys1["title"] = "Item Name";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'title';
	  
	  if(isset($product_id) && $product_id != null && $product_id != '' )
	  {
	  $value = @\App\Products::where('id',@$product_id)->first(['title'])->title;
	  }
	  if(!isset($value) or $value == null or $value == '') { $value = '';}
	  $fields_keys1["value"] = $value;
      $fields_array[] = $fields_keys1;
	  
 	  $fields_keys2["title"] = "Item Categories";
	  $fields_keys2["type"] = 'multipleTag';
	  $fields_keys2["tag_type"] = 'categories';
	  $fields_keys2["identifier"] = 'category_ids';
	  if(isset($product_id) && $product_id != null && $product_id != '' )
	  {
	  $categories = @\App\Products::where('id',$product_id)->first(['category_ids'])->category_ids; /// ethe thoda dimag lgega.... deepu da v lgea c
	  }
	  if(isset($categories) && $categories !='')
	  {
	  $categories = explode(",",$categories);
	  $cats_array = array();
	  
	  if( sizeof($categories) > 0)
	  {
		for($i=0;$i<sizeof($categories);$i++)
		{
			$d3['title'] = @\App\Categories::where('id',$categories[$i])->first(['title'])->title;
			$d3['value']=intval($categories[$i]);
			$d3['id']=$categories[$i];
			$cats_array[] = $d3;
		}
	  }
		$fields_keys2["value"] = $cats_array;
		$fields_array[] = $fields_keys2;
	  }
	  else
	  {
		$fields_keys2["value"] = [];
		$fields_array[] = $fields_keys2;
	  }
	 

	  //base_discount
	  $status = @\App\FeaturesSettings::where('title','base_price')->first(['status'])->status;
	  if($status == 1 or $status == '1')
	  {	 
		$fields_keys3["title"] = "Price";
		$fields_keys3["type"] = 'float';
		$fields_keys3["identifier"] = 'base_price';
		 if(isset($product_id) && $product_id != null && $product_id != '' )
	  {
		$value = @\App\Products::where('id',@$product_id)->first(['base_price'])->base_price;
	  }
		if(!isset($value) or $value == null or $value == '') { $value = '';}
		$fields_keys3["value"] = $value;
		$fields_keys3["symbol"] = env('CURRENCY_SYMBOL');
		$fields_array[] = $fields_keys3;
	  }
 
 
	  $status = @\App\FeaturesSettings::where('title','brand_ids')->first(['status'])->status;
	  if($status == 1 or $status == '1')
	  {
		$fields_keys4["title"] = "Item Brands";
		$fields_keys4["type"] = 'singleTag';
		$fields_keys4["tag_type"] = 'brands';
		$fields_keys4["identifier"] = 'brand_ids';
		
		
			 if(isset($product_id) && $product_id != null && $product_id != '' )
	  {
		$brands = @\App\Products::where('id',$product_id)->first(['brand_ids'])->brand_ids;  
	  }
	  
		if(isset($brands) && $brands !='')
		{
			$brands = explode(",",$brands);
			$cats_array = array();
			for($i=0;$i<sizeof($brands);$i++)
			{
				$d3['title'] = @\App\Brands::where('id',$brands[$i])->first(['title'])->title;
				$d3['value']=intval($brands[$i]);
				$d3['id']=$brands[$i];
				$brands_array[] = $d3;
			}
			$fields_keys4["value"] = $brands_array;
			$fields_array[] = $fields_keys4;
		}
		else
		{
			$fields_keys4["value"] = [];
			$fields_array[] = $fields_keys4;
		}
	  }
 
	  //base_discount
	  $status = @\App\FeaturesSettings::where('title','base_discount')->first(['status'])->status;
	  if($status == 1 or $status == '1')
	  {
		$fields_keys5["title"] = "Discount";
		$fields_keys5["type"] = 'float';
		$fields_keys5["identifier"] = 'base_discount';
		$fields_keys5["symbol"] = '%';
	    $value = @\App\Products::where('id',@$product_id)->first(['base_discount'])->base_discount;
	    if($value == null or $value == '') { $value = '';}
	    $fields_keys5["value"] = $value;
		$fields_array[] = $fields_keys5;
 
		$fields_keys6["title"] = "Discount Expiry Date";
		$fields_keys6["type"] = 'datePicker';
		$fields_keys6["identifier"] = 'discount_expiry_date';
		$value = @\App\Products::where('id',@$product_id)->first(['base_discount'])->base_discount;
	    if($value == null or $value == '') { $value = '';}
		$fields_keys6["value"] = $value;
		$fields_array[] = $fields_keys6;
	  }
	  
	  $status = @\App\FeaturesSettings::where('title','stock_count')->first(['status'])->status;
	  if($status == 1 or $status == '1')
	  {
  		$fields_keys8["title"] = "Stock Count";
		$fields_keys8["type"] = 'float';
		$fields_keys8["message"] = 'If you do not want stock count, leave it blank';
		$fields_keys8["identifier"] = 'stock_count';
	    $value = @\App\Products::where('id',@$product_id)->first(['stock_count'])->stock_count;
	    if($value == null or $value == '') { $value = '';}
	    $fields_keys8["value"] = $value;
		$fields_array[] = $fields_keys8;
 
	  }
	  
	  $status = @\App\FeaturesSettings::where('title','photo')->first(['status'])->status;
	  if($status == 1 or $status == '1')
	  {
		$fields_keys9["title"] = "Photo";
		$fields_keys9["type"] = 'file';
		$fields_keys9["identifier"] = 'photo';
		$value = @\App\Products::where('id',@$product_id)->first(['photo'])->photo;
	    if($value == null or $value == '') { $value = ''; $fields_keys9["value"] = '';}
		else{
		$fields_keys9["value"] = url('/').$value;
		}
		$fields_array[] = $fields_keys9;
	  }
	   
	  $data_array1['fields'] = $fields_array;
	  $data_array1['product_id'] = @$product_id;
      $main_array[] =$data_array1;
	  
	  $result = $main_array;
	  
	  if($ajax == 'yes')
	  {
		  return $data_array1;
	  }
	  
	  return view('products.add-product', compact('result'));
	                  
}
  



// 2. Get List of Products to View Page
public function index()
{
   return view('products.products');
}
  
  
  
  
  // to get a products variants
  public function get_product_variants(Request $request)
  {
	  $product_id = $request->product_id;
	  
	 $product_variants_types = \App\ProductVariantTypes::get();
	 
	 
	 $product_variants_types_array = array();
	 $c=0;
	 foreach($product_variants_types as $pv)
	 {
	
		 $variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$request->product_id)->get();
		 
		 if(count($variants) < 1)
		 {
			  
		 }
		 else
		 {
		   $pv->variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$request->product_id)->get();
		    $product_variants_types_array[] =$pv;
		 }
		 
		 	 
	 }
	
	 return $product_variants_types_array;

 
  }
	
 
	// Get a Products List on load as well as with filters
   public function get_products_list(Request $request) //clean done
    {
	    $cols_array = array();
		$request_data = @$request[0];
		$price_filter = @$_GET['price_filter'];
		$date_filter = @$_GET['date_filter'];
		$fields = @$_GET['fields'];
		$limit = @$_GET['limit'];
		$category = @$_GET['category'];
		$brand = @$_GET['brand'];
		$fields = explode(",",$fields);
		
		if(!isset($limit) or $limit =='' or $limit == null ) { $limit = 25;}
		 
        $products_columns = $fields;
		$main = array();
	    $columns_array = array();
		$data_array = array();
		$columns_final_array = array();
		
		for($x=0;$x<sizeof($products_columns);$x++)
		{
		  $feature_settings_status = @\App\FeaturesSettings::where('title',$products_columns[$x])->first(['status'])->status;
		  if( $products_columns[$x] == 'base_discount' or $products_columns[$x] == 'base_price' or $products_columns[$x] == 'discount_expiry_date' or  $products_columns[$x] == 'stock_count' or  $products_columns[$x] == 'photo' or $products_columns[$x] == 'brand_ids' )
		  {
			  if($feature_settings_status == '1' or $feature_settings_status == 1 )
			  {
				 $columns_final_array[]=$products_columns[$x];
			  }
		  }
		  else
		  {
		   $columns_final_array[]=$products_columns[$x];
		  }
		}
		  
		for($x=0;$x<sizeof($columns_final_array);$x++)
		{
		   $c1['title'] = $columns_final_array[$x];
		   $cols_array[] = $columns_final_array[$x];
		   $c1['visible'] = '1';
		   $c1['index'] = $x;
		   $columns_array[]=$c1;
		}
 
       $data_array = new \App\Products;
    
	   if($price_filter == 'priceLowToHigh')
	   {
		   $data_array = $data_array::orderBy('base_price','ASC');
	   }
	   else if($price_filter == 'priceHighToLow')
	   {
		   $data_array = $data_array::orderBy('base_price','DESC');
	 
	   }
	   
	   else if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC');  
	   }
	   
	   
	   else if($price_filter == 'highToLow')
	   {
		   $data_array = $data_array->orderBy('base_price','DESC');
	   }
	   else if($price_filter == 'lowToHigh')
	   {
		  	   $data_array = $data_array->orderBy('base_price','ASC');  
	   }
	   
 
	   if(isset($_GET['category']) && $_GET['category'] != '' && $_GET['category'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$category."',products.category_ids)");
	   }
	   
	   	   if(isset($_GET['brand']) && $_GET['brand'] != '' && $_GET['brand'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$brand."',products.brand_ids)");
	   }
	   
	   
	      if(isset($_GET['search_text']) && $_GET['search_text'] != '' && $_GET['search_text'] != null)
	   {
		   
		 $d33 = $_GET['search_text'];
		 $data_array = $data_array->where(function($q) use ($d33) {$q->where( DB::raw("title"),'like', '%'.$d33.'%');});
	   }
	   
	   $data_array = $data_array->paginate($limit,$cols_array);
	 
	  foreach($data_array as $c)
	  {
		$date = @$c->created_at;
		$time = @$c->created_at;
		$c->created_at_formatted = @$c->created_at;
	  }
	   
	   $main_data['columns'] = $columns_array;
	   $main_data['data'] = $data_array;
	   
	   
	   for($di=0;$di<sizeof($data_array);$di++)
	   {
		   if(isset($data_array[$di]['category_ids']) && $data_array[$di]['category_ids'] != '')
		   {
			  $category_ids = $data_array[$di]['category_ids'];
		   	  $category_ids = explode(",",$category_ids);
	          $cats_array = array();
	          for($i=0;$i<sizeof($category_ids);$i++)
	          {
			    $d1['value']=$category_ids[$i];
		        $cats_array[] = $d1;
	          }
			  $data_array[$di]['category_ids'] = $cats_array;
		   }
			 
			 
		   if(isset($data_array[$di]['brand_ids']) && $data_array[$di]['brand_ids'] != '')
		   {
			  $brand_ids = $data_array[$di]['brand_ids'];
			  $brand_ids = explode(",",$brand_ids);
	          $brands_array = array();
	           for($i2=0;$i2<sizeof($brand_ids);$i2++)
	         {
		        $d2['value']=$brand_ids[$i2];
		        $brands_array[] = $d2;
	         }
			 $data_array[$di]['brand_ids'] = $brands_array;
		   }
	    }
	    return $main_data;
    }
    
 
 
 public function search_product(Request $request)
 {
	  $data_array = new \App\Products;
	  
	  	      if(isset($_GET['search_text']) && $_GET['search_text'] != '' && $_GET['search_text'] != null)
	   {
		   
		 $d33 = $_GET['search_text'];
		 $data_array = $data_array->where(function($q) use ($d33) {$q->where( DB::raw("title"),'like', '%'.$d33.'%');});
	   }
	   
	    $data_array = $data_array->paginate('100');
		
		return $data_array;
 }

 // to store a product basic form data to data base  == clean done
	public function store(Request $request)
    {
		  $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
			       $fields =  $request[0]['fields'];
				   $flight = new \App\Products;					 
  				   if(sizeof($fields) < 1)
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Unable to find any field';
                       $data['user_data']      =   [];
					   return $data;
						
					}
				   for($t=0;$t<sizeof($fields);$t++)
					{
					    $id = $fields[$t]['identifier'];
						if(getType($fields[$t]['value']) == 'array')
						{
						    $s = sizeof($fields[$t]['value']);
							$values = array();
						    if($s > 0)
						    {
								for($x=0;$x<$s;$x++)
								{
									$values[] = $fields[$t]['value'][$x]["value"];
								}
							$flight->$id = implode(",",$values); 
						   }
						}
						else
						{
							 $flight->$id = $fields[$t]['value'];
						}
				    }
					$flight->save();
					$da = array();
					$d['product_id'] = $flight->id; 
					$da[] = $d;

                   	if($flight !='')
					{
	                   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Product Added Successfully';
                       $data['user_data']      =   $da;
					}
					else
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Some Error Occurred';
                       $data['user_data']      =   [];  
					}
	            return $data;
				}
  }
  
   


public function product_details(Request $request)
    {
		
	
	  $product_id = $request->product_id;
	  //basic details	
	 
	  $d["basic_details"] = \App\Products::where('id',$product_id)->get();
		
	  //meta values	
	  $product_meta_types = \App\ProductMetaTypes::get(['id','title','identifier','limit','type']);
	  $fields_array2 = array();
	  
	  foreach($product_meta_types as $pmt)
	  {
		  $product_meta_type_id = $pmt->id;
		  $pmt->value  = @\App\ProductMetaValues::where('product_meta_type_id',$product_meta_type_id)->where('product_id',$product_id)->first(['value'])->value;
		  
	  }
      $d["meta_values"] = $product_meta_types;
	  
	  //variants
	  $variants = \App\ProductVariants::where('product_id',$product_id)->get(['id','product_variant_type_id','title','product_id','price_difference','price','photo' ]);
  
	  foreach($variants as $v)
	  {
		  $variant_title = \App\ProductVariantTypes::where('id',$v->product_variant_type_id)->first(['title'])->title;
		  $v->variant_title = $variant_title;
	  }
	  
	   $d["variants"] = $variants;
	  return $d;
    } 
    
	
	
 public function product_invoice(Request $request,$product_id,$order_id ='')
    {
	   
	  //basic details	
	 
	  $d["basic_details"] = \App\Products::where('id',$product_id)->get();
		
	  //meta values	
	  $product_meta_types = \App\ProductMetaTypes::get(['id','title','identifier','limit','type']);
	  $fields_array2 = array();
	  
	  
	  	  $omt_id  = @\App\OrdersMetaTypes::where('identifier','customer_id')->first(['id'])->id;
		  $customer_id  = @\App\OrdersMetaValues::where('order_meta_type_id',$omt_id)->where('order_id',$order_id)->first(['value'])->value; 
		  
		  
		  
	  foreach($product_meta_types as $pmt)
	  {
		  $product_meta_type_id = $pmt->id;
		  $pmt->value  = @\App\ProductMetaValues::where('product_meta_type_id',$product_meta_type_id)->where('product_id',$product_id)->first(['value'])->value;
		  
		   
		  
			  
			
	
		 
		   
		  
		  
		  
	  }
      $d["meta_values"] = $product_meta_types;
	  
	  //variants
	  $variants = \App\ProductVariants::where('product_id',$product_id)->get(['id','product_variant_type_id','title','product_id','price_difference','price','photo' ]);
  
	  foreach($variants as $v)
	  {
		  $variant_title = \App\ProductVariantTypes::where('id',$v->product_variant_type_id)->first(['title'])->title;
		  $v->variant_title = $variant_title;
	  }
	  
	  $d["variants"] = @$variants;
	  $d["order_id"] = @$order_id;
	  $d["customer_id"] = @$customer_id;
	  $d["app_name"] = @\App\Settings::where('key_title','app_name')->first(['key_value'])->key_value;
	  
	  $order_meta_types = @\App\OrderProducts::where('order_id',$order_id)->where('product_id',$product_id)->get();
	   
 
	  return view('products.product_invoice',compact('d'));
    } 
      	
	
public function edit($id) // clean done
    {
      $product = \App\Products::find($id);
	  $main_array = array();			
      $ajax = 'yes';
	  $main_array[] = $this->add($id,$ajax);
      $product_meta_types = \App\ProductMetaTypes::get(['id','title','identifier','limit','type']);
	  $fields_array2 = array();
	  
	  foreach($product_meta_types as $pmt)
	  {
		  $product_meta_type_id = $pmt->id;
		  $pmt->value  = @\App\ProductMetaValues::where('product_meta_type_id',$product_meta_type_id)->where('product_id',$id)->first(['value'])->value;
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $product_meta_types;
	  $main_array[] =$data_array2;	
 
	  $distinct_variants_types = \App\ProductVariantTypes::get();
	  $variants_array = array();
	  foreach($distinct_variants_types as $vt)
	  { 
         $cols_array = array();
	  
	  	 $tabs = array();
	     $fields_array_variants = array();
		 $data_array3["title"] =   $vt->title;
		 
		 $d1['title'] = 'Title';
		 $d1['type'] = 'text';
		 $d1['identifier'] = 'title';
	     $fields_array_variants[] = $d1;
		 
		 $col_array[] = 'id';
		 $col_array[] = 'title';
		 
		if( $vt->price_status == 1 or $vt->price_status == "1")
		 {
		   $col_array[] = 'price';
		   $d2['title'] = 'Price';
		   $d2['type'] = 'float';
		   $d2['identifier'] = 'price';
		   $d2['symbol'] = env('CURRENCY_SYMBOL');
		   $fields_array_variants[] = $d2; 
		 }
		  
		if( $vt->photo_status == 1 or $vt->photo_status == "1")
		 {
		   $col_array[] = 'photo';
		   $d3['title'] = 'Photo';
		   $d3['type'] = 'file';
		   $d3['identifier'] = 'photo';
	       $d3['limit'] = '1';
		   $fields_array_variants[] = $d3;
		 }
		 
		if( $vt->price_difference_status == 1 or $vt->price_difference_status == "1")
		 {
		   $col_array[] = 'price_difference';
		   $d4['title'] = 'Price Difference';
		   $d4['type'] = 'float';
		   $d4['identifier'] = 'price_difference';
	       $d4['symbol'] = env('CURRENCY_SYMBOL');
		   $fields_array_variants[] = $d4;
		 }
		 
		  
		$feature_settings_status = @\App\FeaturesSettings::where('title','stock_count')->first(['status'])->status; 
		if( $feature_settings_status == 1 or $feature_settings_status == "1")
		 {
			 $feature_settings_status = @\App\ProductVariantTypes::where('id',$vt->id)->first(['variant_stock_count_status'])->variant_stock_count_status; 
			 if($feature_settings_status == 1 or $feature_settings_status == '1' )
			 {
				$col_array[] = 'stock_count';
				$d5['title'] = 'Stock Count';
				$d5['type'] = 'float';
				$d5['identifier'] = 'stock_count';
				$fields_array_variants[] = $d5;
			 }
		 }
		 
	 		  
		$vd['title'] =   $vt->title;
		$vd['product_variant_type_id'] =   $vt->id;
		$vd['fields'] =   $fields_array_variants;
		$vd['values'] =   @\App\ProductVariants::where('product_variant_type_id',$vt->id)->where('product_id',$id)->get($col_array);
		$variants_array[] = $vd;
	  }
	  
	$v_array = array();
    $v["title"] = "Items Variants";	
	$v["tabs"] = $variants_array;
    $main_array[] =$v; 
    $result = $main_array;
 
 if(isset($_GET['request_type']) && $_GET["request_type"] =='api')
 {
	 return $result;
 }
   return view('products.edit-product', compact('result','id'));
 } 
	
 
 
 
	
	
	
	
	public function update_product(Request $request, $id) //clean done
    {
		   $basic_info = $request[0];
		   $fields =  $basic_info['fields'];
	       $flight = \App\Products::find($id);	
                 for($t=0;$t<sizeof($fields);$t++)
					{
					   $identifier = $fields[$t]['identifier'];
					   if(getType($fields[$t]['value']) == 'array')
						{
							$s = sizeof($fields[$t]['value']);
							$values = array();
							for($x=0;$x<$s;$x++)
							{
								$values[] = $fields[$t]['value'][$x]["value"];
							}
                            $d = str_replace("[","",json_encode($values));
							$d = str_replace("]","",$d);
					   	    $flight->$identifier = $d;
						}
						else
						{
							 $flight->$identifier = $fields[$t]['value'];
					    }
					}
					$flight->save(); 
				 
        // Additional Informations
		$additional_info = $request[1];
		foreach($additional_info['fields'] as $ai)
		{
			$identifier = $ai["identifier"];
			$value = $ai["value"];
			$product_meta_type_id = \App\ProductMetaTypes::where('identifier',$identifier)->first(['id'])->id;
	 		$additional_info_model = \App\ProductMetaValues::firstOrCreate(['product_meta_type_id' => $product_meta_type_id , 'product_id'=>$id]);
            $additional_info_model->value = $value;
            $additional_info_model->save();
		 }
 
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Product Updated Successfully';
					$data['data']        =   [];
					return $data;
    }
	
	
	
	
	
	
 
	
	
 public function add_update_variant(Request $request)
{
		        $product_variant_type_id = @$request->product_variant_type_id;
				$product_id = @$request->product_id;
				$title = @$request->title;
				$stock_count = @$request->stock_count;
				$price = @$request->price;
				$price_difference = @$request->price_difference;
				$variant_id = @$request->variant_id;
				if($variant_id == '' or $variant_id == null)
				{
					$v =   new \App\ProductVariants;
				}
				else
				{
					$v = \App\ProductVariants::find($variant_id);
				}
		 		
				$v->title = @$title;
				$v->product_variant_type_id = @$product_variant_type_id;
				$v->price_difference = @$price_difference;
				$v->stock_count = @$stock_count;
				$v->price = @$price;
				$v->product_id = @$product_id;
                $v->save();
				
			    $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Variant Updated Successfully';
			    $data['id']        =   $v->id;
			    $data['data']        =   [];
				return $data;
}
	
	
public function delete_variant($id)
 {
		    $v = \App\ProductVariants::find($id)->delete();
		 
			
			    $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Variant Deleted Successfully';
			 
				return $data;
 }
	
	
public function destroy($id)
{
        Product::find($id)->delete();
		\App\ProductMetaValues::where('product_id',$id)->delete();
        return response(array(
                'error' => false,
                'message' =>'Product deleted successfully',
               ),200);
 }

	
	
public function delete_products_multiple(Request $request)
{
 $products_array = $request->toArray();
 if(sizeof($products_array) > 0)
 {
	 for($i=0;$i<sizeof($products_array);$i++)
	 {
			$p_id = $products_array[$i]['id'];
		    \App\Products::find($p_id)->delete();
	 }
 }
		return 1;
 }
 
 
 
 
 //new apis 
 
 
 
 
 

 
}
