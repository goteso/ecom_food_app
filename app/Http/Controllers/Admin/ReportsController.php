<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...

class ReportsController extends Controller
{
     use feature;   
     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    
public function reports_page()
{
	
	
	 //graph 3 starts here
  $d3_data = array();
  $d3["type"] = 'orders'; 
  $dates3 = array();
       for($t = 31; $t > 0; $t--) 
     {
       $dates3[] = date('Y-m-d', strtotime( date( 'Y-m-'.date('d') )." -".$t." days"));
	   $dates_label[] = date('d M', strtotime( date( 'Y-m-'.date('d') )." -".$t." days"));
	   
	   
     }

	  
	  
	 
$users_graph_data = array();
$total3 = 0;

 
$months_array = array();

for($m=0;$m<sizeof($dates_label);$m++)
{
	$months_array[] = $dates_label[$m];
}

 



 
 $total_revenues = @\App\OrdersTransactions::sum("amount");
 
 $total_users= @\App\User::count();
 
 $total_orders_count = 0;
 
$orders_y_array = array();
foreach($dates3  as $date)
{
	    
		 $orders3 = \App\Orders::whereDate('created_at', 'like', "%".$date."%")->get();
		 foreach($orders3 as $o)
		 {
			 $total_orders_count++;
			 $t = @\App\OrdersPayments::where('order_id',@$o->id)->first(['total'])->total;
			 $t = intval($t);
			 $total_orders = $total3 + $t;
		 }
	     $orders_y_array[] = \App\Orders::whereDate('created_at', 'like', "%".$date."%")->count();
		 
		 $users_y_array[] = \App\User::whereDate('created_at', 'like', "%".$date."%")->count();
		 
		 $revenues_y_array[] = \App\OrdersTransactions::whereDate('created_at', 'like', "%".$date."%")->sum("amount");
	 
}
$d3["total_orders"] = $total_orders_count;
$d3["total_revenue"] = $total_revenues;
$d3["total_users"] = $total_users;


$d3["desc"] = "Total Order of Last 12 Months";



$d3["months"] = $months_array;
$d3["months"] = $months_array;
$d3["orders_y_array"] = $orders_y_array;
$d3["users_y_array"] = $users_y_array;
$d3["revenues_y_array"] = $revenues_y_array;




$main_array[] = $d3;


 

 $result = $main_array;
 
 
		  return view('reports.reports',compact('result'));
}


 
 
 public function sales_report_page()
{
		  return view('reports.report-detail');
}


 public function orders_report_page()
{
		  return view('reports.report-detail');
} 

 public function cancelled_orders_report_page()
{
		  return view('reports.report-detail');
}


 public function transactions_report_page()
{
		  return view('reports.report-detail');
}

 public function customers_most_sales_report_page()
{
		  return view('reports.report-detail');
}

 public function drivers_most_sales_report_page()
{
		  return view('reports.report-detail');
}
 public function reports_all_customers_page()
{
		  return view('reports.report-detail');
}

 public function reports_all_drivers_page()
{
		  return view('reports.report-detail');
}
 

 //get reports list
 
 
function get_reports_list()
{
//order,cancelled_orders    --------  customers,customer-by most sales ----------transactions------------- Sales---------


$main = array();

$d1["title"] = "Sales";
$d1["identifier"] = "Sales";
$d1["recommended"] = "1";
$d1["url"] = "sales_report_page";
$d1["description"] = "This sales report will give you the idea of sales with respect to monthly revenue";
$main[] = $d1;



$d2["title"] = "Orders";
$d2["identifier"] = "Orders";
$d2["recommended"] = "1";
$d2["url"] = "orders_report_page";
$d2["description"] = "This orders report will give you the idea of orders with respect to monthly revenue";
$main[] = $d2;

$d3["title"] = "Cancelled Orders";
$d3["identifier"] = "Orders";
$d3["recommended"] = "1";
$d3["url"] = "cancelled_orders_report_page";
$d3["description"] = "This orders report will give you the idea of cancelled orders with respect to monthly revenue";
$main[] = $d3;

 
$d4["title"] = "Transactions";
$d4["identifier"] = "Transactions";
$d4["recommended"] = "0";
$d4["url"] = "transactions_report_page";
$d4["description"] = "This orders report will give you the idea of transactions with respect to monthly revenue";
$main[] = $d4;
 
 
$d5["title"] = "Customers by Most Sales";
$d5["identifier"] = "Customers";
$d5["recommended"] = "1";
$d5["url"] = "customers_most_sales_report_page";
$d5["description"] = "This customers report will give you the idea of customers with most orders delivered";
$main[] = $d5;



$d6["title"] = "Customers";
$d6["identifier"] = "Customers";
$d6["recommended"] = "1";
$d6["url"] = "reports_all_customers";
$d6["description"] = "This  report will give you the list customers joined on monthly basis";
$main[] = $d6;





$d7["title"] = "Drivers";
$d7["identifier"] = "Drivers";
$d7["recommended"] = "1";
$d7["url"] = "reports_all_drivers";
$d7["description"] = "This  report will give you the list drivers joined on monthly basis";
$main[] = $d7;





$d8["title"] = "Drivers by Most Sales";
$d8["identifier"] = "Drivers";
$d8["recommended"] = "1";
$d8["url"] = "drivers_most_sales_report_page";
$d8["description"] = "This report will give you the idea of drivers with most orders delivered";
$main[] = $d8;


$app_url = env('APP_URL');
$dt["app_url"] = $app_url;
$dt["reports_list"] = $main;

return $dt;

}





 
     // 1. =================== Get all users ====================	
 public function reports_all_customers(Request $request)
 {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='Users Joined Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
 
	
	$c4["title"] = "Email";
	$c4["index"] = "2";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
	$c5["title"] = "Phone";
	$c5["index"] = "3";
	$c5["visible"] = "1";
	$column_array[] = $c5;
	
    $data_array = array();

	$rows_array = array();
	 
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	 
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	  
    $user_data = \App\User::where('user_type','2')->where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($user_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
     $o_array[] = $od->created_at;
	  
	    $o_array[0] = $o_array[0];
		$o_array[] = $od->first_name." ".$od->last_name;
		$o_array[] = $od->email;
		$o_array[] = $od->mobile;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
 }
 
    $d1['header'] = 'Users Registered';
    $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
    $d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	
	$now = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";
	$d["footer"] = $now;

	$main = array();
 	$main[] = $d;
	return $main;
	 

 }
 
 
 
 public function reports_all_customers_most_sales(Request $request)
 {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='Users by Most Orders';
	
	$column_array = array();
	
 
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
		$c3["title"] = "No. of Orders";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
 
	
	$c4["title"] = "Email";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
	$c5["title"] = "Phone";
	$c5["index"] = "4";
	$c5["visible"] = "1";
	$column_array[] = $c5;
	
    $data_array = array();

	$rows_array = array();
	 
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	 
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	  
    $user_data = \App\User::where('user_type','2')->where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
	
	$d3 = \App\OrdersMetaValues::where('order_meta_type_id','1')->groupBy('value')->get();
		 
	$user_data = array();
	foreach($d3 as $u)
	{
		$user_data[] = \App\User::where('id',$u->value)->first();
	}
 
 
 
 $rows_array = array();
 foreach($user_data as $od)
 {	 
	 
		$o_array =array();
		$order_id = @$od->id;
		$o_array[] = @$od->updated_at;
	  
	    $o_array[0] = $o_array[0];
		$o_array[] = @$od->first_name." ".@$od->last_name;
        $o_array[] =   @\App\OrdersMetaValues::where('order_meta_type_id','1')->where('value',$od->id)->count();
		$o_array[] = @$od->email;
		$o_array[] = @$od->mobile;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
			
 }
 
 
 
    $d1['header'] = 'Users with Most Orders';
    $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
    $d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	
 
	$now = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";
	$d["footer"] = $now;

	$main = array();
 	$main[] = $d;
	return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public function reports_all_drivers_most_sales(Request $request)
 {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='Driver by Most Orders';
	
	$column_array = array();
	
 
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "No. of Orders";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
 
	
	$c4["title"] = "Email";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
	$c5["title"] = "Phone";
	$c5["index"] = "4";
	$c5["visible"] = "1";
	$column_array[] = $c5;
	
    $data_array = array();

	$rows_array = array();
	 
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	 
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	  
    $user_data = @\App\User::where('user_type','3')->where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
	
	$d3 = @\App\OrdersMetaValues::where('order_meta_type_id','7')->groupBy('value')->get();
		 
	$user_data = array();
	foreach($d3 as $u)
	{
		$user_data[] = @\App\User::where('id',$u->value)->first();
	}
 
 
 
 $rows_array = array();
 foreach($user_data as $od)
 {	 
	 
		$o_array =array();
		$order_id = @$od->id;
		$o_array[] = @$od->updated_at;
	  
	    $o_array[0] = @$o_array[0];
		$o_array[] = @$od->first_name." ".@$od->last_name;
        $o_array[] =   @\App\OrdersMetaValues::where('order_meta_type_id','7')->where('value',$od->id)->count();
		$o_array[] = @$od->email;
		$o_array[] = @$od->mobile;
		$s= @$o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
			
 }
 
 
 
    $d1['header'] = 'Drivers with Most Orders';
    $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
    $d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	
 
	$now = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";
	$d["footer"] = $now;

	$main = array();
 	$main[] = $d;
	return $main;
	 

 }

 
 
 
 
      // 1. =================== Get all users ====================	
 public function reports_all_drivers(Request $request)
 {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='Users Joined Report(Drivers)';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
 
	
	$c4["title"] = "Email";
	$c4["index"] = "2";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
	$c5["title"] = "Phone";
	$c5["index"] = "3";
	$c5["visible"] = "1";
	$column_array[] = $c5;
	
    $data_array = array();

	$rows_array = array();
	 
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	 
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	  
    $user_data = \App\User::where('user_type','3')->where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($user_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
     $o_array[] = $od->created_at;
	  
	    $o_array[0] = $o_array[0];
		$o_array[] = $od->first_name." ".$od->last_name;
		$o_array[] = $od->email;
		$o_array[] = $od->mobile;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
 }
 
    $d1['header'] = 'Users Registered';
    $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
    $d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	
	$now = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";
	$d["footer"] = $now;

	$main = array();
 	$main[] = $d;
	return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

   // 1. =================== Get all users ====================	
   public function reports_sales(Request $request)
    {
	$main_array = array();
	
	//$date_from = $request->date_from;
	//$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='General Sales Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "Description";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Amount";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
    $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
   // $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
  //  $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	/**
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	**/ 
	
 
  //  $order_data = @\App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $order_data = @\App\Orders::get();
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
 
	 $user_name = @\App\User::where('id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$user_id)->first(['last_name'])->last_name;
	  
	 $total = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 
 
     $o_array[] = $od->created_at;
	 
	 $o_array[0] = $o_array[0];
 	 $o_array[] = $user_name;
	 $o_array[] = 'Payment of #'.$od->id;
	 $o_array[] =  env("CURRENCY_SYMBOL", "").''.$total;
	 $s= $o_array[0];
	 $o_array[0] = $s."";
	 
	 $rows_array[] = $o_array; 
	  
 }
 
 $d1['header'] = 'Sales';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
    // 1. =================== Get all users ====================	
   public function reports_sales_by_customer(Request $request)
    {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='General Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "Description";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Amount";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
    $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	 
	
 
    $order_data = \App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = \App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
	 
	 
	 if($user_id = $request->user_id)
	 {
		$user_name = \App\User::where('id',$user_id)->first(['first_name'])->first_name." ".\App\User::where('id',$user_id)->first(['last_name'])->last_name;
		$total = \App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 
 
		$o_array[] = $od->created_at;
	 
		$o_array[0] = $o_array[0];
		$o_array[] = $user_name;
		$o_array[] = 'Payment of #'.$od->id;
		$o_array[] =  env("CURRENCY_SYMBOL", "").$total;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
	 }
	  
 }
 
 $d1['header'] = 'Sales in November';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  public function reports_orders(Request $request)
    {
		
	 
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='General Orders Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Order Id";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "Customer";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Total";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	//$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
   // $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	 
	
 
    $order_data = \App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
	 
 
	 
		$user_name = @\App\User::where('id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$user_id)->first(['last_name'])->last_name;
		$total = @$od->total;
	 
	 
	 
 
		$o_array[] = $od->created_at->format('d M,Y | h:i A')."";
	 
		$o_array[0] = $o_array[0];
		
		
		$o_array[] = "#".$od->id;
		$o_array[] = $user_name;
		$o_array[] =  env("CURRENCY_SYMBOL", "").''.$total;
		
		
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
	 
	  
 }
 
 $d1['header'] = 'Orders';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
  
  public function reports_cancelled_orders(Request $request)
    {
		
	 
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] = env("APP_NAME", "");
	$d['subTitle'] ='General Orders Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Order Id";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "Customer";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Total";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	//$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
   // $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	 
	
 
    $order_data = \App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->where('order_status','cancelled')->get();
 
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
	 
 
	 
		$user_name = @\App\User::where('id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$user_id)->first(['last_name'])->last_name;
		$total = @$od->total;
	 
	 
	 
 
		$o_array[] = $od->created_at->format('d M,Y | h:i A')."";
	 
		$o_array[0] = $o_array[0];
		
		
		$o_array[] = $od->id;
		$o_array[] =   $user_name;
		$o_array[] =  env("CURRENCY_SYMBOL", "").''.$total;
		
		
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
	 
	  
 }
 
 $d1['header'] = 'Orders';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = \Carbon\Carbon::now()->format('d M, Y | h:i:s A')."";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

	
 
 
 

 
 
 

 
}
