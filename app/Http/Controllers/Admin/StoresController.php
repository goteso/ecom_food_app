<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...
 
 
 
 


class StoresController extends Controller
{
 
 
 public function stores_list(Request $request)
 {
 $result = @\App\Stores::paginate(20);
		return view('stores.index', compact('result'));
 }
 
 
 public function assign_vendor_to_store(Request $request , $id ='')
 {
	@\App\Stores::where('id', $id)->update(['vendor_id' => $request->vendor_id]);
	return back();
 }

 
 
 
 
 public function add_store(Request $request)
 {
	 
	  	$flight = new \App\Stores;
        $flight->store_name =  $request->store_name;
		$flight->address1 =  $request->address1;
		$flight->address2 =  $request->address2;
		$flight->city =  $request->city;
		$flight->state =  $request->state;
		$flight->country =  $request->country;
		$flight->pincode =  $request->pincode;
		$flight->latitude =  $request->latitude;
		$flight->longitude =  $request->longitude;
        $flight->save();

       return back()->with('success','Stores created successfully!');
	 
 }
 
 
 
 
  
 public function store_edit(Request $request , $store_id)
 {
	  

	  	 $result = @\App\Stores::where('id',$store_id)->first();
		
		
		
			 
			 
		return view('stores.edit-store')->with('result',$result);
	 
 }
 
 
 
 public function store_update(Request $request)
 {
 
	 
	 
	 $flight = @\App\Stores::find($request->store_id);
        $flight->store_name =  $request->store_name;
		$flight->address1 =  $request->address1;
		$flight->address2 =  $request->address2;
		$flight->city =  $request->city;
		$flight->state =  $request->state;
		$flight->country =  $request->country;
		$flight->pincode =  $request->pincode;
		$flight->latitude =  $request->latitude;
		$flight->longitude =  $request->longitude;
        $flight->save();

       return back()->with('success','Stores created successfully!');
	   
	   
 }
 
 
 
 
  public function search_stores(Request $request)
     {
       $search_text = $_GET["search_text"];
      $stores = \App\Stores::where(function($q) use ($request , $search_text) {  $q->orWhere( DB::raw("CONCAT(store_name)"),'like', '%'.$search_text.'%');})->get(['id','store_name']);
       return $stores;
     }
 
 
}
