<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Project;
use App\ProjectEmployees;
use App\ProjectClients;
use App\Contact;
use App\Permission;
use App\ContactType;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class TimeSlotsController extends Controller
{
  
    public function get_time_slots(Request $request)
	{
		 $current_date_time = \Carbon\Carbon::now();
		 $current_date_time_check = \Carbon\Carbon::now();
		 
		  
		 $week_day = $current_date_time->format('D');
		 $time = $current_date_time->format('H:i:s');
		 $slots_days_count = \App\Settings::where('key_title','slots_days_count')->first(["key_value"])->key_value;
		 if(isset($_GET["date1"]) && $_GET["date1"] != '')
		 {  
			 $main_array = array(); 
			 $current_time = $current_date_time->format('H:i:s');
			 for($x=0;$x < intval($slots_days_count);$x++ )
			 {
				 $current_date_time = \Carbon\Carbon::parse($_GET["date1"]);
			     $slots_days_difference = @\App\OrdersMetaTypes::where('identifier',$_GET['identifier'])->first(['slots_days_difference'])->slots_days_difference;
			     $current_date_time = $current_date_time->addDays($slots_days_difference);
			 
			 
				 $current_date_time = $current_date_time->addDays($x); 
				 $d["date"] = $current_date_time.""; 
				 $d["display_date"] = $current_date_time->format('d M, Y').""; 
				 $d["title"] = $current_date_time->format('D').""; 
				 
				 $timeslots = \App\DaysTimeSlots::where('week_day',$current_date_time->format('D')."")->get(['id','from_time','to_time']); 
				 
				 $timeslots_final_array = array();
				 foreach($timeslots as $ts)
				 {
					 
					 
			 
					 
				 
					 
			           if( $current_date_time_check->format('d-m-Y') == $current_date_time->format('d-m-Y'))
					 {
						 if(@\Carbon\Carbon::parse($ts->from_time)->format('H:i:s') > $current_time)
						 {
							 $timeslots_final_array[] = $ts;
						 }
						 else
						 {
						 }
					  }
					 else
					 {
						   
						 $timeslots_final_array[] = $ts;
					 }
				 }
				 $d["slots"] = $timeslots_final_array; 
				 $main_array[] = $d;
			 }
		 }
		 else
		 {
			 $main_array = array();
             $current_time = $current_date_time->format('H:i:s');			 
			 for($x=0;$x < intval($slots_days_count);$x++ )
			 {
				 $current_date_time = \Carbon\Carbon::now();
				 $current_date_time = $current_date_time->addDays($x); 
				 $d["date"] = $current_date_time.""; 
				 $d["display_date"] = $current_date_time->format('d M, Y').""; 
				 $d["title"] = $current_date_time->format('D').""; 
				 
				 $timeslots = \App\DaysTimeSlots::where('week_day',$current_date_time->format('D')."")->get(['id','from_time','to_time']);
				 
				 $timeslots_final_array = array();
				 foreach($timeslots as $ts)
				 {
					
                  if( $current_date_time_check->format('d-m-Y') == $current_date_time->format('d-m-Y'))
					 {
						 if(@\Carbon\Carbon::parse($ts->from_time)->format('H:i:s') > $current_time)
						 {
							 $timeslots_final_array[] = $ts;
						 }
						 else
						 {
						 }
					  }
					 else
					 {
						   
						 $timeslots_final_array[] = $ts;
					 }
				 }
				 
				 $d["slots"] = $timeslots_final_array; 
				 $main_array[] = $d;
			 }
		 }
		  return  $main_array;	
	}
 
 }
