<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...
 
 
 
 


class UserController extends Controller
{
     use feature;   
     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    

  //  Route 1.================================================================== USERS CRUD FUNCTION STARTS ==============================================================================//
	
	public function user_list_page()
    {
		
		return '11';
		return view('users.users');        
	}
	
	
	
   //Route 2.==================================================================== Get all users =============================================================================================//	
  
    public function user_list($date_filter , $search_text='')
    {  
 
 
		$cols_array = array();
	    $date_filter = @$_GET['date_filter'];
		$fields = @$_GET['fields'];
		$limit = @$_GET['limit'];
        $fields = explode(",",$fields);
		
		if(!isset($limit) or $limit =='' or $limit == null ) { $limit = 25;}
 		
		$users_columns = $fields;
		$main = array();
	    $columns_array = array();
		$data_array = array();
		$columns_final_array = array();
		
	    for($x=0;$x<sizeof($users_columns);$x++)
		{
		  $feature_settings_status = @\App\FeaturesSettings::where('title',$users_columns[$x])->first(['status'])->status;
		  if( $users_columns[$x] == 'base_discount' or $users_columns[$x] == 'base_price' or $users_columns[$x] == 'discount_expiry_date' or  $users_columns[$x] == 'stock_count' or  $users_columns[$x] == 'photo' or $users_columns[$x] == 'brand_ids' )
		  {
			  if($feature_settings_status == '1' or $feature_settings_status == 1 )
			  {
				 $columns_final_array[]=$users_columns[$x];
			  }
		  }
		  else
		  {
		   $columns_final_array[]=$users_columns[$x];
		  }
		}
		  
	    for($x=0;$x<sizeof($columns_final_array);$x++)
		{
		   $c1['title'] = $columns_final_array[$x];
		   $cols_array[] = $columns_final_array[$x];
		   $c1['visible'] = '1';
		   $c1['index'] = $x;
		   $columns_array[]=$c1;
		}
		 
        $data_array = new \App\User;
 
	    if($date_filter == 'lastAdded')
	    {
		   $data_array = $data_array::orderBy('created_at','ASC');
	    }
	    if ($date_filter == 'firstAdded')
	    {
		   $data_array = $data_array::orderBy('created_at','DESC'); 
	    }
	   
        
	    if(isset($_GET['search_text']) && $_GET['search_text'] != '' && $_GET['search_text'] != null)
	    {
		  $d = $_GET['search_text'];
	      $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("first_name"),'like', '%'.$d.'%')->orWhere( DB::raw("email"),'like', '%'.$d.'%')->orWhere( DB::raw("mobile"),'like', '%'.$d.'%');});
	    }
	  
    
        if(isset($_GET['user_type_filter']) && $_GET["user_type_filter"] !='' && $_GET["user_type_filter"] != null )
	    {
		   $user_type = $_GET['user_type_filter'];
	       $data_array = $data_array->where('user_type',$user_type);
	    }
    
	    $data_array = $data_array->paginate($limit,$cols_array);
        $main_data['columns'] = $columns_array;
	    $main_data['data'] = $data_array;
	    $result = $main_data;
	  
	    return $result;
        return view('users.users', compact('result'));
	                  
    }
 

 
 
    // Route 3. ================================================= get ADD and EDIT forms for USERS META TABLE =========================================================	
 	public function user_forms($id = '')
    {
		 
      $main_array = array();			
	  $main_array[] = $this->user_basic_form($id);
	  $main_array[] = $this->user_meta_form($id);
   	  $result = $main_array;
	   
	   return view('users.add-user', compact('result'));
	   
	   
    }  
	
	
	// Route 4. ============================================ Store User to the Database ===========================================
 public function user_store(Request $request)
    {
		
	 
	      $validator = Validator::make($request->all(), [
		        
                 ]);
               if ($validator->errors()->all()) 
                {
 				}
                else
                {
				    $fields =$request->fields;
					
				 
			 
			        $basic_fields = $request[0]['fields'];
			        $additional_info_fields = $request[1]['fields'];
					
	                $flight = new \App\User;					 
  				 
				 
                    for($t=0;$t<sizeof($basic_fields);$t++)
					{
						     $id = $basic_fields[$t]['identifier'];
							 
							 
							 
							 //return $id;
							 if($id == 'password') 
							 {
								  $flight->$id =  @\Hash::make($basic_fields[$t]['value']);
								 
							 }
							 else{
							 
							 if( @$basic_fields[$t]['value'] != '' && @$basic_fields[$t]['value'] != null &&  $id == 'email') 
							 {
								  $email_exist_count = @\App\User::where('email', @$basic_fields[$t]['value'])->count();
								  if($email_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Email you entered already Exist or Empty';  
				                          return $data;
								  }
								 
							 }
							 if($id == 'mobile')
							 {
								 
							 
								  $mobile_exist_count = \App\User::where('mobile', $basic_fields[$t]['value'])->count();
								  if($mobile_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Mobile you entered already Exist';  
				                          return $data;
								  }
							 }
							 
							 
					 
							 
							 

                             if($id == 'user_type')
							 {
								$request["roles"] = $basic_fields[$t]['value']; 
							 }								 
							 $flight->$id = @$basic_fields[$t]['value'];
							 }
							 
			 		 }
					 
		           
				  $flight->save();
				  
				  
				  
				  
				 
				  $this->syncPermissions($request, $flight);

					
			  if($flight !='')
					{
						
	   for($x=0;$x<sizeof($additional_info_fields);$x++)
		{
			$identifier = $additional_info_fields[$x]["identifier"];
			$value = $additional_info_fields[$x]["value"];
	        $user_meta_type_id = \App\UsersMetaTypes::where('identifier',$identifier)->first(['id'])->id;
	 		$additional_info_model = \App\UsersMetaValues::firstOrCreate(['user_meta_type_id' => $user_meta_type_id , 'user_id'=>$flight->id]);
            $additional_info_model->value = $value;
            $additional_info_model->save();
	    }
		 
		
		
	                   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'User Added Successfully';
					   $data['user_id'] = $flight->id;
                       $data['user_data']      =   $this->user_forms($flight->id);
					}
					else
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Some Error Occurred';
                       $data['user_data']      =   [];  
					}
	            return $data;
				}
  }
   
 
 
 
	
	// Route 5. ================================================= get ADD and EDIT forms for USERS META TABLE =========================================================	
 	public function user_edit_forms($id = '')
    {
		 
	  $main_array = array();			
	  $main_array[] = $this->user_basic_form($id);
	  $main_array[] = $this->user_meta_form($id);
	  $main_array[] = $id;
   	  $result = $main_array;
	  
	  if(isset($_GET["request_type"]) && $_GET["request_type"] == 'api')
	  {
		   return $result;
	  }
	  	 
	   return view('users.edit-user', compact('result'));
    }  
	
	
	
	
 
// Route 6 . ================================================================ Update User data to Database ============================================================
	public function update_user(Request $request)
    {
		  
		  $id = $request[0]['id'];
		   //update basic info
		   $basic_info = $request[0];
		   $fields =  $basic_info['fields'];
		   
		   $additional_info_fields = $request[1];
		   $additional_info_fields =  $additional_info_fields['fields'];
	       
		   
		if(sizeof($fields) > 0)
		{			
		    $flight = \App\User::find($id);	
			
               for($t=0;$t<sizeof($fields);$t++)
					{
					  $id = $fields[$t]['identifier'];
                      $flight->$id = @$fields[$t]['value'];
				    }
			$flight->save(); 
			

		if(sizeof($additional_info_fields) > 0)
			{			
		
	 
			for($x=0;$x<sizeof($additional_info_fields);$x++)
				{
					 
					$identifier = $additional_info_fields[$x]["identifier"];
					$value = $additional_info_fields[$x]["value"];
					$user_meta_type_id = \App\UsersMetaTypes::where('identifier',$identifier)->first(['id'])->id;
					$additional_info_model = \App\UsersMetaValues::firstOrCreate(['user_meta_type_id' => $user_meta_type_id , 'user_id'=>$flight->id]);
					$additional_info_model->value = $value;
					$additional_info_model->save();
				}
			}
		}
					
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'User Updated Successfully';
                       $data['user_data']      =   [];
					   return $data;
    }
    
	
	
	
	
	
	
	
	// Route 8 . ===================================================GET DRIVER LOCATION ========================================================
	public function get_driver_locations(Request $request)
    {
		return;
        $locations_data = @\App\UsersSessions::where('id',$request->user_session_id)->get(['latitude','longitude','user_id']);
        
        
        $data['status_code']    =   1;
       $data['status_text']    =   'Success';            
       $data['message']        =   'Locations Fetched Successfully';
       $data['user_data']      =   $locations_data;
        return $data;
        
    }
    
	
	
	
    // Route 9 . ===================================================UPDATE DRIVER LOCATION ========================================================
        public function update_driver_locations(Request $request)
    {
          @\App\UsersSessions::where('id',$request->user_session_id)->update(['latitude' => $request->latitude,'longitude' => $request->longitude]);
       $locations_data = @\App\UsersSessions::where('id',$request->user_session_id)->get(['latitude','longitude','user_id']);
        $data['status_code']    =   1;
       $data['status_text']    =   'Success';            
       $data['message']        =   'Locations Updated Successfully';
       $data['user_data']      =   $locations_data;
        return $data;
    }
 
 
  // Route 10. =============================================== Get all users addresses ======================================================
   
   public function user_address_list(Request $request,$user_id)
    {
		  
		$cols_array = array();
		$request_data = @$request[0];
        $date_filter = @$_GET['date_filter'];
		$fields = @$_GET["fields"];
		$limit = @$_GET["limit"];
        $fields = explode(",",$fields);
		
		if(!isset($limit) or $limit =='' or $limit == null ) { $limit = 25;}
		//$products_columns = @\Schema::getColumnListing('products');
		$users_columns = $fields;
		$main = array();
	    $columns_array = array();
		$data_array = array();
		$columns_final_array = array();
		
	    for($x=0;$x<sizeof($users_columns);$x++)
		{
		  $feature_settings_status = @\App\FeaturesSettings::where('title',$users_columns[$x])->first(['status'])->status;
		  if( $users_columns[$x] == 'base_discount' or $users_columns[$x] == 'base_price' or $users_columns[$x] == 'discount_expiry_date' or  $users_columns[$x] == 'stock_count' or  $users_columns[$x] == 'photo' or $users_columns[$x] == 'brand_ids' )
		  {
			  if($feature_settings_status == '1' or $feature_settings_status == 1 )
			  {
				 $columns_final_array[]=$users_columns[$x];
			  }
		  }
		  else
		  {
		   $columns_final_array[]=$users_columns[$x];
		  }
		}
		  
	   for($x=0;$x<sizeof($columns_final_array);$x++)
		{
		   $c1['title'] = $columns_final_array[$x];
		   $cols_array[] = $columns_final_array[$x];
		   $c1['visible'] = '1';
		   $c1['index'] = $x;
		   $columns_array[]=$c1;
		}
		
 
       $data_array = \App\Addresses::where('linked_id',$user_id);
 
	   if(isset($_GET["date_filter"]) && $_GET["date_filter"] == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','ASC');
	   }
	  if (isset($_GET["date_filter"]) && $_GET["date_filter"] == 'firstAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	 
	   }
	   
   /**
	   if(isset($request_data['category']) && $request_data['category'] != '' && $request_data['category'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$category."',products.category_ids)");
	   }
	**/   
 
	    if(isset($_GET["search_text"]) && $_GET["search_text"] != '' && $_GET["search_text"] != null)
	   {
		 $d = $_GET["search_text"];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("first_name"),'like', '%'.$d.'%');});
	   }
	   
 
		    $data_array = $data_array->paginate($limit,$cols_array);
	 
	   
	   
       $main_data['columns'] = $columns_array;
	   $main_data['data'] = $data_array;
	   return $main_data;
    }
 

 
 
    // Route 11. ========================================= get ADD and EDIT forms for USERS META TABLE ===============================================	
 	public function user_address_forms($id = '')
    {
	 
      $main_array = array();			
	  $main_array[] = $this->user_address_basic_form($id);
	  ///$main_array[] = $this->user_meta_form($id);
      return $main_array;
    }  
 
 

  // Route 12. ================================================== Store User to the Database ============================================================
 public function user_address_store(Request $request, $user_id)
    {
	 
		
		//return $request;
	      $validator = Validator::make($request->all(), [
		        
                 ]);
               if ($validator->errors()->all()) 
                {
 				}
                else
                {
				    $fields =$request->fields;
			 
			 
			 //return $request[0]['fields'];
			        $basic_fields = $request[0]['fields'];
			        $additional_info_fields = $request[1]['fields'];
					
	                $flight = new \App\Addresses;					 
  				 
				 
			//return sizeof($basic_fields);
                    for($t=0;$t<sizeof($basic_fields);$t++)
					{
						     $id = $basic_fields[$t]['identifier'];
							  
							
							 /**
							 if($id == 'email') 
							 {
								  $email_exist_count = \App\Addresses::where('email', $basic_fields[$t]['value'])->count();
								  if($email_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Email you entered already Exist';  
				                          return $data;
								  }
								 
							 }
							 if($id == 'mobile')
							 {
								  $mobile_exist_count = \App\Addresses::where('mobile', $basic_fields[$t]['value'])->count();
								  if($mobile_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Mobile you entered already Exist';  
				                          return $data;
								  }
							 }
							 **/
                       
							 $flight->$id = @$basic_fields[$t]['value'];
						 
					 
                    }
					
					 
					$flight->address_type = 'customer';
					$flight->linked_id = $user_id;
				  $flight->save();
 
 
 //return $flight;
                 	if($flight !='')
					{
						
				       $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Address Added Successfully';
                       $data['user_data']      =   $this->user_address_basic_form($flight->id);
					}
					else
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Some Error Occurred';
                       $data['user_data']      =   [];  
					}
	            return $data;
				}
  }
   
   
   
   
   
    // Route 14. ================================================== Delete User Address to the Database ============================================================
    public function user_address_delete(Request $request)
    {
	    @\App\Addresses::where('id',$request->user_address_id)->delete();
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';            
        $data['message']        =   'Address Deleted Successfully';
        $data['user_data']      =   [];
        return $data;
    }
	
	
	
 
 
 
 
 
    // Route 18 . ==========================================================Serach Users ==========================================================================
	 public function users_search(Request $request)
	 {
	 
		 $search_text = $_GET["search_text"];
		 $user_type = $_GET["user_type"];
		 
		 $role_id = @\App\Role::where('name',$user_type)->first(['id'])->id;
		 
		  $users = \App\User::where(function($q) use ($request , $search_text,$user_type) {  $q->orWhere( DB::raw("CONCAT(first_name,' ',mobile,email)"),'like', '%'.$search_text.'%')->orWhere( DB::raw("CONCAT(first_name,' ',mobile,email)"),'=',null);})->where('user_type',$role_id)->get(['id','first_name','email','mobile','last_name']);
	 return $users;
	 }
	 
	 
 
 
     // Route 19 . ==================================================================Serach Users ================================================================= 
	 
	 public function search_areas(Request $request)
	 {
		 
	   $search_text = $_GET["search_text"];
       $users = \App\ServiceAreas::where(function($q) use ($request , $search_text) {  $q->orWhere( DB::raw("CONCAT(area_title)"),'like', '%'.$search_text.'%');})->get(['id','area_title']);
	   return $users;
	 }
	 
	 
	 
	 
	
	// Route 20 . =================================================user_profile_basic_details for Profile Page=======================================================
	 	public function user_profile_basic_details(Request $request)
     {
		 
		 $user_details = \App\User::where('id',$request->user_id)->get();
		 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		 
		 
		 $user_details[0]['orders_count'] =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$request->user_id)->count();
		  $order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$request->user_id)->pluck('order_id');
		 $total_revenues = 0;
		 for($y=0;$y<sizeof($order_ids);$y++)
		 {
			$total_revenues += \App\OrdersPayments::where('order_id',$order_ids[$y])->sum('total');
		 }
		    $user_details[0]['total_revenues'] = $total_revenues;
		 if(count($user_details) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Details Fetched Successfully';
                       $data['user_data']      =    $user_details ;
					   return $data;
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'User Not Found';
                       $data['user_data']      =   [];
					   return $data;
		 }
	 }	
	
	
	
	
	
	// Route 21 . ============================================================user Orders for Profile Page=================================================================
	 
 	public function user_orders(Request $request)
     {
         
		 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		 $order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$request->user_id)->pluck('order_id');
		 
		 $orders = array();
		 
		 for($y=0;$y<sizeof($order_ids);$y++)
		 {
			 $order= array();
			 
			 
			 
			 
			 
			 $basic = @\App\Orders::where('id',$order_ids[$y])->get();
			 
			 foreach($basic as $b)
			 {
				 $b->created_at_formatted = $b->created_at->diffForHumans();
			 }
			 
		 
	       
			 $order['basic_details'] = $basic;
			 
			 $order_products= @\App\OrderProducts::where('order_id',$order_ids[$y])->get();
			 foreach($order_products as $op)
			 {
			   $op['variants'] =  @\App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			 }
			 $order['order_products'] = $order_products;
			 
			  
			 $order['order_payments'] = @\App\OrdersPayments::where('order_id',$order_ids[$y])->get();
			 
			 //meta values
			  $order_meta_values = @\App\OrdersMetaValues::where('order_id',$order_ids[$y])->get();
			  foreach($order_meta_values as $om)
			  {
				    $om->order_meta_title = @\App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			  }
			  $order['order_meta_values'] = $order_meta_values;
			  
			 $orders[] = $order;
		 }
		 
		 
	 
		  
		 if(count($orders) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['user_data']      =     $orders ;
					   $data['app_url'] = env('APP_URL');
					   return $data;
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'No Order Not Found';
                       $data['user_data']      =   [];
					   return $data;
		 }
	 }
	 
	 
	 
	 
	 // Route 22 . ==============================================================Delete Multiple Users =====================================================================
	  public function delete_users_multiple(Request $request)
    {
		 
	    $arr = $request->toArray();
	    if(sizeof($arr) > 0)
		{
		  for($i=0;$i<sizeof($arr);$i++)
		  {
			 $p_id = $arr[$i]['id'];
	         @\App\User::find($p_id)->delete();
		  }
		}
	
	 $d["status_code"] = 1;
	 $d["message"] = 'User Deleted Successfully';
	 $main = array();
 
	 
	 return $d;
 
    }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 //=========================Get Vendor List========================
	
	public function get_vendors_list(Request $request)
	{
		return;
		$vendor_user_type_id = @\App\Role::where('name','Vendor')->first(['id'])->id;
		$users = @\App\User::where('user_type',$vendor_user_type_id)->paginate(10);
		
		foreach($users as $user)
		{
			$meta_types = @\App\UsersMetaTypes::get(['id','title','identifier']);
			
			foreach($meta_types as $mt)
			{
				$mt->meta_value = @\App\UsersMetaValues::where('user_id',$user->id)->where('user_meta_type_id',$mt->id)->first(['value'])->value;
			}
			
			$user->meta_data = $meta_types;
		}
		
		return $users;
	}
	
	
	
 		
   // 3. =================== Get Basic form for ADD and EDIT operations ====================		
  public function user_basic_form( $user_id = '')
  {
	  
	   
	  $main_array = array();
	   
	  $data_array1['title'] = 'User Basic Information';
 	  $fields_array = array();
	  
	  
	  	  //6 photo
 	  $fields_keys6["title"] = "Photo";
	  $fields_keys6["type"] = 'file';
      $fields_keys6["identifier"] = 'photo';
	  $fields_keys6["required_or_not"] = '0';
	  $value = @\App\User::where('id',@$user_id)->first(['photo'])->photo;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys6["value"] = $value;
      $fields_array[] = $fields_keys6;
	  
	  
	  //1 first_name
	  $fields_keys1["title"] = "First Name";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'first_name';
	  $fields_keys1["required_or_not"] = '1';
	  $value = @\App\User::where('id',@$user_id)->first(['first_name'])->first_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys1["value"] = $value;
      $fields_array[] = $fields_keys1;
	  
	  //2 lastName
 	  $fields_keys2["title"] = "Last Name";
	  $fields_keys2["type"] = 'text';
      $fields_keys2["identifier"] = 'last_name';
	  $fields_keys2["required_or_not"] = '0';
	  $value = @\App\User::where('id',@$user_id)->first(['last_name'])->last_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys2["value"] = $value;
      $fields_array[] = $fields_keys2;
		  
	  //3 email
 	  $fields_keys3["title"] = "Email";
	  $fields_keys3["type"] = 'email';
      $fields_keys3["identifier"] = 'email';
	  $fields_keys3["required_or_not"] = '0';
	  $value = @\App\User::where('id',@$user_id)->first(['email'])->email;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys3["value"] = $value;
      $fields_array[] = $fields_keys3;
		  
 
 
    if($user_id == '')
	{
 	  $fields_keys4["title"] = "Password";
	  $fields_keys4["type"] = 'password';
      $fields_keys4["identifier"] = 'password';
	  $fields_keys4["required_or_not"] = '0';
	  $value = @\App\User::where('id',@$user_id)->first(['password'])->password;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys4["value"] = $value;
	  
      $fields_array[] = $fields_keys4;
	}
		  
	  //5 mobile
 	  $fields_keys5["title"] = "Mobile";
	  $fields_keys5["type"] = 'text';
      $fields_keys5["identifier"] = 'mobile';
	  $fields_keys5["required_or_not"] = '1';
	  $value = @\App\User::where('id',@$user_id)->first(['mobile'])->mobile;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys5["value"] = $value;
      $fields_array[] = $fields_keys5;
	  

	  
	  
	  
	  $user_types = \App\Role::get();
	  $user_types_arr = array();
	  foreach($user_types as $t)
	  {
		  
		   $login_id = @Auth::id();
	       $user_type = @Auth::user()->user_type;
		   $role_title = @\App\Role::where('id',$user_type)->first(['name'])->name;
	  
	     if($role_title == 'Admin')
		 {
		   $t->value = $t->id;
		   $t->title = $t->name;
		    $user_types_arr[] = $t;	
			 			 
		 }
		 else
		 {
			 if($t->name != 'Admin')
			 {
			  $t->value = $t->id;
		      $t->title = $t->name;
			
             $user_types_arr[] = $t;			
			 }
			 else
			 {
			 }
		  }
       }
	   
	   
	   
	   
	   
	   	  $service_areas= \App\ServiceAreas::get();
	  $service_areas_arr = array();
	  foreach($service_areas as $t2)
	  {
		  
		   $login_id = @Auth::id();
	       $service_area_id= @Auth::user()->service_area_id;
		   $area_title= @\App\ServiceAreas::where('id',$service_area_id)->first(['area_title'])->area_title;
	  
	    
		    $t2->value = $t2->id;
		    $t2->title = $t2->area_title;
		    $service_areas_arr [] = $t2;	
			 			 
	  }
	   
	   
	   
	   
	   
	   
	   
	  
	  	  	  //7 mobile
 	  $fields_keys7["title"] = "User Type";
	  $fields_keys7["type"] = 'Selectbox';
      $fields_keys7["identifier"] = 'user_type'; 
	  $fields_keys7["required_or_not"] = '1';
	  $fields_keys7["field_options"] = $user_types_arr;
	   $value = @\App\User::where('id',@$user_id)->first(['user_type'])->user_type;
	  if($value == null or $value == '') { $value = '';}
	 
 
	  $fields_keys7["value"] = $value;
      $fields_array[] = $fields_keys7;
	  
	  
	  
	  
	  
	  
	  //7 mobile
 	  $fields_keys77["title"] = "Service Area";
	  $fields_keys77["type"] = 'Selectbox';
      $fields_keys77["identifier"] = 'service_area_id'; 
	  $fields_keys77["required_or_not"] = '1';
	  $fields_keys77["field_options"] = $service_areas_arr ;
	   $value = @\App\User::where('id',@$user_id)->first(['service_area_id'])->service_area_id;
	  if($value == null or $value == '') { $value = '';}
	 
 
	  $fields_keys77["value"] = $value;
      $fields_array[] = $fields_keys77;
	  
	  
	  
	  
	  
	  
	  
	  
	  
	 
	  $data_array1['fields'] = $fields_array;
      $main_array =$data_array1;
	  return $main_array;
	 
 }
 
 
 
 // 4. =================== get ADD and EDIT forms for USERS META TABLE ====================		
public function user_meta_form( $user_id = '')
{
	  $users_meta_types = \App\UsersMetaTypes::get(['id','title','identifier','count_limit','type','field_options','field_options_model','field_options_model_columns']);
	  $fields_array2 = array();
	  foreach($users_meta_types as $umt)
	  {		  
		  if($umt->field_options_model != '')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             $d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			  
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $users_meta_types;
	  return $data_array2;
}



 

	 
	
	
//============================================================================== USERS ADDRESSES FUNCTIONS STARTS ========================================================//


	
	
	
  
	
 		
   // 3. =================== Get Basic form for ADD and EDIT operations ====================		
  public function user_address_basic_form($address_id = '')
  {
	  $main_array = array();
	   
	  $data_array1['title'] = 'User Address Form';
 	  $fields_array = array();
	  
	  //1 Address Title
	  $fields_keys1["title"] = "Address Title";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'address_title';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_title'])->address_title;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys1["value"] = $value;
      $fields_array[] = $fields_keys1;
	  
	  //2 Address Line 1
 	  $fields_keys2["title"] = "Address Line 1";
	  $fields_keys2["type"] = 'text';
      $fields_keys2["identifier"] = 'address_line1';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_line1'])->address_line1;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys2["value"] = $value;
      $fields_array[] = $fields_keys2;
		  
	  //3 Address Line 2
 	  $fields_keys3["title"] = "Address Line 2";
	  $fields_keys3["type"] = 'text';
      $fields_keys3["identifier"] = 'address_line2';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_line2'])->address_line2;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys3["value"] = $value;
      $fields_array[] = $fields_keys3;
		  
	  //4 contact_person_name
 	  $fields_keys4["title"] = "Contact Person Name";
	  $fields_keys4["type"] = 'text';
      $fields_keys4["identifier"] = 'contact_person_name';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['contact_person_name'])->contact_person_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys4["value"] = $value;
      $fields_array[] = $fields_keys4;
		  
	  //5 contact_person_mobile
 	  $fields_keys5["title"] = "Contact Person Mobile";
	  $fields_keys5["type"] = 'text';
      $fields_keys5["identifier"] = 'contact_person_mobile';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['contact_person_mobile'])->contact_person_mobile;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys5["value"] = $value;
      $fields_array[] = $fields_keys5;
	  
	  //6 city
 	  $fields_keys6["title"] = "City";
	  $fields_keys6["type"] = 'text';
      $fields_keys6["identifier"] = 'city';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['city'])->city;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys6["value"] = $value;
      $fields_array[] = $fields_keys6;
	  
	  
	 //7 state
 	  $fields_keys7["title"] = "State";
	  $fields_keys7["type"] = 'text';
      $fields_keys7["identifier"] = 'state';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['state'])->state;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys7["value"] = $value;
      $fields_array[] = $fields_keys7;
	  
	  	 //8 country
 	  $fields_keys8["title"] = "Country";
	  $fields_keys8["type"] = 'text';
      $fields_keys8["identifier"] = 'country';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['country'])->country;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys8["value"] = $value;
      $fields_array[] = $fields_keys8;
	  
	  
	  //9 pincode
 	  $fields_keys9["title"] = "Pincode";
	  $fields_keys9["type"] = 'text';
      $fields_keys9["identifier"] = 'pincode';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['pincode'])->pincode;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys9["value"] = $value;
      $fields_array[] = $fields_keys9;
	  
	  
	  	  //10 latitude
 	  $fields_keys10["title"] = "Latitude";
	  $fields_keys10["type"] = 'locationPicker';
      $fields_keys10["identifier"] = 'latitude';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['latitude'])->latitude;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys10["value"] = $value;
      $fields_array[] = $fields_keys10;
	  
	  	  //11 longitude
 	  $fields_keys11["title"] = "Longitude";
	  $fields_keys11["type"] = 'locationPicker';
      $fields_keys11["identifier"] = 'longitude';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['longitude'])->longitude;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys11["value"] = $value;
      $fields_array[] = $fields_keys11;
	  
	 
	  $data_array1['fields'] = $fields_array;
      $main_array =$data_array1;
	  return $main_array;
	 
 }
 
 
 
 // 4. =================== get ADD and EDIT forms for USERS META TABLE ====================		
public function user_address_meta_form( $user_id = '')
{
	  $users_meta_types = \App\UsersMetaTypes::get(['id','title','identifier','count_limit','type']);
	  $fields_array2 = array();
	  foreach($users_meta_types as $umt)
	  {
		  $user_meta_type_id = $umt->id;
		  $value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
		  if($value == null or $value == '') { $value = '';}
		  $umt->value  = $value;
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $users_meta_types;
	  return $data_array2;
}




   
   
 // 6 . ================================ Update User data to Database ===================================
	public function user_address_update(Request $request, $id)
    {
		 return;
		   //update basic info
		   $basic_info = $request[0];
		   $fields =  $basic_info['fields'];
		   
		   $additional_info_fields = $request[1];
		   $additional_info_fields =  $additional_info_fields['fields'];
	       
		   
		if(sizeof($fields) > 0)
		{			
		      $flight = \App\Addresses::find($id);	
			
               for($t=0;$t<sizeof($fields);$t++)
					{
					  $id = $fields[$t]['identifier'];
                      $flight->$id = $fields[$t]['value'];
				    }
			  $flight->save(); 
	     }
					
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Address Updated Successfully';
                       $data['user_data']      =   [];
					   return $data;
    }
    
	
	
 
 
 
 
 
 
 
 
 
 
 
 
 
 	public function user_profile2(Request $request , $user_id)
     {
		 $user_details = \App\User::where('id',$user_id)->get();
		 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		 
		 
		 $user_details[0]['orders_count'] =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$user_id)->count();
		 $order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$user_id)->pluck('order_id');
		 $total_revenues = 0;
		 for($y=0;$y<sizeof($order_ids);$y++)
		 {
			$total_revenues += \App\OrdersPayments::where('order_id',$order_ids[$y])->sum('total');
		 }
		    $user_details[0]['total_revenues'] = $total_revenues;
		 if(count($user_details) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Details Fetched Successfully';
                       $data['user_data']      =    $user_details ;
				 
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'User Not Found';
                       $data['user_data']      =   [];
					   
		 }
		 
		 
		 $result = $data;
		 return view('users.user-profile',compact('result'));
	 }	

// 8 ============================user_profile_basic_details for Profile Page====================================
 
 
 
 
//get init data
 public function get_init_data(Request $request)
 {
	 
	 
	  $user_id = $request->user_id;
	  $main = array();
	  $d["user_details"] = @\App\User::where('id',$user_id)->get();
	 // $d["user_meta_details"] = @\App\UsersMetaValues::where('user_id',$user_id)->get(["id","user_meta_type_id","user_id","value"]);
	 $settings = @\App\Settings::get(["id","key_title","key_display_title","key_value","type"]);
	 
	 
	 $d1['id'] = '100';
	 $d1['key_title'] = 'CURRENCY_SYMBOL';
	 $d1['key_display_title'] = 'CURRENCY SYMBOL';
	 $d1['key_value'] = env('CURRENCY_SYMBOL');
	 $d1['type'] = 'orders';
	   $settings[] = $d1;     
				
				
	  $d["settings"] = $settings ;
	  $d["categories"] = @\App\Categories::where('parent_id','')->orWhere('parent_id',null)->get(['id','title','photo']);
	  $main[] = $d;
	  return $main;
 }
 



 
 
 

	
	
	
	
 

	 
	           private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }
		   
 

 
}
