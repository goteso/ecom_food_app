<?php
namespace App\Http\Controllers\Api;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use File;
use Mail;
use App\Traits\feature; // <-- you'll need this line...

class UserController extends Controller
{
     use feature;   
     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    

	
 //================================================================================= MOBILE WEB SERVICES STARTS HERe ================================================================//
 //===================================================================================================================================================================================//
 
 
 
	// 1. Api for Login
	
	    public function login(Request $request)
    {   
	 
        $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required',
       // 'notification_token'=>'required',
       // 'login_type'=>'required'
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	        if(\Auth::guard('api')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')  ]))
            {
               // User::where('notification_token',$request->notification_token)->update(['notification_token'=>'']);
              //  User::where('email',$request->email)->update(['notification_token'=>$request->notification_token,'login_type'=>$request->login_type]);
                $user = Auth::guard('api')->user()->toArray();
				$current_date_time = \Carbon\Carbon::now();
				$user_session = new \App\UsersSessions;
                $user_session->user_id = $user['id'];
				$user_session->device_type = @$request->device_type;
				$user_session->device_id = @$request->device_id;
				$user_session->screen_size = @$request->screen_size;
				$user_session->device_os = @$request->device_os;
				$user_session->ip_address = \Request::getClientIp(true);
				$user_session->location_name = @$request->location_name;
				$user_session->latitude = @$request->latitude;
				$user_session->longitude = @$request->longitude;
				$user_session->browser = @$request->browser;
				$user_session->timezone = @$request->timezone;
				$user_session->notification_token = @$request->notification_token;
				$user_session->last_login = $current_date_time;
				$user_session->refresh_token = @$request->refresh_token;
				$user_session->access_token = @$request->access_token;
                $user_session->save();
				
			 
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'You have logged-in successfully.';
                $data['user_data']      =   $user; 
                $data['session_data']      =   $user_session; 				
 
			
            }            
            else
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Please enter correct email and password.';
            }
          }
       return $data;
    }
	
	
	
	
		    public function logout(Request $request)
    {   
	 
        $validator = Validator::make($request->all(), [
        'user_session_id' => 'required',
        
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	
	            $logout = @\App\UsersSessions::where('id',$request->user_session_id)->delete();
	            Auth::guard('api')->logout();
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Logout successfully.';
                $data['user_data']      =   []; 
              			
 
			
             
          }
       return $data;
    }
	
	
	
	
	 
	 
	 // webservice 2 
	 	     //For update user profile
    public function upload_profile_image(Request $request)
    {   
	    $validator = Validator::make($request->all(), [
                'user_id' => 'required',
		 
               ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                $user_data = User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
						
					   if(isset($request->profile_image) && !empty($request->profile_image))
                        {
                          $unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));    
                          $file = $request->profile_image;                       
                          $photo_name = $unique_string.$file->getClientOriginalName();
                          // $thumb_name = "thumb-". $photo_name;                     
                          $file->move($path,$photo_name);
                          // $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'800');
                       }
					   else
					   {
						$photo_name = '';   
					   }
					   
					   $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Image is Uploaded.';
                       $data['data'][]["image_url"] =  $request->user_id.'/'.$photo_name;
					   
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }
	
	
 
	
	
	// webservice 3
		
	
	
	
	
	
	

  //Get User Profile info
	public function get_user_profile(Request $request)
    {
	 
        $validator = Validator::make($request->all(), [
             
				'profile_user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    if(User::where('id',$request->profile_user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
                    $user = User::where('id', $request->profile_user_id)->first()->toArray();
					$user["created_at"]=\Carbon\Carbon::parse($user["created_at"])->setTimezone($request->timezone)->format('F d Y , h:i A');;
 
 
                  /**
	 				$profile_user_ratings = UserRatings::where('profile_user_id', $request->profile_user_id)->where('user_id',$request->user_id)->sum('rating');
					if($profile_user_ratings == null || $profile_user_ratings == NULL || $profile_user_ratings == '' || $profile_user_ratings == ' ' ) { $profile_user_ratings = '0';} 
				    if($profile_user_ratings > 0 )
						{ 
					      $user["profile_user_ratings"] = $profile_user_ratings;
						}
						else
						{
							 $user["profile_user_ratings"] = '0';
						}
				    $profile_user_followers_count = \App\UsersRelations::where('recipient_id', $request->profile_user_id)->get()->count();
					$profile_user_followings_count = \App\UsersRelations::where('user_id', $request->profile_user_id)->get()->count();
					$profile_user_posts_count = \App\Posts::where('user_id', $request->profile_user_id)->get()->count();
					$profile_user_posts = \App\Posts::where('user_id', $request->profile_user_id)->with('posts_images')->get();
                 
				    $user['profile_user_followers_count'] =  $profile_user_followers_count;
					$user['profile_user_followings_count'] =  $profile_user_followings_count;
					$user['profile_user_posts_count'] =  $profile_user_posts_count;
					$user['profile_user_posts'] =  $profile_user_posts;
					**/
			        
					if(count($user) != 0)
                    {
                     $data['status_code']    =   1;
                     $data['status_text']    =   'Success';             
                     $data['message']        =   'User profile fetched sucessfully';
                     $data['data'] = $user; 
                    }                    
                    else
                    {
                     $data['status_code']    =   0;
                     $data['status_text']    =   'Failed';             
                     $data['message']        =   'User not found';
                    }
                }
        return $data;
    }
	
	
	
	
	public function get_other_user_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'user_id' => 'required',
				'profile_user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    if(User::where('id',$request->profile_user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
                    
					$user = User::where('id', $request->profile_user_id)->first()->toArray();
					$user["created_at"]=Carbon::parse($user["created_at"])->setTimezone($request->timezone)->format('F d Y , h:i A');
               
				    $my_ratings_to_user = @UserRatings::where('profile_user_id', $request->profile_user_id)->where('user_id',$request->user_id)->first(['rating'])->rating;
					if($my_ratings_to_user == null || $my_ratings_to_user == NULL || $my_ratings_to_user == '' || $my_ratings_to_user == ' ' ) { $my_ratings_to_user = '0';} 
					$user["my_ratings_to_user"] =  $my_ratings_to_user;
				 
				    $profile_user_ratings = @UserRatings::where('profile_user_id', $request->profile_user_id)->where('user_id',$request->user_id)->sum('rating');
					
					if($profile_user_ratings == null || $profile_user_ratings == NULL || $profile_user_ratings == '' || $profile_user_ratings == ' ' ) { $profile_user_ratings = '0';} 
				    if($profile_user_ratings > 0 )
						{ 
					      $user["profile_user_ratings"] = $profile_user_ratings;
						}
						else
						{
							 $user["profile_user_ratings"] = '0';
						}
					$profile_user_followers_count = @\App\UsersRelations::where('recipient_id', $request->profile_user_id)->get()->count();
					$profile_user_followings_count = @\App\UsersRelations::where('user_id', $request->profile_user_id)->get()->count();
					$profile_user_posts_count = @\App\Posts::where('user_id', $request->profile_user_id)->get()->count();
					$is_follow = @\App\UsersRelations::where('user_id', $request->user_id)->where('recipient_id', $request->profile_user_id)->get()->count();
					
					$profile_user_posts = @\App\Posts::select(DB::raw('id,type'))->where('user_id', $request->profile_user_id)->with('posts_images')->get();
				 
				    $user['profile_user_followers_count'] =  $profile_user_followers_count;
					$user['profile_user_followings_count'] =  $profile_user_followings_count;
					$user['profile_user_posts_count'] =  $profile_user_posts_count;
					$user['is_follow'] =  $is_follow;
					$user['profile_user_posts'] =  $profile_user_posts;
		 
			       if(count($user) != 0)
                    {
                      $data['status_code']    =   1;
                      $data['status_text']    =   'Success';             
                      $data['message']        =   'User profile fetched sucessfully';
                      $data['data'] = $user; 
                    }                    
                    else
                    {
                      $data['status_code']    =   0;
                      $data['status_text']    =   'Failed';             
                      $data['message']        =   'User not found';
                    }
                }
        return $data;
    }
	
	
	
	
	
	
	 

    //For update user profile===================================
    public function update_profile(Request $request)
    {   
     $validator = Validator::make($request->all(), [
                'user_id' => 'required',
			 
                            
                 ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                $user_data = User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
						
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
                        if(isset($request->profile_image) && !empty($request->profile_image))
                        {
                        
                        $file = $request->profile_image;                       
                       
                       //$this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'8000');
                  
                          @\App\User::where('id', $request->user_id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name, 'photo' =>$file,'email' =>$request->email ]);
                     
                        }
                        else
                        {
                          $photo_name = '';
                          $thumb_name = '';
						  $file ='';

                          @\App\User::where('id', $request->user_id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name, 'photo' =>$file,'email' =>$request->email]);
        
                    }
                       $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Your Profile Updated Successfully.';
                       $data['data'] = \App\User::select('*')->where('id',$request->user_id)->first();
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 

 
 
 
 
 
 	
	
	///////////////////////////////// Password Functions Starts ///////////////////////////////////////////
		
    //webservice for Forgot Password

    public function forgotPasswordEmail(Request $request)
    {
		 
		 
 
        $validator = Validator::make($request->all(), [
           // 'email' => 'required|max:255|email',
             ]);
        if ($validator->errors()->all()) 
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();
            $data['email'] = $request->email;			
            return $data;                  
        }
		
	

		
        $email = $request->email;
	 
		
		$c = \App\User::where('email', $email)->count();
	   if($c < 1)
	   {
		    $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Email not Found';
         		
            return $data; 
		   
	   }
		
        $email_verify = \App\User::where('email', $email)->get();

 
        $first_name = $email_verify[0]->first_name;
		
	 
 
 
		
        if(count($email_verify) > 0){
        
        $six_digit_random_number = mt_rand(1000, 9999);
        
        DB::table('users')->where('email', $email)->update(array('one_time_code' => $six_digit_random_number));
		
		
		
		$data=array('otp' => $six_digit_random_number , 'first_name' => $first_name);
		Mail::send('emails.otp', $data, function($message) use ($first_name, $email) {
             
                $message->to($email, 'ECommerceApp')->subject('ECommerceApp - Password Reset');
                $message->from('harvindersingh@goteso.com', 'ECommerceApp');
            });
 
		 
        $data['status_code']    =   1;                      
        $data['status_text']    =   'Success';
        $data['message']        =   'Your new otp sent on your email address.';
        $data['id']             =   $email_verify[0]->id;
        $data['one_time_code']  =   $six_digit_random_number;
        
        } else {
        
        $data['status_code']    =   0;                      
        $data['status_text']    =   'Failed';
        $data['message']        =   'Email address is not registered.';
        }
        
        return $data;
    }
	
	
	
  //webservice for Forgot Password

    public function verify_otp(Request $request){
    
    $email                      =       $request->email;
    $one_time_code     =       $request->one_time_code;    
    $otp_verify = \App\User::where('email', $email)->where('one_time_code', $one_time_code)->get(["id","email"]);
	
	
 
    if(count($otp_verify) > 0){
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Otp matches successfully';
        $data['user_id']        =   $otp_verify[0]->id;
		$data["data"] = $otp_verify;
	 
    
        } else {
        $data['status_code']    =   0;
        $data['status_text']    =   'Failed';
        $data['message']        =   'Invalid OTP';
        
        }
        
        return $data;
    
    }
	
	  //webservice for Forgot Password

    public function forgotPasswordChange(Request $request){
    
    $user_id    =  $request->user_id;
    $password     =       \Hash::make($request->password);     
     \App\User::where('id', $request->user_id)->update(['password' => $password]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Password Changes Successfully';
        $data['user_id']        =   $user_id;
	    return $data;
   }
	
	
	
	
    


    //webservice for New password
     public function newPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'user_id' =>  'required|numeric',
        'new_password' => 'required',
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
  
        else if(isset($request->old_password) && !empty($request->old_password))
        {
            $id                      =       $request->input('user_id');
            $old_password            =       $request->old_password;  
            $password                =       \Hash::make($request->new_password); 
           
		   
		    if(isset($_GET['request_type']) && $_GET["request_type"] == 'api')
			{
			  DB::table('users')->where('id', $id)->update(array('password' => $password));    
              $data['status_code']    =   1;
              $data['status_text']    =   'Success';
              $data['message']        =   'Password changed successfully.';
              $data['user_id']        =   $request->user_id;
			  return $data;
			}
			



			
            $old_password_data = DB::table('users')->select('id','password')->where('id', '=', $id)->get();            
            if(\Hash::check($old_password, $old_password_data[0]->password))
            {  
            DB::table('users')->where('id', $id)->update(array('password' => $password));    
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Password changed successfully.';
            $data['user_id']        =   $old_password_data[0]->id;
        
            } 
            else 
            {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Old Password';            
            }   
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Data'; 
        }        
        return $data;
    
    }
	
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	//================================================================== USERS CRUD FUNCTION STARTS ==============================================================================//
	
   // 1. =================== Get all users ====================	
   public function user_list(Request $request)
    {
		$cols_array = array();
		$request_data = @$request[0];
        $date_filter = @$request_data['date_filter'];
		$fields = @$request_data['fields'];
		$limit = @$request_data['limit'];
        $fields = explode(",",$fields);
		
		if(!isset($limit) or $limit =='' or $limit == null ) { $limit = 25;}
		//$products_columns = @\Schema::getColumnListing('products');
		
		$users_columns = $fields;
		$main = array();
	    $columns_array = array();
		$data_array = array();
		$columns_final_array = array();
		
	    for($x=0;$x<sizeof($users_columns);$x++)
		{
		  $feature_settings_status = @\App\FeaturesSettings::where('title',$users_columns[$x])->first(['status'])->status;
		  if( $users_columns[$x] == 'base_discount' or $users_columns[$x] == 'base_price' or $users_columns[$x] == 'discount_expiry_date' or  $users_columns[$x] == 'stock_count' or  $users_columns[$x] == 'photo' or $users_columns[$x] == 'brand_ids' )
		  {
			  if($feature_settings_status == '1' or $feature_settings_status == 1 )
			  {
				 $columns_final_array[]=$users_columns[$x];
			  }
		  }
		  else
		  {
		   $columns_final_array[]=$users_columns[$x];
		  }
		}
		  
	    for($x=0;$x<sizeof($columns_final_array);$x++)
		{
		   $c1['title'] = $columns_final_array[$x];
		   $cols_array[] = $columns_final_array[$x];
		   $c1['visible'] = '1';
		   $c1['index'] = $x;
		   $columns_array[]=$c1;
		}
		
 
       $data_array = new \App\User;
 
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array::orderBy('created_at','ASC');
	   }
	  if ($date_filter == 'firstAdded')
	   {
		   $data_array = $data_array::orderBy('created_at','DESC');
	 
	   }
	   
   /**
	   if(isset($request_data['category']) && $request_data['category'] != '' && $request_data['category'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$category."',products.category_ids)");
	   }
	**/   
 
	    if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		 $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("first_name"),'like', '%'.$d.'%');});
	   }
	   
	   $data_array = $data_array->paginate($limit,$cols_array);
       $main_data['columns'] = $columns_array;
	   $main_data['data'] = $data_array;
	   return $main_data;
    }
 

     // 2. =================== get ADD and EDIT forms for USERS META TABLE ====================	
 	public function user_forms($id = '')
    {
      $main_array = array();			
	  $main_array[] = $this->user_basic_form($id);
	  $main_array[] = $this->user_meta_form($id);
      return $main_array;
    }  
	
 		
   // 3. =================== Get Basic form for ADD and EDIT operations ====================		
  public function user_basic_form( $user_id = '')
  {
	  $main_array = array();
	   
	  $data_array1['title'] = 'User Register Form';
 	  $fields_array = array();
	  
	  //1 first_name
	  $fields_keys1["title"] = "First Name";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'first_name';
	  $value = @\App\User::where('id',@$user_id)->first(['first_name'])->first_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys1["value"] = $value;
      $fields_array[] = $fields_keys1;
	  
	  //2 lastName
 	  $fields_keys2["title"] = "Last Name";
	  $fields_keys2["type"] = 'text';
      $fields_keys2["identifier"] = 'last_name';
	  $value = @\App\User::where('id',@$user_id)->first(['last_name'])->last_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys2["value"] = $value;
      $fields_array[] = $fields_keys2;
		  
	  //3 email
 	  $fields_keys3["title"] = "Email";
	  $fields_keys3["type"] = 'email';
      $fields_keys3["identifier"] = 'email';
	  $value = @\App\User::where('id',@$user_id)->first(['email'])->email;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys3["value"] = $value;
      $fields_array[] = $fields_keys3;
		  
	  //4 password
 	  $fields_keys4["title"] = "Password";
	  $fields_keys4["type"] = 'password';
      $fields_keys4["identifier"] = 'password';
	  $value = @\App\User::where('id',@$user_id)->first(['password'])->password;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys4["value"] = $value;
      $fields_array[] = $fields_keys4;
		  
	  //5 mobile
 	  $fields_keys5["title"] = "Mobile";
	  $fields_keys5["type"] = 'text';
      $fields_keys5["identifier"] = 'mobile';
	  $value = @\App\User::where('id',@$user_id)->first(['mobile'])->mobile;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys5["value"] = $value;
      $fields_array[] = $fields_keys5;
	  
	  //6 photo
 	  $fields_keys6["title"] = "Photo";
	  $fields_keys6["type"] = 'file';
      $fields_keys6["identifier"] = 'photo';
	  $value = @\App\User::where('id',@$user_id)->first(['photo'])->photo;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys6["value"] = $value;
      $fields_array[] = $fields_keys6;
	 
	  $data_array1['fields'] = $fields_array;
      $main_array =$data_array1;
	  return $main_array;
	 
 }
 
 
 
 // 4. =================== get ADD and EDIT forms for USERS META TABLE ====================		
public function user_meta_form( $user_id = '')
{
	  $users_meta_types = \App\UsersMetaTypes::get(['id','title','identifier','count_limit','type','field_options','field_options_model','field_options_model_columns']);
	  $fields_array2 = array();
	  foreach($users_meta_types as $umt)
	  {		  
		  if($umt->field_options_model != '')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             $d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			  
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $users_meta_types;
	  return $data_array2;
}


// 5. ================= Store User to the Database ============================
 public function user_store(Request $request)
    {
	      $validator = Validator::make($request->all(), [
		        
                 ]);
               if ($validator->errors()->all()) 
                {
 				}
                else
                {
				    $fields =$request->fields;
			 
			        $basic_fields = $request[0]['fields'];
			        $additional_info_fields = $request[1]['fields'];
					
	                $flight = new \App\User;					 
  				 
                    for($t=0;$t<sizeof($basic_fields);$t++)
					{
						     $id = $basic_fields[$t]['identifier'];
							 
							 if($id == 'email') 
							 {
								  $email_exist_count = \App\User::where('email', $basic_fields[$t]['value'])->count();
								  if($email_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Email you entered already Exist';  
				                          return $data;
								  }
								 
							 }
							 if($id == 'mobile')
							 {
								  $mobile_exist_count = \App\User::where('mobile', $basic_fields[$t]['value'])->count();
								  if($mobile_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Mobile you entered already Exist';  
				                          return $data;
								  }
							 }                       
							 $flight->$id = $basic_fields[$t]['value'];
			 		 }
				  $flight->save();

					
			  if($flight !='')
					{
						
	   for($x=0;$x<sizeof($additional_info_fields);$x++)
		{
			$identifier = $additional_info_fields[$x]["identifier"];
			$value = $additional_info_fields[$x]["value"];
	        $user_meta_type_id = \App\UsersMetaTypes::where('identifier',$identifier)->first(['id'])->id;
	 		$additional_info_model = \App\UsersMetaValues::firstOrCreate(['user_meta_type_id' => $user_meta_type_id , 'user_id'=>$flight->id]);
            $additional_info_model->value = $value;
            $additional_info_model->save();
	    }
		 
		
		
	                   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'User Added Successfully';
                       $data['user_data']      =   $this->user_forms($flight->id);
					}
					else
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Some Error Occurred';
                       $data['user_data']      =   [];  
					}
	            return $data;
				}
  }
   
 // 6 . ================================ Update User data to Database ===================================
	public function update_user(Request $request, $id)
    {
		
		
		   //update basic info
		   $basic_info = $request[0];
		   $fields =  $basic_info['fields'];
		   
		   $additional_info_fields = $request[1];
		   $additional_info_fields =  $additional_info_fields['fields'];
	       
		   
		if(sizeof($fields) > 0)
		{			
		    $flight = \App\User::find($id);	
			
               for($t=0;$t<sizeof($fields);$t++)
					{
					  $id = $fields[$t]['identifier'];
                      $flight->$id = $fields[$t]['value'];
				    }
			$flight->save(); 
			

		if(sizeof($additional_info_fields) > 0)
			{			
		
	 
			for($x=0;$x<sizeof($additional_info_fields);$x++)
				{
					 
					$identifier = $additional_info_fields[$x]["identifier"];
					$value = $additional_info_fields[$x]["value"];
					$user_meta_type_id = \App\UsersMetaTypes::where('identifier',$identifier)->first(['id'])->id;
					$additional_info_model = \App\UsersMetaValues::firstOrCreate(['user_meta_type_id' => $user_meta_type_id , 'user_id'=>$flight->id]);
					$additional_info_model->value = $value;
					$additional_info_model->save();
				}
			}
		}
					
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'User Updated Successfully';
                       $data['user_data']      =   [];
					   return $data;
    }
    
	
	
	
	
	
//============================================================================== USERS ADDRESSES FUNCTIONS STARTS ========================================================//


	
	
	
	 // 1. =================== Get all users addresses ====================	
   public function user_address_list(Request $request,$user_id)
    {
		$cols_array = array();
		$request_data = @$request[0];
        $date_filter = @$request_data['date_filter'];
		$fields = @$request_data['fields'];
		$limit = @$request_data['limit'];
        $fields = explode(",",$fields);
		
		if(!isset($limit) or $limit =='' or $limit == null ) { $limit = 25;}
		//$products_columns = @\Schema::getColumnListing('products');
		$users_columns = $fields;
		$main = array();
	    $columns_array = array();
		$data_array = array();
		$columns_final_array = array();
		
	    for($x=0;$x<sizeof($users_columns);$x++)
		{
		  $feature_settings_status = @\App\FeaturesSettings::where('title',$users_columns[$x])->first(['status'])->status;
		  if( $users_columns[$x] == 'base_discount' or $users_columns[$x] == 'base_price' or $users_columns[$x] == 'discount_expiry_date' or  $users_columns[$x] == 'stock_count' or  $users_columns[$x] == 'photo' or $users_columns[$x] == 'brand_ids' )
		  {
			  if($feature_settings_status == '1' or $feature_settings_status == 1 )
			  {
				 $columns_final_array[]=$users_columns[$x];
			  }
		  }
		  else
		  {
		   $columns_final_array[]=$users_columns[$x];
		  }
		}
		  
	   for($x=0;$x<sizeof($columns_final_array);$x++)
		{
		   $c1['title'] = $columns_final_array[$x];
		   $cols_array[] = $columns_final_array[$x];
		   $c1['visible'] = '1';
		   $c1['index'] = $x;
		   $columns_array[]=$c1;
		}
		
 
       $data_array = \App\Addresses::where('linked_id',$user_id);
 
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','ASC');
	   }
	  if ($date_filter == 'firstAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	 
	   }
	   
   /**
	   if(isset($request_data['category']) && $request_data['category'] != '' && $request_data['category'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$category."',products.category_ids)");
	   }
	**/   
 
	    if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		 $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("first_name"),'like', '%'.$d.'%');});
	   }
	   
	   $data_array = $data_array->paginate($limit,$cols_array);
       $main_data['columns'] = $columns_array;
	   $main_data['data'] = $data_array;
	   return $main_data;
    }
 

     // 2. =================== get ADD and EDIT forms for USERS META TABLE ====================	
 	public function user_address_forms($id = '')
    {
      $main_array = array();			
	  $main_array[] = $this->user_address_basic_form($id);
	  ///$main_array[] = $this->user_meta_form($id);
      return $main_array;
    }  
	
 		
   // 3. =================== Get Basic form for ADD and EDIT operations ====================		
  public function user_address_basic_form( $address_id = '')
  {
	  $main_array = array();
	   
	  $data_array1['title'] = 'User Address Form';
 	  $fields_array = array();
	  
	  //1 Address Title
	  $fields_keys1["title"] = "Address Title";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'address_title';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_title'])->address_title;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys1["value"] = $value;
      $fields_array[] = $fields_keys1;
	  
	  //2 Address Line 1
 	  $fields_keys2["title"] = "Address Line 1";
	  $fields_keys2["type"] = 'text';
      $fields_keys2["identifier"] = 'address_line1';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_line1'])->address_line1;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys2["value"] = $value;
      $fields_array[] = $fields_keys2;
		  
	  //3 Address Line 2
 	  $fields_keys3["title"] = "Address Line 2";
	  $fields_keys3["type"] = 'text';
      $fields_keys3["identifier"] = 'address_line2';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['address_line2'])->address_line2;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys3["value"] = $value;
      $fields_array[] = $fields_keys3;
		  
	  //4 contact_person_name
 	  $fields_keys4["title"] = "Contact Person Name";
	  $fields_keys4["type"] = 'text';
      $fields_keys4["identifier"] = 'contact_person_name';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['contact_person_name'])->contact_person_name;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys4["value"] = $value;
      $fields_array[] = $fields_keys4;
		  
	  //5 contact_person_mobile
 	  $fields_keys5["title"] = "Contact Person Mobile";
	  $fields_keys5["type"] = 'text';
      $fields_keys5["identifier"] = 'contact_person_mobile';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['contact_person_mobile'])->contact_person_mobile;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys5["value"] = $value;
      $fields_array[] = $fields_keys5;
	  
	  //6 city
 	  $fields_keys6["title"] = "City";
	  $fields_keys6["type"] = 'text';
      $fields_keys6["identifier"] = 'city';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['city'])->city;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys6["value"] = $value;
      $fields_array[] = $fields_keys6;
	  
	  
	 //7 state
 	  $fields_keys7["title"] = "State";
	  $fields_keys7["type"] = 'text';
      $fields_keys7["identifier"] = 'state';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['state'])->state;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys7["value"] = $value;
      $fields_array[] = $fields_keys7;
	  
	  	 //8 country
 	  $fields_keys8["title"] = "Country";
	  $fields_keys8["type"] = 'text';
      $fields_keys8["identifier"] = 'country';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['country'])->country;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys8["value"] = $value;
      $fields_array[] = $fields_keys8;
	  
	  
	  //9 pincode
 	  $fields_keys9["title"] = "Pincode";
	  $fields_keys9["type"] = 'text';
      $fields_keys9["identifier"] = 'pincode';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['pincode'])->pincode;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys9["value"] = $value;
      $fields_array[] = $fields_keys9;
	  
	  
	  	  //10 latitude
 	  $fields_keys10["title"] = "Latitude";
	  $fields_keys10["type"] = 'text';
      $fields_keys10["identifier"] = 'latitude';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['latitude'])->latitude;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys10["value"] = $value;
      $fields_array[] = $fields_keys10;
	  
	  	  //11 longitude
 	  $fields_keys11["title"] = "Longitude";
	  $fields_keys11["type"] = 'text';
      $fields_keys11["identifier"] = 'longitude';
	  $value = @\App\Addresses::where('id',@$address_id)->first(['longitude'])->longitude;
	  if($value == null or $value == '') { $value = '';}
	  $fields_keys11["value"] = $value;
      $fields_array[] = $fields_keys11;
	  
	 
	  $data_array1['fields'] = $fields_array;
      $main_array =$data_array1;
	  return $main_array;
	 
 }
 
 
 
 // 4. =================== get ADD and EDIT forms for USERS META TABLE ====================		
public function user_address_meta_form( $user_id = '')
{
	  $users_meta_types = \App\UsersMetaTypes::get(['id','title','identifier','count_limit','type']);
	  $fields_array2 = array();
	  foreach($users_meta_types as $umt)
	  {
		  $user_meta_type_id = $umt->id;
		  $value = @\App\UsersMetaValues::where('user_meta_type_id',$user_meta_type_id)->where('user_id',$user_id)->first(['value'])->value;
		  if($value == null or $value == '') { $value = '';}
		  $umt->value  = $value;
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $users_meta_types;
	  return $data_array2;
}


// 5. ================= Store User to the Database ============================
 public function user_address_store(Request $request)
    {
	      $validator = Validator::make($request->all(), [
		        
                 ]);
               if ($validator->errors()->all()) 
                {
 				}
                else
                {
				    $fields =$request->fields;
			 
			        $basic_fields = $request[0]['fields'];
			        $additional_info_fields = $request[1]['fields'];
					
	                $flight = new \App\Addresses;					 
  				 
                    for($t=0;$t<sizeof($basic_fields);$t++)
					{
						     $id = $basic_fields[$t]['identifier'];
							 
							 /**
							 if($id == 'email') 
							 {
								  $email_exist_count = \App\Addresses::where('email', $basic_fields[$t]['value'])->count();
								  if($email_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Email you entered already Exist';  
				                          return $data;
								  }
								 
							 }
							 if($id == 'mobile')
							 {
								  $mobile_exist_count = \App\Addresses::where('mobile', $basic_fields[$t]['value'])->count();
								  if($mobile_exist_count > 0)
								  {
									      $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Mobile you entered already Exist';  
				                          return $data;
								  }
							 }
							 **/
                       
							 $flight->$id = $basic_fields[$t]['value'];
						 
						 
                    }
				  $flight->save();
 
                 	if($flight !='')
					{
						
				       $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Address Added Successfully';
                       $data['user_data']      =   $this->user_address_basic_form($flight->id);
					}
					else
					{
					   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'Some Error Occurred';
                       $data['user_data']      =   [];  
					}
	            return $data;
				}
  }
   
 // 6 . ================================ Update User data to Database ===================================
	public function user_address_update(Request $request, $id)
    {
		 
		   //update basic info
		   $basic_info = $request[0];
		   $fields =  $basic_info['fields'];
		   
		   $additional_info_fields = $request[1];
		   $additional_info_fields =  $additional_info_fields['fields'];
	       
		   
		if(sizeof($fields) > 0)
		{			
		      $flight = \App\Addresses::find($id);	
			
               for($t=0;$t<sizeof($fields);$t++)
					{
					  $id = $fields[$t]['identifier'];
                      $flight->$id = $fields[$t]['value'];
				    }
			  $flight->save(); 
	     }
					
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Address Updated Successfully';
                       $data['user_data']      =   [];
					   return $data;
    }
    
	
	
 
 
 
 
 // 7 . ============================user_profile_basic_details for Profile Page====================================
 
 


 // 8 . ============================user_profile_basic_details for Profile Page====================================
 
	




 



	 
	       
		   
 

 
}
