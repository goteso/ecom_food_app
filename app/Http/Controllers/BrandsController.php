<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\ProductVariants;
use App\ProductVariantTypes;
use App\Brands;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class BrandsController extends Controller
{
    
    public function index()
    {
		 
		$display_modal_name = 'Brands List';
		$model_name = 'brands';
	    $modelName = "App\Brands";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
        return view('brands.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store_brand(Request $request)
    {
    $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = Brands::create($request->toArray()))  {
 
           flash('Brand has been created.');

        } else {
            flash()->error('Unable to create Brand.');
        }

        return back();
    }

 
    public function update_brand(Request $request, $id)
    {
 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = Brands::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Brand has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
 
		if( \App\Brands::findOrFail($id)->delete() ) {
            flash()->success('Brand has been deleted');
        } else {
            flash()->success('Brand not deleted');
        }

        return redirect()->back();
    }

 
}
