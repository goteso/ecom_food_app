<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Categories;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class CategoriesController extends Controller
{
    
    public function index(Request $request)
    {
		
	 
		//filters
		$request_data = @$request[0];
		
	 
		$parent_id = @$request_data['parent_id'];
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
	 
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Categories List';
		$model_name = 'categories';
	    $modelName = "App\Categories";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
		
		       $data_array = new \App\Categories;
 
	   
 
	   
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
		   
	 
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		   
	   }
	   
	   	 if(isset($request_data['parent_id']) && $request_data['parent_id'] != '' && $request_data['parent_id'] != null)
	   {
	     $data_array = $data_array->where('parent_id',$request_data['parent_id']);
	   }
	   
	   
	      if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->paginate(10000);
				
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					
					if(isset($_GET["request_type"]) && $_GET["request_type"] = 'ajax')
					{
						return $data;
					}
					$result = $data_array;
					return view('categories.index', compact('result','model_name','display_modal_name'));
					
    }
	
 
 
    public function store_category(Request $request)
    {
		
		 
		$flight = new \App\Categories;
        $flight->title =  $request->title;
		
		
		 









if ($request->hasFile('image')) {
    //

     @$file = @$request->file('image');
 
      //Move Uploaded File
      $destinationPath = 'categories';
      $file->move($destinationPath,@$file->getClientOriginalName());
  if(@$file->getClientOriginalName() !=''  && @$file->getClientOriginalName() != null)
	  {
		  @$user->photo =  @$file->getClientOriginalName();
	  }
 

}
 



















	  
		$flight->parent_id =  $request->parent_id;
		
		if($request->parent_id== '' or $request->parent_id == null or $request->parent_id ==' ')
		{
			$flight->parent_id =  ' ';
		}
		else
		{
			$flight->parent_id =  $request[0]['parent_id'];
		}
		
		$max_sort_index = @\App\Categories::max('sort_index');
		
		
		if($request->parent_id != null && $request->parent_id != '')
		{$flight->sort_index =  $max_sort_index + 1;
		}
		else{$flight->sort_index =  0;
		}
		
        $flight->save();

		
		           if($flight != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $flight ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return back();
    }
	
	
	public function edit($id)
    {
      $categories = \App\Categories::find($id);
	  return $categories;
		
	}

 
    public function update_category(Request $request,$category_id)
    {
	 
		// Get the user
        $user = Categories::findOrFail($category_id);
        // Update user
        $user->title =  $request->title;
		
	
if ($request->hasFile('image')) {
    //

     @$file = @$request->file('image');
 
      //Move Uploaded File
      $destinationPath = 'categories';
      $file->move($destinationPath,@$file->getClientOriginalName());

 
  if(@$file->getClientOriginalName() !=''  && @$file->getClientOriginalName() != null)
	  {
		  @$user->photo =  @$file->getClientOriginalName();
	  }
 

}
 	  
 
	  
	
		
		$user->parent_id =  $request->parent_id;
		
		
			$max_sort_index = @\App\Categories::max('sort_index');
		
		
		if($request->parent_id != null && $request->parent_id != '')
		{$user->sort_index =  $max_sort_index + 1;
		}
		else{$user->sort_index =  0;
		}
		
		
		
        $user->save();
		
		  return back();
        if($user != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $user ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
 
 
 
 
    public function destroy($id)
    {
        if( \App\Categories::findOrFail($id)->delete() ) {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Deleted Successfully';
                    $data['data'] = [] ;
        } else {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Error Occurred';
                    $data['data'] = [] ;
        }
return back();
        return $data;
    }

 
}
