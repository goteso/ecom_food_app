<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Categories;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;

class FaqsController extends Controller
{
    
    public function index(Request $request)
    {
		
	 
		//filters
		$request_data = @$request[0];
		
	 
		$parent_id = @$request_data['parent_id'];
		$search_text = @$request_data['search_text'];
		$date_filter = @$request_data['date_filter'];
		
	 
		$limit = @$request_data['limit'];
        //filters ends
		
		$display_modal_name = 'Categories List';
		$model_name = 'categories';
	    $modelName = "App\Categories";  
        $model = new $modelName();
		//$result = $model::latest()->paginate(25);
        
		
		       $data_array = new \App\Categories;
 
	   
 
	   
	   if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
		   
	 
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC'); 
 		   
	   }
	   
	   	 if(isset($request_data['parent_id']) && $request_data['parent_id'] != '' && $request_data['parent_id'] != null)
	   {
	     $data_array = $data_array->where('parent_id',$request_data['parent_id']);
	   }
	   
	   
	      if(isset($request_data['search_text']) && $request_data['search_text'] != '' && $request_data['search_text'] != null)
	   {
		   $d = $request_data['search_text'];
	     $data_array = $data_array->where(function($q) use ($d) {$q->orWhere( DB::raw("title"),'like', '%'.$d.'%');});
	   }
	   
	   
 
 
 $data_array = $data_array->paginate(10000);
				
		           if( sizeof($data_array) > 0)
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $data_array ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Categories Found';
					$data['data']        =   [];
                    }
					
					if(isset($_GET["request_type"]) && $_GET["request_type"] = 'ajax')
					{
						return $data;
					}
					$result = $data_array;
					return view('faqs.index', compact('result','model_name','display_modal_name'));
					
    }
	
 
 
    public function store_faq(Request $request)
    {
		
		 
		$flight = new \App\Faqs;
        $flight->question =  $request->question;
		$flight->answer =  $request->answer;
	 
        $flight->save();

		
		           if($flight != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $flight ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return back();
    }
	
	
	public function edit($id)
    {
      $faqs = \App\Faqs::find($id);
	  return $faqs;
		
	}

 
    public function update_faq(Request $request,$category_id)
    {
		
	 
		// Get the user
        $user = \App\Faqs::findOrFail($category_id);
        // Update user
        $user->question =  $request->question;
		$user->answer =  $request->answer;
	 
        $user->save();
		
		  return back();
        if($user != '')
		            {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Added Successfully';
                    $data['data'] = $user ;
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Unknown Error Occurred';
					$data['data']        =   [];
                    }
    
        return $data;
    }
 
 
 
 
    public function destroy($id)
    {
        if( \App\Faqs::findOrFail($id)->delete() ) {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Deleted Successfully';
                    $data['data'] = [] ;
        } else {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Error Occurred';
                    $data['data'] = [] ;
        }
return back();
        return $data;
    }

 
}
