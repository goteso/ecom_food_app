<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\FeaturesSettings;
use App\Permission;
use App\Designations;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use DB;
use App\Traits\feature; // <-- you'll need this line...

class FeaturesSettingsController extends Controller
{
    
  	use feature;  
    public function index()
    {
		//if(!Auth::user()->can('view_designations')){ return view('unauthorised');} 
		$display_modal_name = 'FeaturesSettings';
		$model_name = 'features_settings';
	    $modelName = "App\FeaturesSettings";  
        $model = new $modelName();
	 
 
	 
		//$types = $model::select('type')->distinct()->pluck("type")->toArray();
		 $types = $model::select('type')->distinct()->get();
		  
		return view('features_settings.index', compact('display_modal_name','model_name','types'));
    }
	
 
 
 

 
    public function update_features_settings(Request $request)
    {
		 
		 
		 
		
		//if(!Auth::user()->can('edit_designations')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		
		$settings = \App\FeaturesSettings::get();
		
		foreach($settings as $s)
		{
			 
		     $st = $s->title;
		    DB::table('features_settings')
            ->where('title', $st)
            ->update(['status' => $request[$st]]);
		 
		}
		// Get the user
     

        flash()->success('Features Settings has been updated.');

		$this->index();
        return back();
    }
 
 

 
}
