<?php
namespace App\Http\Controllers;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...
class OrdersController extends Controller {
    use feature;
	
	
//======================================================================================================================================		
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
	
	
//======================================================================================================================================	
    public function barcode() {return;
        \Storage::disk('public')->put('test.png', base64_decode(DNS2D::getBarcodePNG("4", "PDF417")));
    }
	
	
	
	
//======================================================================================================================================	
    public function get_category_products($category_id, $path, $parent_category_title, $category_title) {
        $sub_categories = \App\Categories::where('parent_id', $category_id)->pluck('id');
        for ($j = 0;$j < sizeof($sub_categories);$j++) {
            $category_title = @\App\Categories::where('id', $sub_categories[$j])->first(['title'])->title;
            $s_count = \App\Categories::where('parent_id', $sub_categories[$j])->count();
            if (intval($s_count) < 1) {
                $path.= "/" . $category_title;
                $s['category_id'] = $sub_categories[$j];
                $s['category_title'] = $category_title;
                $s['path'] = $path;
                $s['products'] = \App\Products::whereRaw("FIND_IN_SET(" . $sub_categories[$j] . ",products.category_ids)")->get();
                $sub_categories_array[] = $s;
            } else {
                $path.= "/" . $category_title;
                $s2['sub'] = $this->get_category_products($sub_categories[$j], $path, $parent_category_title, $category_title);
                $s2['category_id'] = $sub_categories[$j];
                $s2['category_title'] = $category_title;
                $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(" . $sub_categories[$j] . ",products.category_ids)")->get();
                $sub_categories_array[] = $s2;
            }
        }
        return $sub_categories_array;
    }
	
	
	
	
	//======================================================================================================================================	
    public function add_order_form(Request $request) {
		return;
        $main = array();
        $categories = array();
        $root_categories = \App\Categories::where('parent_id', '')->pluck('id');
        for ($i = 0;$i < sizeof($root_categories);$i++) {
            $path = '';
            $path2 = '';
            $parent_category_title = @\App\Categories::where('id', $root_categories[$i])->first(['title'])->title;
            $path = $parent_category_title;
            $path2 = $parent_category_title;
            $sub_categories = \App\Categories::where('parent_id', $root_categories[$i])->pluck('id');
            $sub_categories_array = array();
            for ($j = 0;$j < sizeof($sub_categories);$j++) {
                $category_title = @\App\Categories::where('id', $sub_categories[$j])->first(['title'])->title;
                $s_count = \App\Categories::where('parent_id', $sub_categories[$j])->count();
                if (intval($s_count) < 1) {
                    $path2.= "/" . $category_title;
                    $s['category_id'] = $sub_categories[$j];
                    $s['category_title'] = $category_title;
                    $s['path'] = $path2;
                    $s['products'] = \App\Products::whereRaw("FIND_IN_SET(" . $sub_categories[$j] . ",products.category_ids)")->get();
                    $sub_categories_array[] = $s;
                } else {
                    $path.= "/" . $category_title;
                    $s2['sub_categories'] = $this->get_category_products($sub_categories[$j], $path, $parent_category_title, $category_title);
                    $s2['category_id'] = $sub_categories[$j];
                    $s2['category_title'] = $category_title;
                    $s2['path'] = $path;
                    $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(" . $sub_categories[$j] . ",products.category_ids)")->get();
                    $sub_categories_array[] = $s2;
                }
            }
            $c['sub_categories'] = $sub_categories_array;
            $c['category_id'] = $root_categories[$i];
            $c['category_title'] = $parent_category_title;
            $categories[] = $c;
        }
        $data['categories_products'] = $categories;
        $data['order_meta_fields'] = $this->order_meta_form();
        $main = $data;
        return $main;
    }
	
	
	
	//======================================================================================================================================	
    public function order_meta_form($order_id = '') {
        $orders_meta_types = \App\OrdersMetaTypes::where('status', '1')->get(['id', 'title', 'identifier', 'type', 'field_options', 'field_options_model', 'field_options_model_columns', 'parent_identifier']);
        $fields_array2 = array();
        foreach ($orders_meta_types as $umt) {
            if ($umt->field_options_model != '') {
                $modelName = $umt->field_options_model;
                $model = new $modelName();
                $columns_array = explode(",", $umt->field_options_model_columns);
                $columns_data = array();
                $columns_data[] = $columns_array[0];
                $columns_data[] = $columns_array[1];
                $col0 = $columns_array[0];
                $col1 = $columns_array[1];
                $data_field_options = $model::get($columns_data);
                $t = array();
                foreach ($data_field_options as $fo) {
                    $d = '';
                    $d['title'] = $fo->$col0;
                    $d['value'] = $fo->$col1;
                    $t[] = $d;
                }
                $umt->field_options = $t;
                $user_meta_type_id = $umt->id;
                $value = @\App\OrdersMetaValues::where('order_meta_type_id', $user_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
                if ($value == null or $value == '') {
                    $value = '';
                }
                $umt->value = $value;
            } else if ($umt->field_options != '') {
                $user_meta_type_id = $umt->id;
                $value = @\App\OrdersMetaValues::where('order_meta_type_id', $user_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
                if ($value == null or $value == '') {
                    $value = '';
                }
                $umt->field_options = json_decode($umt->field_options);
                $umt->value = $value;
            } else {
                $user_meta_type_id = $umt->id;
                $value = @\App\OrdersMetaValues::where('order_meta_type_id', $user_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
                if ($value == null or $value == '') {
                    $value = '';
                }
                $umt->value = $value;
            }
        }
        $data_array2["title"] = "Additional Information";
        $data_array2["fields"] = $orders_meta_types;
        return $data_array2;
    }
	
	
	
 
//======================================================================================================================================	
    public function place_order(Request $request) {
        $order_basic = $request->order;
        $orders_payments = $request->orders_payments;
        $order_products = $request->order_products;
        $order_meta_fields = $request->order_meta_fields;
        $order_meta_fields = $order_meta_fields['fields'];
        //insert to 'orders' table
        $order = new \App\Orders;
        $order->order_status = $order_basic[0]['order_status'];
        $order->save();
        $order_id = $order->id;
        if (!isset($order_id) or $order_id == '' or $order_id == null) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Order Cannot be Placed';
            return $data;
        }
        //insert order payments
        $order_payments = new \App\OrdersPayments;
        $order_payments->sub_total = $orders_payments[0]['sub_total'];
        $order_payments->coupon_id = $orders_payments[0]['coupon_id'];
        $order_payments->coupon_code = $orders_payments[0]['coupon_code'];
        $order_payments->coupon_discount = $orders_payments[0]['coupon_discount'];
        $order_payments->total = $orders_payments[0]['total'];
        $order_payments->order_id = $order_id;
        $order_payments->save();
        $order_payments_id = $order_payments->id;
        //insert products and variants
        if (sizeof($order_products) > 0) {
            for ($t = 0;$t < sizeof($order_products);$t++) {
                $products = new \App\OrderProducts;
                $products->order_id = $order_id;
                $products->product_id = $order_products[$t]['id'];
                $products->title = $order_products[$t]['title'];
                $products->category_id = $order_products[$t]['category_ids'];
                $products->discount = $order_products[$t]['base_discount'];
                $products->discounted_price = $order_products[$t]['discounted_price'];
                $products->unit = @$order_products[$t]['units'] . "";
                $products->quantity = $order_products[$t]['quantity'];
                $products->save();
                $order_products_variants = @$order_products[$t]['variants'];
                if (sizeof($order_products_variants) > 0) {
                    for ($y = 0;$y < sizeof($order_products_variants);$y++) {
                        $variants = new \App\OrderProductsVariants;
                        $variants->order_product_id = $products->id;
                        $variants->title = $order_products_variants[$y]['title'];
                        $variants->value = $order_products_variants[$y]['value'];
                        $variants->product_variant_id = $order_products_variants[$y]['product_variant_id'];
                        $variants->save();
                    }
                }
            }
        }
        //insert to 'orders_meta_value' table
        if (sizeof($order_meta_fields) > 0) {
            for ($i = 0;$i < sizeof($order_meta_fields);$i++) {
                $inner_array = $order_meta_fields[$i];
                for ($j = 0;$j < sizeof($inner_array);$j++) {
                    if ($inner_array[$j]["type"] == 'api') {
                        $da = $inner_array[$j]['value'];
                        $da = json_decode($da);
                        $field_options_model = $inner_array[$j]['field_options_model'];
                        $order_meta1 = new $field_options_model();
                        foreach ($da as $key => $value) {
                            if ($key != 'id' && $key != '$$hashKey') {
                                $order_meta1->$key = $value;
                            }
                        }
                        $order_meta1->save();
                        $identifier = $inner_array[$j]['identifier'];
                        $order_meta = new \App\OrdersMetaValues;
                        $order_meta->order_id = $order_id;
                        $order_meta->order_meta_type_id = $inner_array[$j]['id'];
                        $order_meta->value = $order_meta1->id;
                        $order_meta->save();
                    } else {
                        $identifier = $inner_array[$j]['identifier'];
                        $order_meta = new \App\OrdersMetaValues;
                        $order_meta->order_id = $order_id;
                        $order_meta->order_meta_type_id = $inner_array[$j]['id'];
                        $order_meta->value = $inner_array[$j]['value'];
                        $order_meta->save();
                    }
                }
            }
        }
        if (isset($order_id) && $order_id != '' && $order_id != null) {
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'Order Placed Successfully';
            return $data;
        } else {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Order Cannot be Placed';
            return $data;
        }
    }
	
	
	
	
	
	
	
//======================================================================================================================================	
    public function orders_list(Request $request) {
        $orders2 = \App\Orders::orderBy("id", "desc")->paginate(1000);
        $orders = array();
        foreach ($orders2 as $or) {
            $order_meta_type_id = \App\OrdersMetaTypes::where("identifier", "customer_id")->first(['id'])->id;
            $customer_id = \App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('order_id', $or->id)->first(['value'])->value;
            $order = array();
            $or->order_details = \App\Orders::where('id', $or->id)->get();
            $or->customer_details = \App\User::where('id', $customer_id)->get();
 
        }
        if (count($orders2) > 0) {
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'Orders Fetched Successfullydddddddddddddd';
            $data['user_data'] = $orders2;
            return $data;
        } else {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'No Order Not Found';
            $data['user_data'] = [];
            return $data;
        }
    }
	
	
	
	
//======================================================================================================================================	
    public function order_details(Request $request) {
		
		 
        $order = array();
        $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier", "customer_id")->first(['id'])->id;
        $customer_id = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('order_id', $request->order_id)->first(['value'])->value;
        $d_array = array();
        $block_array = array();
        //products
        $order_products = @\App\OrderProducts::where('order_id', $request->order_id)->get();
        $sub_total = 0;
        foreach ($order_products as $op) {
            $sub_total = $sub_total + floatval($op['discounted_price'] * $op['quantity']);
            $variants = @\App\OrderProductsVariants::where('order_product_id', $op['id'])->get();
            $var_desc = '';
            foreach ($variants as $va) {
                $var_desc.= $va->title . ' : ' . $va->value . "\n";
            }
            $op['variants_desc'] = $var_desc;
            // $op['variants'] =  @\App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
            
        }
        $d5['title'] = count($order_products) . " Items";
        $d5['total'] = $sub_total;
        $d5['data'] = @$order_products;
        $order['items'] = $d5;
        //$order['items'] = $d2;
        $items['type'] = 'items';
        $items['data'] = $d5;
        $block_array[] = $items;
        //customer_details starts
        $customer_details = @\App\User::where('id', $customer_id)->get();
        @$orders_count = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('value', $customer_id)->count();
        @$order_ids = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('value', $customer_id)->pluck('order_id');
        $total_revenues = 0;
        for ($y = 0;$y < sizeof($order_ids);$y++) {
            $total_revenues+= @\App\OrdersPayments::where('order_id', $order_ids[$y])->sum('total');
        }
        $total_revenues = $total_revenues;
        @$customer_details[0]['title'] = @$customer_details[0]['first_name'] . " " . @$customer_details[0]['last_name'];
        $joined = @$customer_details[0]['created_at'];
        $currency_symbol = @\App\Settings::where('key_title', 'currency_symbol')->first(['key_value'])->key_value;
        $customer_data['description'] = @$customer_details[0]['email'] . "\n" . "Joined : " . $joined . "\nTotal Order : " . $orders_count . "\nTotal Orders Amount : " . $currency_symbol . $total_revenues;
        $d1['type'] = 'user';
        $customer_data["name"] = @$customer_details[0]["first_name"] . " " . @$customer_details[0]["last_name"];
        $customer_data["id"] = @$customer_details[0]["id"];
        $customer_data["title"] = "Customer Details";
        $customer_data_array = array();
        $customer_data_array[] = @$customer_data;
        $d1['data'] = $customer_data_array;
        $block_array[] = $d1;
        // delivery_address
        $delivery_address_meta_type_id = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['id'])->id;
        $delivery_address_meta_type_columns = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['columns'])->columns;
        $delivery_address_meta_type_type = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['type'])->type;
        $delivery_address_meta_type_model = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['field_options_model'])->field_options_model;
        $delivery_address_id = @\App\OrdersMetaValues::where("order_meta_type_id", $delivery_address_meta_type_id)->where('order_id', $request->order_id)->first(['value'])->value;
        $delivery_address_columns = explode(",", @$delivery_address_meta_type_columns);
        $delivery_address = $delivery_address_meta_type_model::where('id', $delivery_address_id)->get($delivery_address_columns);
        $d2['title'] = 'Delivery Address';
        $d2['data'] = $delivery_address;
        $order['delivery_address'] = $d2;
        $delivery_address2['type'] = 'delivery_address';
        $delivery_address2['data'] = $d2;
        $block_array[] = $delivery_address2;
        //payment_details
        $payment_data = @\App\Orders::where('id', @$request->order_id);
        $payment_components = array();
        $d4['title'] = 'Payment Summary';
        $d4['order_total'] = $payment_data->first(['total'])->total;
        $get_attributes = array();
        $get_attributes[] = 'sub_total';
        //Sub Total Concept
        $sub_total_component['title'] = 'Sub Total';
        $sub_total_component['value'] = round($payment_data->first(['sub_total'])->sub_total, 2);
        $payment_components[] = $sub_total_component;
        $coupon_discount_component['title'] = 'Discount';
        $coupon_discount_component['value'] = round($payment_data->first(['coupon_discount'])->coupon_discount, 2);
        $payment_components[] = $coupon_discount_component;
		
		
		/**
        $c = @\App\FeaturesSettings::where('title', 'tax')->where('status', '1')->count();
        if ($c > 0) {
            $get_attributes[] = 'tax';
            //Tax Concept
            $tax['title'] = 'tax';
            $tax['value'] = $payment_data->first(['tax'])->tax;
            $payment_components[] = $tax;
        }
		
		**/
        $c = @\App\FeaturesSettings::where('title', 'shipping')->where('status', '1')->count();
        if ($c > 0) {
            $get_attributes[] = 'shipping';
            //shipping Concept
            $shipping['title'] = 'Delivery';
            $shipping['value'] = $payment_data->first(['shipping'])->shipping;
            $payment_components[] = $shipping;
			 
			$express_shipping['title'] = 'Express Delivery';
            $express_shipping['value'] = $payment_data->first(['express_shipping'])->express_shipping;
            $payment_components[] = $express_shipping;
			
			
			
			
 
			
			
			
			
        }
        $t = json_encode($get_attributes);
        $d4['data'] = $payment_data->get($get_attributes);
        // $order['payment_details'] = $d4;
        $order_transactions = \App\OrdersTransactions::where('order_id', @$request->order_id)->get();
        $paid_sum = $order_transactions->sum('amount');
        $d4['total_paid'] = $paid_sum;
        $order_tax_transactions = @\App\OrderTaxTransactions::where('order_id', @$request->order_id)->get();
        $tax_string = '';
        foreach ($order_tax_transactions as $ot) {
            $tax_string.= @\App\Tax::where('id', $ot->tax_id)->first(['title'])->title . " : " . $ot->amount . "\n";
            //shipping Concept
            $tax['title'] = @\App\Tax::where('id', $ot->tax_id)->first(['title'])->title;
            $tax['value'] = round($ot->amount, 2);
            $payment_components[] = $tax;
        }
        $tax_title = '';
        $d4["data"]["0"]["tax"] = $tax_string;
        $d4['data'] = $payment_components;
        //$d4["data"]["0"]['order_transactions'] = $order_transactions ;
        $order_tax_transactions = \App\OrderTaxTransactions::where('order_id', @$request->order_id)->get();
        //$d4["data"]["0"]['order_tax_transactions'] = $order_tax_transactions ;
        $payment_details['type'] = 'payment_details';
        $payment_details['data'] = $d4;
        $block_array[] = $payment_details;
        //=========== meta values ==========
        $order_meta_values = @\App\OrdersMetaValues::where('order_id', @$request->order_id)->get();
        foreach ($order_meta_values as $om) {
            //  $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
            
        }
        $d6['title'] = 'Order Summary';
        $order_meta_values_array = array();
        $order_meta_types = \App\OrdersMetaTypes::where('important', '0')->where('type', '<>', 'quick_link')->get();
        foreach ($order_meta_types as $omt) {
            $order_meta_type_id = $omt->id;
            //$v['order_meta_type_id'] = $omt->id;
            //$v['title'] = $omt->title;
            $v['display_title'] = $omt->display_title;
            //$v['type'] = $omt->type;
            $value = @\App\OrdersMetaValues::where('order_id', @$request->order_id)->where('order_meta_type_id', @$omt->id)->first(['value'])->value;
            if ($omt->type == 'api') {
                $model_name = $omt->field_options_model;
                $columns = explode(",", $omt->columns);
                $v['value'] = $model_name::where('id', $value)->get($columns);
            } else if ($omt->type == 'dateTimePicker') {
                //diffForHumans();
                $d = \App\OrdersMetaValues::where('order_id', @$request->order_id)->where('order_meta_type_id', @$omt->id)->first(['value'])->value;
                $date = \Carbon\Carbon::parse($d);
                $v['value'] = $date->format('d M Y | h:i A');
            } else {
                $v['value'] = @\App\OrdersMetaValues::where('order_id', @$request->order_id)->where('order_meta_type_id', @$omt->id)->first(['value'])->value;
            }
            $order_meta_values_array[] = $v;
        }
        $meta_data['type'] = 'order_summary';
        $meta_data['data']['title'] = 'Order Summary';
        $meta_data['data']['data'] = $order_meta_values_array;
        $block_array[] = $meta_data;
        $quick_l['type'] = 'quickActions_links';
        $quick_l['data']['title'] = 'Quick Links';
        $quick_l['data']['data'] = $this->order_quick_actions($request, 'links');
        $block_array[] = $quick_l;
        ///return $this->order_quick_actions( $request , 'misc');
        $qa["type"] = 'quickActions_misc';
        $qa["data"] = $this->order_quick_actions($request, 'misc');
        $d_array[] = $qa;
        $quick_a['type'] = 'quickActions_misc';
        $quick_a['data']['title'] = 'Quick Actions';
        $quick_a['data']['data'] = $this->order_quick_actions($request, 'misc');
        $block_array[] = $quick_a;
        $d_blocks["order_number"] = $request->order_id;
        $d_blocks["order_date"] = @\App\Orders::where('id', $request->order_id)->first(['created_at'])->created_at->format('d M , Y h:i A');
        $d_blocks["blocks"] = $block_array;
        $d_blocks["order_status"] = $this->get_order_status_list($request);
        return $d_blocks;
    }
	
	
	
	//======================================================================================================================================	
    public function order_quick_actions(Request $request, $t) {
        $order_id = $request->order_id;
        //payment summary
        $d6["title"] = "paymentSummary";
        $d6["total"] = @\App\OrdersPayments::where('order_id', $order_id)->first(['total'])->total;
        $d6_data_array = array();
        $d6_d1["title"] = "Subtotal";
        $d6_d1["value"] = @\App\OrdersPayments::where('order_id', $order_id)->first(['sub_total'])->sub_total;
        $d6_data_array[] = $d6_d1;
        $d6_d2["title"] = "Tax";
        $d6_d2["value"] = "NaN";
        $d6_data_array[] = $d6_d2;
        $d6_d3["title"] = "Discount";
        $d6_d3["value"] = @\App\OrdersPayments::where('order_id', $order_id)->first(['coupon_discount'])->coupon_discount;
        $d6_data_array[] = $d6_d3;
        $d6["data"] = $d6_data_array;
        $blocks_array[] = $d6;
        $d1["blocks"] = $blocks_array;
        $d7_array = array();
        $d7_d1["links"] = @\App\OrdersQuickActionsLinks::get();
        $misc = @\App\OrdersMetaTypes::where('type', 'quick_link')->get();
        $m_array = array();
        foreach ($misc as $m) {
            $type = $m->identifier;
            if ($type == 'orderStatus') {
                $m->value = @\App\Orders::where('id', $order_id)->first(["order_status"])->order_status;
            }
			
			 
            if ($type == 'driver_id') {
                $order_meta_type_id = \App\OrdersMetaTypes::where("identifier", "driver_id")->first(['id'])->id;
                $vvv = @\App\OrdersMetaValues::where('order_meta_type_id', $order_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
                $user_details = \App\User::where('id', $vvv)->get();
                $m->value = $user_details;
            }
            if ($type == 'vendor_id') {
                $order_meta_type_id = \App\OrdersMetaTypes::where("identifier", "vendor_id")->first(['id'])->id;
                $vvv = @\App\OrdersMetaValues::where('order_meta_type_id', $order_meta_type_id)->where('order_id', $order_id)->first(['value'])->value;
                $user_details = \App\User::where('id', $vvv)->get();
                $m->value = $user_details;
            }
            if ($type == 'orderPaymentReceived') {
                $order_transactions = \App\OrdersTransactions::where('order_id', $order_id)->get();
                $paid_sum = $order_transactions->sum('amount');
                // $c = @\App\OrdersTransactions::where('order_id',$order_id)->first(['total'])->total;
                // $total = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
                if ($paid_sum > 0) {
                    $m->value = "Rs. " . $paid_sum;
                } else {
                    $m->value = "Not Received";
                }
            }
        }
        if (isset($request->user_id) && $request->user_id != '' && $request->user_id != null) {
            $user = @\App\User::where('id', $request->user_id)->first();
        } else {
            $user = Auth::user();
        }
        $d7_d2["misc"] = $misc;
        if ($t == 'misc') {
            $misc_array = array();
            foreach ($misc as $mi) {
                if ($user->can($mi->identifier)) {
                    $mi->title = $mi->title;
                    $mi->buttonTitle = $mi->display_title;
                    $mi->identifier = $mi->identifier;
                    $mi->value = $mi->value;
                    $misc_array[] = $mi;
                } else {
                }
            }
            return $misc_array;
        }
        if ($t == 'links') {
            $links_array = array();
            $links_data = @\App\OrdersQuickActionsLinks::get();
            foreach ($links_data as $li) {
                if($user->can($li->type)) {
                    @$links_array[] = @$li;
                } else {
                }
            }
            return $links_array;
        }
        $d7_array[] = $d7_d1;
        $d7_array[] = $d7_d2;
        return $d7_array;
        $d1["quickActions"] = $d7_array;
        return $d1;
    }
 
	
	
	
	//======================================================================================================================================	
    public function order_invoice(Request $request,$type='') {
		
 
        $invoice_main_array = array();
        $order = array();
        $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier", "customer_id")->first(['id'])->id;
        $customer_id = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('order_id', $request->order_id)->first(['value'])->value;
        $d_array = array();
        $block_array = array();
        //products
        $order_products = @\App\OrderProducts::where('order_id', $request->order_id)->get();
        $sub_total = 0;
        foreach ($order_products as $op) {
            $sub_total = $sub_total + floatval($op['discounted_price'] * $op['quantity']);
			
            $variants = @\App\OrderProductsVariants::where('order_product_id', $op['id'])->get();
            $var_desc = '';
		 
            foreach ($variants as $va) {
				
				if($va->title == 'Type')
				{
                $var_desc.= $va->title . ' : ' . $va->value . "\n";
				}
            }
            $op['variants_desc'] = $var_desc;
            // $op['variants'] =  @\App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
            
        }
        $d5['title'] = count($order_products) . " Items";
        $d5['total'] = $sub_total;
        $d5['data'] = @$order_products;
        $invoice_data['products'] = $d5;
		
		
		
        //customer_details starts
        $customer_details = @\App\User::where('id', $customer_id)->get();
        @$orders_count = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('value', $customer_id)->count();
        @$order_ids = @\App\OrdersMetaValues::where("order_meta_type_id", $order_meta_type_id)->where('value', $customer_id)->pluck('order_id');
        $total_revenues = 0;
        for ($y = 0;$y < sizeof($order_ids);$y++) {
            $total_revenues+= @\App\OrdersPayments::where('order_id', $order_ids[$y])->sum('total');
        }
        $total_revenues = $total_revenues;
        @$customer_details[0]['title'] = @$customer_details[0]['first_name'] . " " . @$customer_details[0]['last_name'];
        $joined = @$customer_details[0]['created_at'];
        $currency_symbol = @\App\Settings::where('key_title', 'currency_symbol')->first(['key_value'])->key_value;
        $customer_data['name'] = @$customer_details[0]['first_name'] . " " . @$customer_details[0]['last_name'];
        $customer_data['email'] = @$customer_details[0]['email'];
        $customer_data['mobile'] = @$customer_details[0]['mobile'];
        $delivery_address_meta_type_id = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['id'])->id;
        $delivery_address_meta_type_columns = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['columns'])->columns;
        $delivery_address_meta_type_type = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['type'])->type;
        $delivery_address_meta_type_model = @\App\OrdersMetaTypes::where("identifier", "delivery_address")->first(['field_options_model'])->field_options_model;
        $delivery_address_id = @\App\OrdersMetaValues::where("order_meta_type_id", $delivery_address_meta_type_id)->where('order_id', $request->order_id)->first(['value'])->value;
        $delivery_address_columns = explode(",", @$delivery_address_meta_type_columns);
        $delivery_address = $delivery_address_meta_type_model::where('id', $delivery_address_id)->first($delivery_address_columns);
        $customer_data["name"] = @$customer_details[0]["first_name"] . " " . @$customer_details[0]["last_name"];
        $customer_data["id"] = @$customer_details[0]["id"];
        $customer_data["address"] = $delivery_address;
        $customer_data_array = array();
        $customer_data_array[] = @$customer_data;
        $d1['data'] = $customer_data_array;
        $invoice_data['customer_details'] = $d1;
        //get vendor details
        $app_name = @\App\Settings::where('key_title', 'app_name')->first(['key_value'])->key_value;
        $currency_symbol = env('CURRENCY_SYMBOL');
        $app_email = @\App\Settings::where('key_title', 'app_email')->first(['key_value'])->key_value;
        $app_phone = @\App\Settings::where('key_title', 'app_phone')->first(['key_value'])->key_value;
        $app_logo = @\App\Settings::where('key_title', 'app_logo')->first(['key_value'])->key_value;
        $app_address = @\App\Settings::where('key_title', 'app_address')->first(['key_value'])->key_value;
        $invoice_data["vendor_details"] = $app_name . "\n" . $app_email . "\n" . $app_phone . "\n" . $app_address;
        //extra data
        $date_now = \Carbon\Carbon::now();
        $invoice_data["date"] = $date_now->format('d/m/Y') . "";
        $invoice_data["app_logo"] = $app_logo;
        $invoice_data["order_id"] = $request->order_id;
        $order_details = @\App\Orders::where('id', $request->order_id)->get();
        $invoice_data['order_details'] = $order_details;
		
		
		$sub_total = $order_details[0]['sub_total'];
		$shipping = $order_details[0]['shipping'];
		$express_shipping = $order_details[0]['express_shipping'];
		$coupon_discount = $order_details[0]['coupon_discount'];
		
		
		
		$total1 = $sub_total - $coupon_discount;
		
		
		$total2 = $total1 + $shipping +  $express_shipping;
		
		$total_tax = 0;
        $order_tax_transactions = @\App\OrderTaxTransactions::where('order_id', $request->order_id)->get(['id', 'tax_id', 'amount']);
        foreach ($order_tax_transactions as $transaction) {
            $transaction->amount = round($transaction->amount, 2);
            $transaction->tax_title = @\App\Tax::where('id', $transaction->tax_id)->first(['title'])->title;
			
			$total_tax = $total_tax + $transaction->amount; 
        }
		$total3 = $total2 + $total_tax;
		
		$invoice_data["total1"] = $total1;
		$invoice_data["total2"] = $total2;
		$invoice_data["total3"] = $total3;
		 
        $invoice_data["order_tax_transactions"] = $order_tax_transactions;
        $invoice_data['footer'] = '<h4>Tool by | <a href="' . env('APP_URL') . '" target="_blank">' . env('APP_NAME') . '</a> </h4>';
		
		
		
		 
        if($type =='a4')
		 {
			   return view('orders.invoice', compact('invoice_data'));
		 }
		 else{
			  return view('orders.invoice_thermal', compact('invoice_data'));
		 }
    }
	
	
	
	//======================================================================================================================================	
    public function get_order_status_list(Request $request) {
        if (isset($_GET["request_type"]) && $_GET["request_type"] == 'api') {
            $role_id = @\App\User::where('id', $request->user_id)->first(['user_type'])->user_type;
            if ($role_id == '3') {
                $current_order_status = @\App\Orders::where('id', $request->order_id)->first(['order_status'])->order_status;
                $assigned_status = \App\orderActionsAuthSettings::where('user_type', 'Driver')->first(['assigned_status'])->assigned_status;
                $actions_sequence_order = \App\orderActionsAuthSettings::where('user_type', 'Driver')->first(['actions_sequence_order'])->actions_sequence_order;
                $sa = array();
                $assigned_status_arr = explode(",", $assigned_status);
                $order_status = \App\OrdersStatus::get();
                for ($x = 0;$x < sizeof($assigned_status_arr);$x++) {
                    if ($actions_sequence_order == 0 or $actions_sequence_order == '0') {
                        $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                    }
                    if ($actions_sequence_order == 1 or $actions_sequence_order == '1') {
                        if ($current_order_status == $assigned_status_arr[$x]) {
                            $sa = array();
                            continue;
                        }
                        $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                    }
                    if ($actions_sequence_order == 2 or $actions_sequence_order == '2') {
                        if ($current_order_status == $assigned_status_arr[$x]) {
                            $sa = array();
                            continue;
                        }
                        if (sizeof($sa) < 1) {
                            $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                        }
                    }
                }
                return $sa;
            } else {
                $s = @\App\OrdersStatus::get();
                return $s;
            }
        } else {
            $login_id = \Auth::id();
            $role_id = @Auth::user()->user_type;
            if ($role_id == '3') {
                $assigned_status = \App\orderActionsAuthSettings::where('user_type', 'Driver')->first(['assigned_status'])->assigned_status;
                $sa = array();
                $assigned_status_arr = explode(",", $assigned_status);
                $order_status = \App\OrdersStatus::get();
                for ($x = 0;$x < sizeof($assigned_status_arr);$x++) {
                    if ($actions_sequence_order == 0 or $actions_sequence_order == '0') {
                        $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                    }
                    if ($actions_sequence_order == 1 or $actions_sequence_order == '1') {
                        if ($current_order_status == $assigned_status_arr[$x]) {
                            $sa = array();
                            continue;
                        }
                        $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                    }
                    if ($actions_sequence_order == 2 or $actions_sequence_order == '2') {
                        if ($current_order_status == $assigned_status_arr[$x]) {
                            $sa = array();
                            continue;
                        }
                        if (sizeof($sa) < 1) {
                            $sa[] = @\App\OrdersStatus::where('title', $assigned_status_arr[$x])->first();
                        }
                    }
                }
                return $sa;
            } else {
                $s = @\App\OrdersStatus::get();
                return $s;
            }
        }
    }
	
	
//======================================================================================================================================	
    public function apply_discount(Request $request) {
        @\App\Orders::where('id', $request->order_id)->update(['coupon_discount' => $request->discount]);
        $this->get_order_subtotal($request, $request->order_id);
        return 1;
    }
	
	
	
	
	
	
	
	    public function apply_shipping(Request $request) {
        @\App\Orders::where('id', $request->order_id)->update(['shipping' => $request->shipping]);
        $this->get_order_subtotal($request, $request->order_id);
        return 1;
    }
	
	
		    public function apply_express_shipping(Request $request) {
				 
        @\App\Orders::where('id', $request->order_id)->update(['express_shipping' => $request->express_shipping]);
        $this->get_order_subtotal($request, $request->order_id);
        return 1;
    }
	



//======================================================================================================================================	
    ///// Important functions for calculating totals , subtotal, etc for particulr order
    function get_order_subtotal(Request $request, $order_id = '') {
        //calculate subtotal of added products from 'order_products' table by loop over table for particular table and 'discounted_price * quantity' and final sum = sub_total
        $order_products = @\App\OrderProducts::where('order_id', $order_id)->get();
        $order_products_subtotal = 0;
        foreach ($order_products as $op) {
            $order_products_subtotal = $order_products_subtotal + (floatval($op->discounted_price) * floatval($op->quantity));
        }
        //update subtotal to orders table
        if (floatval($order_products_subtotal) < 0) {
            $order_products_subtotal == 0;
        }
        @\App\Orders::where('id', $order_id)->update(['sub_total' => $order_products_subtotal]);
        //get shipping and coupon discount from orders table
        $shipping = @\App\Orders::where('id', $order_id)->first(['shipping'])->shipping;
		$express_shipping = @\App\Orders::where('id', $order_id)->first(['express_shipping'])->express_shipping;
		$order_products_subtotal = $order_products_subtotal + floatval($shipping);
		$order_products_subtotal = $order_products_subtotal + floatval($express_shipping);
		
        $coupon_discount = @\App\Orders::where('id', $order_id)->first(['coupon_discount'])->coupon_discount;
		$order_products_subtotal = $order_products_subtotal - floatval($coupon_discount);
		
		
        //add taxes to order_products_subtotal
        $taxes = @\App\Tax::get();
        $total_tax = 0;
        foreach ($taxes as $tax) {
            $tax_amount = (floatval($tax->percentage) / 100) * floatval($order_products_subtotal);
            $total_tax = $total_tax + $tax_amount;
            //insert or update transactions in order_tax_transactions_table
            $order_tax_transactions = @\App\OrderTaxTransactions::firstOrNew(array('order_id' => $order_id, 'tax_id' => $tax->id));
            $order_tax_transactions->amount = round($tax_amount, 2);
            $order_tax_transactions->save();
        }
        
        
        $order_total = round($total_tax, 2) + $order_products_subtotal;
        //update subtotal to orders table
        if (floatval($total_tax) < 0) {
            $total_tax == 0;
        }
        @\App\Orders::where('id', $order_id)->update(['tax' => round($total_tax, 2) ]);
        if (floatval($order_total) < 0) {
            $order_total == 0;
        }
        @\App\Orders::where('id', $order_id)->update(['total' => $order_total]);
        if (floatval($order_total) < 0) {
            @\App\Orders::where('id', $order_id)->update(['coupon_discount' => 0, 'total' => 0]);
        }
    }
}
