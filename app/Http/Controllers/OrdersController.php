<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
 
use Session;
 
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...

class OrdersController extends Controller
{
   use feature;   
 
 
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    
	
	
	// 1 ==================================================== show Order Form =====================================================================
	
	public function get_category_products($category_id,$path,$parent_category_title,$category_title)
	{
	       $sub_categories = \App\Categories::where('parent_id',$category_id)->pluck('id');
		
		   for($j=0;$j<sizeof($sub_categories);$j++)
		   {
			   $category_title = @\App\Categories::where('id',$sub_categories[$j])->first(['title'])->title; 
			   $s_count = \App\Categories::where('parent_id',$sub_categories[$j])->count();
			   if(intval($s_count) < 1)
			   {
				   $path .= "/".$category_title;
				   $s['category_id'] = $sub_categories[$j];
				   $s['category_title'] = $category_title;
				   $s['path'] = $path;
			       $s['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
				   $sub_categories_array[]= $s;
		 
			   }
			   else
			   {
				     $path .= "/".$category_title;
				     $s2['sub'] =$this->get_category_products($sub_categories[$j],$path,$parent_category_title,$category_title);
					 $s2['category_id'] = $sub_categories[$j];
					 $s2['category_title'] = $category_title;
                     $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
					 
			         $sub_categories_array[]= $s2;
				    
			   }
		   }
		   
		   return $sub_categories_array;
	}
	
	
	public function add_order_form(Request $request)
	{
		
		return 'sdsd';
		$main = array();
		$categories = array();
		$root_categories = \App\Categories::where('parent_id','')->pluck('id');
 
		
	 
		for($i=0;$i<sizeof($root_categories);$i++)
		{
			$path ='';
			$path2 ='';
		  
		   $parent_category_title = @\App\Categories::where('id',$root_categories[$i])->first(['title'])->title;
		   $path = $parent_category_title;
		   $path2 = $parent_category_title;
           $sub_categories = \App\Categories::where('parent_id',$root_categories[$i])->pluck('id');
		   
		   
				 
		  $sub_categories_array = array();
		   	for($j=0;$j<sizeof($sub_categories);$j++)
		   {
               
			 
			   $category_title = @\App\Categories::where('id',$sub_categories[$j])->first(['title'])->title;
			   
			   $s_count = \App\Categories::where('parent_id',$sub_categories[$j])->count();
			   
			   if(intval($s_count) < 1)
			   {
				   $path2 .= "/".$category_title;
				   $s['category_id'] = $sub_categories[$j];
				   $s['category_title'] = $category_title;
				   $s['path'] = $path2;
			       $s['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
				   $sub_categories_array[]= $s;
		 
			   }
			   else
			   {
				     $path .= "/".$category_title;
				     $s2['sub_categories'] =$this->get_category_products($sub_categories[$j],$path,$parent_category_title,$category_title);
					 $s2['category_id'] = $sub_categories[$j];
					 $s2['category_title'] = $category_title;
					 $s2['path'] = $path;
                     $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
					 $sub_categories_array[]= $s2;
					
			   }
		   }
		   
		   $c['sub_categories'] = $sub_categories_array; 
		   $c['category_id'] = $root_categories[$i]; 
		   $c['category_title'] = $parent_category_title; 
		   $categories[] = $c;
		}
		$data['categories_products'] = $categories;
		$data['order_meta_fields'] = $this->order_meta_form();
		$main = $data;
		return $main;
	}
	
	
	public function order_meta_form( $order_id = '')
{
	  $orders_meta_types = \App\OrdersMetaTypes::where('status','1')->get(['id','title','identifier','type','field_options','field_options_model','field_options_model_columns','parent_identifier']);
	  $fields_array2 = array();
	  foreach($orders_meta_types as $umt)
	  {		  
		  if($umt->field_options_model != '')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             $d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			$user_meta_type_id = $umt->id;  
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $orders_meta_types;
	  return $data_array2;
}


 

// 2. =============================================================== Place Order Form ========================================================================
	
	public function place_order(Request $request)
  {
	    $order_basic = $request->order;
		$orders_payments = $request->orders_payments;
		$order_products = $request->order_products;
		$order_meta_fields = $request->order_meta_fields;
	    $order_meta_fields = $order_meta_fields['fields'];
	   
	  //insert to 'orders' table
        $order = new \App\Orders;
        $order->order_status = $order_basic[0]['order_status'];
	    $order->save();
		$order_id = $order->id;
		 
	 
		
		if(!isset($order_id) or $order_id == '' or $order_id == null)
		{
			                              $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Order Cannot be Placed';  
				                          return $data;
		}
		
		//insert order payments
		$order_payments = new \App\OrdersPayments;
        $order_payments->sub_total = $orders_payments[0]['sub_total'];
		$order_payments->coupon_id = $orders_payments[0]['coupon_id'];
		$order_payments->coupon_code = $orders_payments[0]['coupon_code'];
		$order_payments->coupon_discount = $orders_payments[0]['coupon_discount'];
		$order_payments->total = $orders_payments[0]['total'];
		$order_payments->order_id = $order_id;
        $order_payments->save();
		$order_payments_id = $order_payments->id;
		
		//insert products and variants
	 
		if( sizeof($order_products) > 0)
		{
			for($t=0;$t<sizeof($order_products);$t++)
			{
		        $products = new \App\OrderProducts;
				$products->order_id = $order_id;
				$products->product_id = $order_products[$t]['product_id'];
				$products->title =$order_products[$t]['title'];
				$products->category_id =$order_products[$t]['category_id'];
				$products->discount =$order_products[$t]['discount'];
				$products->discounted_price =$order_products[$t]['discounted_price'];
				$products->unit = $order_products[$t]['units'];
				$products->quantity = $order_products[$t]['quantity'];
				$products->save();
				 
				$order_products_variants = $order_products[$t]['variants'];
				if( sizeof($order_products_variants) > 0)
						{
							for($y=0;$y<sizeof($order_products_variants);$y++)
								{
									    $variants = new \App\OrderProductsVariants;
										$variants->order_product_id = $products->id;
										$variants->title = $order_products_variants[$y]['title'];
										$variants->value =$order_products_variants[$y]['value'];
										$variants->product_variant_id =$order_products_variants[$y]['product_variant_id'];
                                        $variants->save();
									
								}
						}
			}
			
		}
		 //insert to 'orders_meta_value' table
		  
		 if(sizeof($order_meta_fields) > 0 )
		 {
		    for($i=0;$i<sizeof($order_meta_fields);$i++)
			{
				$identifier = $order_meta_fields[$i]['identifier'];
				$order_meta = new \App\OrdersMetaValues;
				$order_meta->order_id = $order_id;
				$order_meta->order_meta_type_id = $order_meta_fields[$i]['id'];
				$order_meta->value = $order_meta_fields[$i]['value'];
				$order_meta->save();
		    }		
		 }
		 
 
		 if(isset($order_id) && $order_id != '' && $order_id != null)
		{
						                  $data['status_code']    =   1;
                                          $data['status_text']    =   'Success';             
                                          $data['message']        =  'Order Placed Successfully';  
				                          return $data;
			
		}
		else
		{
						                  $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Order Cannot be Placed';  
				                          return $data;
			
		}
		
	
  }

 
 
 //4 order list =======================================================================
 
  	public function orders_list(Request $request)
     {
		 
		 
          
		  $orders2 = \App\Orders::orderBy("id","desc")->paginate(1);
		 
		 
		 
		 $orders = array();
		 
		 foreach($orders2 as $or)
		 {
			 
			 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		     $customer_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$or->id)->first(['value'])->value;
			 
			 $order= array();
			 $or->order_details = \App\Orders::where('id',$or->id)->get();
			 $or->customer_details = \App\User::where('id',$customer_id)->get();
			 
			 /**
			 $order_products= \App\OrderProducts::where('order_id',$or->id)->get();
			 foreach($order_products as $op)
			 {
			   $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			 }
			 $order['order_products'] = $order_products;
			 
			  
			 $order['order_payments'] = \App\OrdersPayments::where('order_id',$or->id)->get();
			 **/
			 //meta values
			 
			 /**
			  $order_meta_values = \App\OrdersMetaValues::where('order_id',$or->id)->get();
			  foreach($order_meta_values as $om)
			  {
				    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			  }
			  $order['order_meta_values'] = $order_meta_values;
			  **/
			  
			 
		 }
		 
		 
	 
		  
		 if(count($orders2) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['user_data']      =     $orders2 ;
					   return $data;
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'No Order Not Found';
                       $data['user_data']      =   [];
					   return $data;
		 }
	 }
	 
	 
 // 3. ======================================= order_details ===================================================
 
  public function order_details(Request $request)
  {
	  
 
 
 
	    $order= array(); 
	  
	    $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		$customer_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$request->order_id)->first(['value'])->value;
		 
	  
			 
		    //products
			$order_products= \App\OrderProducts::where('order_id',$request->order_id)->get();
			foreach($order_products as $op)
			{
			  $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			$d5['title'] = count($order_products)." Items";
			$d5['data'] = $order_products;
			$order['items'] = $d5;
			 
			 
		   //customer_details starts 
			$customer_details = @\App\User::where('id',$customer_id)->get();
			$customer_details[0]['orders_count'] =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
			$order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->pluck('order_id');
			$total_revenues = 0;
			for($y=0;$y<sizeof($order_ids);$y++)
			{
				$total_revenues += \App\OrdersPayments::where('order_id',$order_ids[$y])->sum('total');
			}
			$customer_details[0]['total_revenues']= $total_revenues;
		 
			$d1['title'] = 'Customer Details';
			$d1['data'] = $customer_details;
			$order['customer_details'] = $d1;
		 
		 
			 
			// delivery_address	
		    $delivery_address_meta_type_id = \App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
		    $delivery_address =\App\OrdersMetaValues::where("order_meta_type_id",$delivery_address_meta_type_id)->where('order_id',$request->order_id)->first(['value'])->value;
	        $d2['title'] = 'Delivery Address';
		    $d2['data'] = $delivery_address;
		    $order['delivery_address'] = $d2;
			 
			 
		    //payment_details
		    $d4['title'] ='Payment Summary';
		    $d4['data'] = \App\OrdersPayments::where('id',$request->order_id)->get();
		    $order['payment_details'] = $d4;
		 
			 
			//meta values
			$order_meta_values = \App\OrdersMetaValues::where('order_id',$request->order_id)->get();
			foreach($order_meta_values as $om)
			 {
			    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			 }
			$d6['title'] = 'Order Summary';
			$d6['data'] = $order_meta_values;
			 
			$order['order_meta_values'] = $d6;
			return $order;
	  
  }
  
  
  
  
  // order_quick_actions ===================================================================
  
  public function order_quick_actions(Request $request)
  {
	  $order_id = $request->order_id;
	  
	  $d1["orderNumber"] = $order_id;
	  $created_at = \App\Orders::where('id',$order_id)->first(['created_at'])->created_at->format('d M, Y h:i A')."";
	  $d1["orderDate"] = $created_at;
	  
	  $blocks_array = array();
	  
	  $b1["type"] = "items";
	  $b1["total"] = "";
 
	  
	  
	   //products
	   $order_products= \App\OrderProducts::where('order_id',$order_id)->get();
	   $products_data_array = array();
	   foreach($order_products as $op)
		{
			
		  $product_id = $op->product_id;
		  
		  $b1_d1["img"] = @\App\Products::where("id",$product_id)->first(['photo'])->photo;
		  $b1_d1["product_id"] = $product_id;
		  
		  $b1_d1["title"] = @\App\Products::where("id",$product_id)->first(['title'])->title;
		  $b1_d1["desc"] = @\App\Products::where("id",$product_id)->first(['title'])->title;
		  
		  $id3 = @\App\ProductMetaTypes::where("identifier","unit")->first(['id'])->id;
		  
	 
	      $units = @\App\ProductMetaValues::where("product_meta_type_id",$id3)->where('product_id',$product_id)->first(['value'])->value;
	  
		  $b1_d1["units"] =$units;
		  $b1_d1["total"] ="";
		  
		  $products_data_array[] = $b1_d1;
		  
		  
		}
			$b2['type'] = 'items';
			$b2['total'] =  \App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
			$b2['data'] = $products_data_array;
	 
	 
	 $blocks_array[] = $b2;
	 
	 
	 
	 
	 //customer_details
	  $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	  $customer_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
	  $total_orders =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
	 
	  $d3["type"] = "user";
	  $d3["title"] = "Customer Information";
	  $d3["id"] = $customer_id;
	  $d3["name"] = @\App\User::where("id",$customer_id)->first(['first_name'])->first_name." ".@\App\User::where("id",$customer_id)->first(['last_name'])->last_name;
	  $email = @\App\User::where("id",$customer_id)->first(['email'])->email;
	  $joined = @\App\User::where("id",$customer_id)->first(['created_at'])->created_at->format('d M,Y')."";
	 
	  $d3["desc"] = $email." \n Joined: ".$joined." \n Total orders: ".$total_orders;
	 $blocks_array[] = $d3;
	 
	 
	 //address
	  $order_meta_type_id2 = \App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
	  $address_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id2)->where('order_id',$order_id)->first(['value'])->value;
	  
	  $address_details = \App\Addresses::where('id',$address_id)->first();
	  
	   $d4["type"] = "address";
	   $d4["id"] = $address_id;
	   $d4["desc"] = $address_details->address_title."\n ".$address_details->address_line1."\n".$address_details->address_line2."\n".$address_details->city." ".$address_details->state." ".$address_details->country." ".$address_details->pincode;
       $blocks_array[] = $d4;
 
 
     //order summary
	 
	 $d5["type"] = "orderSummary";
	 $order_summary_data_array = array();
	 
	  $id1 = \App\OrdersMetaTypes::where("identifier","pickup_time")->first(['id'])->id;
	  $pickup_time =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	  $id2 = \App\OrdersMetaTypes::where("identifier","delivery_time")->first(['id'])->id;
	  $delivery_time =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	  $id3 = \App\OrdersMetaTypes::where("identifier","notes")->first(['id'])->id;
	  $notes =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	 
	 $summary_d1["title"] = "Pickup Time";
	 $summary_d1["value"] = $pickup_time;
	 $order_summary_data_array[] = $summary_d1;
	 $summary_d2["title"] = "Delivery Time";
	 $summary_d2["value"] = $delivery_time;
	 $order_summary_data_array[] = $summary_d2;
	 $summary_d3["title"] = "Notes";
	 $summary_d3["value"] = $notes;
	 $order_summary_data_array[] = $summary_d3;
	 $d5["data"] = $order_summary_data_array;
	 $blocks_array[] = $d5;
	 
	 //payment summary
	 
	 $d6["title"] = "paymentSummary";
	 $d6["total"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 $d6_data_array = array();
	 
	 $d6_d1["title"] = "Subtotal";
	 $d6_d1["value"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['sub_total'])->sub_total;
	 $d6_data_array[]=$d6_d1;
	 
	 $d6_d2["title"] = "Tax";
	 $d6_d2["value"] = "NaN";
	 $d6_data_array[]=$d6_d2;
	 
	 $d6_d3["title"] = "Discount";
	 $d6_d3["value"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['coupon_discount'])->coupon_discount;
	 $d6_data_array[]=$d6_d3;
	 
	 $d6["data"] = $d6_data_array;
	 $blocks_array[] = $d6;
	 
	 $d1["blocks"] = $blocks_array;
	 
	 $d7_array = array();
	 
	 $d7_d1["links"] = @\App\OrdersQuickActionsLinks::get();
	 
	 $misc = @\App\OrdersQuickActionsMisc::get();
	 
	 
	 foreach($misc as $m)
	 {
		 $type = $m->type;
		 if($type == 'orderStatus')
		 {
			 $m->value = @\App\Orders::where('id',$order_id)->first(["order_status"])->order_status;
		 }
		 
		  if($type == 'orderPaymentReceived')
		 {
			 $c = @\App\OrdersPayments::where('order_id',$order_id)->count();
			 $total = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
			 
	 
			 if($c > 0)
			 {
				 $m->value = "Rs. ".$total." ( Rs 0 left)";
			 }
			 else
			 {
				$m->value ="Not Received";
			 }
			 
		 }
	 }
	 $d7_d2["misc"] = $misc;
	 
	 $d7_array[] = $d7_d1;
	 $d7_array[] = $d7_d2;
	 
	 $d1["quickActions"] = $d7_array;
	 return $d1;
	 
	 
	 
	  
  }
}
