<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\ProductVariants;
use App\ProductVariantTypes;
use App\Categories;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class ProductVariantTypesController extends Controller
{
    
    public function index()
    {
		$display_modal_name = 'Product Variant Types';
		$model_name = 'product_variant_types';
	    $modelName = "App\ProductVariantTypes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
        return view('product_variant_types.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
    $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = ProductVariantTypes::create($request->toArray()))  {
 
           flash('Variant Type has been created.');

        } else {
            flash()->error('Unable to create Variant Type.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = ProductVariantTypes::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Variant Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
 
		if( \App\ProductVariantTypes::findOrFail($id)->delete() ) {
            flash()->success('Variant Type has been deleted');
        } else {
            flash()->success('Variant Type not deleted');
        }

        return redirect()->back();
    }

 
}
