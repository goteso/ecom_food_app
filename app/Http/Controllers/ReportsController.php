<?php
namespace App\Http\Controllers;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
use Session;
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...

class ReportsController extends Controller
{
     use feature;   
     public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    

 
   // 1. =================== Get all users ====================	
   public function reports_sales(Request $request)
    {
	$main_array = array();
	
	//$date_from = $request->date_from;
	//$date_to = $request->date_to;
	
	$d['title'] =   env("APP_NAME", "");
	$d['subTitle'] ='Orders Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "0";
	$column_array[] = $c2;
	
	$c3["title"] = "Description";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Amount";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
    $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
   // $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
  //  $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	/**
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	**/ 
	
 
  //  $order_data = @\App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $order_data = @\App\Orders::get();
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
 
	 $user_name = @\App\User::where('id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$user_id)->first(['last_name'])->last_name;
	  
	 $total = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 
 
     $o_array[] = $od->created_at;
	 
	 $o_array[0] = $o_array[0];
 	 $o_array[] = $user_name;
	 $o_array[] = 'Payment of #'.$od->id;
	 $o_array[] = env("CURRENCY_SYMBOL", "").$total;
	 $s= $o_array[0];
	 $o_array[0] = $s."";
	 
	 $rows_array[] = $o_array; 
	  
 }
 
 $d1['header'] = 'Sales';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = \Carbon\Carbon::now()->format('d, M Y | h:i A');

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
    // 1. =================== Get all users ====================	
   public function reports_sales_by_customer(Request $request)
    {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] ='Goteso';
	$d['subTitle'] ='General Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
	$c3["title"] = "Description";
	$c3["index"] = "2";
	$c3["visible"] = "1";
	$column_array[] = $c3;
	
	$c4["title"] = "Amount";
	$c4["index"] = "3";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
	$orders[] = \App\Orders::where('created_at','like', '%'.$previous_month.'%')->count();
    $orders[] = \App\Orders::where('created_at','like', '%'.$current_month.'%')->count();
	 
	 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	 
	
 
    $order_data = \App\Orders::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($order_data as $od)
 {
	 $o_array =array();
	 $order_id = $od->id;
	 $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	 $user_id = \App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(["value"])->value;
	 
	 
	 
	 if($user_id = $request->user_id)
	 {
		$user_name = \App\User::where('id',$user_id)->first(['first_name'])->first_name." ".\App\User::where('id',$user_id)->first(['last_name'])->last_name;
		$total = \App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 
 
		$o_array[] = $od->created_at;
	 
		$o_array[0] = $o_array[0];
		$o_array[] = $user_name;
		$o_array[] = 'Payment of #'.$od->id;
		$o_array[] = 'Rs '.$total;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
	 }
	  
 }
 
 $d1['header'] = 'Sales in November';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = "Friday, 23 March, 2018 01:28 PM GMT+05:30";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
     // 1. =================== Get all users ====================	
   public function reports_all_customers(Request $request)
    {
	$main_array = array();
	
	$date_from = $request->date_from;
	$date_to = $request->date_to;
	
	$d['title'] ='Goteso';
	$d['subTitle'] ='Users Joined Report';
	
	$column_array = array();
	
	$c1["title"] = "Date";
	$c1["index"] = "0";
	$c1["visible"] = "1";
	$column_array[] = $c1;
	
	$c2["title"] = "Name";
	$c2["index"] = "1";
	$c2["visible"] = "1";
	$column_array[] = $c2;
	
 
	
	$c4["title"] = "Email";
	$c4["index"] = "2";
	$c4["visible"] = "1";
	$column_array[] = $c4;
	
	$c5["title"] = "Phone";
	$c5["index"] = "3";
	$c5["visible"] = "1";
	$column_array[] = $c5;
	
    $data_array = array();

	$rows_array = array();
	
	
	$no_of_days_current_month = cal_days_in_month(CAL_GREGORIAN, date("m"), date("y"));
	$current_month = date("Y-m");     
    $previous_month = date("Y-m",strtotime("-1 month"));  
    $months[] =  date('F', mktime(0, 0, 0, date("m",strtotime("-1 month")), 10));
    $months[] =  date('F', mktime(0, 0, 0, date("m"), 10));
	
 
    $start = \Carbon\Carbon::parse($request->date_from)->startOfDay();  //2016-09-29 00:00:00.000000
    $end =  \Carbon\Carbon::parse($request->date_to)->endOfDay(); //2016-09-29 23:59:59.000000
	
	
 
	
	$date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	$date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	 
	
 
    $user_data = \App\User::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
 $rows_array = array();
 foreach($user_data as $od)
 {
	 
 
	 $o_array =array();
	 $order_id = $od->id;
 
	 
 
		$o_array[] = $od->created_at;
	 
		$o_array[0] = $o_array[0];
		$o_array[] = $od->first_name." ".$od->last_name;
		$o_array[] = $od->email;
		$o_array[] = $od->mobile;
		$s= $o_array[0];
		$o_array[0] = $s."";
	 
		$rows_array[] = $o_array; 
	 
	  
 }
 
 $d1['header'] = 'Users Registered';
 $d1['rows'] = $rows_array;
	$data_array[] = $d1;
	
	
	$d["columns"] = $column_array;
	$d["data"] = $data_array;
	$d["notes"] = "";
	$d["footer"] = "Friday, 23 March, 2018 01:28 PM GMT+05:30";

	 $main = array();
 	$main[] = $d;
	 return $main;
	 

 }
	
 
 
 
 
 
 
 
 //get reports list
 
 
function get_reports_list()
{
//order,cancelled_orders    --------  customers,customer-by most sales ----------transactions------------- Sales---------


$main = array();

$d1["title"] = "Sales";
$d1["identifier"] = "Sales";
$d1["url"] = "sales_report_page";
$d1["description"] = "This sales report will give you the idea of sales with respect to monthly revenue";
$main[] = $d1;



$d2["title"] = "Orders";
$d2["identifier"] = "Orders";
$d2["url"] = "orders_report_page";
$d2["description"] = "This orders report will give you the idea of orders with respect to monthly revenue";
$main[] = $d2;

$d3["title"] = "Cancelled Orders";
$d3["identifier"] = "Orders";
$d3["url"] = "cancelled_orders_report_page";
$d3["description"] = "This orders report will give you the idea of cancelled orders with respect to monthly revenue";
$main[] = $d3;

 
$d4["title"] = "Transactions";
$d4["identifier"] = "Transactions";
$d4["url"] = "transactions_report_page";
$d4["description"] = "This orders report will give you the idea of transactions with respect to monthly revenue";
$main[] = $d4;





$d5["title"] = "Customers by Most Sales";
$d5["identifier"] = "Customers";
$d5["url"] = "customers_most_sales_report_page";
$d5["description"] = "This customers report will give you the idea of customers with most orders delivered";
$main[] = $d5;



$d6["title"] = "Customers";
$d6["identifier"] = "Customers";
$d6["url"] = "customers_report_page";
$d6["description"] = "This customers report will give you the idea of cancelled customers joined on monthly basis";
$main[] = $d6;



return $main;

}
 

 
 
 

 
}
