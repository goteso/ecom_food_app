<?php
namespace App\Libraries;
class General {
// function to get gravatar photo/image from gravatar.com using email id.
  public static function getGravatarURL($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
  {
    $url = 'http://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    if ($img)
    {
        $url = '<img src="' . $url . '"';
        foreach ($atts as $key => $val)
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
  }
}