<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationTriggers extends Model
{
	
protected $fillable = ['trigger_type', 'email_type', 'email_body', 'email_blade', 'subject', 'title', 'sub_title', 'sms_text', 'notification_type', 'user_type'];
protected $table = 'notification_triggers';
}
