<?php
namespace App;
use DB;
class OnOff {
// function to get gravatar photo/image from gravatar.com using email id.
public static function feature($title){
    $status = DB::table('features_settings')
            ->where('title', $title)->first(["status"])->status;
	return $status;
}
}