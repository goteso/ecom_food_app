<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrderActionsAuthSettings extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assigned_status','user_type','actions_sequence_order' 
    ];
	protected $table = 'order_actions_auth_settings';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
