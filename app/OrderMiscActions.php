<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrderMiscActions extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action_type','user_type','preorder_status' 
    ];
	protected $table = 'order_misc_actions';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
