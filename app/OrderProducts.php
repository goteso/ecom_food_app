<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
              'order_id','product_id','quantity','title','category_id','discount','discounted_price','unit'
    ];
	protected $table = 'order_products';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
