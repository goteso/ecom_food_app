<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderProductsVariants extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
              'order_product_id', 'title', 'value', 'product_variant_id'
    ];
	protected $table = 'order_products_variants';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
