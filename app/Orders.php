<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class Orders extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'order_status', 'sub_total', 'coupon_id', 'coupon_code', 'coupon_discount', 'total', 'shipping', 'tax'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 

	
}
