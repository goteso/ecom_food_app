<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OrdersMetaTypes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
              'title','identifier','slots_days_difference','status','important','columns','count_limit', 'type','display_title','parent_identifier','field_options','field_options_model','field_options_model_columns','required_or_not','show_rule'
    ];
	protected $table ='orders_meta_types';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
