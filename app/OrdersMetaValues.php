<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OrdersMetaValues extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','order_meta_type_id','linked_id','value'
    ];
	protected $table = 'orders_meta_values';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
