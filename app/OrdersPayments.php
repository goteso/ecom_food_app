<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OrdersPayments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
              'order_id','sub_total','coupon_id','coupon_code','coupon_discount','total','shipping','tax','payment_gateway'
    ];
	protected $table = 'orders_payments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
