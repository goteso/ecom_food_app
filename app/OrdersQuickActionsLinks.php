<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrdersQuickActionsLinks extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','detail','buttonTitle','type'
    ];
	protected $table = 'orders_quick_actions_links';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
