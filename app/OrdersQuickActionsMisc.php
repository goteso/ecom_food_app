<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrdersQuickActionsMisc extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','buttonTitle','type'
    ];
	protected $table = 'orders_quick_actions_misc';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
