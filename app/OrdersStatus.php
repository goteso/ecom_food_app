<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrdersStatus extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','color','label_colors','identifier','type','customer_notify'
    ];
	protected $table = 'order_status';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
