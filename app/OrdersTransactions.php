<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class OrdersTransactions extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','amount','payment_gateway' 
    ];
	protected $table = 'order_transactions';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
