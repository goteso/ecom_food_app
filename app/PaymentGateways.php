<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentGateways extends Model
{
	
	
    protected $fillable = ['title'];
    protected $table = 'payment_gateways';
  public function credentials()
    {
        return $this->belongsTo(Credentials::class);
    }
}
