<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModes extends Model
{
	
	
    protected $fillable = ['title'];

  public function credentials()
    {
        return $this->belongsTo(Credentials::class);
    }
}
