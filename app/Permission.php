<?php

namespace App;

class Permission extends \Spatie\Permission\Models\Permission
{

    public static function defaultPermissions()
    {
        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_posts',
            'add_posts',
            'edit_posts',
            'delete_posts',
			
			'export_user',
			'delete_contact_types',
			'add_contact_types',
			'edit_contact_types',
			'export_contact_types',
			
			'add_contact',
			'view_contact',
			'edit_contact',
			'delete_contact',
			'export_contact'
        ];
    }
}
