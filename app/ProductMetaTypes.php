<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMetaTypes extends Model
{
	
protected $fillable = ['title','identifier','limit','type'];
 
}
