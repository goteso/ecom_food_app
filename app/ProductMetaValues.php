<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMetaValues extends Model
{
	
protected $fillable = ['product_meta_type_id','product_id','value'];
 
}
