<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariantTypes extends Model
{
	
protected $fillable = ['title','minimum_selection_needed','maximum_selection_needed','price_difference_status','price_status','photo_status','variant_stock_count_status'];
 
}
