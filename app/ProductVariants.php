<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariants extends Model
{
	 protected $fillable = ['product_variant_type_id','title','stock_count','product_id','price_difference','price','photo'];
     protected $table = 'product_variants';
}
