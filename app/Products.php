<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Http\Controllers\Admin\ProductsController;
  


class Products extends Model
{
  protected $fillable = ['title','category_ids','base_price','base_discount','discount_expiry_date','stock_count','stock_count_type','photo','status','brand_ids'];
  protected $table = 'products';
  
  
  
  	
	
function getVariantsExistAttribute()
{  
     $product_id = $this->attributes['id'];
     $product_variants_types = \App\ProductVariantTypes::get();
	 $product_variants_types_array = array();
	 $c=0;
	 foreach($product_variants_types as $pv)
	 {
	     $variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$product_id)->get();
		 if(count($variants) < 1)
		 { }
		 else
		 {
		   $pv->variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$product_id)->get();
		    $product_variants_types_array[] =$pv;
		 }
     }	
     if(sizeof($product_variants_types_array) > 0)
	 {
		 return 1; 	 
	 }
	 else
	 {
		 return 0;		      
	 }
 	 
}


 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
	
	  public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }
	
	
	
}
