<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;
class ServiceAreas extends Authenticatable
{
	
	use Billable;
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'area_title','latitude', 'longitude','pincode'
    ];

	protected $table = 'service_areas';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
