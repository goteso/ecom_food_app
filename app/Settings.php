<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
	 protected $fillable = ['key_title','key_display_title','key_value'];
     protected $table = 'settings';
}

 