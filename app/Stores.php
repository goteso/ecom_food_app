<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class Stores extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','store_name','address1','address2','city','state','country','pincode','latitude','longitude','vendor_id'
    ];
	protected $table = 'stores';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
