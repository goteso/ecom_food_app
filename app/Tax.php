<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Http\Controllers\Admin\ProductsController;
  


class Tax extends Model
{
  protected $fillable = ['title','percentage'];
  protected $table = 'tax';
  
  
  
  	
	
function getVariantsExistAttribute()
{  
     $product_id = $this->attributes['id'];
     $product_variants_types = \App\ProductVariantTypes::get();
	 $product_variants_types_array = array();
	 $c=0;
	 foreach($product_variants_types as $pv)
	 {
	     $variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$product_id)->get();
		 if(count($variants) < 1)
		 { }
		 else
		 {
		   $pv->variants = \App\ProductVariants::where('product_variant_type_id',$pv->id)->where('product_id',$product_id)->get();
		    $product_variants_types_array[] =$pv;
		 }
     }	
     if(sizeof($product_variants_types_array) > 0)
	 {
		 return 1; 	 
	 }
	 else
	 {
		 return 0;		      
	 }
 	 
}


 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}
