<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;
class UserTypes extends Authenticatable
{
	
	    use Billable;
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','display_title', 'title'
    ];

	protected $table = 'user_type';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 

 
 
	
}
