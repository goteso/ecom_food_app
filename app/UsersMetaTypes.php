<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class UsersMetaTypes extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','identifier', 'count_limit', 'type','field_options','field_options_model','field_options_model_columns'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
