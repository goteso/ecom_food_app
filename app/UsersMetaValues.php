<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class UsersMetaValues extends Model
{
	
	 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_meta_type_id','user_id', 'value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
 
 	
	
	function getMetaTypeTitleAttribute()
{
  return @\App\UsersMetaTypes::where('id',$this->attributes['user_meta_type_id'])->first(['title'])->title;
}


 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
 
	
}
