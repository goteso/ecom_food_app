<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSessions extends Model
{
	
protected $fillable = [ 'user_id', 'device_type', 'device_id', 'screen_size', 'device_os', 'session_id', 'ip_address', 'session_status', 'location_name', 'latitude', 'longitude', 'browser', 'timezone', 'notification_token', 'last_login', 'refresh_token','access_token'];
protected $table = 'users_sessions';
}
