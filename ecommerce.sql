-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2018 at 11:50 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(50) NOT NULL,
  `address_type` varchar(50) NOT NULL,
  `linked_id` varchar(50) NOT NULL,
  `address_title` varchar(200) DEFAULT NULL,
  `address_line1` varchar(500) DEFAULT NULL,
  `address_line2` varchar(500) DEFAULT NULL,
  `contact_person_name` varchar(50) DEFAULT NULL,
  `contact_person_mobile` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `address_type`, `linked_id`, `address_title`, `address_line1`, `address_line2`, `contact_person_name`, `contact_person_mobile`, `city`, `state`, `country`, `pincode`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(2, 'customer', '33', 'gggg bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '67.567', '6767.787', '2018-04-23 15:02:43', '2018-04-23 15:02:43'),
(3, 'customer', '1', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-04-05 10:01:45', '2018-04-05 09:52:00'),
(4, 'customer', '1', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-04-05 10:01:46', '2018-04-05 09:52:01'),
(5, '', '1', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-04-05 10:01:36', '2018-04-05 09:52:01'),
(6, '', '', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-04-05 09:52:02', '2018-04-05 09:52:02'),
(7, '', '', 'sdsd', 'sdsd', '', '', '', '', '', '', '', '', '', '2018-05-02 08:14:16', '2018-05-02 08:14:16'),
(8, '', '', 'h', 'fgh', 'f', 'f', 'ty', 'y', 'dff', 'y', 'tr', 'yty', 'ry', '2018-04-17 05:53:35', '2018-04-17 05:53:35'),
(9, '', '', 'h', 'fgh', 'f', 'f', 'ty', 'y', 'dff', 'y', 'tr', 'yty', 'ry', '2018-04-17 05:53:36', '2018-04-17 05:53:36'),
(10, '', '', 'h', 'fgh', 'f', 'f', 'ty', 'y', 'dff', 'y', 'tr', 'yty', 'ry', '2018-04-17 05:53:36', '2018-04-17 05:53:36'),
(11, '', '', 'h', 'fgh', 'f', 'f', 'ty', 'y', 'dff', 'y', 'tr', 'yty', 'ry', '2018-04-17 05:53:36', '2018-04-17 05:53:36'),
(12, '', '', 'h', 'fgh', 'f', 'f', 'ty', 'y', 'dff', 'y', 'tr', 'yty', 'ry', '2018-04-17 05:53:37', '2018-04-17 05:53:37'),
(13, '', '', 'ju', 'gtyju', 'uy', 't', 'tu', 'ty', 'yu', 't', 'ty', 't', 'yut', '2018-04-17 05:54:34', '2018-04-17 05:54:34'),
(14, 'customer', '343', 'g', 'juyg', 'yg', 'dfdfdf', 'fdfdfdffd', 'fy', 'yt', 't', 'y', 't', 'yt', '2018-04-17 05:56:39', '2018-04-17 05:56:39'),
(15, 'customer', '28', 'my address', 'GOTESO', 'tyy', 'y', 't', 'ty', 'u', 'ty', 'tyu', 'ty', 'yut', '2018-04-17 06:26:08', '2018-04-17 06:06:01'),
(16, 'customer', '30', 'dfdfddfdfdf', 'uyt', 'yut', 'dfdfdfdf', 'ty', 'tu', 'uyt', 't', 'uyt', 'yut', 'uy', '2018-04-17 06:07:44', '2018-04-17 06:07:44'),
(17, 'customer', '28', '', '', '', '', '', '', '', '', '', '', '', '2018-04-18 10:51:32', '2018-04-18 10:51:32'),
(18, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:45:28', '2018-04-23 09:45:28'),
(19, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:46:17', '2018-04-23 09:46:17'),
(20, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:47:47', '2018-04-23 09:47:47'),
(21, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:50:07', '2018-04-23 09:50:07'),
(22, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:50:42', '2018-04-23 09:50:42'),
(23, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:51:22', '2018-04-23 09:51:22'),
(24, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:51:39', '2018-04-23 09:51:39'),
(25, 'home', '67', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-05-01 12:32:48', '2018-04-23 09:51:46'),
(26, 'home', '', 'dfgfdg', 'gdfgdfg', 'bbg', 'fdgghh', '43543543546', 'Banur', 'Punjab', 'India', '140892', '30.6435', '76.8176', '2018-04-23 09:54:11', '2018-04-23 09:54:11'),
(27, 'customer', '[object HTMLInputElement]', 'fdgdg', 'fdgdf', 'fdgfd', 'fgfdg', 'fgf', 'fgf', 'gfg', 'fgf', 'gff', 'gf', 'g', '2018-04-23 15:24:20', '2018-04-23 15:24:20'),
(28, 'customer', '[object HTMLInputElement]', 'fdgdg', 'fdgdf', 'fdgfd', 'fgfdg', 'fgf', 'fgf', 'gfg', 'fgf', 'gff', 'gf', 'g', '2018-04-23 15:24:21', '2018-04-23 15:24:21'),
(29, 'customer', '[object HTMLInputElement]', 'fdgdg', 'fdgdf', 'fdgfd', 'fgfdg', 'fgf', 'fgf', 'gfg', 'fgf', 'gff', 'gf', 'g', '2018-04-23 15:24:21', '2018-04-23 15:24:21'),
(30, 'customer', '[object HTMLInputElement]', 'fdg', 'fdg', 'fdgfd', 'fdg', 'fdg', 'fdg', 'fdg', 'fdg', 'fdg', 'fdg', 'fgdfg', '2018-04-23 15:29:24', '2018-04-23 15:29:24'),
(31, 'customer', '33', 'ffd', 'fgg', 'fdg', 'fg', 'fdgfd', 'fdg', 'fgdg', 'fdg', 'fdg', 'fdg', 'fdg', '2018-04-23 15:31:05', '2018-04-23 15:31:05'),
(32, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-25 03:54:35'),
(33, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-25 03:55:53'),
(34, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-25 04:00:55'),
(35, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-25 04:01:40'),
(36, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-25 05:55:20'),
(37, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:02:14'),
(38, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:03:24'),
(39, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:03:56'),
(40, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:04:11'),
(41, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:04:21'),
(42, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:05:47'),
(43, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 06:06:08'),
(44, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 12:15:21'),
(45, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 13:34:43'),
(46, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 13:49:04'),
(47, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '', '', '2018-04-05 10:01:36', '2018-04-25 13:54:32'),
(48, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 13:58:48'),
(49, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 13:59:16'),
(50, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 14:00:17'),
(51, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 14:03:06'),
(52, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 14:40:45'),
(53, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-25 14:55:58'),
(54, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 04:13:47'),
(55, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-26 04:18:10'),
(56, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-26 04:19:07'),
(57, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 04:19:50'),
(58, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 04:29:35'),
(59, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 04:31:39'),
(60, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 04:36:10'),
(61, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 05:01:29'),
(62, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-26 05:13:38'),
(63, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 05:19:37'),
(64, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-26 07:25:59'),
(65, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-28 13:37:11'),
(66, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-28 13:38:13'),
(67, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-04-30 06:28:42'),
(68, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-30 09:40:45'),
(69, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-04-30 10:06:44'),
(70, '', '', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-05-01 06:03:40', '2018-05-01 06:03:40'),
(71, 'customer', '33', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-05-01 06:30:31', '2018-05-01 06:30:31'),
(72, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-04-23 15:31:05', '2018-05-01 06:53:05'),
(73, 'customer', '66', 'H. No 293', 'Dhuri', 'Dhuri', 'Deepakshi Dhuri', '5455465', 'Dhuri', 'Punjab', 'India', '4545454', '30.6455', '76.37445', '2018-05-01 09:47:51', '2018-05-01 09:47:51'),
(74, 'customer', '66', 'iutyuit', 'itit', 'u', 'uu', 't', 'ui', 'tui', 't', 'ui', 'tu', 'uit', '2018-05-01 10:36:45', '2018-05-01 10:36:45'),
(75, 'customer', '67', 'jg', 'gu', 'guy', 'uyu', 'ut', 'dfdfdf', 't', 'dfdfdf', 'tt', 'u', 't', '2018-05-01 10:57:29', '2018-05-01 10:57:29'),
(76, 'customer', '33', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-05-02 06:25:29', '2018-05-02 06:25:29'),
(77, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 06:25:58', '2018-05-02 06:25:58'),
(78, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 06:33:11', '2018-05-02 06:33:11'),
(79, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 06:34:10', '2018-05-02 06:34:10'),
(80, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 06:37:57', '2018-05-02 06:37:57'),
(81, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-02 06:44:48'),
(82, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-02 06:44:51'),
(83, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-02 06:44:51'),
(84, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-02 06:45:26'),
(85, '', '', '', 'gu', 'guy', '', '', 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-02 07:18:54'),
(86, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:15:11', '2018-05-02 08:15:11'),
(87, '', '', '', 'gdfgdfg', 'bbg', '', '', 'Banur', 'Punjab', 'India', '140892', '', '', '2018-05-01 12:32:48', '2018-05-02 08:22:17'),
(88, '', '', '', 'gu', 'guy', '', '', 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-02 08:26:39'),
(89, '', '', '', 'gdfgdfg', 'bbg', '', '', 'Banur', 'Punjab', 'India', '140892', '', '', '2018-05-01 12:32:48', '2018-05-02 08:40:52'),
(90, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:40:57', '2018-05-02 08:40:57'),
(91, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:50:11', '2018-05-02 08:50:11'),
(92, 'customer', '33', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-05-02 08:50:54', '2018-05-02 08:50:54'),
(93, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:51:28', '2018-05-02 08:51:28'),
(94, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:57:59', '2018-05-02 08:57:59'),
(95, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 08:59:22', '2018-05-02 08:59:22'),
(96, '', '', '', 'gdfgdfg', 'bbg', '', '', 'Banur', 'Punjab', 'India', '140892', '', '', '2018-05-01 12:32:48', '2018-05-02 09:06:18'),
(97, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-02 09:06:39', '2018-05-02 09:06:39'),
(98, 'customer', '63', 'fdfdfdfdf', 'hjgjh', 'gj', 'gjh', 'j', 'gfhj', 'f', 'y', 'fy', 'yudfy', 'fyud', '2018-05-02 09:08:38', '2018-05-02 09:08:38'),
(117, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-02 10:56:05'),
(120, 'customer', '63', 'gbhg', 'gbhf', 'gfb', 'hgf', 'gbh', 'bgbhgf', 'gfgf', 'gfh', 'ghgf', 'ghfg', 'gfh', '2018-05-02 12:57:15', '2018-05-02 12:57:15'),
(121, 'customer', '63', 'fccb', 'c', 'b', 'bcb', 'bvb', 'cb', 'bvb', 'cbv', 'bv', 'bvb', 'cvb', '2018-05-02 13:04:14', '2018-05-02 13:04:14'),
(122, 'customer', '63', 'vbv', 'bvb', 'vb', 'vb', 'bvb', 'vbv', 'v', 'vb', 'bvv', 'v', 'vb', '2018-05-02 13:04:41', '2018-05-02 13:04:41'),
(123, 'customer', '33', 'gggg', 'ghgghggh', 'ghgh', '456546546', 'hgghghh', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '67.567', '6767.787', '2018-05-03 06:19:23', '2018-05-03 06:19:23'),
(124, '', '', '', 'gu', 'guy', '', '', 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-03 09:23:47'),
(125, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-03 12:26:37'),
(126, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-03 12:26:59'),
(127, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-03 12:27:18'),
(128, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-03 12:31:25'),
(129, 'customer', '66', 'df', 'dfs', 'df', 'df', 'dff', 'df', 'df', 'f', 'df', 'df', 'df', '2018-05-16 06:12:01', '2018-05-16 06:12:01'),
(130, '', '', '', 'gu', 'guy', '', '', 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-04 04:18:20'),
(131, '', '', '', 'gu', 'guy', '', '', 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-04 04:19:14'),
(132, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-04 05:18:51'),
(133, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-04 05:30:09'),
(134, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-04 11:43:43'),
(135, 'customer', '33', '', '', '', '', '', '', '', '', '', '', '', '2018-05-04 12:11:46', '2018-05-04 12:11:46'),
(136, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-04 12:18:11'),
(137, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-07 11:46:38'),
(138, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-08 05:33:56'),
(139, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-08 09:25:22'),
(141, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-08 11:13:53'),
(142, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-09 04:58:17'),
(143, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(144, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(145, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(146, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(147, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(148, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(149, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(150, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-10 04:17:43'),
(151, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '54766', '', '', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(152, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(153, 'customer', '66', '', '', '', '', '', '', '', '', '', '', '', '2018-05-10 11:33:45', '2018-05-10 11:33:45'),
(154, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-11 03:50:29'),
(156, '', '', '', 'hjgjh', 'gj', '', '', 'gfhj', 'f', 'y', 'fy', '', '', '2018-05-02 09:08:38', '2018-05-11 05:49:51'),
(157, '', '', '', 'ghgghggh', 'ghgh', '', '', 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-11 10:52:49'),
(158, '', '', '', 'fgg', 'fdg', '', '', 'fdg', 'fgdg', 'fdg', 'fdg', '', '', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(160, '', '', NULL, 'gu', 'guy', NULL, NULL, 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-12 09:05:47'),
(161, '', '', NULL, 'ghgghggh', 'ghgh', NULL, NULL, 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-12 10:19:51'),
(162, '', '', NULL, 'ghgghggh', 'ghgh', NULL, NULL, 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-12 12:05:43'),
(163, '', '', NULL, 'ghgghggh', 'ghgh', NULL, NULL, 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', '', '', '2018-04-23 15:02:43', '2018-05-12 12:07:48'),
(164, 'customer', '67', 'Home', 'Badal Colony', NULL, 'Deepu', '9876543211', 'Zirakpur', 'Punjab', 'India', '140603', '30.65074096486185', '76.81428764550787', '2018-05-14 05:01:28', '2018-05-14 05:01:28'),
(165, '', '', NULL, 'gu', 'guy', NULL, NULL, 'dfdfdf', 't', 'dfdfdf', 'tt', '', '', '2018-05-01 10:57:29', '2018-05-14 05:23:54'),
(166, '', '', NULL, 'Badal Colony', NULL, NULL, NULL, 'Zirakpur', 'Punjab', 'India', '140603', '', '', '2018-05-14 05:01:28', '2018-05-14 05:24:48'),
(167, '', '', NULL, 'gdfgdfg', 'bbg', NULL, NULL, 'Banur', 'Punjab', 'India', '140892', '', '', '2018-05-01 12:32:48', '2018-05-14 05:26:59'),
(190, 'customer', '66', 'tyr', 'ty', 'tr', 't', 'try', 'ty', 'try', 'try', 'ty', NULL, NULL, '2018-05-16 05:39:39', '2018-05-16 05:39:39'),
(192, 'customer', '66', 'dfd', 'fdf', 'df', 'd', 'fd', 'df', 'd', 'df', 'df', NULL, NULL, '2018-05-16 05:56:20', '2018-05-16 05:56:20'),
(193, 'customer', '66', 'h', 'ggf', 'gh', 'ghfg', 'hgh', 'gh', 'gfh', 'gfh', 'gh', NULL, NULL, '2018-05-16 05:58:38', '2018-05-16 05:58:38'),
(194, 'customer', '66', 'h', 'ggf', 'gh', 'ghfg', 'hgh', 'gh', 'gfh', 'gfh', 'gh', NULL, NULL, '2018-05-16 05:58:43', '2018-05-16 05:58:43'),
(195, 'customer', '66', 'gh', 'hg', 'gh', 'gh', 'hg', 'h', 'g', 'gh', 'ghg', NULL, NULL, '2018-05-16 06:09:03', '2018-05-16 06:09:03'),
(196, 'customer', '66', 'fg', 'fg', 'fg', 'fg', 'gdg', 'fg', 'g', 'fg', 'fg', NULL, NULL, '2018-05-16 06:11:06', '2018-05-16 06:11:06'),
(197, '', '', NULL, 'ghgghggh', 'ghgh', NULL, NULL, 'hgghghjhgj', 'hgghjgh', 'hgjg', '444', NULL, NULL, '2018-04-23 15:02:43', '2018-05-18 05:10:06'),
(198, 'customer', '63', 'gh', 'ghf', 'gfh', 'fh', 'gff', 'hgh', 'gh', 'ghg', 'gh', '30.65118399999999', '76.81360100000006', '2018-05-18 05:36:43', '2018-05-18 05:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` mediumtext COLLATE utf8mb4_unicode_ci,
  `user_group_id` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_brand_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `store_notification_text` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `store_beacon_UDID` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `latitude` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `longitude` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `short_address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `long_address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `store_qrcode` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `store_qrcode_string` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `status`, `email`, `password`, `photo`, `user_group_id`, `remember_token`, `store_brand_id`, `store_notification_text`, `store_beacon_UDID`, `latitude`, `longitude`, `short_address`, `long_address`, `store_qrcode`, `store_qrcode_string`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 1, 'test@admin.com', '$2y$10$mJiKMchxmSYpdyVGLb/NNOuf2UTeOrMrM6W39hHTho/mgFR3cIeKa', 'admin/brands/brand1520580740.jpg', 1, 'rzoF7hrJntRMeDaC2PULp7x1QMQAJrsSAdgchBeBgKLdQvveWRNtq3NP3pIG', '', '', '', '', '', '', '', '', '', 'admin', '2018-03-01 12:34:55', '2018-04-11 04:53:40'),
(7, 'sss', 'sss', 1, 'ss@df.dfd', '$2y$10$TY0sjC7/yJx/lHqkiwiCFO0jnv4eHXmgt7CqxdSAF1XoENtP100le', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'admin', '2018-03-01 09:54:29', '2018-02-13 03:38:52'),
(8, 'bbbb', 'bbbb', 1, 'wd@rtg.ghg', '$2y$10$rKH8QhX7D3ql2v6VYf.aqeqwK37mRVPpaz8P9EEWDcnH4IVEnLWY.', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'admin', '2018-03-01 09:54:29', '2018-02-13 04:52:29'),
(10, 'Brand 1', '', 1, 'brand1@gmail.com', '$2y$10$mJiKMchxmSYpdyVGLb/NNOuf2UTeOrMrM6W39hHTho/mgFR3cIeKa', 'admin/brands/brand1520580740.jpg', 1, 'StH1P3G6blbwRBVl17Ywg0raMaBiZClofAzefvEAWMrrK6AVih8sfQWkbSWa', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-01 09:53:18', '2018-04-11 04:32:04'),
(15, '', '', 1, 'dfdf@rfd.gffg', '$2y$10$kuu2HLvlKUBpYGE7EZB0huiFFw/z2Z/mF1rngCgKFhlFp9zKGumyq', 'admin/brands/brand1520580740.jpg', 1, '', '10', 'ds', 'bbbbb', '30.0374187', '81.95348149', 'sjhgshgsh id=15', 'dsdsd id=15', '', '', 'store', '2018-03-01 09:54:29', '2018-02-14 01:56:03'),
(19, 'fddfd', 'dfdfdf', 0, 'dfd@fhg.ght', '$2y$10$VQfe2j94bJy1FDPw5isxI.mE7LCyRSOFzfBJY8oRvJVL91c3zC4m2', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'admin', '2018-03-01 09:54:29', '2018-03-06 04:39:09'),
(21, 'Brand2', '', 1, 'brand2@gmail.com', '$2y$10$mJiKMchxmSYpdyVGLb/NNOuf2UTeOrMrM6W39hHTho/mgFR3cIeKa', 'admin/brands/brand1520580740.jpg', 1, '0gxqlKmtQTZJdX6kdVahI2G2O5krL5zZZUCbyNcjUey7zdTk0e38HApYN0he', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-01 09:54:29', '2018-03-05 23:47:51'),
(22, '', '', 1, 'store2@gmail.com', '$2y$10$mJiKMchxmSYpdyVGLb/NNOuf2UTeOrMrM6W39hHTho/mgFR3cIeKa', 'admin/brands/brand1520580740.jpg', 1, '1g9hjDlazM1RYJD15dBwhQqrJ5NiXGVNcOMlQn1I5wFMNAce1LlXNbgSkFIQ', '10', 'dsds', 'sdsd', '4.214943141390651', '29.70703125', 'sdsds', 'dsds', 'qrcodes/PwxQ1l6Ty3LoH2h65qefskukERpvPAOFRltAiMx1FuO6UaPwZfwX3R61mCBv.png', '', 'store', '2018-03-01 09:54:29', '2018-04-11 04:35:04'),
(23, '', '', 1, 'store1@gmail.com', '$2y$10$mJiKMchxmSYpdyVGLb/NNOuf2UTeOrMrM6W39hHTho/mgFR3cIeKa', 'admin/brands/brand1520580740.jpg', 1, 'ynvefQFMMbWiieiVQMF2ltJIfmoP33XtI3IrFfXv2UKmCON9eCXdawR9uC9h', '10', 'thius is a exciting offer', 'dwrgtfrhhhfhdf bbb', '4.214943141390651', '29.70703125', 'c-156,1 haber market zirakpur', 'haber road , kallor ', 'qrcodes/1520309373.png', 'CZNccBlGuYXTstbIbm9VXof6DwZoaNBcvx0hDrvmE3NrQWvQMCN0VyVGgvBAI1tsJie8SbCjy5tfOyV1ZMbRutw3lLSAWrAxo4aU', 'store', '2018-03-05 22:39:33', '2018-04-11 05:02:31'),
(25, '', '', 1, 'sdsd@ghj.tygdr', '$2y$10$mxfgLG.Uc4k6TcgJnm3/oO4ze7EtePnJkKwJM5OS/9OCLI7B4bVhy', 'admin/brands/brand1520580740.jpg', 1, '', '10', 'fdfs', 'sdfdsd', '-3.8642546157213955', '-23.5546875', 'dsfdsfsd', 'fdfd', 'qrcodes/1518595983.png', '1h1TrWcRZdfVXYwrKTBwWwoK9gUKWz8Ig2q6BzwgFkC6Ob5t51lag633p8CM', 'store', '2018-03-01 09:54:29', '2018-02-14 02:43:03'),
(26, '', '', 1, 'sdsdssdsds@fg.gfgf', '$2y$10$mL6uebZpiWEUtQmnPNxjN.LZ3/qFQGVeMmaxs2rKFgus7cMq5wd82', 'admin/brands/brand1520580740.jpg', 1, '', '10', 'ddfdffd', 'dfdfd', '25.005972656239187', '77.87109375', 'sfdf', 'dfdfd', 'qrcodes/1518596755.png', 'cYFqwrbIzQ8WvuzxmyPq3PfHOso5Uv8hjKXDv7HeuqFzD35JO11QjF4UUeb7', 'store', '2018-03-01 09:54:29', '2018-02-14 02:55:55'),
(28, '', '', 1, 'store4@gmail.com', '$2y$10$.yuUqsfy76GiYJzrVeNUce7z7L9EXEhsYFSoqgLG/e2/9hNy0JHcy', 'admin/brands/brand1520580740.jpg', 1, '', '21', 'dfdfd', 'df i=28', '3.732713674373873', '33.38073492050171', 'cfd', 'gdgfdgfd', 'qrcodes/1520309531.png', 'vadVHV2SKIjMd1b1a9cfFjaZTvZpvZfQz1qQStqtYv6J41d11ya1NnkQM3Fk2YHmVsgHNbN7jp0pYWQrwCmA1hUnW4SRgnSyAhsW', 'store', '2018-03-05 22:42:11', '2018-03-06 04:37:50'),
(29, 'Brand3', '', 1, 'brand3@gmail.com', '$2y$10$HkFK2sl9/dEttT/qvUi0B.Bacm9GgxI62ZqEov/CNGJBTHvL6/uwW', 'admin/brands/brand1520580740.jpg', 1, 'T0C2ebSMWIJLB8yckXzvUdl30BIzmKTjio5uU9zkavUyjGj10T3Pmp0kUbtl', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-06 01:35:26', '2018-03-06 01:36:53'),
(30, 'Brand4', '', 1, 'brand4@gmail.com', '$2y$10$s22TY.z98GajXGIGpGYUmee0rtnn765dg8Klrt1qho7XokkVTbEwu', 'admin/brands/brand1520580740.jpg', 1, 'afRznF64ZuJC2wOKM0Wy4mS2giJFdtRY6TtDwsVYYP8bEtmFMHP3b8tttKZU', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-06 04:58:01', '2018-03-06 05:01:41'),
(31, 'dsfsdfsd', '', 1, 'sdfsd@werter.htghr', '$2y$10$gzsdS/4aNDVt5tnVMJXRhekQLxJNcsUyauidmxGkic9MndyiRyV6i', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-09 02:00:23', '2018-03-09 02:00:23'),
(32, 'dfdfdfd', '', 1, 'dfdfd@fhg.gytyty', '$2y$10$fHJI4GTb0hMkIxcA4GEczeSHNAj9TFtjMjyrWZhmmSGEecJ1b4POW', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-09 02:00:58', '2018-03-09 02:00:58'),
(33, 'sdsdsdsdsddssdds', '', 1, 'sdds@dfg.ghg', '$2y$10$NQEI0r/.gNmClfF8OLx9Uu6Og3EDiEG3K4DH3GB464ZKDvF5JaU3W', 'admin/brands/brand1520580740.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-09 02:01:44', '2018-03-09 02:01:44'),
(34, 'bashgah', '', 1, 'hjasdh@kh.df', '$2y$10$uajOW47iS92J6LMC1TWI3uzEZecyG3E52EQEq5toEOCvqX/i34olG', 'admin/brands/brand1520581239.jpg', 1, '', '', '', '', '', '', '', '', '', '', 'brand', '2018-03-09 02:02:20', '2018-03-09 02:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'Patanjali', '2018-02-05 12:13:20', '2018-02-05 12:13:20'),
(3, 'Dabur', '2018-02-05 12:13:28', '2018-05-04 04:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(50) NOT NULL,
  `parent_id` varchar(50) DEFAULT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `photo` varchar(300) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT '1',
  `sort_index` int(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `photo`, `status`, `sort_index`, `updated_at`, `created_at`) VALUES
(1, '', 'cat1', ' ', '1', 1, '2018-04-16 13:32:59', '2018-04-02 12:11:43'),
(4, '', 'cat4', 'abc4.jpg', '1', 2, '2018-04-02 12:11:43', '2018-04-02 12:11:43'),
(5, '4', 'cat5', 'abc5.jpg', '1', 0, '2018-04-02 12:11:43', '2018-04-02 12:11:43'),
(6, NULL, 'cat6', 'ftpissue.PNG', '1', 0, '2018-05-11 12:31:26', '2018-04-02 12:11:43'),
(7, NULL, 'cat7', 'Capture.PNG', '1', 0, '2018-05-11 12:31:18', '2018-04-02 12:11:43'),
(8, '7', 'cat8', 'abc8.jpg', '1', 0, '2018-04-02 12:11:43', '2018-04-02 12:11:43'),
(9, '7', 'cat9', 'abc9.jpg', '1', 0, '2018-04-02 12:11:43', '2018-04-02 12:11:43'),
(10, '2', 'cat10', 'abc10.jpg', '1', 0, '2018-04-02 12:11:43', '2018-04-02 12:11:43'),
(11, ' ', 'biscuit', ' ', '1', 4, '2018-04-23 15:32:53', '2018-04-23 15:32:53'),
(12, ' ', 'Namkeen', ' ', '1', 3, '2018-04-24 11:09:08', '2018-04-24 11:09:08'),
(13, ' ', 'sdsd', ' ', '1', 6, '2018-04-24 11:09:55', '2018-04-24 11:09:55'),
(14, ' ', 'dfdfdfdfdfdf', ' ', '1', 5, '2018-04-24 11:10:52', '2018-04-24 11:10:52'),
(15, ' ', 'dfdfdfdfd', NULL, '1', 7, '2018-05-08 10:57:27', '2018-05-08 10:57:27'),
(16, NULL, 'dfdfdddddvvvvvvvvvvvvvvvvvvvvvvvvvvvv', NULL, '1', 0, '2018-05-11 12:25:14', '2018-05-11 12:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `days_time_slots`
--

CREATE TABLE `days_time_slots` (
  `id` int(11) NOT NULL,
  `week_day` varchar(50) NOT NULL DEFAULT '',
  `from_time` varchar(50) NOT NULL DEFAULT '',
  `to_time` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days_time_slots`
--

INSERT INTO `days_time_slots` (`id`, `week_day`, `from_time`, `to_time`, `created_at`, `updated_at`) VALUES
(1, 'Tue', '09:00:00', '11:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(2, 'Tue', '11:00:00', '13:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(3, 'Wed', '13:00:00', '15:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(4, 'Thu', '09:00:00', '11:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(5, 'Wed', '09:00:00', '11:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(6, 'Sat', '09:00:00', '11:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(7, 'Sun', '16:00:00', '18:00:00', '2018-04-29 18:30:00', '2018-04-29 18:30:00'),
(8, 'Mon', '15:00:00', '17:00:00', '2018-05-13 20:33:03', '2018-05-13 19:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(50) NOT NULL,
  `question` longtext NOT NULL,
  `answer` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'sdffdfddddddddddddddd', 'dffdf', '2018-05-23 18:30:00', '2018-05-01 05:53:35'),
(3, 'fdfdsdsdsdsdsdsd', 'fdfdfdfdf', '2018-05-01 05:52:07', '2018-05-01 05:53:39'),
(4, 'dfdfd', 'fdfdfdf', '2018-05-01 05:54:36', '2018-05-01 05:54:36');

-- --------------------------------------------------------

--
-- Table structure for table `features_settings`
--

CREATE TABLE `features_settings` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '0' COMMENT '0,1',
  `type` varchar(50) DEFAULT '' COMMENT 'product,coupon etc',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features_settings`
--

INSERT INTO `features_settings` (`id`, `title`, `status`, `type`, `created_at`, `updated_at`) VALUES
(1, 'stock_count', '0', 'product', '2018-04-24 03:36:58', '0000-00-00 00:00:00'),
(2, 'base_discount', '0', 'product', '2018-04-19 11:28:36', '0000-00-00 00:00:00'),
(3, 'base_price', '1', 'product', '2018-04-05 03:54:17', '0000-00-00 00:00:00'),
(5, 'photo', '1', 'product', '2018-04-24 09:13:26', '0000-00-00 00:00:00'),
(6, 'brand_ids', '1', 'brands', '2018-04-19 12:40:46', '0000-00-00 00:00:00'),
(7, 'barcodes', '1', 'order', '2018-04-24 10:37:10', '0000-00-00 00:00:00'),
(8, 'tax', '0', 'order', '2018-05-02 05:43:24', '0000-00-00 00:00:00'),
(9, 'shipping', '0', 'order', '2018-05-02 05:43:24', '0000-00-00 00:00:00'),
(10, 'coupon', '0', 'order', '2018-05-02 05:43:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmailTest\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailTest\",\"command\":\"O:22:\\\"App\\\\Jobs\\\\SendEmailTest\\\":5:{s:10:\\\"\\u0000*\\u0000details\\\";a:1:{s:5:\\\"email\\\";s:25:\\\"harvindersingh@goteso.com\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1525860440, 1525860440),
(2, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmailTest\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailTest\",\"command\":\"O:22:\\\"App\\\\Jobs\\\\SendEmailTest\\\":5:{s:10:\\\"\\u0000*\\u0000details\\\";a:1:{s:5:\\\"email\\\";s:25:\\\"harvindersingh@goteso.com\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1525860442, 1525860442),
(3, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmailTest\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailTest\",\"command\":\"O:22:\\\"App\\\\Jobs\\\\SendEmailTest\\\":5:{s:10:\\\"\\u0000*\\u0000details\\\";a:1:{s:5:\\\"email\\\";s:25:\\\"harvindersingh@goteso.com\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1525861152, 1525861152);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(22, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(23, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(24, '2016_06_01_000004_create_oauth_clients_table', 2),
(25, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(36, '2013_05_01_194506_create_links_table', 3),
(37, '2014_10_12_000000_create_users_table', 3),
(38, '2014_10_12_100000_create_password_resets_table', 3),
(39, '2017_04_30_012311_create_posts_table', 3),
(40, '2017_04_30_014352_create_permission_tables', 3),
(41, '2018_02_02_103541_create_items_table', 3),
(42, '2018_02_02_103541_create_subscriptions_table', 3),
(43, '2018_05_09_150348_create_jobs_table', 4),
(44, '2018_05_09_150425_create_failed_jobs_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_id`, `model_type`) VALUES
(5, 70, 'App\\User'),
(6, 70, 'App\\User'),
(7, 70, 'App\\User'),
(8, 70, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(2, 6, 'App\\User'),
(2, 12, 'App\\User'),
(2, 13, 'App\\User'),
(1, 14, 'App\\User'),
(2, 15, 'App\\User'),
(2, 16, 'App\\User'),
(2, 17, 'App\\User'),
(1, 18, 'App\\User'),
(3, 19, 'App\\User'),
(1, 25, 'App\\User'),
(2, 26, 'App\\User'),
(1, 32, 'App\\User'),
(1, 63, 'App\\User'),
(2, 64, 'App\\User'),
(2, 65, 'App\\User'),
(3, 68, 'App\\User'),
(2, 83, 'App\\User'),
(2, 84, 'App\\User'),
(1, 85, 'App\\User'),
(2, 86, 'App\\User'),
(1, 87, 'App\\User'),
(4, 88, 'App\\User'),
(3, 89, 'App\\User'),
(3, 90, 'App\\User'),
(1, 91, 'App\\User'),
(2, 92, 'App\\User'),
(2, 93, 'App\\User'),
(2, 94, 'App\\User'),
(2, 95, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `notification_triggers`
--

CREATE TABLE `notification_triggers` (
  `id` int(50) NOT NULL,
  `trigger_type` varchar(100) NOT NULL DEFAULT '',
  `email_type` varchar(200) NOT NULL DEFAULT '' COMMENT 'regular,special',
  `email_body` longtext NOT NULL,
  `email_blade` varchar(200) NOT NULL DEFAULT '',
  `subject` varchar(500) NOT NULL DEFAULT '',
  `title` varchar(500) NOT NULL DEFAULT '',
  `sub_title` varchar(500) NOT NULL DEFAULT '',
  `sms_text` varchar(500) NOT NULL DEFAULT '',
  `notification_type` varchar(50) NOT NULL DEFAULT '',
  `user_type` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_triggers`
--

INSERT INTO `notification_triggers` (`id`, `trigger_type`, `email_type`, `email_body`, `email_blade`, `subject`, `title`, `sub_title`, `sms_text`, `notification_type`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{customer_name}} (customer) ,  Your Order #{{order_id}} is changed to {{order_status}} ', 'Hi , {{customer_name}} ,  Your Order {{order_id}} is changed to {{order_status}} ', 'sms2', 'Customer', NULL, NULL),
(2, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{driver_name}}(driver)   Order #{{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'Hi , {{driver_name}} Order {{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'sms2', 'Driver', '2018-05-09 18:30:00', '2018-05-09 18:30:00'),
(3, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{driver_name}}(driver)   Order #{{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'Hi , {{customer_name}} Order {{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'push', 'Customer', NULL, NULL),
(4, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{driver_name}}(driver)   Order #{{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'Hi , {{driver_name}} Order {{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'push', 'Driver', NULL, NULL),
(5, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{driver_name}}(driver)   Order #{{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'Hi , {{customer_name}} Order {{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'web', 'Customer', NULL, NULL),
(6, 'order_status_update', 'regular', 'Hi , <b>{{customer_name}}</b>  Your Order #{{order_id}} is changed to {{order_status}} ', '', 'Order Update', 'Order Updated', 'Hi , {{driver_name}}(driver)   Order #{{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'Hi , {{driver_name}} Order {{order_id}} by {{customer_name}} is updated to {{order_status}} ', 'web', 'Driver', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notification_variables`
--

CREATE TABLE `notification_variables` (
  `id` int(50) NOT NULL,
  `variable_key` varchar(100) NOT NULL DEFAULT '',
  `variable_value` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_variables`
--

INSERT INTO `notification_variables` (`id`, `variable_key`, `variable_value`, `created_at`, `updated_at`) VALUES
(1, '{{customer_email}}', 'customer_email', NULL, NULL),
(2, '{{order_id}}', 'order_id', '2018-05-09 18:30:00', '2018-05-09 18:30:00'),
(3, '{{order_status}}', 'order_status', '2018-05-09 18:30:00', '2018-05-09 18:30:00'),
(4, '{{customer_name}}', 'customer_name', '2018-05-09 18:30:00', '2018-05-09 18:30:00'),
(5, '{{driver_name}}', 'driver_name', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'GroceryApp Personal Access Client', '7dA1G7hNqFH6AsIo1Jj6XouH6eeDJMptu3xHyoTk', 'http://localhost', 1, 0, 0, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(2, NULL, 'GroceryApp Password Grant Client', 'rqMM60n4T5LTrigk10qFN1smlMGRbe5ynXjhaKqA', 'http://localhost', 0, 1, 0, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(3, NULL, 'GroceryApp Personal Access Client', 'ws5mAmi68Dd0SBxzIMlAKLEi4i3RovN8Uy722WEK', 'http://localhost', 1, 0, 0, '2018-01-12 06:18:18', '2018-01-12 06:18:18'),
(4, NULL, 'GroceryApp Password Grant Client', 'JaFZl2A2TrHWhTXv4k4jFF7Vaa2VZtF3riWSr1mT', 'http://localhost', 0, 1, 0, '2018-01-12 06:18:18', '2018-01-12 06:18:18'),
(5, NULL, 'Launspace Personal Access Client', 'BPGNKFrGbCYns4y5QjrAVD8L6XeV8yE49nbiP9CJ', 'http://localhost', 1, 0, 0, '2018-05-01 10:24:30', '2018-05-01 10:24:30'),
(6, NULL, 'Launspace Password Grant Client', '790cjhW80oajO8xPv6A4xyhSFvrLsOiRTHCM7R7m', 'http://localhost', 0, 1, 0, '2018-05-01 10:24:30', '2018-05-01 10:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(2, 3, '2018-01-12 06:18:18', '2018-01-12 06:18:18'),
(3, 5, '2018-05-01 10:24:30', '2018-05-01 10:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(50) NOT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `sub_total` double NOT NULL DEFAULT '0',
  `coupon_id` varchar(50) NOT NULL DEFAULT '',
  `coupon_code` varchar(50) NOT NULL DEFAULT '',
  `coupon_discount` double NOT NULL DEFAULT '0',
  `total` int(50) NOT NULL DEFAULT '0',
  `shipping` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_status`, `sub_total`, `coupon_id`, `coupon_code`, `coupon_discount`, `total`, `shipping`, `tax`, `created_at`, `updated_at`) VALUES
(5, 'Pending', -60.90299999999996, 'acdfdf', '213234', 0, -61, 50, 50, '2018-05-01 06:53:05', '2018-05-12 06:44:23'),
(6, 'Pending', 307.752, 'acdfdf', '213234', 0, 308, 50, 50, '2018-05-02 06:44:48', '2018-05-12 06:48:44'),
(7, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 50, 50, '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(8, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 50, 50, '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(9, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 50, 50, '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(10, 'Pending', 920, 'acdfdf', '213234', 0, 920, 50, 50, '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(11, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(12, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(13, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:35:01', '2018-05-02 08:35:01'),
(14, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:36:02', '2018-05-02 08:36:02'),
(15, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:36:20', '2018-05-02 08:36:20'),
(16, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:36:44', '2018-05-02 08:36:44'),
(17, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:36:57', '2018-05-02 08:36:57'),
(18, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:37:44', '2018-05-02 08:37:44'),
(19, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:38:41', '2018-05-02 08:38:41'),
(20, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:38:50', '2018-05-02 08:38:50'),
(21, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:39:08', '2018-05-02 08:39:08'),
(22, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:40:23', '2018-05-02 08:40:23'),
(23, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:40:34', '2018-05-02 08:40:34'),
(24, '', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 08:40:52', '2018-05-08 05:08:02'),
(25, 'Pending', 278, 'acdfdf', '213234', 0, 278, 50, 50, '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(26, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 50, 50, '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(27, 'Pending', 488, 'acdfdf', '213234', 0, 488, 50, 50, '2018-05-03 09:23:46', '2018-05-03 09:23:46'),
(28, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 50, '2018-05-03 12:25:18', '2018-05-03 12:25:18'),
(29, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 50, '2018-05-03 12:25:33', '2018-05-03 12:25:33'),
(30, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 50, '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(31, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 50, '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(32, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 50, '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(33, 'Pending', 1035, 'acdfdf', '213234', 0, 1035, 0, 186.3, '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(34, 'Cancelled', 575.84, 'acdfdf', '213234', 0, 488, 0, 87.84, '2018-05-04 04:18:20', '2018-05-08 07:20:43'),
(35, '', 488, 'acdfdf', '213234', 0, 576, 0, 87.84, '2018-05-04 04:19:14', '2018-05-08 07:14:01'),
(36, 'Processing', 488, 'acdfdf', '213234', 0, 576, 0, 87.84, '2018-05-04 05:18:51', '2018-05-12 09:01:45'),
(37, 'Pending', 39457.78, 'acdfdf', '213234', 0, 47427, 0, 87.84, '2018-05-04 05:30:09', '2018-05-12 12:41:10'),
(38, 'Pending', 690, 'acdfdf', '213234', 0, 814, 0, 124.2, '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(39, 'Pending', 944, 'acdfdf', '213234', 0, 1114, 0, 169.92, '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(40, 'processing', 1035, 'acdfdf', '213234', 0, 1221, 0, 186.3, '2018-05-07 11:46:38', '2018-05-08 03:34:33'),
(41, 'Completed', 1035, 'acdfdf', '213234', 0, 1221, 0, 186.3, '2018-05-08 05:33:56', '2018-05-12 10:16:45'),
(42, 'Pending', 1035, 'acdfdf', '213234', 0, 1221, 0, 186.3, '2018-05-08 09:25:21', '2018-05-08 09:25:22'),
(43, '', 1035, 'acdfdf', '213234', 0, 1221, 0, 186.3, '2018-05-08 11:13:53', '2018-05-12 09:49:24'),
(44, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(45, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(46, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(47, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(48, 'Processing', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:36:21', '2018-05-09 04:36:21'),
(49, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(50, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(51, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(52, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(53, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(54, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(55, 'Processing', 1035, 'acdfdf', '213234', 0, 1221, 0, 186.3, '2018-05-09 04:58:17', '2018-05-12 07:55:06'),
(56, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(57, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(58, 'Ready', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(59, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(60, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:06:37', '2018-05-09 10:06:37'),
(61, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:11:28', '2018-05-09 10:11:28'),
(62, 'Pending', 2169, 'acdfdf', '213234', 0, 2559, 0, 390.42, '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(63, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:14:45', '2018-05-09 10:14:45'),
(64, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:16:51', '2018-05-09 10:16:51'),
(65, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:17:04', '2018-05-09 10:17:04'),
(66, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:17:34', '2018-05-09 10:17:34'),
(67, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-09 10:17:58', '2018-05-09 10:17:58'),
(68, 'Pending', 234, '0', '0', 0, 276, 0, 42.12, '2018-05-09 10:19:03', '2018-05-09 10:19:03'),
(69, 'Pending', 234, '0', '0', 0, 276, 0, 42.12, '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(70, 'Pending', 234, '0', '0', 0, 276, 0, 42.12, '2018-05-09 10:40:57', '2018-05-12 09:46:14'),
(71, 'Pending', 234, '0', '0', 0, 276, 0, 42.12, '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(72, 'Cancelled', 3271, 'acdfdf', '213234', 0, 2781, 0, 543.06, '2018-05-10 04:17:43', '2018-05-12 12:42:16'),
(73, 'Pending', 468, '0', '0', 0, 552, 0, 84.24, '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(74, 'Pending', 5176, '0', '0', 0, 6110, 0, 793.08, '2018-05-10 09:26:35', '2018-05-12 07:19:26'),
(75, 'Pending', 2280, 'acdfdf', '213234', 0, 2690, 0, 410.4, '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(76, 'Pending', 1737, '0', '0', 0, 2050, 0, 312.66, '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(77, 'Pending', 20619.789999999997, 'acdfdf', '213234', 0, 26067, 0, 5447.16, '2018-05-11 05:49:51', '2018-05-12 10:15:25'),
(78, 'Pending', 658.98, 'acdfdf', '213234', 0, 1264, 0, 186.3, '2018-05-11 10:52:49', '2018-05-12 10:16:38'),
(79, 'Pending', 697.17, '0', '0', 0, 883, 0, 82.08, '2018-05-11 12:15:18', '2018-05-12 12:40:17'),
(80, 'Pending', 666, 'acdfdf', '213234', 0, 786, 0, 119.88, '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(81, 'Pending', 5969, 'acdfdf', '213234', 0, 7023, 0, 1122.3, '2018-05-12 10:19:51', '2018-05-12 10:34:01'),
(82, 'Pending', 234, 'acdfdf', '213234', 0, 276, 0, 42.12, '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(83, 'Pending', 1016, 'acdfdf', '213234', 0, 441, 0, 571.68, '2018-05-12 12:07:48', '2018-05-14 03:19:56'),
(84, 'Pending', 254, 'acdfdf', '213234', 0, 300, 0, 45.72, '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(85, 'Pending', 234, 'acdfdf', '213234', 0, 276, 0, 42.12, '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(86, 'Pending', 234, 'acdfdf', '213234', 0, 276, 0, 42.12, '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(87, 'Pending', 1368, 'acdfdf', '213234', 0, 1614, 0, 246.24, '2018-05-18 05:10:06', '2018-05-18 05:10:06');

-- --------------------------------------------------------

--
-- Table structure for table `orders_meta_types`
--

CREATE TABLE `orders_meta_types` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `display_title` varchar(100) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `important` varchar(40) NOT NULL DEFAULT '0',
  `identifier` varchar(100) NOT NULL DEFAULT '',
  `required_or_not` int(50) NOT NULL DEFAULT '0',
  `slots_days_difference` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT '',
  `columns` varchar(300) NOT NULL DEFAULT '',
  `parent_identifier` varchar(50) NOT NULL DEFAULT '',
  `field_options` varchar(500) NOT NULL DEFAULT '',
  `field_options_model` varchar(300) NOT NULL DEFAULT '',
  `field_options_model_columns` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_meta_types`
--

INSERT INTO `orders_meta_types` (`id`, `title`, `display_title`, `status`, `important`, `identifier`, `required_or_not`, `slots_days_difference`, `type`, `columns`, `parent_identifier`, `field_options`, `field_options_model`, `field_options_model_columns`, `created_at`, `updated_at`) VALUES
(1, 'Select Customer', 'Customer', '1', '1', 'customer_id', 0, '', 'select', '', '', '', '\\App\\User', 'first_name,id', NULL, '0000-00-00 00:00:00'),
(3, 'Select Address', 'Address', '1', '1', 'delivery_address', 0, '', 'api', 'address_line1,address_line2,city,state,country,pincode', 'customer_id', '', '\\App\\Addresses', '', NULL, '0000-00-00 00:00:00'),
(4, 'Select Pickup Time', 'Pickup Time', '1', '0', 'pickup_time', 0, '', 'timeSlotsPicker', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(5, 'Select Delivery Time', 'Delivery Time', '1', '0', 'delivery_time', 0, '0', 'timeSlotsPicker', '', 'pickup_time', '', '', '', NULL, '0000-00-00 00:00:00'),
(6, 'Notes for Delivery Boy', 'Notes', '1', '0', 'notes', 1, '', 'textarea', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(7, 'Driver', 'Driver', '1', '0', 'driver_id', 0, '', 'quick_link', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(8, 'Vendor', 'Vendor', '1', '0', 'vendor_id', 0, '', 'quick_link', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(11, 'Order Status', 'Update', '0', '0', 'orderStatus', 0, '', 'quick_link', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(12, 'Payment Received', 'Received', '0', '0', 'orderPaymentReceived', 0, '', 'quick_link', '', '', '', '', '', NULL, '0000-00-00 00:00:00'),
(13, 'Apply Discount', 'Discount', '0', '0', 'applyDiscount', 0, '', 'quick_link', '', '', '', '', '', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders_meta_values`
--

CREATE TABLE `orders_meta_values` (
  `id` int(50) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `order_meta_type_id` varchar(50) NOT NULL DEFAULT '',
  `linked_id` varchar(50) DEFAULT ' ',
  `value` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_meta_values`
--

INSERT INTO `orders_meta_values` (`id`, `order_id`, `order_meta_type_id`, `linked_id`, `value`, `created_at`, `updated_at`) VALUES
(15, '5', '1', ' ', '67', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(16, '5', '3', ' ', '72', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(17, '5', '4', ' ', '2018-05-24', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(18, '5', '5', ' ', '2018-05-26', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(19, '5', '6', ' ', 'rtrt', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(20, '5', '7', ' ', '39', '2018-05-01 06:53:05', '2018-05-01 08:12:37'),
(21, '6', '1', ' ', '33', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(22, '6', '3', ' ', '81', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(23, '6', '4', ' ', '2018-04-10', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(24, '6', '5', ' ', '2018-04-22', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(25, '6', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(26, '7', '1', ' ', '33', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(27, '7', '3', ' ', '82', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(28, '7', '4', ' ', '2018-04-10', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(29, '7', '5', ' ', '2018-04-22', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(30, '7', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(31, '8', '1', ' ', '33', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(32, '8', '3', ' ', '83', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(33, '8', '4', ' ', '2018-04-10', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(34, '8', '5', ' ', '2018-04-22', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(35, '8', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(36, '9', '1', ' ', '33', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(37, '9', '3', ' ', '84', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(38, '9', '4', ' ', '2018-04-10', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(39, '9', '5', ' ', '2018-04-22', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(40, '9', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(41, '10', '1', ' ', '67', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(42, '10', '3', ' ', '85', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(43, '10', '4', ' ', '2018-05-03 12:48:46', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(44, '10', '5', ' ', '2018-05-04 12:48:46', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(45, '10', '6', ' ', 'sdfgfdhgdfg', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(46, '10', '8', ' ', '63', '2018-05-02 07:27:06', '2018-05-02 08:10:04'),
(47, '10', '7', ' ', '69', '2018-05-02 07:51:04', '2018-05-02 07:51:04'),
(48, '11', '1', ' ', '67', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(49, '11', '3', ' ', '87', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(50, '11', '4', ' ', '2018-05-03 13:52:08', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(51, '11', '5', ' ', '2018-05-04 13:52:08', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(52, '11', '6', ' ', 'dfdfdfdfd', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(53, '12', '1', ' ', '67', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(54, '12', '3', ' ', '88', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(55, '12', '4', ' ', '2018-05-03 13:56:13', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(56, '12', '5', ' ', '2018-05-04 13:56:13', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(57, '12', '6', ' ', 'dfgdgfdgdfgfgfdgdfgfdgdfgg', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(58, '24', '1', ' ', '67', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(59, '24', '3', ' ', '89', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(60, '24', '4', ' ', '2018-05-05 13:57:15', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(61, '24', '5', ' ', '2018-05-13 13:57:15', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(62, '24', '6', ' ', 'fdfdfd', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(63, '24', '8', ' ', NULL, '2018-05-02 08:41:45', '2018-05-03 09:22:27'),
(64, '24', '7', ' ', '71', '2018-05-02 08:43:35', '2018-05-03 09:22:37'),
(65, '25', '1', ' ', '67', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(66, '25', '3', ' ', '96', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(67, '25', '4', ' ', '03 May, 2018 | 09:00 AM - 11:00 AM', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(68, '25', '5', ' ', '04 May, 2018 | 09:00 AM - 11:00 AM', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(69, '25', '6', ' ', 'dfdfdfdf', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(70, '26', '1', ' ', '33', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(71, '26', '3', ' ', '117', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(72, '26', '4', ' ', '2018-04-10', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(73, '26', '5', ' ', '2018-04-22', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(74, '26', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(75, '26', '8', ' ', '88', '2018-05-02 12:10:46', '2018-05-02 12:10:46'),
(76, '27', '1', ' ', '67', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(77, '27', '3', ' ', '124', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(78, '27', '4', ' ', '2018-05-04 14:53:27', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(79, '27', '5', ' ', '2018-05-06 14:53:27', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(80, '27', '6', ' ', 'fghjsdf gf df g f g df gfd g d g f gdfgdfghdgfhsd dfghsdgdhsgdsfdysufre fehjgefyhs', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(81, '30', '1', ' ', '33', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(82, '30', '3', ' ', '125', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(83, '30', '4', ' ', '2018-04-10', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(84, '30', '5', ' ', '2018-04-22', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(85, '30', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(86, '31', '1', ' ', '33', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(87, '31', '3', ' ', '126', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(88, '31', '4', ' ', '2018-04-10', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(89, '31', '5', ' ', '2018-04-22', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(90, '31', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(91, '32', '1', ' ', '33', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(92, '32', '3', ' ', '127', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(93, '32', '4', ' ', '2018-04-10', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(94, '32', '5', ' ', '2018-04-22', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(95, '32', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(96, '33', '1', ' ', '33', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(97, '33', '3', ' ', '128', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(98, '33', '4', ' ', '2018-04-10', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(99, '33', '5', ' ', '2018-04-22', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(100, '33', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(101, '34', '1', ' ', '67', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(102, '34', '3', ' ', '130', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(103, '34', '4', ' ', '2018-05-05 09:48:07', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(104, '34', '5', ' ', '2018-05-13 09:48:07', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(105, '34', '6', ' ', 'dfdfdf', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(106, '35', '1', ' ', '67', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(107, '35', '3', ' ', '131', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(108, '35', '4', ' ', '2018-05-05 09:49:01', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(109, '35', '5', ' ', '2018-05-08 09:49:01', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(110, '35', '6', ' ', 'df dgfgfg rtrtrt', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(111, '36', '1', ' ', '33', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(112, '36', '3', ' ', '132', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(113, '36', '4', ' ', '2018-05-05 10:48:30', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(114, '36', '5', ' ', '2018-05-10 10:48:30', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(115, '36', '6', ' ', 'cvbfg ghgthhh ghhtyty tytyty ytytyytytyt', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(116, '36', '8', ' ', '88', '2018-05-04 05:19:07', '2018-05-04 05:19:07'),
(117, '37', '1', ' ', '33', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(118, '37', '3', ' ', '133', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(119, '37', '4', ' ', '2018-05-05 10:59:50', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(120, '37', '5', ' ', '2018-05-08 10:59:50', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(121, '37', '6', ' ', 'ffdd frrgtrt', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(122, '37', '8', ' ', '88', '2018-05-04 05:31:03', '2018-05-04 05:31:03'),
(123, '36', '7', ' ', '89', '2018-05-04 05:36:41', '2018-05-04 05:36:41'),
(124, '35', '7', ' ', '89', '2018-05-04 05:36:51', '2018-05-04 05:36:51'),
(125, '34', '7', ' ', '89', '2018-05-04 05:36:58', '2018-05-04 05:36:58'),
(126, '37', '7', ' ', '89', '2018-05-04 05:43:01', '2018-05-04 05:43:01'),
(127, '38', '1', ' ', '33', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(128, '38', '3', ' ', '134', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(129, '38', '4', ' ', '2018-05-06 17:13:26', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(130, '38', '5', ' ', '2018-05-08 17:13:26', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(131, '38', '6', ' ', 'yghygnj', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(132, '39', '1', ' ', '33', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(133, '39', '3', ' ', '136', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(134, '39', '4', ' ', '2018-05-06 17:47:58', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(135, '39', '5', ' ', '2018-05-08 17:47:58', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(136, '39', '6', ' ', 'gjjhkhjk', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(137, '40', '1', ' ', '33', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(138, '40', '3', ' ', '137', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(139, '40', '4', ' ', '2018-04-10', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(140, '40', '5', ' ', '2018-04-22', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(141, '40', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(142, '41', '1', ' ', '33', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(143, '41', '3', ' ', '138', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(144, '41', '4', ' ', '2018-04-10', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(145, '41', '5', ' ', '2018-04-22', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(146, '41', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(147, '41', '7', ' ', '89', '2018-05-08 08:33:19', '2018-05-08 08:42:48'),
(148, '42', '1', ' ', '33', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(149, '42', '3', ' ', '139', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(150, '42', '4', ' ', '2018-04-10', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(151, '42', '5', ' ', '2018-04-22', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(152, '42', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(153, '43', '1', ' ', '33', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(154, '43', '3', ' ', '141', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(155, '43', '4', ' ', '2018-04-10', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(156, '43', '5', ' ', '2018-04-22', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(157, '43', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(158, '43', '8', ' ', '88', '2018-05-08 11:47:18', '2018-05-08 11:47:18'),
(159, '43', '7', ' ', '89', '2018-05-08 11:47:22', '2018-05-08 11:47:22'),
(160, '44', '1', ' ', NULL, '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(161, '45', '1', ' ', NULL, '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(162, '46', '1', ' ', NULL, '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(163, '47', '1', ' ', NULL, '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(164, '49', '1', ' ', NULL, '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(165, '50', '1', ' ', NULL, '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(166, '51', '1', ' ', NULL, '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(167, '52', '1', ' ', NULL, '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(168, '53', '1', ' ', NULL, '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(169, '54', '1', ' ', NULL, '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(170, '55', '1', ' ', '33', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(171, '55', '3', ' ', '142', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(172, '55', '4', ' ', '2018-04-10', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(173, '55', '5', ' ', '2018-04-22', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(174, '55', '7', ' ', '33', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(175, '56', '1', ' ', NULL, '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(176, '56', '3', ' ', '143', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(177, '56', '4', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(178, '56', '5', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(179, '56', '6', ' ', 'BB hdjdnsv', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(180, '57', '1', ' ', NULL, '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(181, '58', '1', ' ', NULL, '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(182, '58', '3', ' ', '144', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(183, '58', '4', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(184, '58', '5', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(185, '58', '6', ' ', 'BB hdjdnsv', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(186, '59', '1', ' ', NULL, '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(187, '59', '3', ' ', '145', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(188, '59', '4', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(189, '59', '5', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(190, '59', '6', ' ', 'sgehrb', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(191, '62', '1', ' ', NULL, '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(192, '62', '3', ' ', '146', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(193, '62', '4', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(194, '62', '5', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(195, '62', '6', ' ', 'BB hdjdnsv', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(196, '69', '1', ' ', NULL, '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(197, '69', '3', ' ', '147', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(198, '69', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(199, '69', '5', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(200, '69', '6', ' ', 'ixidof', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(201, '70', '1', ' ', NULL, '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(202, '70', '3', ' ', '148', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(203, '70', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(204, '70', '5', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(205, '70', '7', ' ', '89', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(206, '71', '1', ' ', NULL, '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(207, '71', '3', ' ', '149', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(208, '71', '4', ' ', '10 May, 2018, 09:00 AM - 11:00 AM', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(209, '71', '5', ' ', '13 May, 2018, 04:00 PM - 06:00 PM', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(210, '71', '6', ' ', 'dfgdd', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(211, '72', '1', ' ', '33', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(212, '72', '3', ' ', '150', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(213, '72', '4', ' ', '2018-05-12 09:47:30', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(214, '72', '5', ' ', '2018-05-16 09:47:30', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(215, '72', '7', ' ', '89', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(216, '72', '8', ' ', '88', '2018-05-10 07:28:24', '2018-05-10 07:28:24'),
(217, '73', '1', ' ', NULL, '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(218, '73', '3', ' ', '151', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(219, '73', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(220, '73', '5', ' ', '15 May, 2018, 09:00 AM - 11:00 AM', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(221, '73', '6', ' ', 'vxcrffffddedft', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(222, '74', '1', ' ', NULL, '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(223, '74', '3', ' ', '152', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(224, '74', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(225, '74', '5', ' ', '17 May, 2018, 09:00 AM - 11:00 AM', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(226, '74', '6', ' ', 'photo fifofo difficult kfidid hatch yum LCL', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(227, '75', '1', ' ', '33', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(228, '75', '3', ' ', '154', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(229, '75', '4', ' ', '2018-05-12 09:20:15', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(230, '75', '5', ' ', '2018-05-15 09:20:15', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(231, '75', '6', ' ', 'gh', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(232, '76', '1', ' ', NULL, '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(233, '76', '3', ' ', '155', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(234, '76', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(235, '76', '5', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(236, '76', '6', ' ', NULL, '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(237, '77', '1', ' ', '63', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(238, '77', '3', ' ', '156', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(239, '77', '4', ' ', '2018-05-12 11:19:06', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(240, '77', '5', ' ', '2018-05-15 11:19:06', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(241, '77', '6', ' ', 'hamare pass bahut paisa hai...bhagwan ka diya sab kuchh hai.....paisa hai , shohrat hai, izzat hai...........hahahaha', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(242, '78', '1', ' ', '33', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(243, '78', '3', ' ', '157', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(244, '78', '4', ' ', '2018-04-10', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(245, '78', '5', ' ', '2018-04-22', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(246, '78', '6', ' ', 'I need it for free............pls pls pls.......', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(247, '79', '1', ' ', NULL, '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(248, '79', '3', ' ', '158', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(249, '79', '4', ' ', '12 May, 2018, 09:00 AM - 11:00 AM', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(250, '79', '5', ' ', '13 May, 2018, 04:00 PM - 06:00 PM', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(251, '79', '6', ' ', 'the', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(252, '79', '7', ' ', '89', '2018-05-12 05:44:57', '2018-05-12 05:44:57'),
(253, '79', '8', ' ', '88', '2018-05-12 05:45:01', '2018-05-12 05:45:01'),
(254, '80', '1', ' ', '67', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(255, '80', '3', ' ', '160', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(256, '80', '4', ' ', '2018-05-13 14:35:37', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(257, '80', '5', ' ', '2018-05-15 14:35:37', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(258, '80', '6', ' ', 'gfg', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(259, '81', '1', ' ', '33', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(260, '81', '3', ' ', '161', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(261, '81', '4', ' ', '2018-05-13 15:49:36', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(262, '81', '5', ' ', '2018-05-16 15:49:36', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(263, '81', '6', ' ', 'drgdfhgd', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(264, '82', '1', ' ', '33', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(265, '82', '3', ' ', '162', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(266, '82', '4', ' ', '2018-05-13 17:35:33', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(267, '82', '5', ' ', '2018-05-15 17:35:33', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(268, '82', '6', ' ', 'hyjhj', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(269, '83', '1', ' ', '33', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(270, '83', '3', ' ', '163', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(271, '83', '4', ' ', '2018-05-15 17:37:37', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(272, '83', '5', ' ', '2018-05-17 17:37:37', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(273, '83', '6', ' ', 'tyu', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(274, '84', '1', ' ', '67', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(275, '84', '3', ' ', '165', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(276, '84', '4', ' ', '2018-05-15 10:53:47', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(277, '84', '5', ' ', '2018-05-16 10:53:47', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(278, '84', '6', ' ', NULL, '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(279, '85', '1', ' ', '67', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(280, '85', '3', ' ', '166', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(281, '85', '4', ' ', '2018-05-16 10:54:41', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(282, '85', '5', ' ', '2018-05-17 10:54:41', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(283, '85', '6', ' ', NULL, '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(284, '86', '1', ' ', '67', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(285, '86', '3', ' ', '167', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(286, '86', '4', ' ', '2018-05-15 10:56:49', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(287, '86', '5', ' ', '2018-05-16 10:56:49', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(288, '86', '6', ' ', NULL, '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(289, '87', '1', ' ', '33', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(290, '87', '3', ' ', '197', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(291, '87', '4', ' ', '2018-05-20 10:37:38', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(292, '87', '5', ' ', '2018-05-20 10:37:38', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(293, '87', '6', ' ', 'ghgh', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(294, '86', '7', ' ', '89', '2018-05-18 05:24:27', '2018-05-18 05:24:27');

-- --------------------------------------------------------

--
-- Table structure for table `orders_payments`
--

CREATE TABLE `orders_payments` (
  `id` int(50) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `sub_total` varchar(50) NOT NULL DEFAULT '',
  `coupon_id` varchar(50) NOT NULL DEFAULT '',
  `coupon_code` varchar(50) NOT NULL DEFAULT '',
  `coupon_discount` varchar(50) NOT NULL DEFAULT '',
  `total` double NOT NULL,
  `shipping` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `payment_gateway` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_payments`
--

INSERT INTO `orders_payments` (`id`, `order_id`, `sub_total`, `coupon_id`, `coupon_code`, `coupon_discount`, `total`, `shipping`, `tax`, `payment_gateway`, `created_at`, `updated_at`) VALUES
(5, '5', '676.7', 'acdfdf', '213234', '0', 676.7, NULL, NULL, '', '2018-05-01 06:53:05', '2018-05-01 06:53:05'),
(6, '6', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(7, '7', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(8, '8', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(9, '9', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(10, '10', '920', 'acdfdf', '213234', '0', 920, NULL, NULL, '', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(11, '11', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(12, '12', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(13, '13', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:35:01', '2018-05-02 08:35:01'),
(14, '14', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:36:02', '2018-05-02 08:36:02'),
(15, '15', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:36:20', '2018-05-02 08:36:20'),
(16, '16', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:36:44', '2018-05-02 08:36:44'),
(17, '17', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:36:57', '2018-05-02 08:36:57'),
(18, '18', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:37:44', '2018-05-02 08:37:44'),
(19, '19', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:38:41', '2018-05-02 08:38:41'),
(20, '20', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:38:50', '2018-05-02 08:38:50'),
(21, '21', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:39:08', '2018-05-02 08:39:08'),
(22, '22', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:40:23', '2018-05-02 08:40:23'),
(23, '23', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:40:34', '2018-05-02 08:40:34'),
(24, '24', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(25, '25', '278', 'acdfdf', '213234', '0', 278, NULL, NULL, '', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(26, '26', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(27, '27', '488', 'acdfdf', '213234', '0', 488, NULL, NULL, '', '2018-05-03 09:23:46', '2018-05-03 09:23:46'),
(28, '30', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(29, '31', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(30, '32', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(31, '33', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(32, '34', '488', 'acdfdf', '213234', '0', 488, NULL, NULL, '', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(33, '35', '488', 'acdfdf', '213234', '0', 488, NULL, NULL, '', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(34, '36', '488', 'acdfdf', '213234', '0', 488, NULL, NULL, '', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(35, '37', '488', 'acdfdf', '213234', '0', 488, NULL, NULL, '', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(36, '38', '690', 'acdfdf', '213234', '0', 690, NULL, NULL, '', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(37, '39', '944', 'acdfdf', '213234', '0', 944, NULL, NULL, '', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(38, '40', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(39, '41', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(40, '42', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(41, '43', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(42, '44', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(43, '45', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(44, '46', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(45, '47', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(46, '48', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:36:21', '2018-05-09 04:36:21'),
(47, '49', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(48, '50', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(49, '51', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(50, '52', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(51, '53', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(52, '54', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(53, '55', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(54, '56', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(55, '57', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(56, '58', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(57, '59', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(58, '60', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:06:37', '2018-05-09 10:06:37'),
(59, '61', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:11:28', '2018-05-09 10:11:28'),
(60, '62', '2169', 'acdfdf', '213234', '0', 2169, NULL, NULL, '', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(61, '63', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:14:45', '2018-05-09 10:14:45'),
(62, '64', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:16:51', '2018-05-09 10:16:51'),
(63, '65', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:17:04', '2018-05-09 10:17:04'),
(64, '66', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:17:34', '2018-05-09 10:17:34'),
(65, '67', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-09 10:17:58', '2018-05-09 10:17:58'),
(66, '68', '234', '0', '0', '0', 276.12, NULL, NULL, '', '2018-05-09 10:19:03', '2018-05-09 10:19:03'),
(67, '69', '234', '0', '0', '0', 276.12, NULL, NULL, '', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(68, '70', '234', '0', '0', '0', 276.12, NULL, NULL, '', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(69, '71', '234', '0', '0', '0', 276.12, NULL, NULL, '', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(70, '72', '3017', 'acdfdf', '213234', '0', 3017, NULL, NULL, '', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(71, '73', '468', '0', '0', '0', 552.24, NULL, NULL, '', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(72, '74', '4406', '0', '0', '0', 5199.08, NULL, NULL, '', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(73, '75', '2280', 'acdfdf', '213234', '0', 2280, NULL, NULL, '', '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(74, '76', '1737', '0', '0', '0', 2049.66, NULL, NULL, '', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(75, '77', '30262', 'acdfdf', '213234', '0', 30262, NULL, NULL, '', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(76, '78', '1035.00', 'acdfdf', '213234', '0', 1035, NULL, NULL, '', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(77, '79', '456', '0', '0', '0', 538.08, NULL, NULL, '', '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(78, '80', '666', 'acdfdf', '213234', '0', 666, NULL, NULL, '', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(79, '81', '6235', 'acdfdf', '213234', '0', 6235, NULL, NULL, '', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(80, '82', '234', 'acdfdf', '213234', '0', 234, NULL, NULL, '', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(81, '83', '3176', 'acdfdf', '213234', '0', 3176, NULL, NULL, '', '2018-05-12 12:07:48', '2018-05-12 12:07:48'),
(82, '84', '254', 'acdfdf', '213234', '0', 254, NULL, NULL, '', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(83, '85', '234', 'acdfdf', '213234', '0', 234, NULL, NULL, '', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(84, '86', '234', 'acdfdf', '213234', '0', 234, NULL, NULL, '', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(85, '87', '1368', 'acdfdf', '213234', '0', 1368, NULL, NULL, '', '2018-05-18 05:10:06', '2018-05-18 05:10:06');

-- --------------------------------------------------------

--
-- Table structure for table `orders_quick_actions_links`
--

CREATE TABLE `orders_quick_actions_links` (
  `id` int(50) NOT NULL,
  `buttonTitle` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_quick_actions_links`
--

INSERT INTO `orders_quick_actions_links` (`id`, `buttonTitle`, `type`) VALUES
(1, 'Send Email Invoice', 'emailInvoice'),
(2, 'Print Invoice (A4)', 'printInvoice'),
(3, 'Invoice Print', 'printInvoiceThermal');

-- --------------------------------------------------------

--
-- Table structure for table `orders_quick_actions_misc`
--

CREATE TABLE `orders_quick_actions_misc` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `buttonTitle` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_quick_actions_misc`
--

INSERT INTO `orders_quick_actions_misc` (`id`, `title`, `buttonTitle`, `type`) VALUES
(1, 'Order Status', 'Update', 'orderStatus'),
(2, 'Payment Received', 'Receive', 'orderPaymentReceived');

-- --------------------------------------------------------

--
-- Table structure for table `order_actions_auth_settings`
--

CREATE TABLE `order_actions_auth_settings` (
  `id` int(50) NOT NULL,
  `user_type` varchar(300) NOT NULL DEFAULT '',
  `assigned_status` varchar(300) NOT NULL DEFAULT '',
  `actions_sequence_order` varchar(300) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_actions_auth_settings`
--

INSERT INTO `order_actions_auth_settings` (`id`, `user_type`, `assigned_status`, `actions_sequence_order`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'processing,ready,completed,cancelled', '0', '2018-04-26 08:49:19', '0000-00-00 00:00:00'),
(2, 'Driver', 'Pending,Processing,Ready,Completed,Cancelled', '1', '2018-05-09 05:34:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_misc_actions`
--

CREATE TABLE `order_misc_actions` (
  `id` int(50) NOT NULL,
  `action_type` varchar(200) NOT NULL DEFAULT '',
  `user_type` varchar(200) NOT NULL DEFAULT '',
  `preorder_status` varchar(200) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_misc_actions`
--

INSERT INTO `order_misc_actions` (`id`, `action_type`, `user_type`, `preorder_status`, `created_at`, `updated_at`) VALUES
(1, 'signature', 'admin', 'completed', NULL, NULL),
(2, 'feedback', 'customer', 'completed', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(50) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `product_id` varchar(50) NOT NULL DEFAULT '',
  `quantity` varchar(50) NOT NULL DEFAULT '1',
  `title` varchar(50) NOT NULL DEFAULT '',
  `category_id` varchar(50) DEFAULT '',
  `discount` varchar(50) NOT NULL DEFAULT '0',
  `discounted_price` varchar(50) NOT NULL DEFAULT '',
  `unit` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `quantity`, `title`, `category_id`, `discount`, `discounted_price`, `unit`, `created_at`, `updated_at`) VALUES
(11, '6', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-02 06:44:48', '2018-05-02 06:44:48'),
(12, '7', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(13, '7', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(14, '7', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(15, '8', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(16, '8', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(17, '8', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(18, '9', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(19, '9', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(20, '9', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(21, '10', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(22, '10', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(23, '10', '240', '1', 'Hair Oil', '5', '0', '432', '', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(24, '11', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(25, '12', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(26, '13', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:35:01', '2018-05-02 08:35:01'),
(27, '14', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:36:02', '2018-05-02 08:36:02'),
(28, '15', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:36:20', '2018-05-02 08:36:20'),
(29, '16', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:36:44', '2018-05-02 08:36:44'),
(30, '17', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:36:57', '2018-05-02 08:36:57'),
(31, '18', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:37:44', '2018-05-02 08:37:44'),
(32, '19', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:38:41', '2018-05-02 08:38:41'),
(33, '20', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:38:50', '2018-05-02 08:38:50'),
(34, '21', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:39:08', '2018-05-02 08:39:08'),
(35, '22', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:40:23', '2018-05-02 08:40:23'),
(36, '23', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:40:34', '2018-05-02 08:40:34'),
(37, '24', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(38, '25', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(40, '26', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-02 10:56:05', '2018-05-02 10:56:05'),
(44, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:09:56', '2018-05-02 11:09:56'),
(45, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:10:32', '2018-05-02 11:10:32'),
(46, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:10:42', '2018-05-02 11:10:42'),
(47, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:10:55', '2018-05-02 11:10:55'),
(48, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:11:03', '2018-05-02 11:11:03'),
(49, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:11:16', '2018-05-02 11:11:16'),
(50, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:11:44', '2018-05-02 11:11:44'),
(51, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:12:22', '2018-05-02 11:12:22'),
(52, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:13:49', '2018-05-02 11:13:49'),
(53, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:14:01', '2018-05-02 11:14:01'),
(54, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:14:08', '2018-05-02 11:14:08'),
(55, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:14:38', '2018-05-02 11:14:38'),
(56, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:15:05', '2018-05-02 11:15:05'),
(57, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:15:11', '2018-05-02 11:15:11'),
(58, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:15:20', '2018-05-02 11:15:20'),
(59, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:16:09', '2018-05-02 11:16:09'),
(61, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-02 11:19:55', '2018-05-02 11:19:55'),
(62, '27', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-03 09:23:46', '2018-05-03 09:23:46'),
(63, '27', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(64, '30', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(65, '30', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(66, '30', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(67, '31', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(68, '31', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(69, '31', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(70, '32', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(71, '32', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(72, '32', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(74, '33', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(76, '34', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(77, '34', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(78, '35', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(79, '35', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(80, '36', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(81, '36', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(82, '37', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(83, '37', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(84, '26', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-04 06:42:05', '2018-05-04 06:42:05'),
(86, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-04 06:43:59', '2018-05-04 06:43:59'),
(87, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-04 10:11:59', '2018-05-04 10:11:59'),
(88, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-04 10:27:18', '2018-05-04 10:27:18'),
(89, '38', '236', '1', 'Deepu pack', '1', '0', '258', '', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(90, '38', '240', '1', 'Hair Oil', '5', '0', '432', '', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(91, '39', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(92, '39', '240', '1', 'Hair Oil', '5', '0', '432', '', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(93, '39', '236', '1', 'Deepu pack', '1', '0', '278', '', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(94, '40', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(95, '40', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(96, '40', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(97, '41', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(98, '41', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(99, '42', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(100, '42', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(101, '42', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(102, '43', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(103, '43', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(104, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-08 11:19:43', '2018-05-08 11:19:43'),
(105, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-08 11:59:30', '2018-05-08 11:59:30'),
(106, '44', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(107, '230', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-08 13:02:07', '2018-05-08 13:02:07'),
(108, '37', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-08 13:02:26', '2018-05-08 13:02:26'),
(109, '45', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(110, '46', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(111, '47', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(112, '48', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:36:21', '2018-05-09 04:36:21'),
(113, '49', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(114, '50', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(115, '51', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(116, '52', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(117, '53', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(118, '54', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(119, '55', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(120, '55', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(121, '56', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(122, '57', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(123, '58', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(124, '59', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(125, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:23:35', '2018-05-09 07:23:35'),
(126, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:24:22', '2018-05-09 07:24:22'),
(127, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:24:49', '2018-05-09 07:24:49'),
(128, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:25:14', '2018-05-09 07:25:14'),
(129, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:30:01', '2018-05-09 07:30:01'),
(130, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:31:51', '2018-05-09 07:31:51'),
(131, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:32:18', '2018-05-09 07:32:18'),
(132, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:32:49', '2018-05-09 07:32:49'),
(133, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:33:10', '2018-05-09 07:33:10'),
(134, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:34:22', '2018-05-09 07:34:22'),
(135, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:34:41', '2018-05-09 07:34:41'),
(136, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:34:56', '2018-05-09 07:34:56'),
(137, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:35:18', '2018-05-09 07:35:18'),
(138, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:35:46', '2018-05-09 07:35:46'),
(139, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:35:57', '2018-05-09 07:35:57'),
(140, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:36:14', '2018-05-09 07:36:14'),
(141, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:37:50', '2018-05-09 07:37:50'),
(142, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:38:21', '2018-05-09 07:38:21'),
(143, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 07:38:21', '2018-05-09 07:38:21'),
(144, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:17:06', '2018-05-09 08:17:06'),
(145, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:17:06', '2018-05-09 08:17:06'),
(146, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:17:57', '2018-05-09 08:17:57'),
(147, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:17:57', '2018-05-09 08:17:57'),
(148, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:18:13', '2018-05-09 08:18:13'),
(149, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:18:13', '2018-05-09 08:18:13'),
(150, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:18:25', '2018-05-09 08:18:25'),
(151, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:18:25', '2018-05-09 08:18:25'),
(152, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:18', '2018-05-09 08:19:18'),
(153, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:18', '2018-05-09 08:19:18'),
(154, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:43', '2018-05-09 08:19:43'),
(155, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:43', '2018-05-09 08:19:43'),
(156, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:50', '2018-05-09 08:19:50'),
(157, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:19:50', '2018-05-09 08:19:50'),
(158, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:14', '2018-05-09 08:20:14'),
(159, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:14', '2018-05-09 08:20:14'),
(160, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:26', '2018-05-09 08:20:26'),
(161, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:26', '2018-05-09 08:20:26'),
(162, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:36', '2018-05-09 08:20:36'),
(163, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:20:36', '2018-05-09 08:20:36'),
(164, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:23:07', '2018-05-09 08:23:07'),
(165, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:23:07', '2018-05-09 08:23:07'),
(166, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:29:23', '2018-05-09 08:29:23'),
(167, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:29:23', '2018-05-09 08:29:23'),
(168, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:29:47', '2018-05-09 08:29:47'),
(169, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:29:47', '2018-05-09 08:29:47'),
(170, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:30:04', '2018-05-09 08:30:04'),
(171, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:30:04', '2018-05-09 08:30:04'),
(172, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:30:19', '2018-05-09 08:30:19'),
(173, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-09 08:30:19', '2018-05-09 08:30:19'),
(174, '62', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(175, '69', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(176, '70', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(177, '71', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(178, '37', '242', '1', 'Hair Gel', '7', '0', '456', '', '2018-05-09 12:45:07', '2018-05-09 12:45:07'),
(179, '37', '242', '3', 'Hair Gel', '7', '0', '456', '', '2018-05-09 12:47:11', '2018-05-09 12:47:11'),
(180, '37', '242', '1', 'Hair Gel', '7', '0', '456', '', '2018-05-09 12:51:37', '2018-05-09 12:51:37'),
(181, '37', '242', '5', 'Hair Gel', '7', '0', '456', '', '2018-05-09 12:52:03', '2018-05-09 12:52:03'),
(182, '37', '239', '3', 'Honey', '4', '0', '234', '', '2018-05-09 12:52:03', '2018-05-09 12:52:03'),
(184, '72', '239', '2', 'Honey', '4', '0', '234', '', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(185, '72', '245', '1', 'Product 1', '8', '0', '389', '', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(186, '37', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-10 08:33:13', '2018-05-10 08:33:13'),
(187, '37', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-10 08:33:18', '2018-05-10 08:33:18'),
(188, '37', '242', '3', 'Hair Gel', '7', '0', '456', '', '2018-05-10 08:34:14', '2018-05-10 08:34:14'),
(189, '73', '239', '2', 'Honey', '4', '0', '234', '', '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(190, '74', '242', '7', 'Hair Gel', '7', '0', '456', '', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(191, '74', '236', '3', 'Deepu pack', '1', '0', '234', '', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(192, '74', '236', '2', 'Deepu pack', '1', '0', '234', '', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(193, '37', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-10 11:18:21', '2018-05-10 11:18:21'),
(194, '37', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-10 11:18:21', '2018-05-10 11:18:21'),
(195, '37', '242', '10', 'Hair Gel', '7', '0', '456', '', '2018-05-10 11:58:30', '2018-05-10 11:58:30'),
(197, '76', '237', '3', 'Product 3', '11', '0', '33', '', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(198, '76', '239', '4', 'Honey', '4', '0', '234', '', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(199, '76', '236', '3', 'Deepu pack', '1', '0', '234', '', '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(201, '77', '245', '1', 'Product 1', '8', '0', '389', '', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(206, '78', '230', '1', 'dff', '1', '5', '433.2', '', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(207, '78', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(208, '78', '238', '1', 'Colgate', '3', '0', '345', '', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(209, '37', '242', '4', 'Hair Gel', '7', '0', '456', '', '2018-05-11 11:01:45', '2018-05-11 11:01:45'),
(210, '37', '236', '4', 'Deepu pack', '1', '0', '234', '', '2018-05-11 11:48:04', '2018-05-11 11:48:04'),
(212, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:27:56', '2018-05-12 04:27:56'),
(213, '37', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:27:56', '2018-05-12 04:27:56'),
(214, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:57:58', '2018-05-12 04:57:58'),
(215, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:57:58', '2018-05-12 04:57:58'),
(216, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:58:20', '2018-05-12 04:58:20'),
(217, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:58:20', '2018-05-12 04:58:20'),
(218, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:58:38', '2018-05-12 04:58:38'),
(219, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 04:58:38', '2018-05-12 04:58:38'),
(220, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 05:30:58', '2018-05-12 05:30:58'),
(221, '74', '245', '1', 'Product 1', '8', '0', '33', '', '2018-05-12 05:30:58', '2018-05-12 05:30:58'),
(222, '74', '245', '1', 'Product 1', '8', '0', '77', '', '2018-05-12 07:19:26', '2018-05-12 07:19:26'),
(223, '74', '245', '1', 'Product 1', '8', '0', '77', '', '2018-05-12 07:19:26', '2018-05-12 07:19:26'),
(224, '37', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-12 07:33:35', '2018-05-12 07:33:35'),
(225, '37', '239', '3', 'Honey', '4', '0', '234', '', '2018-05-12 07:33:35', '2018-05-12 07:33:35'),
(235, '80', '240', '1', 'Hair Oil', '5', '0', '432', '', '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(238, '72', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-12 10:18:42', '2018-05-12 10:18:42'),
(239, '81', '239', '5', 'Honey', '4', '0', '234', '', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(240, '81', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(244, '81', '249', '1', 'dfdfdf', '14', '0', '4545', '', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(246, '82', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(249, '83', '236', '4', 'Deepu pack', '1', '0', '254', '', '2018-05-14 03:19:56', '2018-05-14 03:19:56'),
(250, '84', '236', '1', 'Deepu pack', '1', '0', '254', '', '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(251, '85', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(252, '86', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(253, '87', '241', '1', 'Face Cream', '6', '0', '444', '', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(254, '87', '242', '1', 'Hair Gel', '7', '0', '456', '', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(255, '87', '239', '1', 'Honey', '4', '0', '234', '', '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(256, '87', '236', '1', 'Deepu pack', '1', '0', '234', '', '2018-05-18 05:10:06', '2018-05-18 05:10:06');

-- --------------------------------------------------------

--
-- Table structure for table `order_products_variants`
--

CREATE TABLE `order_products_variants` (
  `id` int(50) NOT NULL,
  `order_product_id` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(300) NOT NULL DEFAULT '',
  `value` varchar(300) NOT NULL DEFAULT '',
  `product_variant_id` varchar(300) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_products_variants`
--

INSERT INTO `order_products_variants` (`id`, `order_product_id`, `title`, `value`, `product_variant_id`, `created_at`, `updated_at`) VALUES
(7, '12', 'Size', 'fdgdgd', '4', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(8, '15', 'Size', 'fdgdgd', '4', '2018-05-02 06:44:51', '2018-05-02 06:44:51'),
(9, '18', 'Size', 'fdgdgd', '4', '2018-05-02 06:45:26', '2018-05-02 06:45:26'),
(10, '21', 'Size', 'Small', '53', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(11, '21', 'Color', 'Blue', '54', '2018-05-02 07:18:54', '2018-05-02 07:18:54'),
(12, '24', 'Color', 'Blue', '54', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(13, '24', 'Color', 'Blue', '55', '2018-05-02 08:22:17', '2018-05-02 08:22:17'),
(14, '25', 'Color', 'Blue', '54', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(15, '25', 'Color', 'Blue', '55', '2018-05-02 08:26:39', '2018-05-02 08:26:39'),
(16, '27', 'Color', 'Blue', '54', '2018-05-02 08:36:02', '2018-05-02 08:36:02'),
(17, '27', 'Color', 'Blue', '55', '2018-05-02 08:36:02', '2018-05-02 08:36:02'),
(18, '28', 'Color', 'Blue', '54', '2018-05-02 08:36:20', '2018-05-02 08:36:20'),
(19, '33', 'Color', 'Blue', '54', '2018-05-02 08:38:50', '2018-05-02 08:38:50'),
(20, '33', 'Color', 'Blue', '55', '2018-05-02 08:38:50', '2018-05-02 08:38:50'),
(21, '34', 'Color', 'Blue', '54', '2018-05-02 08:39:08', '2018-05-02 08:39:08'),
(22, '34', 'Color', 'Blue', '55', '2018-05-02 08:39:08', '2018-05-02 08:39:08'),
(23, '37', 'Color', 'Blue', '54', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(24, '37', 'Color', 'Red', '55', '2018-05-02 08:40:52', '2018-05-02 08:40:52'),
(25, '38', 'Color', 'Blue', '54', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(26, '38', 'Color', 'Red', '55', '2018-05-02 09:06:18', '2018-05-02 09:06:18'),
(28, '61', 'Size', 'vbbbbbbbb', '4', '2018-05-02 11:19:55', '2018-05-02 11:19:55'),
(29, '62', 'Size', 'Small', '53', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(30, '62', 'Color', 'Blue', '54', '2018-05-03 09:23:47', '2018-05-03 09:23:47'),
(31, '64', 'Size', 'vbbbbbbbb', '4', '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(32, '67', 'Size', 'vbbbbbbbb', '4', '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(33, '70', 'Size', 'vbbbbbbbb', '4', '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(35, '76', 'Size', 'Small', '53', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(36, '76', 'Color', 'Blue', '54', '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(37, '78', 'Size', 'Small', '53', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(38, '78', 'Color', 'Blue', '54', '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(39, '80', 'Size', 'Small', '53', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(40, '80', 'Color', 'Blue', '54', '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(41, '82', 'Color', 'Blue', '54', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(42, '82', 'Size', 'Small', '53', '2018-05-04 05:30:09', '2018-05-04 05:30:09'),
(43, '84', 'Size', 'vbbbbbbbb', '4', '2018-05-04 06:42:05', '2018-05-04 06:42:05'),
(45, '86', 'Size', 'vbbbbbbbb', '4', '2018-05-04 06:43:59', '2018-05-04 06:43:59'),
(46, '87', 'Size', 'vbbbbbbbb', '4', '2018-05-04 10:11:59', '2018-05-04 10:11:59'),
(47, '88', 'Size', 'vbbbbbbbb', '4', '2018-05-04 10:27:18', '2018-05-04 10:27:18'),
(48, '89', 'Color', 'Red', '55', '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(49, '93', 'Color', 'Red', '55', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(50, '93', 'Color', 'Blue', '54', '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(51, '94', 'Size', 'vbbbbbbbb', '4', '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(52, '99', 'Size', 'vbbbbbbbb', '4', '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(53, '104', 'Size', 'vbbbbbbbb', '4', '2018-05-08 11:19:43', '2018-05-08 11:19:43'),
(54, '105', 'Size', 'vbbbbbbbb', '4', '2018-05-08 11:59:30', '2018-05-08 11:59:30'),
(55, '107', 'Size', 'vbbbbbbbb', '4', '2018-05-08 13:02:07', '2018-05-08 13:02:07'),
(56, '108', 'Size', 'vbbbbbbbb', '4', '2018-05-08 13:02:26', '2018-05-08 13:02:26'),
(57, '127', 'Size', 'Small', '51', '2018-05-09 07:24:49', '2018-05-09 07:24:49'),
(58, '128', 'Size', 'Small', '51', '2018-05-09 07:25:14', '2018-05-09 07:25:14'),
(59, '129', 'Size', 'Small', '51', '2018-05-09 07:30:01', '2018-05-09 07:30:01'),
(60, '130', 'Size', 'Small', '51', '2018-05-09 07:31:51', '2018-05-09 07:31:51'),
(61, '131', 'Size', 'Small', '51', '2018-05-09 07:32:18', '2018-05-09 07:32:18'),
(62, '132', 'Size', 'Small', '51', '2018-05-09 07:32:49', '2018-05-09 07:32:49'),
(63, '133', 'Size', 'Small', '51', '2018-05-09 07:33:10', '2018-05-09 07:33:10'),
(64, '134', 'Size', 'Small', '51', '2018-05-09 07:34:22', '2018-05-09 07:34:22'),
(65, '135', 'Size', 'Small', '51', '2018-05-09 07:34:41', '2018-05-09 07:34:41'),
(66, '136', 'Size', 'Small', '51', '2018-05-09 07:34:56', '2018-05-09 07:34:56'),
(67, '137', 'Size', 'Small', '51', '2018-05-09 07:35:18', '2018-05-09 07:35:18'),
(68, '138', 'Size', 'Small', '51', '2018-05-09 07:35:47', '2018-05-09 07:35:47'),
(69, '139', 'Size', 'Small', '51', '2018-05-09 07:35:57', '2018-05-09 07:35:57'),
(70, '140', 'Size', 'Small', '51', '2018-05-09 07:36:14', '2018-05-09 07:36:14'),
(71, '141', 'Size', 'Small', '51', '2018-05-09 07:37:50', '2018-05-09 07:37:50'),
(72, '142', 'Size', 'Small', '51', '2018-05-09 07:38:21', '2018-05-09 07:38:21'),
(73, '143', 'Size', 'Small', '51', '2018-05-09 07:38:21', '2018-05-09 07:38:21'),
(74, '144', 'Size', 'Small', '51', '2018-05-09 08:17:06', '2018-05-09 08:17:06'),
(75, '145', 'Size', 'Small', '51', '2018-05-09 08:17:06', '2018-05-09 08:17:06'),
(76, '146', 'Size', 'Small', '51', '2018-05-09 08:17:57', '2018-05-09 08:17:57'),
(77, '147', 'Size', 'Small', '51', '2018-05-09 08:17:57', '2018-05-09 08:17:57'),
(78, '148', 'Size', 'Small', '51', '2018-05-09 08:18:13', '2018-05-09 08:18:13'),
(79, '149', 'Size', 'Small', '51', '2018-05-09 08:18:13', '2018-05-09 08:18:13'),
(80, '150', 'Size', 'Small', '51', '2018-05-09 08:18:25', '2018-05-09 08:18:25'),
(81, '151', 'Size', 'Small', '51', '2018-05-09 08:18:25', '2018-05-09 08:18:25'),
(82, '152', 'Size', 'Small', '51', '2018-05-09 08:19:18', '2018-05-09 08:19:18'),
(83, '153', 'Size', 'Small', '51', '2018-05-09 08:19:18', '2018-05-09 08:19:18'),
(84, '154', 'Size', 'Small', '51', '2018-05-09 08:19:43', '2018-05-09 08:19:43'),
(85, '155', 'Size', 'Small', '51', '2018-05-09 08:19:43', '2018-05-09 08:19:43'),
(86, '156', 'Size', 'Small', '51', '2018-05-09 08:19:50', '2018-05-09 08:19:50'),
(87, '157', 'Size', 'Small', '51', '2018-05-09 08:19:50', '2018-05-09 08:19:50'),
(88, '158', 'Size', 'Small', '51', '2018-05-09 08:20:14', '2018-05-09 08:20:14'),
(89, '159', 'Size', 'Small', '51', '2018-05-09 08:20:14', '2018-05-09 08:20:14'),
(90, '160', 'Size', 'Small', '51', '2018-05-09 08:20:26', '2018-05-09 08:20:26'),
(91, '161', 'Size', 'Small', '51', '2018-05-09 08:20:26', '2018-05-09 08:20:26'),
(92, '162', 'Size', 'Small', '51', '2018-05-09 08:20:36', '2018-05-09 08:20:36'),
(93, '163', 'Size', 'Small', '51', '2018-05-09 08:20:36', '2018-05-09 08:20:36'),
(94, '164', 'Size', 'Small', '51', '2018-05-09 08:23:07', '2018-05-09 08:23:07'),
(95, '165', 'Size', 'Small', '51', '2018-05-09 08:23:07', '2018-05-09 08:23:07'),
(96, '166', 'Size', 'Small', '51', '2018-05-09 08:29:23', '2018-05-09 08:29:23'),
(97, '167', 'Size', 'Small', '51', '2018-05-09 08:29:23', '2018-05-09 08:29:23'),
(98, '168', 'Size', 'Small', '51', '2018-05-09 08:29:47', '2018-05-09 08:29:47'),
(99, '169', 'Size', 'Small', '51', '2018-05-09 08:29:47', '2018-05-09 08:29:47'),
(100, '170', 'Size', 'Small', '51', '2018-05-09 08:30:04', '2018-05-09 08:30:04'),
(101, '171', 'Size', 'Small', '51', '2018-05-09 08:30:04', '2018-05-09 08:30:04'),
(102, '172', 'Size', 'Small', '51', '2018-05-09 08:30:19', '2018-05-09 08:30:19'),
(103, '173', 'Size', 'Small', '51', '2018-05-09 08:30:19', '2018-05-09 08:30:19'),
(104, '185', 'Size', 'Small', '51', '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(105, '191', 'Size', 'Small', '53', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(106, '191', 'Color', 'Blue', '54', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(107, '192', 'Color', 'Red', '55', '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(108, '201', 'Size', 'Small', '51', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(109, '201', 'Size', 'Bigger', '52', '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(110, '206', 'Size', 'vbbbbbbbb', '4', '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(111, '210', 'Size', 'Small', '53', '2018-05-11 11:48:04', '2018-05-11 11:48:04'),
(112, '212', 'Size', 'Small', '51', '2018-05-12 04:27:56', '2018-05-12 04:27:56'),
(113, '213', 'Size', 'Small', '51', '2018-05-12 04:27:56', '2018-05-12 04:27:56'),
(114, '214', 'Size', 'Small', '51', '2018-05-12 04:57:58', '2018-05-12 04:57:58'),
(115, '215', 'Size', 'Small', '51', '2018-05-12 04:57:58', '2018-05-12 04:57:58'),
(116, '216', 'Size', 'Small', '51', '2018-05-12 04:58:20', '2018-05-12 04:58:20'),
(117, '217', 'Size', 'Small', '51', '2018-05-12 04:58:20', '2018-05-12 04:58:20'),
(118, '218', 'Size', 'Small', '51', '2018-05-12 04:58:38', '2018-05-12 04:58:38'),
(119, '219', 'Size', 'Small', '51', '2018-05-12 04:58:38', '2018-05-12 04:58:38'),
(120, '220', 'Size', 'Small', '51', '2018-05-12 05:30:58', '2018-05-12 05:30:58'),
(121, '221', 'Size', 'Small', '51', '2018-05-12 05:30:58', '2018-05-12 05:30:58'),
(122, '222', 'Size', 'Small', '51', '2018-05-12 07:19:26', '2018-05-12 07:19:26'),
(123, '223', 'Size', 'Small', '51', '2018-05-12 07:19:26', '2018-05-12 07:19:26'),
(124, '224', 'Color', 'Blue', '54', '2018-05-12 07:33:35', '2018-05-12 07:33:35'),
(133, '238', 'Color', 'Blue', '54', '2018-05-12 10:18:42', '2018-05-12 10:18:42'),
(134, '240', 'Size', 'Small', '53', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(135, '240', 'Color', 'Blue', '54', '2018-05-12 10:19:51', '2018-05-12 10:19:51'),
(139, '249', 'Size', 'Small', '53', '2018-05-14 03:19:56', '2018-05-14 03:19:56'),
(140, '249', 'Color', 'Blue', '54', '2018-05-14 03:19:56', '2018-05-14 03:19:56'),
(141, '250', 'Color', 'Blue', '54', '2018-05-14 05:23:54', '2018-05-14 05:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `color` varchar(100) NOT NULL DEFAULT '',
  `label_colors` varchar(50) NOT NULL DEFAULT '',
  `identifier` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT '',
  `customer_notify` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `title`, `color`, `label_colors`, `identifier`, `type`, `customer_notify`, `created_at`, `updated_at`) VALUES
(1, 'Pending', '#22A9F2', '#FFF1C0', 'pending', 'new_order', '1', NULL, NULL),
(2, 'Processing', '#0074C1', '#DEF1FD', 'processing', 'processing_order', '1', NULL, NULL),
(3, 'Ready', '#FB7E02', '#FFE4CE', 'ready', 'ready_order', '1', NULL, NULL),
(4, 'Completed', '#42C100', '#E5F8D2', 'completed', 'completed_order', '1', NULL, NULL),
(5, 'Cancelled', '#D8D8D8', '#F0F0F0', 'cancelled', 'cancelled_order', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_tax_transactions`
--

CREATE TABLE `order_tax_transactions` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `tax_id` varchar(50) NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tax_transactions`
--

INSERT INTO `order_tax_transactions` (`id`, `order_id`, `tax_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, '30', '1', 0, '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(2, '30', '2', 0, '2018-05-03 12:26:37', '2018-05-03 12:26:37'),
(3, '31', '1', 93.15, '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(4, '31', '2', 186.3, '2018-05-03 12:26:59', '2018-05-03 12:26:59'),
(5, '32', '1', 93.15, '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(6, '32', '2', 93.15, '2018-05-03 12:27:18', '2018-05-03 12:27:18'),
(7, '33', '1', 93.15, '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(8, '33', '2', 93.15, '2018-05-03 12:31:25', '2018-05-03 12:31:25'),
(9, '34', '1', 43.92, '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(10, '34', '2', 43.92, '2018-05-04 04:18:20', '2018-05-04 04:18:20'),
(11, '35', '1', 43.92, '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(12, '35', '2', 43.92, '2018-05-04 04:19:14', '2018-05-04 04:19:14'),
(13, '36', '1', 43.92, '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(14, '36', '2', 43.92, '2018-05-04 05:18:51', '2018-05-04 05:18:51'),
(15, '37', '1', -19.02799999999999, '2018-05-12 11:16:31', '2018-05-12 11:16:31'),
(16, '37', '2', 0.9720000000000084, '2018-05-12 11:16:31', '2018-05-12 11:16:31'),
(17, '38', '1', 62.1, '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(18, '38', '2', 62.1, '2018-05-04 11:43:43', '2018-05-04 11:43:43'),
(19, '39', '1', 84.96, '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(20, '39', '2', 84.96, '2018-05-04 12:18:11', '2018-05-04 12:18:11'),
(21, '40', '1', 93.15, '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(22, '40', '2', 93.15, '2018-05-07 11:46:38', '2018-05-07 11:46:38'),
(23, '41', '1', 93.15, '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(24, '41', '2', 93.15, '2018-05-08 05:33:56', '2018-05-08 05:33:56'),
(25, '42', '1', 93.15, '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(26, '42', '2', 93.15, '2018-05-08 09:25:22', '2018-05-08 09:25:22'),
(27, '43', '1', 93.15, '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(28, '43', '2', 93.15, '2018-05-08 11:13:53', '2018-05-08 11:13:53'),
(29, '44', '1', 195.21, '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(30, '44', '2', 195.21, '2018-05-08 12:40:22', '2018-05-08 12:40:22'),
(31, '45', '1', 195.21, '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(32, '45', '2', 195.21, '2018-05-09 04:32:36', '2018-05-09 04:32:36'),
(33, '46', '1', 195.21, '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(34, '46', '2', 195.21, '2018-05-09 04:34:41', '2018-05-09 04:34:41'),
(35, '47', '1', 195.21, '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(36, '47', '2', 195.21, '2018-05-09 04:35:38', '2018-05-09 04:35:38'),
(37, '48', '1', 195.21, '2018-05-09 04:36:21', '2018-05-09 04:36:21'),
(38, '48', '2', 195.21, '2018-05-09 04:36:21', '2018-05-09 04:36:21'),
(39, '49', '1', 195.21, '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(40, '49', '2', 195.21, '2018-05-09 04:36:42', '2018-05-09 04:36:42'),
(41, '50', '1', 195.21, '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(42, '50', '2', 195.21, '2018-05-09 04:37:51', '2018-05-09 04:37:51'),
(43, '51', '1', 195.21, '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(44, '51', '2', 195.21, '2018-05-09 04:38:05', '2018-05-09 04:38:05'),
(45, '52', '1', 195.21, '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(46, '52', '2', 195.21, '2018-05-09 04:54:58', '2018-05-09 04:54:58'),
(47, '53', '1', 195.21, '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(48, '53', '2', 195.21, '2018-05-09 04:56:45', '2018-05-09 04:56:45'),
(49, '54', '1', 195.21, '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(50, '54', '2', 195.21, '2018-05-09 04:56:49', '2018-05-09 04:56:49'),
(51, '55', '1', 93.15, '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(52, '55', '2', 93.15, '2018-05-09 04:58:17', '2018-05-09 04:58:17'),
(53, '56', '1', 195.21, '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(54, '56', '2', 195.21, '2018-05-09 04:59:27', '2018-05-09 04:59:27'),
(55, '57', '1', 195.21, '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(56, '57', '2', 195.21, '2018-05-09 05:00:06', '2018-05-09 05:00:06'),
(57, '58', '1', 195.21, '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(58, '58', '2', 195.21, '2018-05-09 05:00:20', '2018-05-09 05:00:20'),
(59, '59', '1', 195.21, '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(60, '59', '2', 195.21, '2018-05-09 05:01:17', '2018-05-09 05:01:17'),
(61, '60', '1', 42.12, '2018-05-09 10:06:37', '2018-05-09 10:06:37'),
(62, '60', '2', 42.12, '2018-05-09 10:06:37', '2018-05-09 10:06:37'),
(63, '61', '1', 42.12, '2018-05-09 10:11:28', '2018-05-09 10:11:28'),
(64, '61', '2', 42.12, '2018-05-09 10:11:28', '2018-05-09 10:11:28'),
(65, '62', '1', 195.21, '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(66, '62', '2', 195.21, '2018-05-09 10:11:40', '2018-05-09 10:11:40'),
(67, '63', '1', 42.12, '2018-05-09 10:14:45', '2018-05-09 10:14:45'),
(68, '63', '2', 42.12, '2018-05-09 10:14:45', '2018-05-09 10:14:45'),
(69, '64', '1', 42.12, '2018-05-09 10:16:51', '2018-05-09 10:16:51'),
(70, '64', '2', 42.12, '2018-05-09 10:16:51', '2018-05-09 10:16:51'),
(71, '65', '1', 42.12, '2018-05-09 10:17:04', '2018-05-09 10:17:04'),
(72, '65', '2', 42.12, '2018-05-09 10:17:04', '2018-05-09 10:17:04'),
(73, '66', '1', 42.12, '2018-05-09 10:17:34', '2018-05-09 10:17:34'),
(74, '66', '2', 42.12, '2018-05-09 10:17:34', '2018-05-09 10:17:34'),
(75, '67', '1', 42.12, '2018-05-09 10:17:58', '2018-05-09 10:17:58'),
(76, '67', '2', 42.12, '2018-05-09 10:17:58', '2018-05-09 10:17:58'),
(77, '68', '1', 21.06, '2018-05-09 10:19:03', '2018-05-09 10:19:03'),
(78, '68', '2', 21.06, '2018-05-09 10:19:03', '2018-05-09 10:19:03'),
(79, '69', '1', 21.06, '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(80, '69', '2', 21.06, '2018-05-09 10:21:53', '2018-05-09 10:21:53'),
(81, '70', '1', 21.06, '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(82, '70', '2', 21.06, '2018-05-09 10:40:57', '2018-05-09 10:40:57'),
(83, '71', '1', 21.06, '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(84, '71', '2', 21.06, '2018-05-09 10:44:48', '2018-05-09 10:44:48'),
(85, '72', '1', 271.53, '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(86, '72', '2', 271.53, '2018-05-10 04:17:43', '2018-05-10 04:17:43'),
(87, '73', '1', 42.12, '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(88, '73', '2', 42.12, '2018-05-10 08:41:14', '2018-05-10 08:41:14'),
(89, '74', '1', 396.54, '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(90, '74', '2', 396.54, '2018-05-10 09:26:35', '2018-05-10 09:26:35'),
(91, '75', '1', 205.2, '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(92, '75', '2', 205.2, '2018-05-11 03:50:29', '2018-05-11 03:50:29'),
(93, '76', '1', 156.33, '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(94, '76', '2', 156.33, '2018-05-11 05:01:49', '2018-05-11 05:01:49'),
(95, '77', '1', 2723.58, '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(96, '77', '2', 2723.58, '2018-05-11 05:49:51', '2018-05-11 05:49:51'),
(97, '78', '1', 93.15, '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(98, '78', '2', 93.15, '2018-05-11 10:52:49', '2018-05-11 10:52:49'),
(99, '79', '1', 41.04, '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(100, '79', '2', 41.04, '2018-05-11 12:15:18', '2018-05-11 12:15:18'),
(101, '80', '1', 59.94, '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(102, '80', '2', 59.94, '2018-05-12 09:05:47', '2018-05-12 09:05:47'),
(103, '81', '1', 561.15, '2018-05-12 10:34:01', '2018-05-12 10:34:01'),
(104, '81', '2', 561.15, '2018-05-12 10:34:01', '2018-05-12 10:34:01'),
(105, '82', '1', 21.06, '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(106, '82', '2', 21.06, '2018-05-12 12:05:43', '2018-05-12 12:05:43'),
(107, '83', '1', 91.43999999999997, '2018-05-14 03:19:56', '2018-05-14 03:19:56'),
(108, '83', '2', 91.43999999999997, '2018-05-14 03:19:56', '2018-05-14 03:19:56'),
(109, '84', '1', 22.86, '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(110, '84', '2', 22.86, '2018-05-14 05:23:54', '2018-05-14 05:23:54'),
(111, '85', '1', 21.06, '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(112, '85', '2', 21.06, '2018-05-14 05:24:48', '2018-05-14 05:24:48'),
(113, '86', '1', 21.06, '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(114, '86', '2', 21.06, '2018-05-14 05:26:59', '2018-05-14 05:26:59'),
(115, '87', '1', 123.12, '2018-05-18 05:10:06', '2018-05-18 05:10:06'),
(116, '87', '2', 123.12, '2018-05-18 05:10:06', '2018-05-18 05:10:06');

-- --------------------------------------------------------

--
-- Table structure for table `order_transactions`
--

CREATE TABLE `order_transactions` (
  `id` int(50) NOT NULL,
  `order_id` varchar(100) NOT NULL DEFAULT '',
  `amount` double NOT NULL DEFAULT '0',
  `payment_gateway` varchar(300) NOT NULL DEFAULT '',
  `transaction_id` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_transactions`
--

INSERT INTO `order_transactions` (`id`, `order_id`, `amount`, `payment_gateway`, `transaction_id`, `created_at`, `updated_at`) VALUES
(1, '27', 40, 'stripe', 'drtrtrt', '2018-05-03 09:31:36', '2018-05-03 09:31:36'),
(2, '72', 4566, 'stripe', 'yty', '2018-05-10 07:26:34', '2018-05-10 07:26:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

CREATE TABLE `payment_gateways` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `identifier` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `title`, `identifier`, `created_at`, `updated_at`) VALUES
(1, 'Stripe', 'stripe', '2018-04-24 18:30:00', '2018-04-24 18:30:00'),
(2, 'Paypal', 'paypal', '2018-04-24 18:30:00', '2018-04-24 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `category`, `created_at`, `updated_at`) VALUES
(1, 'view_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(2, 'add_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(3, 'edit_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(4, 'delete_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(5, 'view_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(6, 'add_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(7, 'edit_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(8, 'delete_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(9, 'add_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(10, 'edit_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(11, 'view_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(12, 'delete_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(13, 'add_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(14, 'edit_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(15, 'view_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(16, 'delete_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(17, 'add_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(18, 'edit_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(19, 'view_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(20, 'delete_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(21, 'add_categories', 'web', 'categories', NULL, NULL),
(22, 'edit_categories', 'web', 'categories', NULL, NULL),
(23, 'view_categories', 'web', 'categories', NULL, NULL),
(24, 'delete_categories', 'web', 'categories', NULL, NULL),
(25, 'add_faqs', 'web', 'faqs', NULL, NULL),
(26, 'edit_faqs', 'web', 'faqs', NULL, NULL),
(27, 'view_faqs', 'web', 'faqs', NULL, NULL),
(28, 'delete_faqs', 'web', 'faqs', NULL, NULL),
(29, 'add_transactions', 'web', 'transactions', NULL, NULL),
(30, 'edit_transactions', 'web', 'transactions', NULL, NULL),
(31, 'view_transactions', 'web', 'transactions', NULL, NULL),
(32, 'delete_transactions', 'web', 'transactions', NULL, NULL),
(33, 'view_features_settings', 'web', 'features_settings', NULL, NULL),
(34, 'view_reports', 'web', 'reports', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `category_ids` varchar(100) DEFAULT NULL,
  `base_price` int(50) NOT NULL DEFAULT '0',
  `base_discount` int(50) NOT NULL DEFAULT '0',
  `stock_count` varchar(50) NOT NULL DEFAULT '0',
  `stock_count_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'products,variants',
  `discount_expiry_date` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(1000) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT '1',
  `brand_ids` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `category_ids`, `base_price`, `base_discount`, `stock_count`, `stock_count_type`, `discount_expiry_date`, `photo`, `status`, `brand_ids`, `created_at`, `updated_at`) VALUES
(236, 'Deepu pack', '1', 234, 0, '0', '', '', NULL, '1', '2', '2018-04-19 12:57:42', '2018-04-19 12:57:42'),
(237, 'Product 3', '11', 33, 0, '0', '', '', NULL, '1', '2', '2018-04-24 11:13:15', '2018-04-24 11:13:15'),
(238, 'Colgate', '3', 345, 0, '0', '', '', NULL, '1', '2', '2018-04-24 11:30:06', '2018-04-24 11:30:06'),
(239, 'Honey', '4', 234, 0, '0', '', '', NULL, '1', '2', '2018-04-24 11:30:23', '2018-04-24 11:30:23'),
(240, 'Hair Oil', '5', 432, 0, '0', '', '', NULL, '1', '2', '2018-04-24 11:30:40', '2018-04-24 11:30:40'),
(241, 'Face Cream', '6', 444, 0, '0', '', '', NULL, '1', '2', '2018-04-24 11:30:54', '2018-04-24 11:30:54'),
(242, 'Hair Gel', '7', 456, 0, '0', '', '', NULL, '1', '3', '2018-04-24 11:31:24', '2018-04-24 11:31:24'),
(245, 'Product 1', '8', 345, 0, '0', '', '', NULL, '1', '2', '2018-04-30 04:45:53', '2018-05-01 10:30:40'),
(246, 'hdgfhsdgf', '10', 44, 0, '0', '', '', '5af6875df2f0e_1526105949.png', '1', '2', '2018-05-11 06:07:24', '2018-05-12 06:19:11'),
(247, 'dfdfdfdf', '14', 45, 0, '0', '', '', '5af67bc078969_1526102976.png', '1', '2', '2018-05-12 05:15:54', '2018-05-12 05:29:38'),
(248, 'dfdfd', '14', 44, 0, '0', '', '', '5af67bae7556c_1526102958.PNG', '1', '2', '2018-05-12 05:18:49', '2018-05-12 05:29:20'),
(249, 'dfdfdf', '14', 4545, 0, '0', '', '', '5afe72841740d_1526624900.png', '1', '2', '2018-05-12 05:23:01', '2018-05-18 06:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `product_meta_types`
--

CREATE TABLE `product_meta_types` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `identifier` varchar(200) NOT NULL DEFAULT '',
  `limit` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_meta_types`
--

INSERT INTO `product_meta_types` (`id`, `title`, `identifier`, `limit`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Short Description', 'short_description', '', 'longText', '2018-02-04 18:30:00', '2018-02-04 18:30:00'),
(2, 'Full Description', 'full_description', '', 'longText', '2018-02-04 18:30:00', '2018-02-04 18:30:00'),
(3, 'Medium Description', 'medium_description', '', 'longText', '2018-02-04 18:30:00', '2018-02-04 18:30:00'),
(4, 'Product Images', 'product_images', '1', 'File', '2018-02-05 18:30:00', '2018-02-05 18:30:00'),
(5, 'Unit', 'unit', '', 'string', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_meta_values`
--

CREATE TABLE `product_meta_values` (
  `id` int(50) NOT NULL,
  `product_meta_type_id` varchar(50) NOT NULL DEFAULT '',
  `product_id` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(20000) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_meta_values`
--

INSERT INTO `product_meta_values` (`id`, `product_meta_type_id`, `product_id`, `value`, `created_at`, `updated_at`) VALUES
(236, '1', '222', 'dfdf', '2018-04-12 09:14:42', '2018-04-12 09:14:42'),
(237, '2', '222', 'dfdf', '2018-04-12 09:14:42', '2018-04-12 09:14:51'),
(238, '3', '222', 'fdf', '2018-04-12 09:14:42', '2018-04-12 09:14:51'),
(239, '4', '222', 'dfddffgfgfgfggfg', '2018-04-12 09:14:42', '2018-04-12 09:15:00'),
(240, '5', '222', 'dfdfdf', '2018-04-12 09:14:42', '2018-04-12 09:14:51'),
(241, '1', '226', 'iuyiuy', '2018-04-16 09:27:44', '2018-04-16 13:48:07'),
(242, '2', '226', 'bhjgyjgyutt', '2018-04-16 09:27:44', '2018-04-16 13:50:49'),
(243, '3', '226', 'yyjgyutu', '2018-04-16 09:27:44', '2018-04-16 13:50:49'),
(244, '4', '226', NULL, '2018-04-16 09:27:44', '2018-04-16 09:27:44'),
(245, '5', '226', 'Kg', '2018-04-16 09:27:44', '2018-04-16 13:50:49'),
(246, '1', '223', 'etert', '2018-04-16 09:32:49', '2018-04-16 09:39:35'),
(247, '2', '223', 'etet', '2018-04-16 09:32:49', '2018-04-16 09:39:35'),
(248, '3', '223', 'ertret', '2018-04-16 09:32:49', '2018-04-16 09:39:35'),
(249, '4', '223', 'ertret', '2018-04-16 09:32:49', '2018-04-16 09:39:35'),
(250, '5', '223', 'kg', '2018-04-16 09:32:49', '2018-04-16 09:39:35'),
(251, '1', '225', 'sdsdsdsd', '2018-04-16 09:44:36', '2018-04-16 09:44:36'),
(252, '2', '225', NULL, '2018-04-16 09:44:36', '2018-04-16 09:44:36'),
(253, '3', '225', NULL, '2018-04-16 09:44:36', '2018-04-16 09:44:36'),
(254, '4', '225', NULL, '2018-04-16 09:44:36', '2018-04-16 09:44:36'),
(255, '5', '225', 'kg', '2018-04-16 09:44:36', '2018-04-16 09:44:40'),
(256, '1', '231', 'fdd', '2018-04-18 03:55:55', '2018-04-18 03:55:55'),
(257, '2', '231', 'fdfd', '2018-04-18 03:55:55', '2018-04-18 03:55:55'),
(258, '3', '231', 'fdf', '2018-04-18 03:55:55', '2018-04-18 03:55:55'),
(259, '4', '231', 'dfdf', '2018-04-18 03:55:55', '2018-04-18 03:55:55'),
(260, '5', '231', 'dfd', '2018-04-18 03:55:55', '2018-04-18 03:55:55'),
(261, '1', '232', 'kjsghdjgsdfjg', '2018-04-18 04:38:09', '2018-04-18 04:38:09'),
(262, '2', '232', 'guyg', '2018-04-18 04:38:09', '2018-04-18 04:38:09'),
(263, '3', '232', 'gui', '2018-04-18 04:38:09', '2018-04-18 04:38:09'),
(264, '4', '232', 'iguui', '2018-04-18 04:38:09', '2018-04-18 04:38:09'),
(265, '5', '232', 'iguugi', '2018-04-18 04:38:09', '2018-04-18 04:38:09'),
(266, '1', '233', 'ffggf', '2018-04-18 04:53:40', '2018-04-18 04:58:22'),
(267, '2', '233', 'fgf', '2018-04-18 04:53:40', '2018-04-18 04:58:22'),
(268, '3', '233', 'gfgf', '2018-04-18 04:53:40', '2018-04-18 04:58:22'),
(269, '4', '233', 'gf', '2018-04-18 04:53:40', '2018-04-18 04:58:22'),
(270, '5', '233', 'fghgh', '2018-04-18 04:53:40', '2018-04-18 04:58:22'),
(271, '1', '234', NULL, '2018-04-18 13:41:25', '2018-04-18 13:41:25'),
(272, '2', '234', NULL, '2018-04-18 13:41:25', '2018-04-18 13:41:25'),
(273, '3', '234', NULL, '2018-04-18 13:41:25', '2018-04-18 13:41:25'),
(274, '4', '234', NULL, '2018-04-18 13:41:25', '2018-04-18 13:41:25'),
(275, '5', '234', '25', '2018-04-18 13:41:25', '2018-04-18 13:41:25'),
(276, '1', '235', 'et', '2018-04-19 12:11:59', '2018-04-19 12:11:59'),
(277, '2', '235', 'sduth', '2018-04-19 12:11:59', '2018-04-19 12:14:15'),
(278, '3', '235', 'dssd', '2018-04-19 12:11:59', '2018-04-19 12:14:15'),
(279, '4', '235', NULL, '2018-04-19 12:11:59', '2018-04-19 12:11:59'),
(280, '5', '235', NULL, '2018-04-19 12:11:59', '2018-04-19 12:11:59'),
(281, '1', '236', NULL, '2018-04-19 13:52:27', '2018-04-19 13:52:27'),
(282, '2', '236', NULL, '2018-04-19 13:52:27', '2018-04-19 13:52:27'),
(283, '3', '236', NULL, '2018-04-19 13:52:27', '2018-04-19 13:52:27'),
(284, '4', '236', NULL, '2018-04-19 13:52:27', '2018-04-19 13:52:27'),
(285, '5', '236', 'Kg', '2018-04-19 13:52:27', '2018-04-19 13:52:27'),
(286, '1', '243', NULL, '2018-04-26 09:19:52', '2018-04-26 09:19:52'),
(287, '2', '243', NULL, '2018-04-26 09:19:52', '2018-04-26 09:19:52'),
(288, '3', '243', NULL, '2018-04-26 09:19:52', '2018-04-26 09:19:52'),
(289, '4', '243', NULL, '2018-04-26 09:19:52', '2018-04-26 09:19:52'),
(290, '5', '243', 'pcs', '2018-04-26 09:19:52', '2018-04-26 09:19:52'),
(291, '1', '242', 'dfdf', '2018-04-26 09:22:52', '2018-05-03 05:22:36'),
(292, '2', '242', 'fdfd', '2018-04-26 09:22:52', '2018-05-03 05:22:36'),
(293, '3', '242', 'fdf', '2018-04-26 09:22:52', '2018-05-03 05:22:36'),
(294, '4', '242', NULL, '2018-04-26 09:22:52', '2018-04-26 09:22:52'),
(295, '5', '242', 'fgg', '2018-04-26 09:22:52', '2018-05-03 05:22:36'),
(296, '1', '245', 'gfgfggfg', '2018-04-30 05:16:05', '2018-05-03 05:22:24'),
(297, '2', '245', 'gfgfg', '2018-04-30 05:16:05', '2018-05-03 05:22:24'),
(298, '3', '245', 'fgfgfg', '2018-04-30 05:16:05', '2018-05-03 05:22:24'),
(299, '4', '245', NULL, '2018-04-30 05:16:05', '2018-04-30 05:16:05'),
(300, '5', '245', 'kg', '2018-04-30 05:16:05', '2018-05-03 05:22:24'),
(301, '1', '230', 'fdfdf', '2018-04-30 05:56:52', '2018-04-30 05:56:52'),
(302, '2', '230', 'dfdf', '2018-04-30 05:56:52', '2018-04-30 05:56:52'),
(303, '3', '230', 'dfdf', '2018-04-30 05:56:52', '2018-04-30 05:56:52'),
(304, '4', '230', NULL, '2018-04-30 05:56:52', '2018-04-30 05:56:52'),
(305, '5', '230', 'kg', '2018-04-30 05:56:52', '2018-04-30 05:56:52'),
(306, '1', '241', NULL, '2018-05-03 05:22:49', '2018-05-03 05:22:49'),
(307, '2', '241', NULL, '2018-05-03 05:22:49', '2018-05-03 05:22:49'),
(308, '3', '241', NULL, '2018-05-03 05:22:49', '2018-05-03 05:22:49'),
(309, '4', '241', NULL, '2018-05-03 05:22:49', '2018-05-03 05:22:49'),
(310, '5', '241', 'kg', '2018-05-03 05:22:49', '2018-05-03 05:22:49'),
(311, '1', '240', NULL, '2018-05-03 05:23:11', '2018-05-03 05:23:11'),
(312, '2', '240', NULL, '2018-05-03 05:23:11', '2018-05-03 05:23:11'),
(313, '3', '240', NULL, '2018-05-03 05:23:11', '2018-05-03 05:23:11'),
(314, '4', '240', NULL, '2018-05-03 05:23:11', '2018-05-03 05:23:11'),
(315, '5', '240', 'kg', '2018-05-03 05:23:11', '2018-05-03 05:23:11'),
(316, '1', '239', NULL, '2018-05-03 05:23:24', '2018-05-03 05:23:24'),
(317, '2', '239', NULL, '2018-05-03 05:23:24', '2018-05-03 05:23:24'),
(318, '3', '239', NULL, '2018-05-03 05:23:24', '2018-05-03 05:23:24'),
(319, '4', '239', NULL, '2018-05-03 05:23:24', '2018-05-03 05:23:24'),
(320, '5', '239', 'pcs', '2018-05-03 05:23:24', '2018-05-03 05:23:24'),
(321, '1', '248', NULL, '2018-05-12 05:29:20', '2018-05-12 05:29:20'),
(322, '2', '248', NULL, '2018-05-12 05:29:20', '2018-05-12 05:29:20'),
(323, '3', '248', NULL, '2018-05-12 05:29:20', '2018-05-12 05:29:20'),
(324, '4', '248', NULL, '2018-05-12 05:29:20', '2018-05-12 05:29:20'),
(325, '5', '248', NULL, '2018-05-12 05:29:20', '2018-05-12 05:29:20'),
(326, '1', '247', NULL, '2018-05-12 05:29:38', '2018-05-12 05:29:38'),
(327, '2', '247', NULL, '2018-05-12 05:29:38', '2018-05-12 05:29:38'),
(328, '3', '247', NULL, '2018-05-12 05:29:38', '2018-05-12 05:29:38'),
(329, '4', '247', NULL, '2018-05-12 05:29:38', '2018-05-12 05:29:38'),
(330, '5', '247', NULL, '2018-05-12 05:29:38', '2018-05-12 05:29:38'),
(331, '1', '246', NULL, '2018-05-12 06:19:11', '2018-05-12 06:19:11'),
(332, '2', '246', NULL, '2018-05-12 06:19:11', '2018-05-12 06:19:11'),
(333, '3', '246', NULL, '2018-05-12 06:19:11', '2018-05-12 06:19:11'),
(334, '4', '246', NULL, '2018-05-12 06:19:11', '2018-05-12 06:19:11'),
(335, '5', '246', NULL, '2018-05-12 06:19:11', '2018-05-12 06:19:11'),
(336, '1', '249', NULL, '2018-05-18 06:28:24', '2018-05-18 06:28:24'),
(337, '2', '249', NULL, '2018-05-18 06:28:24', '2018-05-18 06:28:24'),
(338, '3', '249', NULL, '2018-05-18 06:28:24', '2018-05-18 06:28:24'),
(339, '4', '249', NULL, '2018-05-18 06:28:24', '2018-05-18 06:28:24'),
(340, '5', '249', NULL, '2018-05-18 06:28:24', '2018-05-18 06:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(50) NOT NULL,
  `product_variant_type_id` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(300) NOT NULL DEFAULT '',
  `stock_count` varchar(50) DEFAULT NULL,
  `product_id` varchar(50) NOT NULL DEFAULT '',
  `price_difference` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `product_variant_type_id`, `title`, `stock_count`, `product_id`, `price_difference`, `price`, `photo`, `created_at`, `updated_at`) VALUES
(4, '2', 'vbbbbbbbb', '4545', '230', '4545', '', '', '2018-04-12 09:30:53', '2018-04-16 11:15:04'),
(5, '2', 'Biggssssssssssssssssssserssssss', NULL, '222', '22', NULL, '', '2018-04-12 09:31:13', '2018-04-25 07:15:39'),
(15, '2', 'fggfgfgfgfgfg', '5454545457777787878788', '230', '45454545', NULL, '', '2018-04-16 11:43:37', '2018-04-16 11:43:57'),
(17, '3', 'rtrtrt', '7778888', '225', '777', NULL, '', '2018-04-16 11:52:32', '2018-04-16 11:52:42'),
(19, '3', 'deepu namkeen', '45', '225', '45', NULL, '', '2018-04-16 12:16:48', '2018-04-16 12:16:48'),
(21, '2', 'jhgyutuy', '7657', '226', NULL, NULL, NULL, '2018-04-16 13:52:49', '2018-04-16 13:52:49'),
(22, '2', 'sdfsdfsdf', '4354554656546', '226', NULL, NULL, NULL, '2018-04-16 13:53:02', '2018-04-16 13:53:02'),
(23, '2', 'ssrerrbbbbbbb dfdfdfdf', '454545', '232', NULL, NULL, NULL, '2018-04-18 03:56:13', '2018-04-18 04:36:28'),
(24, '2', 'fggfhgh', '454546556', '233', NULL, NULL, NULL, '2018-04-18 03:56:24', '2018-04-18 03:56:24'),
(25, '3', 'fdfgfgfg', '454545', '230', '45545', NULL, NULL, '2018-04-18 03:56:38', '2018-04-18 03:56:38'),
(26, '3', 'dfdfdfdf', '565656', '231', '344545', NULL, NULL, '2018-04-18 03:56:48', '2018-04-18 03:56:48'),
(27, '2', 'fhgghgh', '67878', '232', NULL, NULL, NULL, '2018-04-18 04:38:22', '2018-04-18 04:38:22'),
(28, '3', 'REd', '6778789', '232', '565', NULL, NULL, '2018-04-18 04:40:09', '2018-04-18 04:40:09'),
(37, '3', 'gfgh', 'hkghk', '230', '10', NULL, NULL, '2018-04-18 14:35:40', '2018-04-18 14:35:40'),
(43, '2', 'fdgdgd', 'dfgdfg', '230', NULL, NULL, NULL, '2018-04-18 14:38:15', '2018-04-18 14:38:15'),
(48, '2', 'xcxcxcxcxc', NULL, '235', NULL, NULL, NULL, '2018-04-19 12:30:10', '2018-04-19 12:30:10'),
(50, '3', 'sdsdsds', NULL, '235', '45454', NULL, NULL, '2018-04-19 12:32:22', '2018-04-19 12:32:22'),
(51, '2', 'Small', NULL, '245', '44', '', NULL, '2018-05-01 10:26:24', '2018-05-01 10:30:13'),
(52, '2', 'Bigger', NULL, '245', NULL, NULL, NULL, '2018-05-01 10:30:31', '2018-05-01 10:30:31'),
(53, '2', 'Small', NULL, '236', NULL, NULL, NULL, '2018-05-01 10:47:33', '2018-05-01 10:47:33'),
(54, '3', 'Blue', NULL, '236', '20', NULL, NULL, '2018-05-01 10:48:14', '2018-05-01 10:48:14'),
(55, '3', 'Red', NULL, '236', '24', NULL, NULL, '2018-05-01 10:48:29', '2018-05-01 10:48:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_types`
--

CREATE TABLE `product_variant_types` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `minimum_selection_needed` varchar(50) NOT NULL DEFAULT '',
  `maximum_selection_needed` varchar(50) NOT NULL DEFAULT '',
  `price_difference_status` varchar(50) NOT NULL DEFAULT '1',
  `price_status` varchar(50) NOT NULL DEFAULT '1',
  `photo_status` varchar(50) NOT NULL DEFAULT '1',
  `variant_stock_count_status` int(50) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variant_types`
--

INSERT INTO `product_variant_types` (`id`, `title`, `minimum_selection_needed`, `maximum_selection_needed`, `price_difference_status`, `price_status`, `photo_status`, `variant_stock_count_status`, `created_at`, `updated_at`) VALUES
(2, 'Size', '2', '2', '0', '0', '1', 1, '2018-02-05 08:50:06', '2018-02-05 08:52:19'),
(3, 'Color', '1', '1', '1', '0', '1', 1, '2018-02-05 08:50:20', '2018-02-05 08:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2018-01-11 00:45:10', '2018-01-11 00:45:10'),
(2, 'Customer', 'web', '2018-01-11 03:30:00', '2018-01-11 03:30:00'),
(3, 'Driver', 'web', '2018-01-18 05:43:43', '2018-01-18 05:43:43'),
(4, 'Vendor', 'web', '2018-05-02 09:19:43', '2018-05-02 09:19:43'),
(5, 'Peon', 'web', '2018-05-10 06:56:54', '2018-05-10 06:56:54'),
(6, 'test2', 'web', '2018-05-14 03:24:06', '2018-05-14 03:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 1),
(2, 2),
(2, 4),
(3, 1),
(3, 2),
(3, 4),
(4, 1),
(4, 2),
(4, 4),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(9, 1),
(9, 2),
(9, 3),
(9, 4),
(10, 1),
(10, 2),
(10, 3),
(10, 4),
(11, 1),
(11, 2),
(11, 3),
(11, 4),
(12, 1),
(12, 2),
(12, 3),
(12, 4),
(13, 1),
(13, 2),
(13, 3),
(13, 4),
(14, 1),
(14, 2),
(14, 4),
(15, 1),
(15, 2),
(15, 3),
(15, 4),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(17, 1),
(17, 2),
(17, 3),
(17, 4),
(18, 1),
(18, 2),
(18, 3),
(18, 4),
(19, 1),
(19, 2),
(19, 3),
(19, 4),
(20, 1),
(20, 2),
(20, 3),
(20, 4),
(21, 1),
(21, 2),
(21, 3),
(21, 4),
(22, 1),
(22, 2),
(22, 4),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(24, 4),
(25, 1),
(25, 2),
(25, 3),
(25, 4),
(26, 1),
(26, 2),
(26, 3),
(26, 4),
(27, 1),
(27, 3),
(27, 4),
(28, 1),
(28, 2),
(28, 3),
(28, 4),
(29, 1),
(29, 2),
(29, 3),
(29, 4),
(30, 1),
(30, 2),
(30, 3),
(30, 4),
(31, 1),
(31, 2),
(31, 3),
(31, 4),
(32, 1),
(32, 2),
(32, 3),
(32, 4),
(33, 1),
(33, 2),
(33, 3),
(33, 4),
(34, 1),
(34, 2),
(34, 3),
(34, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(50) NOT NULL,
  `key_title` varchar(100) NOT NULL DEFAULT '',
  `key_display_title` varchar(100) NOT NULL DEFAULT '',
  `key_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key_title`, `key_display_title`, `key_value`, `type`, `created_at`, `updated_at`) VALUES
(1, 'app_name', 'APP NAME', 'Launspace', 'project', NULL, NULL),
(3, 'app_email', 'APP EMAIL', 'xyz@gmail.com', 'project', NULL, NULL),
(4, 'app_phone', 'APP PHONE', '95675643434', 'project', NULL, NULL),
(5, 'app_logo', 'APP LOGO', '5afea0300a541_1526636592.png', 'project', NULL, '2018-05-18 09:43:15'),
(8, 'categories_level', 'Categories Levels', '1', 'project', NULL, NULL),
(9, 'slots_days_count', 'Slots Days Count', '10', 'orders', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `percentage` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `title`, `percentage`, `created_at`, `updated_at`) VALUES
(1, 'SGST', 9, '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(2, 'CGST', 9, '2018-05-03 18:30:00', '2018-05-02 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `one_time_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_token` varchar(555) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `mobile`, `password`, `photo`, `user_type`, `one_time_code`, `remember_token`, `email_token`, `created_at`, `updated_at`) VALUES
(33, 'Harvinder11', 'Singh', 'harvindersingh@goteso.com', '7345465465', '$2y$10$qHJJIqGR5l1vP7nmpbqqvO75RKGx37SnU.Wb/h5I2UWaoFQ8MLJDW', NULL, '2', NULL, NULL, NULL, '2018-05-03 04:28:19', '2018-05-17 07:21:40'),
(63, 'Sachin', 'Kashyap', 'kashyapsk147@gmail.com', '9876525345', '$2y$10$0jQCfWp7jMWa8kptUjyi7uKSzQ6tQzF1erWjLNaL1remHp53uRiFi', '5af44a0776964_1525959175.png', '2', NULL, NULL, NULL, '2018-04-26 07:15:05', '2018-05-10 13:32:57'),
(64, 'Siddharth', 'Bhatheja', 'siddharthbhatheja30@gmail.com', '83778635625', '$2y$10$q3/7cWMf.L9uupCkQEv3Ge29C4AUET2zEUbkqzyGoXN4aJczwcQyq', '5afe70076f298_1526624263.png', 'customer', NULL, NULL, NULL, '2018-04-26 07:17:56', '2018-05-18 06:17:47'),
(65, 'Reet', 'Dhiman', 'samreet@goteso.com', '123456789', '$2y$10$JFsWrHUE0ImWTQgj4mUsAedW35MMm3RBFit08un4dK/K6yOzP0DWC', NULL, 'customer', '5280', NULL, NULL, '2018-04-27 11:43:40', '2018-05-01 10:47:50'),
(67, 'Deepu', 'Singla', 'deepakshisingla@goteso.com', '873245467', '$2y$10$uvnorulg408K98cW1BYy/u68djRT9Tqdxdcxgxn.o6KXj6FlFABJC', NULL, '2', NULL, 'aVbPXTF0DnaWdHLs0h707LUZjx3UBOtR47MHmExZRQt2JYfUt0bzaz4ZmN0z', NULL, '2018-05-01 10:50:58', '2018-05-12 09:05:25'),
(69, 'vendor1', 'gyu', 'vendor1@gmail.com', 'u76786', '$2y$10$qHJJIqGR5l1vP7nmpbqqvO75RKGx37SnU.Wb/h5I2UWaoFQ8MLJDW', NULL, 'vendor', NULL, 'o5yuyPYapWfMthyPVSk2QXkZXaPZhSxQSuCDcNvkVehxLtjGxIE1FrS2XNO0', NULL, '2018-05-02 04:00:17', '2018-05-02 04:00:17'),
(71, 'User1', 'jkhj', 'user1@gmail.com', '87567', '$2y$10$7hb8ezpO/RIG66yyqDsj1ulAIvu/6IX/pmCPF1XuybqQfQFWMfT46', NULL, 'driver', NULL, NULL, NULL, '2018-05-02 09:41:50', '2018-05-02 09:41:50'),
(72, 'Deepu', 'Rani', 'deepurfafnf4ddfdfffddfi4@fdffgmfail.com', 'fffdf', '$2y$10$u4lN4YUqOtDdrRzU5SD5L.3SJ1HqXuGwWYoAawB.1t4AsXrXiaGx2', '5af6aff708fcb_1526116343.PNG', '2', NULL, NULL, NULL, '2018-05-02 09:42:55', '2018-05-12 09:12:24'),
(73, 'Deepu', 'Rani', 'deepurfafnf4ddfddddfffddfi4@fdffgmfail.com', 'fff5df', '$2y$10$v7dvheZXpFqofs0KnFZN2uHlQKxc8yYYT8a.gq9BOWk4cC0rBmvkW', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-02 09:43:41', '2018-05-02 09:43:41'),
(74, 'Deepu', 'Rani', 'deepurfafnf4ddfddddddddfffddfi4@fdffgmfail.com', 'ff565676f5df', '$2y$10$maQjwn3m6YUlDj7UXl7Ije.n2679D4G6c1kFMEGIGNCZR1UOBY6jS', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-02 09:44:42', '2018-05-02 09:44:42'),
(75, 'Deepu', 'Rani', 'deepurfafnf4ddfdddddddddfffddfi4@fdffgmfail.com', 'ff56567d6f5df', '$2y$10$7lZMceo/.MCfbhZDleu1cOvqvjemjFQneCsmI8/TBbdQp3YOUGwgO', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-02 09:46:35', '2018-05-02 09:46:35'),
(76, 'Mohit', 'Dhiman', 'mohit@gmail.com', '8776776567567', '$2y$10$7hb8ezpO/RIG66yyqDsj1ulAIvu/6IX/pmCPF1XuybqQfQFWMfT46', NULL, 'customer', NULL, NULL, NULL, '2018-05-02 11:13:05', '2018-05-02 11:13:05'),
(77, 'dfghy', 'fuytryutryu', 'jgju@jkgjh.dfd', '7654674', '$2y$10$qopENRPOuH2KBFVHyMOS9OAqHyE4h4nrcR1EFvHfpnnCgVnEQKxrK', NULL, 'customer', NULL, NULL, NULL, '2018-05-03 03:49:17', '2018-05-03 03:49:17'),
(78, 'ghiu', 'ytu', 'uytfg@rhy.ghyty', 'yut', '$2y$10$f1/WwDV61zGSCfwbPwjBmeFzyBAqI1iBstdFZk3As.KSwINVSMcZq', NULL, 'customer', NULL, NULL, NULL, '2018-05-03 03:51:47', '2018-05-03 03:51:47'),
(79, 'Deepu', 'Rani', 'deepurfafnf4ddfdddddddfdddfffddfi4@fdffgmfail.com', 'ff5656f7d6f5df', '$2y$10$mv9x2mkVJGvs3/10TvIvb.nXEV4e6vojfBqT1A9ceI8/D.j3gYuZG', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-03 03:57:06', '2018-05-03 03:57:06'),
(80, 'Deepu', 'Rani', 'deepurfafnf4ddfdfddddddfdddfffddfi4@fdffgmfail.com', 'ff56f56f7d6f5df', '$2y$10$HQUjJH2TqRH7B6qRCtSt/u9BNGTDN5GyUsn2LsdfL81v5JUZVAt/e', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-03 03:58:21', '2018-05-03 03:58:21'),
(81, 'Deepu', 'Rani', 'deepurfafnf4ddfdfddddddffdddfffddfi4@fdffgmfail.com', 'ff56f56f7df6f5df', '$2y$10$COHcZxTyxbPAxvCC7Inb7.VjjTzPYWU6xNrqp9xtW58DHcz.eW8zy', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-03 03:58:47', '2018-05-03 03:58:47'),
(82, 'Deepu', 'Rani', 'deepurfafnf4ddfdfddddddffdddfffddfi4@ffdffgmfail.com', 'ff56f56f7dff6f5df', '$2y$10$d5dF7/tpmLim2061KqtnL.FU.UttxcKkBpDVCUHeuPYcbgeZLWj1O', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-03 04:00:06', '2018-05-03 04:00:06'),
(83, 'Deepu', 'Rani', 'deepurfafnf4ddfdfdddddddffdddfffddfi4@ffdffgmfail.com', 'ff56f564f7dff6f5df', '$2y$10$fN4elRTu3jNRYrb.Nok/5Ol.7/TdYKew.1bQu4PTHUQfddSO0mggC', 'abc.jpg', 'user', NULL, NULL, NULL, '2018-05-03 04:00:30', '2018-05-03 04:00:30'),
(84, 'djhjkdfdfuyduu', 'uityytuy', 'ugug@hjgh.dfdf', '876775', '$2y$10$QU5wc8zU/LkMLcA6TekKSeoFj0VcAtit0JGneAqJ1QdIwpolxvdg.', NULL, '2', NULL, NULL, NULL, '2018-05-03 04:24:15', '2018-05-03 04:24:15'),
(85, 'dsytuyt', 'uytuyrtyuruyryu', 'jygygfy@ujgy.dfd', '76567765', '$2y$10$BkqmL.LCbIwCRxbKqHmenevcLHepNumdpQWPzIBD/nrN58YF4Dejy', NULL, '1', NULL, NULL, NULL, '2018-05-03 04:24:42', '2018-05-03 04:24:42'),
(87, 'admin', 'admin', 'admin@admin.com', '7565', '$2y$10$KMoMQM8xGHzlXddwpoIJx.XTfa4SzIzV2MHLKMKYfsKgpDr0zw5t6', NULL, '1', NULL, '3MsfzL5xpIMdQeMwFSDpSqlx9XcJuCzL2llYKec0D1zONWRTDSLlY3ni7NoV', NULL, '2018-05-03 05:02:32', '2018-05-03 05:02:32'),
(88, 'Vendor', 'Vendor', 'vendor@vendor.com', '7867433', '$2y$10$uAMCOQM6Aba3S2j.qbL72un0IyJUOzGP.5KH0jEfpjk01Ykv3q2JO', NULL, '4', NULL, 'LGYOKUZQpD8kynBVijHnVowedbwRYnyrPShB3xatm2N6irY8I368xeI7KjCa', NULL, '2018-05-03 05:36:27', '2018-05-03 05:36:27'),
(89, 'Aman', 'Kashyap', 'driver@driver.com', '9814237127', '$2y$10$uAMCOQM6Aba3S2j.qbL72un0IyJUOzGP.5KH0jEfpjk01Ykv3q2JO', NULL, '3', '4906', '7z8z2fEAirfWg8tq9LMimSq2kYp1sDso1ylBPvisLW7DL8SADkPnOYNU2YSn', NULL, '2018-05-04 04:55:01', '2018-05-11 05:59:57'),
(90, 'Driver2', 'dr', 'driver2@driver.com', '875667567', '$2y$10$IcdWLbpAY8b3662J/sqaT.AiZLxMR5mcq2IHHS24PUWnDNhByFj7m', NULL, '3', NULL, 'RBu4uypVbbcv2UgFfaVxeEAVopfzlpnQSOq4lP13BZ5xMJwpLlgTsrl4Q0Pq', NULL, '2018-05-04 05:42:14', '2018-05-04 05:42:14'),
(91, 'dfdffddfdf', 'dfdfdf', 'dfd@gmail.com', '45466', '$2y$10$LEF4SmbJd86eY4eREOosnunoHTG09MjGMBa6bTyZOiyALwx3UfKWO', NULL, '1', NULL, NULL, NULL, '2018-05-10 04:44:57', '2018-05-10 04:44:57'),
(92, 'jhg', 'jhg', 'guuygguyg@fdg.ghfdfdf', 'rt56456545656', '$2y$10$evw5KdnfRh3CumD65Sc/UOlToGuAoeJcjgBoU6KIETnyoZWHujaXO', NULL, '2', NULL, NULL, NULL, '2018-05-10 04:55:55', '2018-05-10 04:55:55'),
(93, 'dfdfdf', 'dfdf', 'admin@admin244222.com', '4545', '$2y$10$X8Nihmij9h6umqo/Fon1WuzQ/bmLHnW2UgH0vB2jfXGgTh07E/Wui', '5af40148432f7_1525940552.png', '2', NULL, NULL, NULL, '2018-05-10 08:22:43', '2018-05-10 08:22:43'),
(94, 'dfdfdf', 'dfdf', 'admin@admin244444222.com', '454545454', '$2y$10$W7c1i1GEjRInrIWNI/jKVuSABHvu.uFG8SpcSrYtMPd984mGEOkxO', '5af4115591161_1525944661.jpg', '2', NULL, NULL, NULL, '2018-05-10 08:23:05', '2018-05-10 09:31:03'),
(95, 'Deepu', 'Singla', 'deepakshisingla@goteso.com', '9876543210', '$2y$10$RQRw5v6uGlMfFVDzfMeXPOvRvbEc6GGSQG1l8lpR7RlTRQ.1DYGm6', '5af6ada24fc94_1526115746.PNG', '2', NULL, NULL, NULL, '2018-05-12 09:03:29', '2018-05-12 09:03:29');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta_types`
--

CREATE TABLE `users_meta_types` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `identifier` varchar(100) NOT NULL DEFAULT '',
  `count_limit` int(50) NOT NULL DEFAULT '1',
  `type` varchar(50) NOT NULL DEFAULT '',
  `field_options` varchar(500) NOT NULL DEFAULT '',
  `field_options_model` varchar(300) NOT NULL DEFAULT '',
  `field_options_model_columns` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_meta_types`
--

INSERT INTO `users_meta_types` (`id`, `title`, `identifier`, `count_limit`, `type`, `field_options`, `field_options_model`, `field_options_model_columns`, `created_at`, `updated_at`) VALUES
(1, 'Date of Birth', 'dob', 1, 'datePicker', '', '', '', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(2, 'Gender', 'gender', 1, 'radio', '[{\"title\":\"Male\",\"value\":\"male\"},{\"title\":\"Female\",\"value\":\"female\"}]', '', '', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(3, 'Bio', 'bio', 1, 'text', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Select Address Type', 'address_type', 1, 'select', '', '', 'address_line1,address_line2', '2018-04-04 18:30:00', '2018-04-11 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_meta_values`
--

CREATE TABLE `users_meta_values` (
  `id` int(50) NOT NULL,
  `user_meta_type_id` varchar(50) NOT NULL DEFAULT '',
  `user_id` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_meta_values`
--

INSERT INTO `users_meta_values` (`id`, `user_meta_type_id`, `user_id`, `value`, `created_at`, `updated_at`) VALUES
(16, '2', '28', 'female', '2018-04-18 05:56:22', '2018-04-18 05:56:22'),
(17, '3', '28', 'fdfdf vvvvvvvvvvvvvvv', '2018-04-18 05:56:04', '2018-04-18 05:56:04'),
(18, '1', '28', '2018-04-21', '2018-04-18 05:56:09', '2018-04-18 05:56:09'),
(19, '4', '28', '', '2018-04-18 05:56:22', '2018-04-18 05:56:22'),
(20, '1', '29', '2002-12-12', '2018-04-10 10:22:13', '2018-04-10 10:22:13'),
(21, '2', '29', 'female', '2018-04-10 10:22:13', '2018-04-10 10:22:13'),
(22, '3', '29', 'this is my bio', '2018-04-10 10:22:13', '2018-04-10 10:22:13'),
(23, '1', '30', '2018-04-06', '2018-04-16 05:33:25', '2018-04-16 05:33:25'),
(24, '2', '30', 'male', '2018-04-16 05:33:25', '2018-04-16 05:33:25'),
(25, '3', '30', 'hghguyg', '2018-04-16 05:33:25', '2018-04-16 05:33:25'),
(26, '4', '30', '', '2018-04-16 05:33:25', '2018-04-16 05:33:25'),
(27, '1', '31', '2018-04-06', '2018-04-16 06:21:32', '2018-04-16 06:21:32'),
(28, '2', '31', 'male', '2018-04-16 06:21:32', '2018-04-16 06:21:32'),
(29, '3', '31', 'jhgyu', '2018-04-16 06:21:32', '2018-04-16 06:21:32'),
(30, '4', '31', '', '2018-04-16 06:21:32', '2018-04-16 06:21:32'),
(31, '4', '29', '', '2018-04-16 06:42:18', '2018-04-16 06:42:18'),
(32, '1', '32', '1962-12-12', '2018-04-16 06:50:22', '2018-04-16 06:50:22'),
(33, '2', '32', 'female', '2018-04-16 06:50:22', '2018-04-16 06:50:22'),
(34, '3', '32', 'dfgfgfdgfd', '2018-04-16 06:50:22', '2018-04-16 06:50:22'),
(35, '4', '32', '', '2018-04-16 06:50:22', '2018-04-16 06:50:22'),
(36, '1', '33', '1990-02-27', '2018-05-17 06:43:26', '2018-05-17 06:43:26'),
(37, '2', '33', 'male', '2018-05-17 06:47:23', '2018-05-17 06:47:23'),
(38, '3', '33', 'ggffgfg', '2018-05-17 06:46:02', '2018-05-17 06:46:02'),
(39, '1', '34', '2018-04-06', '2018-04-17 10:41:50', '2018-04-17 10:41:50'),
(40, '2', '34', 'male', '2018-04-17 10:41:50', '2018-04-17 10:41:50'),
(41, '3', '34', 'dfdfdfd', '2018-04-17 10:41:50', '2018-04-17 10:41:50'),
(42, '4', '34', '', '2018-04-17 10:41:50', '2018-04-17 10:41:50'),
(43, '1', '35', '2018-04-18', '2018-04-17 10:54:30', '2018-04-17 10:54:30'),
(44, '2', '35', 'male', '2018-04-17 10:54:30', '2018-04-17 10:54:30'),
(45, '3', '35', 'fgfggffg', '2018-04-17 10:54:30', '2018-04-17 10:54:30'),
(46, '4', '35', '', '2018-04-17 10:54:30', '2018-04-17 10:54:30'),
(47, '1', '36', '2018-04-27', '2018-04-17 10:56:29', '2018-04-17 10:56:29'),
(48, '2', '36', 'female', '2018-04-17 10:56:29', '2018-04-17 10:56:29'),
(49, '3', '36', 'dgfyd', '2018-04-17 10:56:29', '2018-04-17 10:56:29'),
(50, '4', '36', '', '2018-04-17 10:56:29', '2018-04-17 10:56:29'),
(51, '1', '37', '2018-04-19', '2018-04-17 11:09:33', '2018-04-17 11:09:33'),
(52, '2', '37', 'male', '2018-04-17 11:09:33', '2018-04-17 11:09:33'),
(53, '3', '37', 'jhgyjtu', '2018-04-17 11:09:33', '2018-04-17 11:09:33'),
(54, '4', '37', '', '2018-04-17 11:09:33', '2018-04-17 11:09:33'),
(55, '1', '38', '2018-04-10', '2018-04-17 11:12:17', '2018-04-17 11:12:17'),
(56, '2', '38', 'male', '2018-04-17 11:12:17', '2018-04-17 11:12:17'),
(57, '3', '38', 'fddfd', '2018-04-17 11:12:17', '2018-04-17 11:12:17'),
(58, '4', '38', '', '2018-04-27 11:41:51', '2018-04-27 11:41:51'),
(59, '1', '39', '2018-04-17', '2018-04-17 11:14:22', '2018-04-17 11:14:22'),
(60, '2', '39', 'male', '2018-04-17 11:14:22', '2018-04-17 11:14:22'),
(61, '3', '39', 'DFDFD', '2018-04-17 11:14:22', '2018-04-17 11:14:22'),
(62, '4', '39', '', '2018-04-17 11:14:22', '2018-04-17 11:14:22'),
(63, '1', '40', '2018-04-12', '2018-04-18 05:08:45', '2018-04-18 05:08:45'),
(64, '2', '40', 'male', '2018-04-18 05:08:45', '2018-04-18 05:08:45'),
(65, '3', '40', 'dffdfdfdf', '2018-04-18 05:08:45', '2018-04-18 05:08:45'),
(66, '4', '40', '', '2018-04-18 05:08:45', '2018-04-18 05:08:45'),
(67, '1', '41', '2018-04-20', '2018-04-18 05:10:44', '2018-04-18 05:10:44'),
(68, '2', '41', 'male', '2018-04-18 05:10:44', '2018-04-18 05:10:44'),
(69, '3', '41', 'dfddff', '2018-04-18 05:10:44', '2018-04-18 05:10:44'),
(70, '4', '41', '', '2018-04-18 05:10:44', '2018-04-18 05:10:44'),
(71, '1', '42', '2018-04-11', '2018-04-18 05:11:09', '2018-04-18 05:11:09'),
(72, '2', '42', 'female', '2018-04-18 05:11:09', '2018-04-18 05:11:09'),
(73, '3', '42', 'fdfdf', '2018-04-18 05:11:09', '2018-04-18 05:11:09'),
(74, '4', '42', '', '2018-04-18 05:11:09', '2018-04-18 05:11:09'),
(75, '1', '43', '2018-04-11', '2018-04-18 05:18:53', '2018-04-18 05:18:53'),
(76, '2', '43', 'male', '2018-04-18 05:18:53', '2018-04-18 05:18:53'),
(77, '3', '43', 'fyu', '2018-04-18 05:18:53', '2018-04-18 05:18:53'),
(78, '4', '43', '', '2018-04-18 05:18:53', '2018-04-18 05:18:53'),
(79, '1', '44', '2018-04-11', '2018-04-18 05:19:20', '2018-04-18 05:19:20'),
(80, '2', '44', 'male', '2018-04-18 05:19:20', '2018-04-18 05:19:20'),
(81, '3', '44', 'fyu', '2018-04-18 05:19:20', '2018-04-18 05:19:20'),
(82, '4', '44', '', '2018-04-18 05:19:20', '2018-04-18 05:19:20'),
(83, '1', '45', '2018-04-20', '2018-04-18 05:24:18', '2018-04-18 05:24:18'),
(84, '2', '45', 'female', '2018-04-18 05:24:18', '2018-04-18 05:24:18'),
(85, '3', '45', 'sjhgdsdfsyd', '2018-04-18 05:24:18', '2018-04-18 05:24:18'),
(86, '4', '45', '', '2018-04-18 05:24:18', '2018-04-18 05:24:18'),
(87, '1', '46', '2018-04-12', '2018-04-18 05:32:32', '2018-04-18 05:32:32'),
(88, '2', '46', 'male', '2018-04-18 05:32:32', '2018-04-18 05:32:32'),
(89, '3', '46', 'kjdhfdu', '2018-04-18 05:32:32', '2018-04-18 05:32:32'),
(90, '4', '46', '', '2018-04-18 05:32:32', '2018-04-18 05:32:32'),
(91, '1', '47', '2018-04-18', '2018-04-18 05:34:13', '2018-04-18 05:34:13'),
(92, '2', '47', 'female', '2018-04-18 05:34:13', '2018-04-18 05:34:13'),
(93, '3', '47', 'dfgfgfg', '2018-04-18 05:34:13', '2018-04-18 05:34:13'),
(94, '4', '47', '', '2018-04-18 05:34:13', '2018-04-18 05:34:13'),
(95, '1', '48', '2002-12-12', '2018-04-18 05:35:15', '2018-04-18 05:35:15'),
(96, '2', '48', 'female', '2018-04-18 05:35:15', '2018-04-18 05:35:15'),
(97, '3', '48', 'this is my bio', '2018-04-18 05:35:15', '2018-04-18 05:35:15'),
(98, '1', '49', '2002-12-12', '2018-04-18 05:35:35', '2018-04-18 05:35:35'),
(99, '2', '49', 'female', '2018-04-18 05:35:35', '2018-04-18 05:35:35'),
(100, '3', '49', 'this is my bio', '2018-04-18 05:35:35', '2018-04-18 05:35:35'),
(101, '1', '50', '2002-12-12', '2018-04-18 05:36:40', '2018-04-18 05:36:40'),
(102, '2', '50', 'female', '2018-04-18 05:36:40', '2018-04-18 05:36:40'),
(103, '3', '50', 'this is my bio', '2018-04-18 05:36:40', '2018-04-18 05:36:40'),
(104, '1', '51', '2002-12-12', '2018-04-18 05:37:01', '2018-04-18 05:37:01'),
(105, '2', '51', 'female', '2018-04-18 05:37:01', '2018-04-18 05:37:01'),
(106, '3', '51', 'this is my bio', '2018-04-18 05:37:01', '2018-04-18 05:37:01'),
(107, '1', '52', '2002-12-12', '2018-04-18 05:49:52', '2018-04-18 05:49:52'),
(108, '2', '52', 'female', '2018-04-18 05:49:52', '2018-04-18 05:49:52'),
(109, '3', '52', 'this is my bio', '2018-04-18 05:49:52', '2018-04-18 05:49:52'),
(110, '1', '53', '2018-04-18', '2018-04-18 05:50:54', '2018-04-18 05:50:54'),
(111, '2', '53', 'male', '2018-04-18 05:50:54', '2018-04-18 05:50:54'),
(112, '3', '53', 'fgdfgd', '2018-04-18 05:50:54', '2018-04-18 05:50:54'),
(113, '4', '53', '', '2018-04-18 05:50:54', '2018-04-18 05:50:54'),
(114, '1', '54', '2018-04-18', '2018-04-18 05:51:38', '2018-04-18 05:51:38'),
(115, '2', '54', 'male', '2018-04-18 05:51:39', '2018-04-18 05:51:39'),
(116, '3', '54', 'kljhyiu', '2018-04-18 05:51:39', '2018-04-18 05:51:39'),
(117, '4', '54', '', '2018-04-18 05:51:39', '2018-04-18 05:51:39'),
(118, '1', '55', '2018-04-11', '2018-04-18 13:32:15', '2018-04-18 13:32:15'),
(119, '2', '55', 'male', '2018-04-18 13:32:15', '2018-04-18 13:32:15'),
(120, '3', '55', 'This is my Bio', '2018-04-18 13:32:15', '2018-04-18 13:32:15'),
(121, '4', '55', '', '2018-04-18 13:32:15', '2018-04-18 13:32:15'),
(122, '1', '56', '2018-04-10', '2018-04-18 14:46:34', '2018-04-18 14:46:34'),
(123, '2', '56', 'female', '2018-04-18 14:46:34', '2018-04-18 14:46:34'),
(124, '3', '56', 'gfhyfhgh', '2018-04-18 14:46:34', '2018-04-18 14:46:34'),
(125, '4', '56', '', '2018-04-18 14:46:34', '2018-04-18 14:46:34'),
(126, '1', '57', '2018-04-17', '2018-04-18 14:48:18', '2018-04-18 14:48:18'),
(127, '2', '57', 'female', '2018-04-18 14:48:18', '2018-04-18 14:48:18'),
(128, '3', '57', 'dgdg', '2018-04-18 14:48:18', '2018-04-18 14:48:18'),
(129, '4', '57', '', '2018-04-18 14:48:18', '2018-04-18 14:48:18'),
(130, '1', '58', '2018-04-09', '2018-04-18 14:49:15', '2018-04-18 14:49:15'),
(131, '2', '58', 'female', '2018-04-18 14:49:15', '2018-04-18 14:49:15'),
(132, '3', '58', 'dfg', '2018-04-18 14:49:15', '2018-04-18 14:49:15'),
(133, '4', '58', '', '2018-04-18 14:49:15', '2018-04-18 14:49:15'),
(134, '1', '59', '2018-04-10', '2018-04-18 14:50:02', '2018-04-18 14:50:02'),
(135, '2', '59', 'female', '2018-04-18 14:50:02', '2018-04-18 14:50:02'),
(136, '3', '59', 'hj', '2018-04-18 14:50:02', '2018-04-18 14:50:02'),
(137, '4', '59', '', '2018-04-18 14:50:02', '2018-04-18 14:50:02'),
(138, '1', '60', '', '2018-05-01 08:48:16', '2018-05-01 08:48:16'),
(139, '2', '60', '', '2018-05-01 08:48:16', '2018-05-01 08:48:16'),
(140, '3', '60', '', '2018-05-01 08:48:16', '2018-05-01 08:48:16'),
(141, '4', '60', '', '2018-05-01 08:48:16', '2018-05-01 08:48:16'),
(142, '1', '61', '2018-04-20', '2018-04-19 04:10:24', '2018-04-19 04:10:24'),
(143, '2', '61', 'male', '2018-04-19 04:10:24', '2018-04-19 04:10:24'),
(144, '3', '61', 'dfdfdfdf', '2018-04-19 04:10:24', '2018-04-19 04:10:24'),
(145, '4', '61', '', '2018-04-19 04:10:24', '2018-04-19 04:10:24'),
(146, '1', '62', '2018-04-20', '2018-04-19 04:12:44', '2018-04-19 04:12:44'),
(147, '2', '62', 'male', '2018-04-19 04:12:44', '2018-04-19 04:12:44'),
(148, '3', '62', 'dkjhfjdghj dhjgdhyfg', '2018-04-19 04:12:44', '2018-04-19 04:12:44'),
(149, '4', '62', '', '2018-04-19 04:12:44', '2018-04-19 04:12:44'),
(150, '1', '63', '1989-10-20', '2018-04-26 07:15:05', '2018-04-26 07:15:05'),
(151, '2', '63', 'male', '2018-04-19 06:08:08', '2018-04-19 06:08:08'),
(152, '3', '63', 'jrhfuf hsdfsdfdsf', '2018-04-26 07:15:05', '2018-04-26 07:15:05'),
(153, '4', '63', '', '2018-05-10 13:32:57', '2018-05-10 13:32:57'),
(154, '4', '33', '', '2018-05-17 07:21:40', '2018-05-17 07:21:40'),
(155, '1', '64', '1984-04-12', '2018-04-26 07:17:56', '2018-04-26 07:17:56'),
(156, '2', '64', 'male', '2018-04-26 07:17:56', '2018-04-26 07:17:56'),
(157, '3', '64', 'fsdfsdf', '2018-04-26 07:17:56', '2018-04-26 07:17:56'),
(158, '4', '64', '', '2018-05-18 06:17:47', '2018-05-18 06:17:47'),
(159, '1', '65', '1985-12-08', '2018-04-27 11:43:40', '2018-04-27 11:43:40'),
(160, '2', '65', 'female', '2018-04-27 11:43:40', '2018-04-27 11:43:40'),
(161, '3', '65', '', '2018-05-01 10:44:31', '2018-05-01 10:44:31'),
(162, '4', '65', '', '2018-05-01 10:44:31', '2018-05-01 10:44:31'),
(163, '1', '66', '2018-05-08', '2018-05-01 09:39:18', '2018-05-01 09:39:18'),
(164, '2', '66', 'female', '2018-05-01 09:39:18', '2018-05-01 09:39:18'),
(165, '3', '66', 'xhfdhfdvf', '2018-05-01 09:39:18', '2018-05-01 09:39:18'),
(166, '4', '66', '', '2018-05-01 10:45:52', '2018-05-01 10:45:52'),
(167, '1', '67', '2018-05-10', '2018-05-01 10:50:58', '2018-05-01 10:50:58'),
(168, '2', '67', 'female', '2018-05-01 10:50:58', '2018-05-01 10:50:58'),
(169, '3', '67', 'ssffdsf', '2018-05-01 10:50:58', '2018-05-01 10:50:58'),
(170, '4', '67', '', '2018-05-12 09:05:25', '2018-05-12 09:05:25'),
(171, '1', '69', '2018-05-09', '2018-05-02 04:00:17', '2018-05-02 04:00:17'),
(172, '2', '69', 'male', '2018-05-02 04:00:17', '2018-05-02 04:00:17'),
(173, '3', '69', 'dfdfdf', '2018-05-02 04:00:17', '2018-05-02 04:00:17'),
(174, '4', '69', '', '2018-05-02 04:00:18', '2018-05-02 04:00:18'),
(175, '1', '70', '2018-05-09', '2018-05-02 04:00:52', '2018-05-02 04:00:52'),
(176, '2', '70', 'male', '2018-05-02 04:00:52', '2018-05-02 04:00:52'),
(177, '3', '70', 'sdfdfdf', '2018-05-02 04:00:52', '2018-05-02 04:00:52'),
(178, '4', '70', '', '2018-05-02 05:32:43', '2018-05-02 05:32:43'),
(179, '1', '72', '2002-12-12', '2018-05-02 09:42:55', '2018-05-02 09:42:55'),
(180, '2', '72', 'female', '2018-05-02 09:42:55', '2018-05-02 09:42:55'),
(181, '3', '72', 'this is my bio', '2018-05-02 09:42:55', '2018-05-02 09:42:55'),
(182, '1', '75', '2002-12-12', '2018-05-02 09:46:35', '2018-05-02 09:46:35'),
(183, '2', '75', 'female', '2018-05-02 09:46:35', '2018-05-02 09:46:35'),
(184, '3', '75', 'this is my bio', '2018-05-02 09:46:35', '2018-05-02 09:46:35'),
(185, '1', '76', '2018-05-15', '2018-05-02 11:13:05', '2018-05-02 11:13:05'),
(186, '2', '76', 'male', '2018-05-02 11:13:05', '2018-05-02 11:13:05'),
(187, '3', '76', 'sdfdfdfdf', '2018-05-02 11:13:05', '2018-05-02 11:13:05'),
(188, '4', '76', '', '2018-05-02 11:13:05', '2018-05-02 11:13:05'),
(189, '1', '77', '2018-05-15', '2018-05-03 03:49:17', '2018-05-03 03:49:17'),
(190, '2', '77', 'male', '2018-05-03 03:49:17', '2018-05-03 03:49:17'),
(191, '3', '77', 'sdfdfdf', '2018-05-03 03:49:17', '2018-05-03 03:49:17'),
(192, '4', '77', '', '2018-05-03 03:49:17', '2018-05-03 03:49:17'),
(193, '1', '83', '2002-12-12', '2018-05-03 04:00:30', '2018-05-03 04:00:30'),
(194, '2', '83', 'female', '2018-05-03 04:00:30', '2018-05-03 04:00:30'),
(195, '3', '83', 'this is my bio', '2018-05-03 04:00:30', '2018-05-03 04:00:30'),
(196, '1', '84', '2018-05-16', '2018-05-03 04:24:15', '2018-05-03 04:24:15'),
(197, '2', '84', 'male', '2018-05-03 04:24:15', '2018-05-03 04:24:15'),
(198, '3', '84', 'sdsdsds', '2018-05-03 04:24:15', '2018-05-03 04:24:15'),
(199, '4', '84', '', '2018-05-03 04:24:15', '2018-05-03 04:24:15'),
(200, '1', '85', '2018-05-16', '2018-05-03 04:24:42', '2018-05-03 04:24:42'),
(201, '2', '85', 'male', '2018-05-03 04:24:42', '2018-05-03 04:24:42'),
(202, '3', '85', 'lkhkjhkj', '2018-05-03 04:24:42', '2018-05-03 04:24:42'),
(203, '4', '85', '', '2018-05-03 04:24:42', '2018-05-03 04:24:42'),
(204, '1', '86', '2018-05-16', '2018-05-03 04:28:19', '2018-05-03 04:28:19'),
(205, '2', '86', 'male', '2018-05-03 04:28:19', '2018-05-03 04:28:19'),
(206, '3', '86', 'dfdfdfdf', '2018-05-03 04:28:19', '2018-05-03 04:28:19'),
(207, '4', '86', '', '2018-05-03 04:28:19', '2018-05-03 04:28:19'),
(208, '1', '87', '2018-05-16', '2018-05-03 05:02:32', '2018-05-03 05:02:32'),
(209, '2', '87', 'male', '2018-05-03 05:02:32', '2018-05-03 05:02:32'),
(210, '3', '87', 'sjkdhgjh', '2018-05-03 05:02:32', '2018-05-03 05:02:32'),
(211, '4', '87', '', '2018-05-03 05:02:32', '2018-05-03 05:02:32'),
(212, '1', '88', '2018-05-16', '2018-05-03 05:36:27', '2018-05-03 05:36:27'),
(213, '2', '88', 'male', '2018-05-03 05:36:27', '2018-05-03 05:36:27'),
(214, '3', '88', 'dfdfdfd', '2018-05-03 05:36:27', '2018-05-03 05:36:27'),
(215, '4', '88', '', '2018-05-03 05:36:27', '2018-05-03 05:36:27'),
(216, '1', '89', '04 Apr, 1994', '2018-05-10 12:37:26', '2018-05-10 12:37:26'),
(217, '2', '89', 'male', '2018-05-10 12:28:22', '2018-05-10 12:28:22'),
(218, '3', '89', 'dfgyduf', '2018-05-04 04:55:02', '2018-05-04 04:55:02'),
(219, '4', '89', '', '2018-05-12 10:48:38', '2018-05-12 10:48:38'),
(220, '1', '90', '2018-05-17', '2018-05-04 05:42:14', '2018-05-04 05:42:14'),
(221, '2', '90', 'male', '2018-05-04 05:42:14', '2018-05-04 05:42:14'),
(222, '3', '90', 'dfdfdf', '2018-05-04 05:42:14', '2018-05-04 05:42:14'),
(223, '4', '90', '', '2018-05-04 05:42:14', '2018-05-04 05:42:14'),
(224, '1', '91', '2018-05-08', '2018-05-10 04:44:57', '2018-05-10 04:44:57'),
(225, '2', '91', 'male', '2018-05-10 04:44:57', '2018-05-10 04:44:57'),
(226, '3', '91', 'dfdfdf', '2018-05-10 04:44:57', '2018-05-10 04:44:57'),
(227, '4', '91', '', '2018-05-10 04:44:57', '2018-05-10 04:44:57'),
(228, '1', '92', '2018-05-16', '2018-05-10 04:55:55', '2018-05-10 04:55:55'),
(229, '2', '92', 'male', '2018-05-10 04:55:55', '2018-05-10 04:55:55'),
(230, '3', '92', 'gyu', '2018-05-10 04:55:55', '2018-05-10 04:55:55'),
(231, '4', '92', '', '2018-05-10 04:55:55', '2018-05-10 04:55:55'),
(232, '1', '93', '2018-05-15', '2018-05-10 08:22:43', '2018-05-10 08:22:43'),
(233, '2', '93', 'male', '2018-05-10 08:22:43', '2018-05-10 08:22:43'),
(234, '3', '93', 'dfdfdf', '2018-05-10 08:22:43', '2018-05-10 08:22:43'),
(235, '4', '93', '', '2018-05-10 08:22:43', '2018-05-10 08:22:43'),
(236, '1', '94', '2018-05-15', '2018-05-10 08:23:05', '2018-05-10 08:23:05'),
(237, '2', '94', 'male', '2018-05-10 08:23:05', '2018-05-10 08:23:05'),
(238, '3', '94', 'dfdfdf', '2018-05-10 08:23:05', '2018-05-10 08:23:05'),
(239, '4', '94', '', '2018-05-10 09:31:03', '2018-05-10 09:31:03'),
(240, '1', '95', '2018-05-08', '2018-05-12 09:03:29', '2018-05-12 09:03:29'),
(241, '2', '95', 'female', '2018-05-12 09:03:29', '2018-05-12 09:03:29'),
(242, '3', '95', 'jkk', '2018-05-12 09:03:29', '2018-05-12 09:03:29'),
(243, '4', '95', '', '2018-05-12 09:03:29', '2018-05-12 09:03:29'),
(244, '4', '72', '', '2018-05-12 09:12:24', '2018-05-12 09:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE `users_sessions` (
  `id` int(50) NOT NULL,
  `user_id` varchar(50) NOT NULL DEFAULT '',
  `device_type` varchar(50) DEFAULT NULL,
  `device_id` varchar(200) DEFAULT NULL,
  `screen_size` varchar(200) DEFAULT NULL,
  `device_os` varchar(200) DEFAULT NULL,
  `session_id` varchar(200) DEFAULT NULL,
  `ip_address` varchar(200) DEFAULT NULL,
  `session_status` varchar(200) DEFAULT NULL,
  `location_name` varchar(200) DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `timezone` varchar(50) DEFAULT NULL,
  `notification_token` varchar(300) DEFAULT NULL,
  `last_login` varchar(50) DEFAULT NULL,
  `refresh_token` varchar(1000) DEFAULT NULL,
  `access_token` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_sessions`
--

INSERT INTO `users_sessions` (`id`, `user_id`, `device_type`, `device_id`, `screen_size`, `device_os`, `session_id`, `ip_address`, `session_status`, `location_name`, `latitude`, `longitude`, `browser`, `timezone`, `notification_token`, `last_login`, `refresh_token`, `access_token`, `created_at`, `updated_at`) VALUES
(24, '33', NULL, NULL, NULL, NULL, NULL, '192.168.0.100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-12 10:20:09', NULL, NULL, '2018-05-12 04:50:09', '2018-05-12 04:50:09'),
(25, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '329eb1ff-8db1-4401-aede-e4c27d1c8e2e', '2018-05-12 12:44:46', NULL, NULL, '2018-05-12 07:14:46', '2018-05-12 07:14:46'),
(26, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'a9688949-ae8d-477e-8951-40f94f03c7f9', '2018-05-12 12:53:26', NULL, NULL, '2018-05-12 07:23:26', '2018-05-12 07:23:26'),
(27, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'c63ebff1-16ad-4c0a-a0cb-de6115b8c5a0', '2018-05-12 13:43:51', NULL, NULL, '2018-05-12 08:13:51', '2018-05-12 08:13:51'),
(28, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '4ca15f69-c4cf-46e4-9e34-8c9054aa2717', '2018-05-12 13:59:54', NULL, NULL, '2018-05-12 08:29:54', '2018-05-12 08:29:54'),
(29, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'e667645e-35dd-4add-adbe-0698228fede6', '2018-05-12 14:06:48', NULL, NULL, '2018-05-12 08:36:48', '2018-05-12 08:36:48'),
(30, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'a9638598-032c-4bd6-aafd-6b2e4b1b81f1', '2018-05-12 14:17:41', NULL, NULL, '2018-05-12 08:47:41', '2018-05-12 08:47:41'),
(32, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '3c11fb0f-8448-4e9a-abe1-9b723d8fb4a6', '2018-05-12 14:37:38', NULL, NULL, '2018-05-12 09:07:38', '2018-05-12 09:07:38'),
(33, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '94f441c7-e923-44b4-8220-9caf3f611a41', '2018-05-12 14:51:04', NULL, NULL, '2018-05-12 09:21:04', '2018-05-12 09:21:04'),
(34, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '247c463c-aadb-4342-9d38-cd45b01be501', '2018-05-12 15:11:54', NULL, NULL, '2018-05-12 09:41:54', '2018-05-12 09:41:54'),
(35, '89', NULL, NULL, NULL, NULL, NULL, '192.168.0.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-12 15:32:46', NULL, NULL, '2018-05-12 10:02:46', '2018-05-12 10:02:46'),
(36, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'f0a48927-ca07-4f60-9d42-b8f300c6cb75', '2018-05-12 15:39:08', NULL, NULL, '2018-05-12 10:09:08', '2018-05-12 10:09:08'),
(37, '89', NULL, NULL, NULL, NULL, NULL, '192.168.0.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-12 15:58:46', NULL, NULL, '2018-05-12 10:28:46', '2018-05-12 10:28:46'),
(38, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', 'c057ef82-b5a2-4b04-81ee-b4408ece7fe6', '2018-05-12 16:18:22', NULL, NULL, '2018-05-12 10:48:22', '2018-05-12 10:48:22'),
(39, '89', NULL, 'dfdffrtrt', '1200*800', 'uyu', NULL, '192.168.0.113', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Kolkata', 'shdfhsdsd7sds7dsdgf5fd5f4dfdfd', '2018-05-12 17:16:28', 'dfdfhgdfhdgyu', 'sdvghsdfghsfdsdtsrtdrysty', '2018-05-12 11:46:28', '2018-05-12 11:46:28'),
(40, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '672fdad7-a6ab-495b-a3c4-e5c56c2f063b', '2018-05-12 17:29:57', NULL, NULL, '2018-05-12 11:59:57', '2018-05-12 11:59:57'),
(41, '89', 'android', 'd776a4ef0e322551', '1280*720', '25', NULL, '192.168.0.104', NULL, 'Chandigarh', '30.6425', '76.8173', NULL, 'Asia/Calcutta', '67f5a82a-9885-45a2-9e24-fbb300c54ac8', '2018-05-12 18:06:53', NULL, NULL, '2018-05-12 12:36:53', '2018-05-12 12:36:53'),
(42, '33', NULL, NULL, NULL, NULL, NULL, '192.168.1.7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 12:21:22', NULL, NULL, '2018-05-16 06:51:22', '2018-05-16 06:51:22'),
(43, '33', NULL, NULL, NULL, NULL, NULL, '192.168.1.7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 12:22:27', NULL, NULL, '2018-05-16 06:52:27', '2018-05-16 06:52:27'),
(44, '33', NULL, NULL, NULL, NULL, NULL, '192.168.1.7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 12:23:46', NULL, NULL, '2018-05-16 06:53:46', '2018-05-16 06:53:46'),
(45, '33', NULL, NULL, NULL, NULL, NULL, '192.168.1.7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 12:25:25', NULL, NULL, '2018-05-16 06:55:25', '2018-05-16 06:55:25');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `display_title` varchar(300) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `title`, `display_title`, `created_at`, `updated_at`) VALUES
(1, 'customer', 'Customer', '2018-04-23 18:30:00', '2018-04-23 18:30:00'),
(2, 'driver', 'Driver', '2018-04-23 18:30:00', '2018-04-23 18:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `days_time_slots`
--
ALTER TABLE `days_time_slots`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `features_settings`
--
ALTER TABLE `features_settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notification_triggers`
--
ALTER TABLE `notification_triggers`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `notification_variables`
--
ALTER TABLE `notification_variables`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders_meta_types`
--
ALTER TABLE `orders_meta_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders_meta_values`
--
ALTER TABLE `orders_meta_values`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders_payments`
--
ALTER TABLE `orders_payments`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders_quick_actions_links`
--
ALTER TABLE `orders_quick_actions_links`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders_quick_actions_misc`
--
ALTER TABLE `orders_quick_actions_misc`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_actions_auth_settings`
--
ALTER TABLE `order_actions_auth_settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_misc_actions`
--
ALTER TABLE `order_misc_actions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_products_variants`
--
ALTER TABLE `order_products_variants`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_tax_transactions`
--
ALTER TABLE `order_tax_transactions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_meta_types`
--
ALTER TABLE `product_meta_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_meta_values`
--
ALTER TABLE `product_meta_values`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_variant_types`
--
ALTER TABLE `product_variant_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_meta_types`
--
ALTER TABLE `users_meta_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users_meta_values`
--
ALTER TABLE `users_meta_values`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `days_time_slots`
--
ALTER TABLE `days_time_slots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `features_settings`
--
ALTER TABLE `features_settings`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `notification_triggers`
--
ALTER TABLE `notification_triggers`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notification_variables`
--
ALTER TABLE `notification_variables`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `orders_meta_types`
--
ALTER TABLE `orders_meta_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orders_meta_values`
--
ALTER TABLE `orders_meta_values`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `orders_payments`
--
ALTER TABLE `orders_payments`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `orders_quick_actions_links`
--
ALTER TABLE `orders_quick_actions_links`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders_quick_actions_misc`
--
ALTER TABLE `orders_quick_actions_misc`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_actions_auth_settings`
--
ALTER TABLE `order_actions_auth_settings`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_misc_actions`
--
ALTER TABLE `order_misc_actions`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `order_products_variants`
--
ALTER TABLE `order_products_variants`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_tax_transactions`
--
ALTER TABLE `order_tax_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `order_transactions`
--
ALTER TABLE `order_transactions`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `product_meta_types`
--
ALTER TABLE `product_meta_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_meta_values`
--
ALTER TABLE `product_meta_values`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `product_variant_types`
--
ALTER TABLE `product_variant_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `users_meta_types`
--
ALTER TABLE `users_meta_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_meta_values`
--
ALTER TABLE `users_meta_values`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT for table `users_sessions`
--
ALTER TABLE `users_sessions`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
