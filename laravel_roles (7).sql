-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2018 at 10:19 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_roles`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(50) NOT NULL,
  `role` varchar(191) NOT NULL,
  `linked_id` varchar(191) NOT NULL,
  `contact_type_id` varchar(191) NOT NULL,
  `contact_value` varchar(191) NOT NULL,
  `contact_title` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE `board` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `boardTitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_starred` tinyint(1) NOT NULL,
  `boardPrivacyType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `user_id`, `boardTitle`, `is_starred`, `boardPrivacyType`, `created_at`, `updated_at`) VALUES
(5, 2, 'Board 1 by deepakshi', 0, 'team', '2018-01-30 09:07:26', '2018-01-30 09:07:26');

-- --------------------------------------------------------

--
-- Table structure for table `board_card`
--

CREATE TABLE `board_card` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `list_id` int(10) UNSIGNED NOT NULL,
  `card_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `board_list`
--

CREATE TABLE `board_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `list_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `card_tag`
--

CREATE TABLE `card_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `card_id` int(10) UNSIGNED DEFAULT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `card_task`
--

CREATE TABLE `card_task` (
  `id` int(10) UNSIGNED NOT NULL,
  `card_id` int(10) UNSIGNED DEFAULT NULL,
  `task_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `card_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `card_id`, `user_id`, `comment_description`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'This is not done , it will be donw tomorrow', '2018-01-25 03:31:19', '2018-01-25 03:31:19'),
(2, 3, 1, 'eh ni ajj ho skda ', '2018-01-25 03:33:51', '2018-01-25 03:33:51'),
(3, 22, 1, 'This is first comment', '2018-01-30 03:38:11', '2018-01-30 03:38:11'),
(4, 22, 1, 'this is second one', '2018-01-30 03:38:18', '2018-01-30 03:38:18');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(50) NOT NULL,
  `role` varchar(191) NOT NULL DEFAULT '',
  `linked_id` varchar(191) NOT NULL DEFAULT '',
  `contact_type_id` varchar(191) NOT NULL DEFAULT '',
  `contact_value` varchar(191) NOT NULL DEFAULT '',
  `contact_title` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `role`, `linked_id`, `contact_type_id`, `contact_value`, `contact_title`, `created_at`, `updated_at`) VALUES
(8, 'User', '2', '1', 'sdsd', 'sds', '2018-01-17 01:28:13', '2018-01-17 01:28:13'),
(9, 'User', '2', '2', '565656455', 'fdfdssdsd', '2018-01-17 08:57:43', '2018-01-17 03:27:43'),
(10, 'Admin', '1', '1', 'Harvindersingh@goteso.com', 'Official Email', '2018-01-17 03:31:24', '2018-01-17 03:31:24'),
(12, 'User', '4', '2', '78236565674', 'Manpreet Phone', '2018-01-17 07:23:34', '2018-01-17 07:23:34'),
(13, 'User', '3', '2', 'sdsds', 'sdsd', '2018-01-19 01:09:31', '2018-01-19 01:09:31'),
(14, 'Admin', '1', '3', 'dfdf', 'dfdf', '2018-01-20 04:37:20', '2018-01-20 04:37:20'),
(15, 'Admin', '1', '1', 'dfdf', 'dfdf', '2018-01-20 04:38:25', '2018-01-20 04:38:25'),
(16, 'Employee', '26', '3', 'tert', 'ertert', '2018-01-24 06:31:58', '2018-01-24 06:31:58'),
(17, 'Client', '30', '1', '45654655', '98735463343', '2018-01-25 10:38:54', '2018-01-25 05:08:54');

-- --------------------------------------------------------

--
-- Table structure for table `contact_types`
--

CREATE TABLE `contact_types` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_types`
--

INSERT INTO `contact_types` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Phone', '2018-01-18 18:30:00', '2018-01-19 01:51:10'),
(3, 'Fax', '2018-01-19 01:57:22', '2018-01-19 02:06:54'),
(6, 'Fax2', '2018-01-19 02:03:57', '2018-01-19 02:07:05'),
(9, 'ssdsds vvv', '2018-01-19 02:10:47', '2018-01-19 06:57:55'),
(10, 'fgg', '2018-01-19 02:38:03', '2018-01-19 02:38:03'),
(11, 'hfhfj', '2018-01-20 04:05:48', '2018-01-20 04:05:48'),
(12, 'ydhddh', '2018-01-20 04:05:55', '2018-01-20 04:05:55'),
(13, 'FAX 3', '2018-01-25 04:27:33', '2018-01-25 04:27:33'),
(14, 'cfdd', '2018-01-25 04:29:17', '2018-01-25 04:29:17'),
(15, 'dwdw', '2018-01-25 04:30:52', '2018-01-25 04:30:52'),
(16, 'sdsd', '2018-01-25 04:31:54', '2018-01-25 04:31:54'),
(17, 'ssdsdsddsd', '2018-01-25 04:32:28', '2018-01-25 04:32:28'),
(18, 'sdegttr', '2018-01-25 04:32:40', '2018-01-25 04:32:40'),
(19, 'wwe', '2018-01-25 04:33:46', '2018-01-25 04:33:46'),
(20, 'yfyu', '2018-01-25 04:35:56', '2018-01-25 04:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

CREATE TABLE `credentials` (
  `id` int(50) NOT NULL,
  `project_id` varchar(191) NOT NULL,
  `credentials_type_id` varchar(191) NOT NULL DEFAULT '',
  `host` varchar(191) NOT NULL,
  `user` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `type` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

INSERT INTO `credentials` (`id`, `project_id`, `credentials_type_id`, `host`, `user`, `password`, `type`, `created_at`, `updated_at`) VALUES
(4, '10', '2', 'asdasdsdasd', 'asdsds', 'dasdas', 'Live', '2018-01-19 00:40:31', '2018-01-19 02:57:49'),
(5, '35', '7', 'fff', 'ff', 'ff', 'Live', '2018-01-22 01:26:34', '2018-01-22 01:26:34');

-- --------------------------------------------------------

--
-- Table structure for table `credentials_types`
--

CREATE TABLE `credentials_types` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials_types`
--

INSERT INTO `credentials_types` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'sdsd', '2018-01-19 06:57:31', '2018-01-19 06:57:31'),
(6, 'fgfgt', '2018-01-19 07:00:45', '2018-01-19 07:00:45'),
(7, 'FTP', '2018-01-20 04:06:06', '2018-01-20 04:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'Front End Developer', '2018-01-22 23:51:25', '2018-01-22 23:51:25'),
(4, 'Android Developer', '2018-01-22 23:51:40', '2018-01-22 23:52:16'),
(5, 'IOS Developer', '2018-01-22 23:51:55', '2018-01-22 23:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `employee_designations`
--

CREATE TABLE `employee_designations` (
  `id` int(50) NOT NULL,
  `user_id` varchar(50) NOT NULL DEFAULT '',
  `designation_id` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_designations`
--

INSERT INTO `employee_designations` (`id`, `user_id`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, '1', '5', '2018-01-23 00:37:09', '2018-01-23 00:37:09'),
(3, '1', '2', '2018-01-23 00:37:47', '2018-01-23 00:37:47'),
(11, '1', '4', '2018-01-23 01:16:41', '2018-01-23 01:16:41'),
(17, '12', '5', '2018-01-23 06:44:37', '2018-01-23 06:44:37'),
(18, '12', '4', '2018-01-23 06:44:37', '2018-01-23 06:44:37'),
(19, '6', '4', '2018-01-23 06:44:46', '2018-01-23 06:44:46'),
(20, '4', '4', '2018-01-23 06:44:58', '2018-01-23 06:44:58'),
(21, '4', '2', '2018-01-23 06:44:58', '2018-01-23 06:44:58'),
(22, '2', '5', '2018-01-23 06:45:06', '2018-01-23 06:45:06'),
(23, '2', '4', '2018-01-23 06:45:06', '2018-01-23 06:45:06'),
(24, '2', '2', '2018-01-23 06:45:06', '2018-01-23 06:45:06'),
(26, '14', '5', '2018-01-24 03:22:38', '2018-01-24 03:22:38'),
(28, '5', '5', '2018-01-24 06:10:01', '2018-01-24 06:10:01'),
(29, '16', '5', '2018-01-24 06:14:55', '2018-01-24 06:14:55'),
(30, '24', '5', '2018-01-24 06:21:57', '2018-01-24 06:21:57'),
(31, '25', '5', '2018-01-24 06:22:54', '2018-01-24 06:22:54'),
(32, '27', '5', '2018-01-24 06:26:45', '2018-01-24 06:26:45'),
(33, '31', '5', '2018-01-25 05:52:59', '2018-01-25 05:52:59'),
(34, '31', '4', '2018-01-25 05:52:59', '2018-01-25 05:52:59'),
(35, '31', '2', '2018-01-25 05:52:59', '2018-01-25 05:52:59'),
(36, '32', '5', '2018-01-25 05:55:19', '2018-01-25 05:55:19'),
(37, '33', '5', '2018-01-25 05:55:43', '2018-01-25 05:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(50) NOT NULL,
  `url` varchar(300) NOT NULL DEFAULT '',
  `hash` varchar(300) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2014_10_12_000000_create_users_table', 1),
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2017_04_30_012311_create_posts_table', 1),
(20, '2017_04_30_014352_create_permission_tables', 1),
(21, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(22, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(23, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(24, '2016_06_01_000004_create_oauth_clients_table', 2),
(25, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(26, '2013_05_01_194506_create_links_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(2, 4, 'App\\User'),
(3, 5, 'App\\User'),
(2, 6, 'App\\User'),
(2, 12, 'App\\User'),
(2, 13, 'App\\User'),
(1, 14, 'App\\User'),
(2, 15, 'App\\User'),
(2, 16, 'App\\User'),
(2, 17, 'App\\User'),
(1, 18, 'App\\User'),
(3, 19, 'App\\User'),
(1, 25, 'App\\User'),
(2, 26, 'App\\User'),
(1, 28, 'App\\User'),
(2, 29, 'App\\User'),
(3, 30, 'App\\User'),
(1, 31, 'App\\User'),
(1, 32, 'App\\User'),
(2, 33, 'App\\User'),
(3, 34, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(50) NOT NULL,
  `notes_type` varchar(200) NOT NULL COMMENT '( user, projectSales, projectDevelopment, lead, client, company )',
  `notes` longtext NOT NULL,
  `linked_id` varchar(50) NOT NULL DEFAULT '',
  `pinned` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `notes_type`, `notes`, `linked_id`, `pinned`, `created_at`, `updated_at`) VALUES
(1, 'project_development', 'this is a very first note to this project', '10', '0', '2018-01-22 08:23:39', '0000-00-00 00:00:00'),
(3, 'project_development', 'asdasfas sgreg trtjyuyu jer rtyjythe', '10', '0', '2018-01-22 03:14:25', '2018-01-22 03:14:25'),
(4, 'project_development', 'sdfsdfsdf \'', '10', '0', '2018-01-22 03:14:29', '2018-01-22 03:14:29'),
(5, 'project_development', 'retretert', '37', '0', '2018-01-25 03:51:51', '2018-01-25 03:51:51'),
(6, 'project_development', 'Hello this is a note', '38', '0', '2018-01-25 05:33:50', '2018-01-25 05:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'GroceryApp Personal Access Client', '7dA1G7hNqFH6AsIo1Jj6XouH6eeDJMptu3xHyoTk', 'http://localhost', 1, 0, 0, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(2, NULL, 'GroceryApp Password Grant Client', 'rqMM60n4T5LTrigk10qFN1smlMGRbe5ynXjhaKqA', 'http://localhost', 0, 1, 0, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(3, NULL, 'GroceryApp Personal Access Client', 'ws5mAmi68Dd0SBxzIMlAKLEi4i3RovN8Uy722WEK', 'http://localhost', 1, 0, 0, '2018-01-12 06:18:18', '2018-01-12 06:18:18'),
(4, NULL, 'GroceryApp Password Grant Client', 'JaFZl2A2TrHWhTXv4k4jFF7Vaa2VZtF3riWSr1mT', 'http://localhost', 0, 1, 0, '2018-01-12 06:18:18', '2018-01-12 06:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-01-11 22:36:01', '2018-01-11 22:36:01'),
(2, 3, '2018-01-12 06:18:18', '2018-01-12 06:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_fee_types`
--

CREATE TABLE `payment_fee_types` (
  `id` int(50) NOT NULL,
  `payment_mode_id` varchar(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `fee_value` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE `payment_modes` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `title`, `created_at`, `updated_at`) VALUES
(3, 'sdsds', '2018-01-22 06:10:09', '2018-01-22 06:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `category`, `created_at`, `updated_at`) VALUES
(1, 'view_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(2, 'add_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(3, 'edit_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(4, 'delete_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(5, 'view_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(6, 'add_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(7, 'edit_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(8, 'delete_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(9, 'export_user', 'web', 'users', NULL, NULL),
(10, 'add_contact_types', 'web', 'contact_types', '2018-01-16 22:40:08', NULL),
(11, 'delete_contact_types', 'web', 'contact_types', '2018-01-16 22:40:08', NULL),
(12, 'edit_contact_types', 'web', 'contact_types', '2018-01-16 22:40:08', NULL),
(13, 'export_contact_types', 'web', 'contact_types', '2018-01-16 22:40:08', NULL),
(14, 'view_contact_types', 'web', 'contact_types', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(15, 'add_contact', 'web', 'contacts', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(16, 'view_contact', 'web', 'contacts', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(17, 'delete_contact', 'web', 'contacts', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(18, 'edit_contact', 'web', 'contacts', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(19, 'export_contact', 'web', 'contacts', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(20, 'add_project', 'web', 'projects', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(21, 'view_project', 'web', 'projects', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(22, 'edit_project', 'web', 'projects', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(23, 'delete_project', 'web', 'projects', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(24, 'export_project', 'web', 'projects', '2018-01-16 18:30:00', '2018-01-16 18:30:00'),
(25, 'add_trello_boards', 'web', 'Trello', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(26, 'edit_trello_boards', 'web', 'Trello', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(27, 'delete_trello_boards', 'web', 'Trello', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(28, 'view_trello_boards', 'web', 'Trello', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(29, 'add_realtime_boards', 'web', 'RealTimeBoard', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(30, 'view_realtime_boards', 'web', 'RealTimeBoard', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(31, 'delete_realtime_boards', 'web', 'RealTimeBoard', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(32, 'edit_realtime_boards', 'web', 'RealTimeBoard', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(33, 'add_credentials', 'web', 'Credentials', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(34, 'edit_credentials', 'web', 'Credentials', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(35, 'delete_credentials', 'web', 'Credentials', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(36, 'view_credentials', 'web', 'Credentials', '2018-01-19 18:30:00', '2018-01-19 18:30:00'),
(37, 'add_notes', 'web', 'Notes', '2018-01-21 18:30:00', '2018-01-21 18:30:00'),
(38, 'edit_notes', 'web', 'Notes', '2018-01-21 18:30:00', '2018-01-21 18:30:00'),
(39, 'delete_notes', 'web', 'Notes', '2018-01-21 18:30:00', '2018-01-21 18:30:00'),
(40, 'view_notes', 'web', 'Notes', '2018-01-21 18:30:00', '2018-01-21 18:30:00'),
(41, 'add_credentials_types', 'web', 'CredentialsTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(42, 'view_credentials_types', 'web', 'CredentialsTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(43, 'edit_credentials_types', 'web', 'CredentialsTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(44, 'delete_credentials_types', 'web', 'CredentialsTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(45, 'view_payment_modes', 'web', 'PaymentModes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(46, 'edit_payment_modes', 'web', 'PaymentModes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(47, 'add_payment_modes', 'web', 'PaymentModes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(48, 'delete_payment_modes', 'web', 'PaymentModes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(49, 'view_payment_fee_types', 'web', 'PaymentFeeTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(50, 'edit_payment_fee_types', 'web', 'PaymentFeeTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(51, 'add_payment_fee_types', 'web', 'PaymentFeeTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(52, 'delete_payment_fee_types', 'web', 'PaymentFeeTypes', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(53, 'view_designations', 'web', 'Designations', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(54, 'edit_designations', 'web', 'Designations', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(55, 'add_designations', 'web', 'Designations', '2018-01-24 18:30:00', '2018-01-24 18:30:00'),
(56, 'delete_designations', 'web', 'Designations', '2018-01-24 18:30:00', '2018-01-24 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(50) NOT NULL,
  `project_title` varchar(300) NOT NULL DEFAULT '',
  `project_start_date` varchar(191) DEFAULT NULL,
  `slack_channel_id` varchar(300) NOT NULL DEFAULT '',
  `drive_admin_folder` varchar(2000) NOT NULL DEFAULT '',
  `drive_development_folder` varchar(2000) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_title`, `project_start_date`, `slack_channel_id`, `drive_admin_folder`, `drive_development_folder`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Maluka IAS', '25/05/2018 11:16 PM', 'fdfdf', 'asasasasAdmin', 'asasasa DEVELOPMENT', '0', '2018-01-17 04:47:11.000000', '2018-01-25 05:44:39.000000'),
(8, 'sds', '19/01/2018 12:00 AM', 'sdfsdsdsd', '', '', '0', '2018-01-17 04:54:40.000000', '2018-01-17 04:54:40.000000'),
(10, 'fdg', '18/01/2018 12:00 AM', 'fgdfg', '', '', '3', '2018-01-17 05:50:42.000000', '2018-01-19 06:23:54.000000'),
(11, 'sdsdsd', '19/01/2018 12:00 AM', 'sdsdsd', '', '', '1', '2018-01-19 05:34:05.000000', '2018-01-19 05:34:05.000000'),
(12, 'sdsdsd', '19/01/2018 12:00 AM', 'sdsdsd', '', '', '1', '2018-01-19 05:34:28.000000', '2018-01-19 05:34:28.000000'),
(13, 'sdsdsd', '20/01/2018 12:00 AM', 'sdsdsd d', '', '', '1', '2018-01-19 05:36:05.000000', '2018-01-19 06:21:52.000000'),
(14, 'sdsdsd', '19/01/2018 12:00 AM', 'sdsdsd', '', '', '1', '2018-01-19 05:36:32.000000', '2018-01-19 05:36:32.000000'),
(15, 'sdsdsd', '19/01/2018 12:00 AM', 'sdsdsd', '', '', '1', '2018-01-19 05:36:46.000000', '2018-01-19 05:36:46.000000'),
(17, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:02.000000', '2018-01-19 05:40:02.000000'),
(18, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:16.000000', '2018-01-19 05:40:16.000000'),
(19, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:26.000000', '2018-01-19 05:40:26.000000'),
(20, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:36.000000', '2018-01-19 05:40:36.000000'),
(21, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:49.000000', '2018-01-19 05:40:49.000000'),
(22, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:40:56.000000', '2018-01-19 05:40:56.000000'),
(23, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:41:15.000000', '2018-01-19 05:41:15.000000'),
(24, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:41:24.000000', '2018-01-19 05:41:24.000000'),
(25, 'sdsd', '20/04/2018 12:00 AM', 'sdsds', '', '', '0', '2018-01-19 05:42:26.000000', '2018-01-19 05:42:26.000000'),
(26, 'asdasdas', '19/01/2018 12:00 AM', 'sadasd', '', '', '0', '2018-01-19 05:42:44.000000', '2018-01-19 05:42:44.000000'),
(27, 'asdasdas', '19/01/2018 12:00 AM', 'sadasd', '', '', '0', '2018-01-19 05:43:16.000000', '2018-01-19 05:43:16.000000'),
(28, 'asdasdas', '19/01/2018 12:00 AM', 'sadasd', '', '', '1', '2018-01-19 05:43:49.000000', '2018-01-19 06:34:01.000000'),
(29, 'asdasdas', '19/01/2018 12:00 AM', 'sadasd', '', '', '0', '2018-01-19 05:44:45.000000', '2018-01-19 05:44:45.000000'),
(30, 'asdasdas', '20/01/2018 12:00 AM', 'sadasd', '', '', '0', '2018-01-19 05:45:19.000000', '2018-01-20 04:50:30.000000'),
(34, 'sdsdsd', '20/01/2018 12:00 AM', 'sdsdsd', '', '', '1', '2018-01-19 07:15:02.000000', '2018-01-19 07:15:02.000000'),
(35, 'New Project', '22/01/2018 12:00 AM', 'random', '', '', '0', '2018-01-22 00:40:21.000000', '2018-01-25 04:06:34.000000'),
(36, 'sdasdafsdfsd', '22/01/2018 12:00 AM', 'dsfsdfsdf', '0B0i5b-bmW8wpaXN2cnVCaVFQMFU', '0B0i5b-bmW8wpaXN2cnVCaVFQMFU', '0', '2018-01-22 04:19:01.000000', '2018-01-22 04:19:01.000000'),
(37, 'fdsfdf', '23/01/2018 12:00 AM', 'sdfsdf d', '0B0i5b-bmW8wpaXN2cnVCaVFQMFU', '0B0i5b-bmW8wpaXN2cnVCaVFQMFU', '0', '2018-01-23 05:55:54.000000', '2018-01-25 04:07:46.000000'),
(38, 'sdefffff dfuydfd', '24/01/2018 12:00 AM', 'sdsdsd', '', '', '3', '2018-01-24 06:40:06.000000', '2018-01-25 06:23:08.000000');

-- --------------------------------------------------------

--
-- Table structure for table `project_clients`
--

CREATE TABLE `project_clients` (
  `id` int(50) NOT NULL,
  `project_id` varchar(50) NOT NULL DEFAULT '',
  `client_id` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_clients`
--

INSERT INTO `project_clients` (`id`, `project_id`, `client_id`, `created_at`, `updated_at`) VALUES
(2, '16', '5', '2018-01-19 05:37:19', '2018-01-19 05:37:19'),
(3, '32', '5', '2018-01-19 05:45:44', '2018-01-19 05:45:44'),
(4, '33', '5', '2018-01-19 05:46:02', '2018-01-19 05:46:02'),
(10, '13', '5', '2018-01-19 06:21:53', '2018-01-19 06:21:53'),
(11, '28', '5', '2018-01-19 06:34:04', '2018-01-19 06:34:04'),
(15, '34', '5', '2018-01-19 22:19:25', '2018-01-19 22:19:25'),
(16, '30', '5', '2018-01-20 04:50:30', '2018-01-20 04:50:30'),
(19, '36', '5', '2018-01-22 04:19:01', '2018-01-22 04:19:01'),
(32, '35', '30', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(33, '35', '19', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(34, '35', '5', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(37, '37', '30', '2018-01-25 04:07:46', '2018-01-25 04:07:46'),
(38, '37', '5', '2018-01-25 04:07:46', '2018-01-25 04:07:46'),
(39, '4', '19', '2018-01-25 05:44:39', '2018-01-25 05:44:39'),
(43, '38', '30', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(44, '38', '19', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(45, '38', '5', '2018-01-25 06:23:08', '2018-01-25 06:23:08');

-- --------------------------------------------------------

--
-- Table structure for table `project_employees`
--

CREATE TABLE `project_employees` (
  `id` int(50) NOT NULL,
  `project_id` varchar(50) NOT NULL DEFAULT '',
  `employee_id` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_employees`
--

INSERT INTO `project_employees` (`id`, `project_id`, `employee_id`, `created_at`, `updated_at`) VALUES
(12, '16', '3', '2018-01-19 05:37:19', '2018-01-19 05:37:19'),
(14, '25', '4', '2018-01-19 05:42:26', '2018-01-19 05:42:26'),
(15, '25', '3', '2018-01-19 05:42:26', '2018-01-19 05:42:26'),
(16, '25', '2', '2018-01-19 05:42:26', '2018-01-19 05:42:26'),
(17, '26', '4', '2018-01-19 05:42:44', '2018-01-19 05:42:44'),
(18, '26', '3', '2018-01-19 05:42:44', '2018-01-19 05:42:44'),
(19, '26', '2', '2018-01-19 05:42:44', '2018-01-19 05:42:44'),
(20, '27', '4', '2018-01-19 05:43:16', '2018-01-19 05:43:16'),
(21, '27', '3', '2018-01-19 05:43:16', '2018-01-19 05:43:16'),
(22, '27', '2', '2018-01-19 05:43:16', '2018-01-19 05:43:16'),
(26, '29', '4', '2018-01-19 05:44:45', '2018-01-19 05:44:45'),
(27, '29', '3', '2018-01-19 05:44:45', '2018-01-19 05:44:45'),
(28, '29', '2', '2018-01-19 05:44:45', '2018-01-19 05:44:45'),
(32, '31', '4', '2018-01-19 05:45:32', '2018-01-19 05:45:32'),
(33, '31', '3', '2018-01-19 05:45:32', '2018-01-19 05:45:32'),
(34, '31', '2', '2018-01-19 05:45:32', '2018-01-19 05:45:32'),
(35, '32', '4', '2018-01-19 05:45:44', '2018-01-19 05:45:44'),
(36, '32', '3', '2018-01-19 05:45:44', '2018-01-19 05:45:44'),
(37, '32', '2', '2018-01-19 05:45:44', '2018-01-19 05:45:44'),
(38, '33', '4', '2018-01-19 05:46:02', '2018-01-19 05:46:02'),
(39, '33', '3', '2018-01-19 05:46:02', '2018-01-19 05:46:02'),
(40, '33', '2', '2018-01-19 05:46:02', '2018-01-19 05:46:02'),
(68, '13', '1', '2018-01-19 06:21:53', '2018-01-19 06:21:53'),
(69, '13', '1', '2018-01-19 06:21:53', '2018-01-19 06:21:53'),
(70, '21', '4', '2018-01-19 06:23:44', '2018-01-19 06:23:44'),
(74, '28', '4', '2018-01-19 06:34:04', '2018-01-19 06:34:04'),
(75, '28', '3', '2018-01-19 06:34:04', '2018-01-19 06:34:04'),
(76, '28', '2', '2018-01-19 06:34:04', '2018-01-19 06:34:04'),
(82, '34', '2', '2018-01-19 22:19:25', '2018-01-19 22:19:25'),
(83, '30', '4', '2018-01-20 04:50:30', '2018-01-20 04:50:30'),
(84, '30', '2', '2018-01-20 04:50:30', '2018-01-20 04:50:30'),
(87, '36', '8', '2018-01-22 04:19:01', '2018-01-22 04:19:01'),
(88, '36', '6', '2018-01-22 04:19:01', '2018-01-22 04:19:01'),
(89, '36', '4', '2018-01-22 04:19:01', '2018-01-22 04:19:01'),
(90, '36', '2', '2018-01-22 04:19:01', '2018-01-22 04:19:01'),
(112, '35', '29', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(113, '35', '26', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(114, '35', '17', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(115, '35', '6', '2018-01-25 04:06:34', '2018-01-25 04:06:34'),
(119, '37', '29', '2018-01-25 04:07:46', '2018-01-25 04:07:46'),
(120, '37', '6', '2018-01-25 04:07:46', '2018-01-25 04:07:46'),
(121, '37', '4', '2018-01-25 04:07:46', '2018-01-25 04:07:46'),
(122, '4', '2', '2018-01-25 05:44:39', '2018-01-25 05:44:39'),
(128, '38', '29', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(129, '38', '26', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(130, '38', '17', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(131, '38', '16', '2018-01-25 06:23:08', '2018-01-25 06:23:08'),
(132, '38', '15', '2018-01-25 06:23:08', '2018-01-25 06:23:08');

-- --------------------------------------------------------

--
-- Table structure for table `project_real_time_boards`
--

CREATE TABLE `project_real_time_boards` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `project_id` varchar(200) NOT NULL DEFAULT '',
  `realtime_board_id` varchar(200) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_real_time_boards`
--

INSERT INTO `project_real_time_boards` (`id`, `title`, `project_id`, `realtime_board_id`, `created_at`, `updated_at`) VALUES
(2, 'dsdsdsdsdsdsf', '10', 'sdsd', '2018-01-18 01:06:35', '2018-01-18 02:21:52'),
(3, 'dfdfdfddf dfdfdfd  vvvv', '10', 'dfdfdfdf hhhhhhhhh', '2018-01-18 01:05:21', '2018-01-19 00:01:30'),
(7, 'resaltimeboard1sdsdsdds', '10', 'o9J_k0aVj08=/?', '2018-01-18 01:08:00', '2018-01-19 00:00:48'),
(9, 'Testing', '35', 'Testing', '2018-01-22 01:21:27', '2018-01-22 01:21:27');

-- --------------------------------------------------------

--
-- Table structure for table `project_trello_boards`
--

CREATE TABLE `project_trello_boards` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `project_id` varchar(200) NOT NULL DEFAULT '',
  `trello_board_id` varchar(200) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_trello_boards`
--

INSERT INTO `project_trello_boards` (`id`, `title`, `project_id`, `trello_board_id`, `created_at`, `updated_at`) VALUES
(1, 'treeloboard1 vvv', '7', 'kV07RP9z', '2018-01-17 18:30:00', '2018-01-18 02:28:25'),
(5, 'sdsdsdsddfdfdfdfdfdfdfdfdfdfdfdf treekl9', '7', 'kV07RP9z', '2018-01-18 01:10:02', '2018-01-18 02:28:16'),
(6, 'dfdfdfdfdfdfd', '8', 'dfdfdfdfdfdffdf', '2018-01-18 02:28:57', '2018-01-18 02:28:57'),
(7, 'dfdfdfdfbbbdfdfdf', '8', 'dfdfddfdfdfdfdfnnn', '2018-01-18 02:29:04', '2018-01-18 02:30:27'),
(9, 'rtytry', '10', 'yrtytry', '2018-01-18 03:06:22', '2018-01-18 03:06:22'),
(11, 'sdsdsd', '10', 'sdsd', '2018-01-19 00:10:43', '2018-01-19 00:10:43'),
(14, 'random', '35', 'hbbAGJEw', '2018-01-22 01:23:57', '2018-01-22 01:27:53');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2018-01-11 00:45:10', '2018-01-11 00:45:10'),
(2, 'Employee', 'web', '2018-01-11 03:30:00', '2018-01-11 03:30:00'),
(3, 'Client', 'web', '2018-01-18 05:43:43', '2018-01-18 05:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 3),
(6, 1),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(8, 3),
(9, 1),
(9, 3),
(10, 1),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(25, 2),
(26, 1),
(27, 1),
(28, 1),
(28, 2),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(39, 1),
(40, 1),
(40, 2),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(49, 2),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(50) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `designation_id` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `designation_id`, `created_at`, `updated_at`) VALUES
(12, 'tag1', '4', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(13, 'tag2', '4', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(14, 'tag3', '4', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(15, 'ios1', '5', '2018-01-23 23:20:04', '2018-01-23 23:20:04'),
(16, 'ios2', '5', '2018-01-23 23:20:04', '2018-01-23 23:20:04'),
(17, 'frontend1', '2', '2018-01-23 23:20:28', '2018-01-23 23:20:28'),
(18, 'frontend2', '2', '2018-01-23 23:20:28', '2018-01-23 23:20:28'),
(19, 'dfdsf', '5', '2018-01-24 04:17:41', '2018-01-24 04:17:41'),
(20, 'ghj', '5', '2018-01-24 04:17:41', '2018-01-24 04:17:41'),
(21, 'sdfsdf', '5', '2018-01-24 04:17:49', '2018-01-24 04:17:49'),
(22, 'ddd', '5', '2018-01-24 04:23:14', '2018-01-24 04:23:14'),
(23, 'Amsterdam', '2', '2018-01-25 04:44:13', '2018-01-25 04:44:13'),
(24, 'Washington', '2', '2018-01-25 04:44:13', '2018-01-25 04:44:13'),
(25, 'Amsterdam', '5', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(26, 'Washington', '5', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(27, 'dfdf', '5', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(28, 'sjdhgj', '5', '2018-01-25 06:26:12', '2018-01-25 06:26:12'),
(29, 'sdsd', '5', '2018-01-25 06:26:12', '2018-01-25 06:26:12'),
(30, 'df', '5', '2018-01-25 06:27:45', '2018-01-25 06:27:45'),
(31, 'erwer', '5', '2018-01-25 22:34:06', '2018-01-25 22:34:06'),
(32, 'gfyhfyu', '5', '2018-01-25 22:34:06', '2018-01-25 22:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE `tutorials` (
  `id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `author_id` varchar(50) NOT NULL,
  `designation_id` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutorials`
--

INSERT INTO `tutorials` (`id`, `title`, `author_id`, `designation_id`, `content`, `created_at`, `updated_at`) VALUES
(13, 'Test Tutorials 1', '1', '4', '<p>This is a first tutorial added by harvinder</p>', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(14, 'IOS tutorials edited', '1', '5', '<p>this is ios tutorial edutedd dfdfdf</p>', '2018-01-23 23:20:04', '2018-01-24 04:23:19'),
(15, 'Front end tutorial', '1', '2', '<p>This is front end tutorial</p>', '2018-01-23 23:20:28', '2018-01-23 23:20:28'),
(16, 'sdasdfsada', '1', '2', '<p>asdasdasd</p>', '2018-01-25 04:44:13', '2018-01-25 04:44:13'),
(17, 'sdfsdfsdfsdf', '1', '5', '<p>dfdfdfdfdff</p>', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(18, 'hhh', '1', '2', '<p>ggggg</p>', '2018-01-25 04:45:00', '2018-01-25 04:45:00'),
(19, 'Test for samreet', '1', '5', '<p>dsdadsadfdffdf</p>', '2018-01-25 06:26:12', '2018-01-25 06:26:12'),
(20, 'ddfdfdfdf', '1', '5', '<p>dfdfdfdfdf</p>', '2018-01-25 06:27:45', '2018-01-25 06:27:45'),
(21, 'ddfdfdfdf', '1', '5', '<p>dfdfdfdfdf</p>', '2018-01-25 06:43:43', '2018-01-25 06:43:43'),
(22, 'ddfdfdfdf', '1', '5', '<p>dfdfdfdfdf</p>', '2018-01-25 06:43:57', '2018-01-25 06:43:57'),
(23, 'gretertt', '1', '5', '<p>fdgtygrt</p>', '2018-01-25 22:34:06', '2018-01-25 22:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials_tags`
--

CREATE TABLE `tutorials_tags` (
  `id` int(50) NOT NULL,
  `tag_id` varchar(50) NOT NULL DEFAULT '',
  `tutorial_id` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutorials_tags`
--

INSERT INTO `tutorials_tags` (`id`, `tag_id`, `tutorial_id`, `created_at`, `updated_at`) VALUES
(6, '12', '13', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(7, '13', '13', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(8, '14', '13', '2018-01-23 22:16:41', '2018-01-23 22:16:41'),
(11, '17', '15', '2018-01-23 23:20:28', '2018-01-23 23:20:28'),
(12, '18', '15', '2018-01-23 23:20:28', '2018-01-23 23:20:28'),
(19, '21', '14', '2018-01-24 04:23:19', '2018-01-24 04:23:19'),
(20, '22', '14', '2018-01-24 04:23:19', '2018-01-24 04:23:19'),
(21, '23', '16', '2018-01-25 04:44:13', '2018-01-25 04:44:13'),
(22, '24', '16', '2018-01-25 04:44:13', '2018-01-25 04:44:13'),
(23, '25', '17', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(24, '26', '17', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(25, '27', '17', '2018-01-25 04:44:43', '2018-01-25 04:44:43'),
(26, '28', '19', '2018-01-25 06:26:12', '2018-01-25 06:26:12'),
(27, '29', '19', '2018-01-25 06:26:12', '2018-01-25 06:26:12'),
(28, '30', '20', '2018-01-25 06:27:45', '2018-01-25 06:27:45'),
(29, '31', '23', '2018-01-25 22:34:06', '2018-01-25 22:34:06'),
(30, '32', '23', '2018-01-25 22:34:06', '2018-01-25 22:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Harvinder', 'Singh', 'harvindersingh@goteso.com', '$2y$10$BQFDo0NFjBs.MFKstd6eEets70akJoxUcbijyxsh8iXvmgHpx/.Ni', 'ucNigxm9M8J2GGr8KbPrKNxd7jK3I77E0jKD2aLSf8WOZYle6dNeoW55HurK', '2018-01-11 00:47:36', '2018-01-25 04:22:54'),
(2, 'Deepakshi', 'Singla', 'deepakshisingla@goteso.com', '$2y$10$BQFDo0NFjBs.MFKstd6eEets70akJoxUcbijyxsh8iXvmgHpx/.Ni', 'yRU2oiPmHD8jUtailyDPjFNUphegyH14hmgzxNhPcho7Z1ygVEPR2WomEUXw', '2018-01-11 00:46:13', '2018-01-23 06:45:06'),
(4, 'Manpreet', 'Singh', 'manpreetsingh@goteso.com', '$2y$10$c17QwXAA6FUjPaRIupK1D.GfywOhM5O6cO7paU6szg2MD8lFoAvqu', NULL, '2018-01-17 07:21:46', '2018-01-17 07:21:46'),
(5, 'John', 'Doe', 'johndoe@gmail.com', '$2y$10$ldWFrGRAEZ195Mq8Oaj7qu5viZn9pGqr7z5YZ/jgIobYydP.JZO4G', NULL, '2018-01-19 05:21:11', '2018-01-19 05:21:11'),
(6, 'sdsds', 'Sdsd', 'harvindersingh3@goteso.com', '$2y$10$ufBDFyIRhvf.02lsauHydeRCrgVVMr47tY5/plBaITSuCypxOG/CO', NULL, '2018-01-20 01:59:22', '2018-01-20 01:59:22'),
(12, 'dfdfdffdf', 'ddfdfdfdfd', 'harvindersinghdfdfdfdfdfdf@goteso.com', '$2y$10$JXlBPL/EAgmXTgC4jr3h6uWyGL8.jOAMLziMMFCq1sLNE7uPsrTQe', NULL, '2018-01-23 00:58:33', '2018-01-23 00:58:33'),
(13, 'Samreet', 'Kaur', 'samreet@goteso.com', '$2y$10$0caHViwAui4dnoupFpCuweAyzLmgt6rJ5N0E./dK0GeePCQqCh/SG', NULL, '2018-01-24 03:18:59', '2018-01-24 03:18:59'),
(14, 'dgffgfg', 'gfgfgf', 'fgfg@fhg.yytyty', '$2y$10$VI54KtQkThPhixE1BOS6S.lGhGjxhOSfFX4cMh6ien5PB9T26XL9q', NULL, '2018-01-24 03:22:38', '2018-01-24 03:22:38'),
(15, 'Samreet', 'kaur', 'samreet@gmail.com', '$2y$10$natCgLylrDudtytpT3zzqO9MYBG24PSMGtm5ycgu6gxVR.Bq.BOZO', NULL, '2018-01-24 06:03:40', '2018-01-24 06:17:52'),
(16, 'test1', 'sdhgh', 'test1@gmail.com', '$2y$10$XVbwzrKMDbZ/hkekOr59s.R9qGGIccv9ftj0tDZ7wArbQEnhgsJLi', NULL, '2018-01-24 06:14:55', '2018-01-24 06:14:55'),
(17, 'Test 2', 'ghdgu', 'test2@gmail.com', '$2y$10$IVIIA89ikoWffTD8uresG.Z.h1L2aNsPeBRwGdFcpSggoOEGohMMO', NULL, '2018-01-24 06:15:11', '2018-01-24 06:15:11'),
(18, 'Test4', 'jhgyu', 'test4@gmail.com', '$2y$10$19M68aILPA70EIqQsny0uu9jR/iWill.j0Q/ljbRybt9jeIM8XAY2', NULL, '2018-01-24 06:17:02', '2018-01-24 06:17:02'),
(19, 'wsewe', 'wewew', 'ewe@dfg.erer', '$2y$10$0QhilVrOBJhfj5IwTeEPMOkNkAyWK21aCCXYKg50TrgCKvtK8gPJy', NULL, '2018-01-24 06:17:34', '2018-01-24 06:17:34'),
(23, 'adm', 'adm', 'adm@gmail.com', '$2y$10$pjOgihJ4g5j8fxGhsaua2eapwPBBCe2X719Wd7DKiB732231cqxie', NULL, '2018-01-24 06:21:33', '2018-01-24 06:21:33'),
(24, 'admd', 'admd', 'admd@gmail.com', '$2y$10$ZG6YFvx6pU1PXfP2QlzmWuwFjdT6g5WDNnH82xze1Tt5jky.KcqWW', NULL, '2018-01-24 06:21:57', '2018-01-24 06:21:57'),
(25, 'aaa', 'fsdfsdf', 'aaa@gmail.com', '$2y$10$h97LBJW.FVBbe6nKs01SaulxgMfXW8eFAR0BM/8MkwZ5JjO/i9uIG', NULL, '2018-01-24 06:22:54', '2018-01-24 06:22:54'),
(26, 'sssssssss', 'ssssss', 'ssssss@ssss.com', '$2y$10$4iuRbO7EehYBrAooI6mo4u5k2RoHbZTEYHiNlEF9vo3J68.llpj0K', NULL, '2018-01-24 06:23:29', '2018-01-24 06:23:29'),
(28, 'admin', 'admin', 'admin@admin.com', '$2y$10$osmQvtX7jFZ7/U9OWub7Bel/lN8f/YhwcsV1d/kfXwTBIomf.Jf9C', NULL, '2018-01-24 06:28:46', '2018-01-24 06:28:46'),
(29, 'emp', 'emp', 'emp@emp.com', '$2y$10$5CONBNa8eU7LwKYot5ekE.8lmQ90.wJsuDk6Yi234xKW8PdbFSMuS', NULL, '2018-01-24 06:29:00', '2018-01-24 06:29:00'),
(30, 'cli', 'cli', 'cli@cli.com', '$2y$10$bDqLXhk5pSY33naLPGRaLOuNCPEUQC4GqvRx2/Dd4dddfWPS85ZIq', NULL, '2018-01-24 06:29:19', '2018-01-24 06:29:19'),
(31, 'All1', 'sdhsdghu', 'all1@gmailc.om', '$2y$10$UOQqDJpJ63aZyetob7cimOcbynx5YpzfUjp7Gt2bgMZTUFRCmL1zW', NULL, '2018-01-25 05:52:59', '2018-01-25 05:52:59'),
(32, 'Admin1', 'hjghu', 'admin1@gmail.com', '$2y$10$3Ye0k/VltO1EpczT.ktHTeszJ65SVuKBnCEiCnbQJ72iFUqv9L8ia', NULL, '2018-01-25 05:55:19', '2018-01-25 05:55:19'),
(33, 'Emp11', 'iuhyiu', 'jkhsuydfg!@jkuy.fgd', '$2y$10$drFZRfupS4hYFvQEJFovcOz7jhvGlLxmHSeLs/23cuxdzyVGCLe4i', NULL, '2018-01-25 05:55:43', '2018-01-25 05:55:43'),
(34, 'cccq', 'cccccc', 'cccc@ccc.ccc', '$2y$10$fMV4QFFDuR2vylKBXzCCtOsVoWCiXfc25a.ZpP4XXimLdnHvasAga', NULL, '2018-01-25 05:56:40', '2018-01-25 05:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `changed_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activity_in_id` int(11) NOT NULL,
  `activity_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `user_id`, `changed_in`, `activity_in_id`, `activity_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'board', 1, 'created a board', '2018-01-25 03:28:45', '2018-01-25 03:28:45'),
(2, 1, 'board_list', 1, 'created a list', '2018-01-25 03:28:55', '2018-01-25 03:28:55'),
(3, 1, 'board_list', 2, 'created a list', '2018-01-25 03:29:03', '2018-01-25 03:29:03'),
(4, 1, 'board_card', 1, 'created a card', '2018-01-25 03:29:10', '2018-01-25 03:29:10'),
(5, 1, 'board_card', 2, 'created a card', '2018-01-25 03:29:17', '2018-01-25 03:29:17'),
(6, 1, 'board_card', 3, 'created a card', '2018-01-25 03:29:24', '2018-01-25 03:29:24'),
(7, 1, 'board_card', 4, 'created a card', '2018-01-25 03:29:32', '2018-01-25 03:29:32'),
(8, 1, 'board_card', 2, 'card is edited', '2018-01-25 03:30:23', '2018-01-25 03:30:23'),
(9, 1, 'card_task', 2, 'task is added', '2018-01-25 03:30:42', '2018-01-25 03:30:42'),
(10, 1, 'card_task', 2, 'task is added', '2018-01-25 03:30:53', '2018-01-25 03:30:53'),
(11, 1, 'comment', 1, 'posted a comment', '2018-01-25 03:31:19', '2018-01-25 03:31:19'),
(12, 1, 'board_card', 2, 'card is edited', '2018-01-25 03:31:28', '2018-01-25 03:31:28'),
(13, 1, 'card_task', 3, 'task is added', '2018-01-25 03:33:04', '2018-01-25 03:33:04'),
(14, 1, 'card_task', 3, 'task is added', '2018-01-25 03:33:09', '2018-01-25 03:33:09'),
(15, 1, 'card_task', 3, 'task is added', '2018-01-25 03:33:13', '2018-01-25 03:33:13'),
(16, 1, 'comment', 2, 'posted a comment', '2018-01-25 03:33:52', '2018-01-25 03:33:52'),
(17, 1, 'card_task', 4, 'task is added', '2018-01-25 03:37:02', '2018-01-25 03:37:02'),
(18, 1, 'card_task', 4, 'task is added', '2018-01-25 03:37:08', '2018-01-25 03:37:08'),
(19, 1, 'board_card', 8, 'deleted a card', '2018-01-26 06:23:01', '2018-01-26 06:23:01'),
(20, 1, 'board_card', 13, 'created a card', '2018-01-26 06:23:09', '2018-01-26 06:23:09'),
(21, 1, 'board_card', 13, 'deleted a card', '2018-01-26 06:23:27', '2018-01-26 06:23:27'),
(22, 1, 'board_list', 8, 'deleted a list', '2018-01-26 06:25:37', '2018-01-26 06:25:37'),
(23, 1, 'board_list', 9, 'deleted a list', '2018-01-26 06:25:42', '2018-01-26 06:25:42'),
(24, 1, 'board_list', 11, 'deleted a list', '2018-01-26 06:25:45', '2018-01-26 06:25:45'),
(25, 1, 'board_list', 10, 'deleted a list', '2018-01-26 06:25:52', '2018-01-26 06:25:52'),
(26, 1, 'board_card', 14, 'created a card', '2018-01-26 06:26:05', '2018-01-26 06:26:05'),
(27, 1, 'board_list', 14, 'created a list', '2018-01-26 06:28:39', '2018-01-26 06:28:39'),
(28, 1, 'board_card', 15, 'created a card', '2018-01-26 06:28:50', '2018-01-26 06:28:50'),
(29, 1, 'board_card', 15, 'deleted a card', '2018-01-26 06:34:47', '2018-01-26 06:34:47'),
(30, 1, 'board_card', 16, 'created a card', '2018-01-26 06:35:08', '2018-01-26 06:35:08'),
(31, 1, 'board_list', 15, 'created a list', '2018-01-26 06:46:00', '2018-01-26 06:46:00'),
(32, 1, 'board_card', 17, 'created a card', '2018-01-26 06:46:04', '2018-01-26 06:46:04'),
(33, 1, 'board', 3, 'created a board', '2018-01-26 06:47:33', '2018-01-26 06:47:33'),
(34, 1, 'board_list', 16, 'created a list', '2018-01-26 06:47:57', '2018-01-26 06:47:57'),
(35, 1, 'board_card', 18, 'created a card', '2018-01-26 06:48:19', '2018-01-26 06:48:19'),
(36, 1, 'board', 4, 'created a board', '2018-01-26 06:52:25', '2018-01-26 06:52:25'),
(37, 1, 'board_list', 17, 'created a list', '2018-01-26 06:52:37', '2018-01-26 06:52:37'),
(38, 1, 'board_card', 19, 'created a card', '2018-01-26 06:52:54', '2018-01-26 06:52:54'),
(39, 1, 'board_list', 18, 'created a list', '2018-01-26 07:14:01', '2018-01-26 07:14:01'),
(40, 1, 'board_card', 20, 'created a card', '2018-01-26 07:14:17', '2018-01-26 07:14:17'),
(41, 1, 'board_card', 21, 'created a card', '2018-01-26 09:17:55', '2018-01-26 09:17:55'),
(42, 1, 'board_list', 19, 'created a list', '2018-01-26 09:18:13', '2018-01-26 09:18:13'),
(43, 1, 'board_card', 22, 'created a card', '2018-01-26 09:18:22', '2018-01-26 09:18:22'),
(44, 1, 'board_card', 22, 'card is edited', '2018-01-26 09:40:26', '2018-01-26 09:40:26'),
(45, 1, 'board_card', 22, 'card is edited', '2018-01-26 09:40:46', '2018-01-26 09:40:46'),
(46, 1, 'card_task', 22, 'task is added', '2018-01-26 09:40:55', '2018-01-26 09:40:55'),
(47, 1, 'card_task', 22, 'task is added', '2018-01-26 09:41:01', '2018-01-26 09:41:01'),
(48, 1, 'card_task', 22, 'task is added', '2018-01-26 09:41:17', '2018-01-26 09:41:17'),
(49, 1, 'card_task', 22, 'task is added', '2018-01-26 09:41:22', '2018-01-26 09:41:22'),
(50, 1, 'card_task', 22, 'task is added', '2018-01-26 09:41:25', '2018-01-26 09:41:25'),
(51, 1, 'board_card', 22, 'card is edited', '2018-01-30 03:37:26', '2018-01-30 03:37:26'),
(52, 1, 'board_card', 22, 'card is edited', '2018-01-30 03:37:32', '2018-01-30 03:37:32'),
(53, 1, 'board_card', 22, 'card is edited', '2018-01-30 03:37:40', '2018-01-30 03:37:40'),
(54, 1, 'board_card', 22, 'card is edited', '2018-01-30 03:37:54', '2018-01-30 03:37:54'),
(55, 1, 'comment', 3, 'posted a comment', '2018-01-30 03:38:11', '2018-01-30 03:38:11'),
(56, 1, 'comment', 4, 'posted a comment', '2018-01-30 03:38:18', '2018-01-30 03:38:18'),
(57, 1, 'board_card', 22, 'card is edited', '2018-01-30 03:38:20', '2018-01-30 03:38:20'),
(58, 1, 'board_list', 19, 'deleted a list', '2018-01-30 03:38:26', '2018-01-30 03:38:26'),
(59, 1, 'board_card', 20, 'deleted a card', '2018-01-30 03:38:33', '2018-01-30 03:38:33'),
(60, 1, 'board_card', 21, 'card is edited', '2018-01-30 03:40:38', '2018-01-30 03:40:38'),
(61, 1, 'board_card', 21, 'card is edited', '2018-01-30 03:40:43', '2018-01-30 03:40:43'),
(62, 1, 'board_card', 21, 'card is edited', '2018-01-30 03:40:49', '2018-01-30 03:40:49'),
(63, 1, 'board_card', 23, 'created a card', '2018-01-30 03:41:40', '2018-01-30 03:41:40'),
(64, 1, 'board_card', 24, 'created a card', '2018-01-30 03:41:44', '2018-01-30 03:41:44'),
(65, 1, 'card_task', 23, 'task is added', '2018-01-30 03:42:10', '2018-01-30 03:42:10'),
(66, 1, 'board_card', 23, 'card is edited', '2018-01-30 03:42:15', '2018-01-30 03:42:15'),
(67, 1, 'card_task', 21, 'task is added', '2018-01-30 03:42:31', '2018-01-30 03:42:31'),
(68, 1, 'card_task', 21, 'task is added', '2018-01-30 03:42:34', '2018-01-30 03:42:34'),
(69, 1, 'card_task', 21, 'task is added', '2018-01-30 03:42:37', '2018-01-30 03:42:37'),
(70, 1, 'board_card', 21, 'card is edited', '2018-01-30 03:42:40', '2018-01-30 03:42:40'),
(71, 2, 'board', 5, 'created a board', '2018-01-30 09:07:26', '2018-01-30 09:07:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `board`
--
ALTER TABLE `board`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_user_id_foreign` (`user_id`);

--
-- Indexes for table `board_card`
--
ALTER TABLE `board_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_card_board_id_foreign` (`board_id`),
  ADD KEY `board_card_user_id_foreign` (`user_id`),
  ADD KEY `board_card_list_id_foreign` (`list_id`);

--
-- Indexes for table `board_list`
--
ALTER TABLE `board_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `board_list_list_name_unique` (`list_name`),
  ADD KEY `board_list_board_id_foreign` (`board_id`),
  ADD KEY `board_list_user_id_foreign` (`user_id`);

--
-- Indexes for table `card_tag`
--
ALTER TABLE `card_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_tag_card_id_foreign` (`card_id`);

--
-- Indexes for table `card_task`
--
ALTER TABLE `card_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_task_card_id_foreign` (`card_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_card_id_foreign` (`card_id`),
  ADD KEY `comment_user_id_foreign` (`user_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `contact_types`
--
ALTER TABLE `contact_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `credentials`
--
ALTER TABLE `credentials`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `credentials_types`
--
ALTER TABLE `credentials_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `employee_designations`
--
ALTER TABLE `employee_designations`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD UNIQUE KEY `50` (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_fee_types`
--
ALTER TABLE `payment_fee_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `project_clients`
--
ALTER TABLE `project_clients`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `project_employees`
--
ALTER TABLE `project_employees`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `project_real_time_boards`
--
ALTER TABLE `project_real_time_boards`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `project_trello_boards`
--
ALTER TABLE `project_trello_boards`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tutorials_tags`
--
ALTER TABLE `tutorials_tags`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_activity_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `board`
--
ALTER TABLE `board`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `board_card`
--
ALTER TABLE `board_card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `board_list`
--
ALTER TABLE `board_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `card_tag`
--
ALTER TABLE `card_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `card_task`
--
ALTER TABLE `card_task`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `contact_types`
--
ALTER TABLE `contact_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `credentials`
--
ALTER TABLE `credentials`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `credentials_types`
--
ALTER TABLE `credentials_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_designations`
--
ALTER TABLE `employee_designations`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_fee_types`
--
ALTER TABLE `payment_fee_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `project_clients`
--
ALTER TABLE `project_clients`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `project_employees`
--
ALTER TABLE `project_employees`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `project_real_time_boards`
--
ALTER TABLE `project_real_time_boards`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `project_trello_boards`
--
ALTER TABLE `project_trello_boards`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tutorials`
--
ALTER TABLE `tutorials`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tutorials_tags`
--
ALTER TABLE `tutorials_tags`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
