var app = angular.module("mainApp", ['ui.directives', 'ui.filters', '720kb.datepicker', 'ngMaterial', 'ngMessages', 'ngAnimate', 'toastr'])

app.factory('cartStorage', function() {
    var _cart = {
        products: [],
    };
    var service = {
        get cart() {
            return _cart;
        }
    }
    return service;
})


app.controller('orderController', function($http, $scope, cartStorage, $window, $log, $q, $timeout, toastr) {



    $scope.time = '';
    var _this = this;
    _this.cartStorage = cartStorage.cart;
    _this.total = 0;

    $('.order-content').css('display', 'none');
    $('#loading').css('display', 'block');


    $scope.isDisabled = false;
    $http.get(APP_URL + '/add_order_form')
        .success(function(data, status, headers, config) {

            _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');

        })
        .error(function(data, status, headers, config) {

            document.getElementById('res').value = JSON.stringify(data);
        });



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.checkedIndex = function(values) { 
        $scope.selected_values = values; 
        //alert(JSON.stringify($scope.selected_values));
    }
 
    
	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.isSelected = function(field) {
        if (field.type == 'api') {
            //alert(JSON.stringify(field.id));
            field.value = JSON.stringify($scope.selected_values);
        }
        return $scope.selected_values === values;
    }


	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _this.addToCart = function(product) {
 
        $scope.product = product;
 
        $scope.id = product.id;
        $scope.product_id = {
            "product_id": $scope.id
        }
        //alert(JSON.stringify($scope.product_id));

        if (product.variants_exist == '0') {
            var alreadyInCart = '';
            var cartIndex = 0;

            if (_this.cartStorage.products.length == 0) {

                product.discounted_price = product.base_price - (product.base_price * product.base_discount / 100);

                product.quantity = 1;
				product.variants = [];
                _this.cartStorage.products.push(product);
                document.getElementById("res8").value = JSON.stringify(_this.cartStorage.products);
				
				$scope.order_products = _this.cartStorage.products;
				
				$scope.order_products.data = {"order_products": $scope.order_products};
				 
				 var request = $http({
                    method: "POST",
                    url: APP_URL + '/place_order_calculate',
                    data: $scope.order_products.data,
                    headers: {
                       'Accept': 'application/json'
                    }
                });

                 /* Check whether the HTTP Request is successful or not. */
                 request.success(function(data) { 
                    _this.paymentData = data;  
					$(".main-cart").show();	
					$(".cart").hide();	
				    //document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        alert('error'); 
                    document.getElementById("res8").value = JSON.stringify(data);
                });
                return;
				
            }

            for (var i = 0; i < _this.cartStorage.products.length; i++) {

                if (_this.cartStorage.products[i].id == product.id) {
                    alreadyInCart = 'YES';
                    cartIndex = i;
                }

            }

            if (alreadyInCart == 'YES') {
                _this.cartStorage.products[cartIndex].quantity = _this.cartStorage.products[cartIndex].quantity + 1;
				
				$scope.order_products = _this.cartStorage.products;
				
				$scope.order_products.data = {"order_products": $scope.order_products};
				
				
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) { 
                    _this.paymentData = data;  
					$(".main-cart").show();	
					$(".cart").hide();	
				    //document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        //alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });
 
                return;
            } else {

                product.quantity = 1;
				product.variants = [];
                product.discounted_price = product.base_price - (product.base_price * product.base_discount / 100);

                _this.cartStorage.products.push(product); 
               document.getElementById("res8").value = JSON.stringify(_this.cartStorage.products);
				
				
				$scope.order_products = _this.cartStorage.products;
				
				$scope.order_products.data = {"order_products": $scope.order_products};
				
				
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) { 
                    _this.paymentData = data;  
					$(".main-cart").show();	
					$(".cart").hide();	
				    //document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });
 
 
 
                //return;

            }
        } else {
            var request = $http({
                method: "POST",
                url: APP_URL + '/get_product_variants',
                data: $scope.product_id,
                headers: {
                    'Accept': 'application/json'
                }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {

                //alert('success'); 
                _this.variantsdata = data;
                 //alert(JSON.stringify(_this.variantsdata));
                //alert(_this.cartStorage.products.indexOf(product.title)); 
                // alert(_this.cartStorage.products.length);
                /*
                 for(var i=0; i<=_this.cartStorage.products.length; i++){
                      alert( _this.cartStorage.products[i].id);
                	  alert(name);
                   alert(i);
                   
                  }*/
 
                $("#variantsModal").modal('show');

                $scope.variantsArray = [];
 
            }).error(function(data, status, headers, config) {

                document.getElementById("res").value = JSON.stringify(data);
            });
 
            //alert(JSON.stringify(product));
            //var index2 = _this.findIndex(_this.cartStorage.products);
            //alert("index"+JSON.stringify(index));

        }


    };




	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.getVariantValue = function(variants) {
        $scope.variantsSelectedTitle = []; 
        var alreadyInVariants = '';
        var variantsIndex = 0;
        $scope.v = variants;
        if ($scope.variantsArray.length == 0) {
            $scope.variantsArray.push(variants);
            return;
        }

        for (var i = 0; i < $scope.variantsArray.length; i++) {

            if ($scope.variantsArray[i].id == variants.id) {
                alreadyInVariants = 'YES';
                variantsIndex = i;
            }

        }

        if (alreadyInVariants == 'YES') {
            $scope.variantsArray.splice(variantsIndex, 1);
            return;
        } else {

            $scope.variantsArray.push(variants);
            return;

        }
		 
    }



	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.getVariantValue2 = function(variants) {
		
		
        $scope.variantsSelectedTitle = [];
         //alert(JSON.stringify(variants));
        var alreadyInVariants = '';
        var variantsIndex = 0;

        if ($scope.variantsArray.length == 0) {
            $scope.variantsArray.push(variants);
            return;
        }

        for (var i = 0; i < $scope.variantsArray.length; i++) {

            if ($scope.variantsArray[i].product_variant_type_id == variants.product_variant_type_id) {
                alreadyInVariants = 'YES';
                variantsIndex = i;
            }

        }

        if (alreadyInVariants == 'YES') {
            $scope.variantsArray.splice(variantsIndex, 1); 
            $scope.variantsArray.push(variants); 
            return;
        } else {

            $scope.variantsArray.push(variants);
            return;

        }
		
		
    }

    /*		if (itemIndex > -1) {
                        _this.cartStorage.products.splice(itemIndex, 1);
    					_this.enableMe = true;
                    }
    				*/


    $scope.variantsSelectedTitle = [];
	
	
	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _this.addVariant = function() {
        // alert(JSON.stringify($scope.product)); 

        //$scope.value =  $('#v').val();
        //alert($scope.value);
        if ($scope.variantsArray == '') {
            alert('Please choose atleast one variant');
        } 
		
		else {
			$scope.product.variants = $scope.variantsArray;
            $scope.product.quantity = 1;

            var price = $scope.product.base_price;

            for (var j = 0; j < $scope.variantsArray.length; j++) {
                if ($scope.variantsArray[j].price) {

                    price = $scope.variantsArray[j].price;
                }
            }

            var productToAdd = angular.copy($scope.product);
            productToAdd.base_price = price;
 
              for (var j = 0; j < $scope.variantsArray.length; j++) {

                 if ($scope.variantsArray[j].price_difference) {
                      productToAdd.base_price = parseInt(productToAdd.base_price) + parseInt($scope.variantsArray[j].price_difference);
                 }
              }
           productToAdd.discounted_price = productToAdd.base_price - (productToAdd.base_price * productToAdd.base_discount /100);
         
 
            for (var j = 0; j < $scope.variantsArray.length; j++) {
                if ($scope.product.id == $scope.variantsArray[j].product_id) {

                    $scope.variantsSelectedTitle.push($scope.variantsArray[j].title);
                }

                $scope.variantsSelectedTitleData = $scope.variantsSelectedTitle.toString();
            }
			
			var t = 0;
			/*for (var j = 0; j < $scope.variantsArray.length; j++) {
			for (var k = 0; k < _this.variantsdata.length; k++) {
				alert("variant id"+_this.variantsdata[k].id );
			    if (_this.variantsdata[k].id == $scope.variantsArray[j].product_variant_type_id) {
					
					t++;
					
					alert("count"+t);
					//if(_this.variantsdata[k].minimum_selection_needed ){
						//alert('Please select atleast two');
					//}
					
				}
			}
			}*/
			
			for (var j = 0; j < $scope.variantsArray.length; j++) {  
			
				for (var k = 0; k < _this.variantsdata.length; k++) {
				//alert("variant Array selcted id"+ $scope.variantsArray[j].product_variant_type_id );
				    if(_this.variantsdata[k].minimum_selection_needed >= 2){
			            if($scope.variantsArray[j].product_variant_type_id == $scope.v.product_variant_type_id) {
					        t++; 
				        } 		
				    }
			    }
			}
			
			
			/*for (var k = 0; k < _this.variantsdata.length; k++) {  
			    // alert("count"+t);
			    //alert("data-"+JSON.stringify(_this.variantsdata));
				//alert("min-"+JSON.stringify(_this.variantsdata[k].maximum_selection_needed));
				/* for (var j = 0; j < $scope.variantsArray.length; j++) {
                if(_this.variantsdata[k].minimum_selection_needed >= 1  && _this.variantsdata[k].maximum_selection_needed <= 1){
	               
						 alert("v pid "+ JSON.stringify($scope.variantsArray));
						 alert("v id"+JSON.stringify(_this.variantsdata));
	                if($scope.variantsArray[j].product_variant_type_id != _this.variantsdata[k].id){
	                    alert( "choose "+_this.variantsdata[k].title+' variant is required');
	                    return;
	                }
					else{}
					 }
                }* 
	
	            if(_this.variantsdata[k].minimum_selection_needed >= 2){
							
				    //alert(JSON.stringify(_this.variantsdata[k].maximum_selection_needed)+"-"+JSON.stringify(_this.variantsdata[k].title));
				    //alert(JSON.stringify("v id"+_this.variantsdata[k].id));
								
				    for (var j = 0; j < $scope.variantsArray.length; j++) {
					//alert(JSON.stringify("sel v id"+$scope.variantsArray[j].product_variant_type_id));
						
						if($scope.variantsArray[j].product_variant_type_id == _this.variantsdata[k].id) {
							  
                            if(t > _this.variantsdata[k].maximum_selection_needed){
						        alert('you can choose maximum'+_this.variantsdata[k].maximum_selection_needed+' values of '+ _this.variantsdata[k].title+ 'variant');
						        return;
					        }
					  
					        else if(t < _this.variantsdata[k].minimum_selection_needed){
						        alert('you can choose '+_this.variantsdata[k].maximum_selection_needed+' values of '+ _this.variantsdata[k].title+ ' variant');
						        return;
					        }
					    }
 
				    }		
						 
			    } 
					 		
			}*/
					   
		    _this.cartStorage.products.push(productToAdd);
            $("#variantsModal").modal('hide');
			
			$scope.order_products = _this.cartStorage.products;
				
				$scope.order_products.data = {"order_products": $scope.order_products};
			document.getElementById("res8").value = JSON.stringify($scope.order_products.data);
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) { 
                    _this.paymentData = data;  
					$(".main-cart").show();	
					$(".cart").hide();	
				    //document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });
            //document.getElementById("res4").value = JSON.stringify(_this.cartStorage.products);
        }

    }
	
	
	
	
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _this.payment = function() {
        _this.subTotal = _this.sumPrice(_this.cartStorage.products);

        //alert(JSON.stringify("total--"+ _this.total))
        product.addedToCart = true;
        product.discounted_price = product.base_price - (product.base_price * product.base_discount / 100);

        product.quantity = 1;
        //alert(JSON.stringify(product.totalPrice));
        // _this.subtotal = product.base_price;
        //alert(JSON.stringify("subtotal--"+ _this.subtotal));
    };


	
	
	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 
    _this.sumPrice = function(product, index) {

        var total = 0;
        angular.forEach(product, function(value, index) {
            //alert(JSON.stringify(value));
            //alert(JSON.stringify(value.base_price));
            total += parseFloat(value.base_price);
            //alert("t"+total);
        });
        return total.toFixed(2);
    }

    /*   _this.increaseProductAmount = function(product) {
                product.quantity++;
                product.showAddToCart = true;
            }

            _this.decreaseProductAmount = function(product) {
                product.quantity--;
                if (product.quantity <= 0) {
                    product.quantity = 0;
                    product.addedToCart = false;
                    product.showAddToCart = false;
                    var itemIndex = _this.cartStorage.products.indexOf(product);
                    if (itemIndex > -1) {
                        _this.cartStorage.products.splice(itemIndex, 1);
    					 
                    }
                } else {
                    product.showAddToCart = true;
    				
                }
            } */



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.get_address = function(parent_identifier) {

        //alert(parent_identifier);

        var customer_id = $('#' + parent_identifier + '2').val();
        if (customer_id == '') {
            alert('Please Select User First');
        } else {
            $("#myModal").modal('show');

            /*get data from api**/
            $http.get(APP_URL + '/user_address_list/' + customer_id + '?search_text=&limit=100&date_filter=lastAdded&fields=id,address_line1,address_line2,city,state,country,pincode,created_at')
                .success(function(data, status, headers, config) {

                    _this.columns_addresses = data.columns;
                    _this.data_addresses = data.data;
                    _this.currentpage_addresses = data.current_page;
                    _this.user_id = user_id;
                })
                .error(function(data, status, headers, config) {
                    document.getElementById('res').value = JSON.stringify(data);
                });
        }
    };


    
    $scope.selected = 0;
    $scope.selected1 = 6;

	
	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.get_pickupTime = function() {

        $("#pickupModal").modal('show');

        $http.get(APP_URL + '/get_time_slots')
            .success(function(data, status, headers, config) {

                _this.pickupDateData = data;

                $scope.selectedValue = _this.pickupDateData[0].display_date;
                $scope.selectedValue1 = _this.pickupDateData[0].date;
                $scope.pickupDate2 = $scope.selectedValue;
                $scope.pickupDate3 = $scope.selectedValue1;

                document.getElementById('res9').value = JSON.stringify(data);
            })
            .error(function(data, status, headers, config) {
                document.getElementById('res').value = JSON.stringify(data);
            });
    };


    $scope.getPickupD = function(value, value1) {
        $scope.pickupDate = value;
        $scope.pickupDate1 = value1;
    }

    $scope.savePickup = function(identifier, id, field, from_time, to_time) {

        if (!$scope.pickupDate) {
            $scope.pickupDate = $scope.pickupDate2;
            var time = id;
            $scope.timeShowFrom = from_time;
            $scope.timeShowTo = to_time;
            $scope.pickupTime = $scope.pickupDate3;

            $scope.pickupTimeShow = $scope.pickupDate + ' | ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo;
            field.value = $scope.pickupTime;

            $("#pickupModal").modal('hide');
            $scope.deliveryTime = '';
            $scope.deliveryTimeShow = '';
            document.getElementById('delivery_time').value = '';
            document.getElementById(identifier).value = $scope.pickupTime;

            //alert('Please choose pickup Date First'); 
        } else {

            var time = id;
            $scope.timeShowFrom = from_time;
            $scope.timeShowTo = to_time;
            $scope.pickupTime = $scope.pickupDate1;

            $scope.pickupTimeShow = $scope.pickupDate + ' | ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo;
            field.value = $scope.pickupTime;

            $("#pickupModal").modal('hide');
            $scope.deliveryTime = '';
            $scope.deliveryTimeShow = '';
            document.getElementById('delivery_time').value = '';
            document.getElementById(identifier).value = $scope.pickupTime;
        }
    };



    $scope.get_deliveryTime = function(parent_identifier, identifier) {
        var pickup_time = $('#' + parent_identifier).val();

        if (pickup_time) {
            $("#deliveryModal").modal('show');


            $http.get(APP_URL + '/get_time_slots?date1=' + pickup_time + '&identifier=' + identifier)
                .success(function(data, status, headers, config) {
                    _this.deliveryDateData = data;
                    //alert(JSON.stringify( _this.deliveryDateData));
                    $scope.selectedValue = _this.deliveryDateData[0].display_date;
                    $scope.selectedValue1 = _this.deliveryDateData[0].date;
                    $scope.deliveryDate2 = $scope.selectedValue;
                    $scope.deliveryDate3 = $scope.selectedValue1;
                    $scope.deliveryDate = '';
                    document.getElementById('res13').value = JSON.stringify(_this.deliveryDateData);
                })
                .error(function(data, status, headers, config) {
                    document.getElementById('res').value = JSON.stringify(data);
                });
        } else {
            alert('Please choose pickup time');
        }
    };


    $scope.getDeliveryD = function(value, value1) {

        $scope.deliveryDate = value;
        $scope.deliveryDate1 = value1;
    }

    $scope.saveDelivery = function(identifier, id, field, from_time, to_time) {
        if (!$scope.deliveryDate) {
            $scope.deliveryDate = $scope.deliveryDate2;
            $scope.time1 = id;
            $scope.timeShowFrom1 = from_time;
            $scope.timeShowTo1 = to_time;
            $scope.deliveryTime = $scope.deliveryDate3;;
            $scope.deliveryTimeShow = $scope.deliveryDate + ' | ' + $scope.timeShowFrom1 + ' - ' + $scope.timeShowTo1;
            field.value = $scope.deliveryTime;
            $("#deliveryModal").modal('hide');
        } else {
            $scope.time1 = id;
            $scope.timeShowFrom1 = from_time;
            $scope.timeShowTo1 = to_time;
            $scope.deliveryTime = $scope.deliveryDate1;;
            $scope.deliveryTimeShow = $scope.deliveryDate + ' | ' + $scope.timeShowFrom1 + ' - ' + $scope.timeShowTo1;
            field.value = $scope.deliveryTime;
            $("#deliveryModal").modal('hide');
        }
    };



    $scope.addOrder = function() {


        var subtotal = 0;
        for (var i = 0; i < _this.cartStorage.products.length; i++) {

            subtotal = subtotal + (_this.cartStorage.products[i].discounted_price * _this.cartStorage.products[i].quantity);
        }

        $scope.msg = 'Data sent: ' + angular.toJson(_this.addOrderMeta);
        $scope.msg1 = 'Data sent: ' + angular.toJson(_this.cartStorage.products);

        for (var j = 0; j < _this.addOrderMeta.fields.length; j++) {

            for (var k = 0; k < _this.addOrderMeta.fields[j].length; k++) {
                $scope.required = _this.addOrderMeta.fields[j][k].required_or_not;

                if ($scope.required == 1 || $scope.required == '1') {

                    $scope.valuecheck = _this.addOrderMeta.fields[j][k].value;
                    if ($scope.valuecheck == '' || $scope.valuecheck == 'undefined') {
                        toastr.error('Please ' + _this.addOrderMeta.fields[j][k].title, 'Error!');
                        return false;
                    }

                }
            }
        }



        $scope.order_status = "Pending";
        $scope.order = [{
            "order_status": $scope.order_status
        }];

        $scope.coupon_discount = 0;
        //$scope.total = _this.subTotal - $scope.coupon_discount;
        $scope.total = subtotal;

        //alert(JSON.stringify($scope.total));

        $scope.orders_payments = [{
            "sub_total": subtotal,
            "coupon_id": "NaN",
            "coupon_code": "NaN",
            "coupon_discount": '0',
            "total": $scope.total
        }];

        $scope.orderData = {
            "order": $scope.order,
            "orders_payments": $scope.orders_payments,
            "order_products": _this.cartStorage.products,
            "order_meta_fields": _this.addOrderMeta
        };
 
 
        var request = $http({
            method: "POST",
            url: APP_URL + '/place_order',
            data: $scope.orderData,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
            toastr.success('Order Added Successfully', 'Success');
            $window.location.href = 'order_detail_page?order_id=' + data.order_id;


            document.getElementById("res").value = JSON.stringify(data);

        }).error(function(data, status, headers, config) {
            toastr.error('Error Occurs', 'Error!');

            document.getElementById("res8").value = JSON.stringify(data);
        });


    };




    _this.querySearch = function(query) {

        return $http.get(APP_URL + "/users_search?search_text=" + query + "&user_type=customer", {
                params: {
                    q: query
                }
            })
            .then(function(response) {
                return response.data;
            })
    };



    _this.selectedItemChange = function(item, field) {
        $log.info('Item changed to ' + JSON.stringify(item));
        if (field.identifier == 'customer_id') {
            $scope.customer_id_value = field.value = item.id;

        }

        $scope.selected_values = '';
    }


	
	
	
	
	
	
	
	
	
	//following code is used for select area select box
	
	 _this.areaSearch = function(query1) {
 
          return $http.get(APP_URL + "/search_areas?search_text="+query1, {
                params: {
                    q: query1
                }
            })
            .then(function(response) {
				
		 
                return response.data;
            })
    };



    _this.selectedAreaChange = function(item, field) {
        $log.info('Item changed to ' + JSON.stringify(item));
        if (field.identifier == 'service_area_id') {
            $scope.service_area_id_value = field.value = item.id;

        }
 
    }


});




app.controller('cartController', function($http, cartStorage,$scope,$window, $log, $q, $timeout, toastr) {
    var _this = this;
    _this.cartStorage = cartStorage.cart;

    _this.increaseItemAmount = function(product) {
        //product.quantity = 1;
        product.price = product.base_price;
        var quantity = product.quantity++;
        
		
		$scope.order_products = _this.cartStorage.products;
				$scope.order_products.data = {"order_products": $scope.order_products};
				 
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                   // alert('success'); 
                    _this.paymentData = data; 
                $(".cart").show();
               $(".main-cart").hide();					 
				    document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        //alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });
				
				document.getElementById('quantity').value = quantity;
        product.totalPrice = product.price * product.quantity;
		
        //alert(product.totalPrice);
    }

    _this.decreaseItemAmount = function(product) {
        product.quantity--;
		
       $scope.order_products = _this.cartStorage.products;
				$scope.order_products.data = {"order_products": $scope.order_products};
				 
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                   // alert('success'); 
                    _this.paymentData = data; 
                $(".cart").show();
               $(".main-cart").hide();					 
				    document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        //alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });
				
        if (product.quantity <= 0) {
			 
            product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
            if (itemIndex > -1) {
				
                _this.cartStorage.products.splice(itemIndex, 1);
document.getElementById("res8").value = JSON.stringify(_this.cartStorage.products);
                _this.enableMe = true;
				
				
				
 
            }
        }
    }

    _this.removeFromCart = function(product) {

        var itemIndex = _this.cartStorage.products.indexOf(product);
        _this.cartStorage.products.splice(itemIndex, 1); 
		$scope.order_products = _this.cartStorage.products;
				
				$scope.order_products.data = {"order_products": $scope.order_products}; 
				
				 var request = $http({
                     method: "POST",
                     url: APP_URL + '/place_order_calculate',
                     data: $scope.order_products.data,
                     headers: {
                       'Accept': 'application/json'
                     }
                });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) { 
                    _this.paymentData = data; 
$(".main-cart").hide();		
$(".cart").show();				
				    document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                }).error(function(data, status, headers, config) { 
			        alert('error'); 
                    document.getElementById("res").value = JSON.stringify(data);
                });

        /*	   product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
			alert(itemIndex);
            if (itemIndex > -1) {
              var a =  _this.cartStorage.products.splice(itemIndex, 1);
				alert(JSON.stringify(a));
				
				alert(JSON.stringify(_this.cartStorage.products)); */

        document.getElementById('res1').value = JSON.stringify(_this.cartStorage.products);
        //$scope.enableMe = true;
        // }
    }
});

function getPickupTime() {

    var x = document.getElementById("pickupTimePicker").value;

}