var app = angular.module("mainApp", ['ngRoute','ngAnimate', 'toastr','angular-jquery-locationpicker']);

  
app.controller('storeController', function($http,$scope,toastr) {
 
  
 
  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address')
                        },
                        radius: 300,
                        enableAutocomplete: true
                    };
					
 
});




app.controller('editStoreController', function($http,$scope,toastr) {
  
	   $scope.locationpickerOptions = {
                        location: {
                            latitude: document.getElementById('latitude').value ,
                            longitude: document.getElementById('longitude').value
                        },
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                             longitudeInput: $('#longitude'),
                            radiusInput: $('#us3-radius'),
                             locationNameInput: $('#us3-address')
                        },
                        radius: 300,
                        enableAutocomplete: true
                    };
 
					
 
});






