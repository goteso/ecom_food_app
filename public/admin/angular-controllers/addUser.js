var app = angular.module("mainApp", ['ngTagsInput', '720kb.datepicker','ngAnimate', 'toastr']) 
  
   
//====================================================== ADD USER CONTROLLER================================================
 
app.controller('userController', function($http,$scope,$window,toastr) {
	 
	$('#loading').css('display', 'block');
    $scope.time = '';
	$scope.addUser = $window.controller_data; 
    $('#loading').css('display', 'none');
	
	 
//Route 4 - to add user========================================================================= 
//============================================================================================== 
  
    $scope.add = function() {	 
         var photo = $('#photo').val(); 
		 for(var i=0;i<$scope.addUser[0].fields.length;i++){
			 $scope.required = $scope.addUser[0].fields[i].required_or_not;
			  
			 if( $scope.required == 1 || $scope.required == '1') 
			 {  
		       $scope.valuecheck = $scope.addUser[0].fields[i].value;  
		        if($scope.valuecheck == '' || $scope.valuecheck == 'undefined'){
			   toastr.error('Please Enter '+$scope.addUser[0].fields[i].identifier,'Error!'); 
			   return false;
				}
			 
			 }
            if($scope.addUser[0].fields[i].identifier == 'photo'){
	           $scope.addUser[0].fields[i].value = photo;
            } 
		} 
		 var request = $http({
            method: "POST",
            url: APP_URL+'/user',
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });
       request.success(function(data, status, headers, config) {
            if(data.status_code == 0){
                toastr.error(data.message,'Error!');
            }
            else{
		         $scope.user_id = data["user_id"];
			     document.getElementById("res").value = JSON.stringify(data);	 
		         $window.location.href = 'user_edit_form/'+$scope.user_id;
            }
        }).error(function (data, status, headers, config) { 
            toastr.error('Error Occurred','Error!');
	        document.getElementById("res").value = JSON.stringify(data);
        });    
 
    }; 
  
 //============================================================================================== 
 //============================================================================================== 
   $scope.selected = 0;
});




 
 
//====================================================== Edit USER CONTROLLER================================================

 app.controller('userEditController', function($http,$scope,$window,toastr) {

    $('#loading').css('display', 'block');
	
    ////////Get Data Direct from controller////////////
    $scope.editUser = $window.controller_data_edit;
    $scope.user_id = $window.controller_data_edit[2];
	
    $('#loading').css('display', 'none');
 
  
  
   //Route 6 - Controller to edit user===================================================================== 
   //====================================================================================================== 
  	$scope.edit = function() {
	    var photo = $('#photo').val();
	    for(var i=0;i<$scope.editUser[0].fields.length;i++){
	        if($scope.editUser[0].fields[i].identifier == 'photo'){
	            $scope.editUser[0].fields[i].value = photo;
            }  
		}
	  $scope.editUser[0]['id'] = $scope.user_id;
      var request = $http({
            method: "PUT",
            url: APP_URL+'/user',
            data: $scope.editUser ,
            headers: { 'Accept':'application/json'  }
		});
		/* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            toastr.success(data.message,'Success!');
            document.getElementById("res").value = JSON.stringify(data);	
        }).error(function (data, status, headers, config) { 
			toastr.error('Error Occurred','Error!');
			document.getElementById("res").value = JSON.stringify(data);
        });    
 
    }; 
	
   //===========================================================================================================
   //=========================================================================================================== 
	
	
 });
  
