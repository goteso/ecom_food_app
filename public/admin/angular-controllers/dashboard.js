var app = angular.module("mainApp", [])

app.directive('hcChart', function() {
    return {
        restrict: 'E',
        template: '<div id="container" style="margin: 0 auto">not working</div',
        scope: {
            options: '='
        },
        link: function(scope, element) {
            var chart = new Highcharts.chart(element[0], scope.options);
            $(window).resize(function() {
                chart.reflow();
            });
        }
    };
})

app.controller('dashboardController', function($http, $scope, $window) {
	
	   $('#loading').css('display', 'block');
  
    $scope.dashboardData = $window.controller_data;
 
    document.getElementById('res').value = JSON.stringify($scope.dashboardData[2].data);

        $('#loading').css('display', 'none');
    $scope.orders_x_data = $scope.dashboardData[2].data;
    var d_array_labels = [];
    var d_array_values = [];
    angular.forEach($scope.orders_x_data, function(value, key) {
        d_array_labels.push(value.label);
        d_array_values.push(value.y);
    }, '');



    $scope.ordersChartOptions = {
		chart: { 
		        height: 70 + '%', // 16:9 ratio
		},
        title: {
            text: ''
        },
        xAxis: {
            gridLineWidth: 0,
            tickMarks: 0,
            tickInterval: 1,
            categories: d_array_labels
        },
        yAxis: [{ // Primary yAxis 
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
				minPadding: 0,
                maxPadding: 0
            }
        }],
        credits: {
            enabled: false
        },
        legend: {
            enabled: false,
        },
        exporting: {
            enabled: false
        },

        // legend: { },
        options: {
            chart: {  
                type: 'line'
            }
        },
        colors: ['#a2cd3b', '#babbbd'],
        tooltip: {
            backgroundColor: '#f5f5f5',
            borderColor: '#fff',
            borderRadius: 10,
            borderWidth: 1,
            crosshairs: true,
           /* formatter: function() {
                var point = this.points[0];
                return '<b>' + point.series.name + '</b><br/>' + Highcharts.dateFormat('%A %B %e %Y', this.x) + ': <br/>' +
                    '1 USD = ' + Highcharts.numberFormat(point.y, 2) + ' EUR';
            },*/
            shared: true
        },
        series: [{
			name:'Orders',
            type: 'areaspline',
            data: d_array_values
        }],

    };



    $scope.sales_x_data = $scope.dashboardData[1].data; 
    $scope.sales_x_data = $scope.dashboardData[1].data;
    var d_array_sales_labels = [];
    var d_array_sales_values = [];

    document.getElementById("res").value = JSON.stringify($scope.sales_x_data);
 
    angular.forEach($scope.sales_x_data, function(value, key) {
        d_array_sales_labels.push(value.label);
        d_array_sales_values.push(value.y);
    }, '');

 
    $scope.salesChartOptions = {
		chart: { 
		        height: 70 + '%', // 16:9 ratio
		},
        title: {
            text: ''
        },
        xAxis: {
            categories: d_array_sales_labels,
            labels: {
                rotation: -45,
                style: {
                    fontSize: '11px',
                    fontFamily: 'Open Sans, sans-serif'
                }
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                }

            }
        },

        series: {
            showInNavigator: false
			},
        credits: {
            enabled: false
        },
        legend: {
            enabled: false,
        },
        exporting: {
            enabled: false
        },

        // legend: { },
        options: {
            chart: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        series: [{
			name:'Sales',
            type: 'line',
            data: d_array_sales_values
        }],


        colors: ['#4b8f42'],
		tooltip: {
    backgroundColor: '#f5f5f5',
    borderColor: 'transparent',
    borderRadius: 10,
    borderWidth: 3
},

        /* 		responsive: {
  rules: [{
    condition: {
      maxWidth: 500
    },
    chartOptions: {
      legend: {
        enabled: false
      }
    }
  }]
}
responsive: {
                        rules: [{
                         condition: {
                             maxWidth: 900
                           },
						    chartOptions: {
                                        legend: {
                                            align: 'center',
                                            verticalAlign: 'bottom',
                                            layout: 'horizontal'
                                        },
                                        yAxis: {
                                            labels: {
                                                align: 'left',
                                                x: 0,
                                                y:0
                                            },
                                            title: {
                                                text: null
                                            }
                                        },
                                        subtitle: {
                                            text: null
                                        },
                                        credits: {
                                            enabled: false
                                        }
                            }
						}]
					} */
    };


});