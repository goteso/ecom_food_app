var app = angular.module("mainApp", ['ngRoute','ngAnimate', 'toastr','angular-jquery-locationpicker']);
 
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'driver-location.html',
            controller: 'locationController'
        })
        .when('/assigned-orders', {
            templateUrl: 'driver-assigned-orders.html',
            controller: 'ordersController'
        }) 
			.when('/payments', {
            templateUrl: 'payments.html',
            controller: 'paymentsController'
        }) 
        .otherwise({
            redirectTo: '/'
        });
});
  
 
app.controller('profileController', function($http,$scope,toastr) {
 
 
	   $('#loading').css('display', 'block');
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
	  
	  
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_profile_basic_details',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	
			$scope.profileData = data;
			
	   $('#loading').css('display', 'none');
			document.getElementById("res").value =JSON.stringify(data);
			
			
		 
      }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});




app.controller('locationController', function($http,$scope,toastr) {
 
 
	   $('#loading').css('display', 'block');
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
	  
	  
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_profile_basic_details',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	
			$scope.profileData = data;
			
	   $('#loading').css('display', 'none');
			document.getElementById("res").value =JSON.stringify(data);
			
			
		 
      }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});



 
 
app.controller('ordersController', function($http,$scope,toastr) {
 
 
 
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
		 
 
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/drivers_orders_list',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				 
			
	        //$scope.app_url = data.app_url;
			 
			$scope.ordersData = data;
			
		 	document.getElementById("res2").value =JSON.stringify(data);
			 
      }).error(function (data, status, headers, config) {  
				document.getElementById("res2").value =JSON.stringify(data);
               
        });   
});







 
app.controller('paymentsController', function($http,$scope,toastr) {
 
 
       //this will fetch all the list of payments driver received from customer
  	   $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
		
 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/get_admin_payments_received',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			$scope.app_url = data.app_url;
			$scope.driverPaymentData = data; 
		 	document.getElementById("res6").value =JSON.stringify(data);
	     }).error(function (data, status, headers, config) { 
			 	 document.getElementById("res2").value =JSON.stringify(data);
         });   
		 
		 
		 
		 
		 
		 
		 
		 
		  //this will fetch all the list of payments driver received from customer
  	   $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
		
 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/get_driver_payments_received',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				//toastr.success(data.message,'Success');
			$scope.app_url = data.app_url;
			$scope.driverPaymentData = data; 
		 	document.getElementById("res6").value =JSON.stringify(data);
	     }).error(function (data, status, headers, config) { 
		 //toastr.error('Error Occurs','Error');
			 	 document.getElementById("res2").value =JSON.stringify(data);
         });   
		 
		 
		 
		 
		 
		 
		
		
		
	   //this will update the payment received from driver
	   $scope.submitPayments = function(){
		   
	   $scope.data = [];
	   $scope.sender_id=  document.getElementById("user_id").value;
	   $scope.order_id=  document.getElementById("order_id").value;
	   $scope.receiver_id = '';
	    
	   $scope.amount=  document.getElementById("amount").value ;
	   $scope.payment_gateway= 'COD';
	   $scope.transaction_id= 'NaN';
       $scope.data = {"sender_id":$scope.sender_id, "order_id":$scope.order_id, "receiver_id":$scope.receiver_id, "amount":$scope.amount, "payment_gateway":$scope.payment_gateway, "transaction_id":$scope.transaction_id};
		 
 
		 var request = $http({
								method: "POST",
								url: APP_URL+'/update_admin_payments_received',
								data:  $scope.data,
								headers: { 'Accept':'application/json' }
                            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				toastr.success(data.message,'Success');
				 
			 location.reload();
			 
     }).error(function (data, status, headers, config) { 
	  toastr.error('Error Occurs','Error');
		 document.getElementById("res2").value =JSON.stringify(data);
               
        });   
		
		}
		
		
		
		
		
		
		
		
		
		
		
		
				//get payments list admin received from driver
	   $scope.getPayments=function(){
	   $scope.data = [];
	   $scope.user_id=  document.getElementById("user_id").value ;
       $scope.data = {"user_id":$scope.user_id};
		

 
 
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/get_admin_payments_received',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				$scope.adminPaymentData = data;
		 
			  document.getElementById("res6").value =JSON.stringify(data);
     }).error(function (data, status, headers, config) { 
		 document.getElementById("res99").value =JSON.stringify(data);
               
        });   
		
		}
		
	   $scope.getPayments();
		
		
		
		
		
		
		
		
	 
  
		
		
});












