 var app = angular.module("mainApp", ['ngAnimate', 'toastr']) 
	 app.controller('logoController',  function($http,$scope,toastr) {
		  $scope.upload_logo = function() {
			 
	    $scope.photo = $('#photo').val();
		
		$scope.data = {'photo':$scope.photo};
		 
		   var request = $http({
                 method: "POST",
                 url: APP_URL+'/upload_app_logo',
                 data: $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
			   toastr.success(data.message,'Success!');
            }).error(function (data) { 
			toastr.error('Error Occurs','Error!');
			   // document.getElementById('res').value = JSON.stringify(data);
            });  
			
		  }
	 });