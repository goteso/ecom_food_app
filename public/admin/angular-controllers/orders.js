var app = angular.module("mainApp", ['angularUtils.directives.dirPagination','ngAnimate', 'toastr'])


app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	
 
app.controller('orderController',  orderController);
	
	function orderController($http,$scope,Excel,$timeout,$window,$location,toastr) {
	
	 $('#loading').css('display', 'block');
	var pro = this;
	pro.orders = $window.controller_data;
	
     //document.getElementById('res').value = JSON.stringify(pro.orders);
	pro.orders_count = pro.orders.orders_count_data;
  
   
  $('#loading').css('display', 'none');
    // function used to filter orders
   	$scope.filter_orders = function()
	{
		 
     $scope.Model = {};
	 var date_filter = $('#date_filter').val();
     var order_status = $('#order_status').val();
     var price_filter = $('#price_filter').val();
 
	  
   $location.url('?date_filter='+date_filter+'&price_filter='+price_filter+'&order_status='+order_status);
   
    var request = $http({
                 method: "GET",
                 url: APP_URL+'/orders_list_filtered?request_type=api&date_filter='+date_filter+'&price_filter='+price_filter+'&order_status='+order_status,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 
				 
                    pro.orders = data;
					
					  document.getElementById("res").value = JSON.stringify(data);
			 }).error(function (data, status, headers, config) {
	 			 
                  document.getElementById("res").value = JSON.stringify(data);
        });   
		
	};	
	
	
	
	
	
	
		/**delete Row**/
		pro.removeChoice = removeChoice; 
		function removeChoice(itemId, index) {
	 
      for (var i = 0; i < pro.orders.length; i++) { 
	 
          pro.orders[i].data.splice(index, 1);
          break;
        
      }
    };
	
	
	
	
	
	/***Checkbox delete Row**/
	 $scope.chckedIndexs=[];

     $scope.checkedIndex = function (values) {
		 
         if ($scope.chckedIndexs.indexOf(values) === -1) {
             $scope.chckedIndexs.push(values);
         }
         else {
             $scope.chckedIndexs.splice($scope.chckedIndexs.indexOf(values), 1);
         }
     }

      
      $scope.remove=function(index){
		  angular.forEach($scope.chckedIndexs, function (value, index) { 
		  for (var i = 0; i < pro.orders.length; i++) { 
            var index = pro.orders[i].data.indexOf(value); 		
		   pro.orders[i].data.splice(index, 1);
          break;
		  }
       }); 
        
		$scope.chckedIndexs = [];
     };
	 
	 
		


 

     $scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        }
		
		
   $scope.export = function(){
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
        });
   };
  
 /**pagination**/
    $scope.currentPage = 1;
  $scope.pageSize = 5;
   
    $scope.pageChangeHandler = function(num) {
      console.log('meals page changed to ' + num);
  };
  
  
  
  
  
  
  
  
   $scope.printToCart = function(printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
      }



 
	  
	  
	 /**search ng-click**/ 
	  
	  $scope.search = '';
	   $scope.SearchData = function () {

            var parameters = {
                search: $scope.search
            };
            var config = {
                params: parameters
            };

            $http.get('/ServerRequest/GetData', config)
            .success(function (data, status, headers, config) {
                $scope.Details = data;
            })
            .error(function (data, status, header, config) {
                $scope.ResponseDetails = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });
        };
		
		
		
};

app.controller('OtherController',  OtherController);
function OtherController($scope) {
  $scope.pageChangeHandler = function(num) {
    console.log('going to page ' + num);
  };
};



