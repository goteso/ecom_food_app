var app = angular.module("mainApp", ['ngMaterial', 'ngMessages','ngAnimate', 'toastr']);
 
 
app.controller('orderDetailController', function($http,$scope,$window,$log, toastr) {
    $scope.customers = [
        {'name': 'Jane Stewart', 'city':'San Francisco'},
        {'name': 'Sam Jenkins', 'city':'Moscow'},
        {'name': 'Mark Andrews', 'city':'New York'}
    ];

 $scope.order_id =  {"order_id":$window.order_id};
 
	
	 var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/order_details',
                 data: $scope.order_id,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.orders =  data;
            
				 document.getElementById('res').value = JSON.stringify(data);
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
			
 
 $scope.printInvoice = function(){
 
 location.url(APP_URL+'/bill.html?'+$scope.order_id);
	
}

 
 $scope.paymentReceive = function(){
	 
	  var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_payment_gateways',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.paymentReceiveData =  data;
             
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 }
 
 $scope.submitPaymentGateway = function(){
	// alert(JSON.stringify($scope.order_id));
	 
	 $scope.payment_method = $('#payment-method').val();
	 $scope.amount = $('#amount').val();
	 $scope.transactionId = $('#transactionId').val();
	  
	 $scope.paymentData =  {"order_id":$scope.order_id.order_id,"amount":$scope.amount,"payment_gateway":$scope.payment_method,"transaction_id":$scope.transactionId}
	   
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/add_transactions',
                 data: $scope.paymentData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			document.getElementById('res').value = JSON.stringify(data);
				//  alert('sfsdf'); 
             
              $("#paymentReceiveModal").modal('hide');
			  location.reload();
			   
			  
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
 
 
 
 $scope.orderUpdate = function(){
	 
	  var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_order_status_list',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.orderStatusdata =  data;
             
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 }
 
    $scope.IsVisible = false;
  $scope.driverDataChange = function(){
	    $scope.IsVisible = $scope.IsVisible ? false : true;
	    $scope.querySearch = function(query){
		 
			 
            return $http.get(APP_URL+"/search_users?search_text="+query+"&user_type=driver", {params: {q: query}})
            .then(function(response){ 
              return response.data;
            }) 
          };
		  
		  
		  	  $scope.selectedItemChange = function(item) {
      $log.info('Item changed to ' + JSON.stringify(item)); 
	     $scope.customer_id_value =  item.id;
		 
    }
	   
  };
  
  
  $scope.submitDriverId = function(){
	  $scope.driver_id = $('#driver_id').val();
	  
	  $scope.driver = {"order_id":$scope.order_id.order_id,"driver_id":$scope.driver_id}
	   var request = $http({
                 method: "POST",
                 url: APP_URL+'/assign_driver',
                 data: $scope.driver ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			document.getElementById('res').value = JSON.stringify(data);
				   
	      $scope.IsVisible = false;
				   
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {              	document.getElementById('res').value = JSON.stringify(data);            }); 
	  
  }
  
 $scope.submitOrderStatus = function(){ 
	 
	 $scope.order_status = $('#order-status').val();  
	 $scope.orderStatusData =  {"order_id":$scope.order_id.order_id,"status":$scope.order_status}; 
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/order_status_update',
                 data: $scope.orderStatusData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				//  alert('sfsdf'); 
              $("#orderStatusModal").modal('hide');
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {               	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
 
    $scope.add = function() {
          d = {}
          d['name'] = $scope.name;
          d['city'] = $scope.city;
          $scope.message = $scope.name + " " + $scope.city
          $scope.customers.push(d);
          $scope.name = "";
          $scope.city = "";
          $scope.message = $scope.customers;
    };
	
	 
	
});














app.controller('invoiceController', function($http,$scope,$location,$window,toastr,$timeout) {
 
 var url = $location.absUrl();
 
	 
var n = url.lastIndexOf('#');
var result = url.substring(n + 1);

 
 $scope.abc = function(){
 $http.get('http://192.168.0.112/ecommerce/public/order_invoice_data/'+result)
            .then(function(response){ 
			 
			   $scope.invoice = response.data; 
			   
				setTimeout(function () {   
			 $scope.pppp();
			   }, 1);
            
            });  
 }
	



 $scope.pppp = function(){
 $window.print();
 }
	 
$scope.abc();




		/*download as pdf**/
   $scope.export = function(){
   
    
        html2canvas(document.getElementById('exportthis'), {
		
		//var doc = new jsPDF("p", "mm", "a4"); 
       //var width = doc.internal.pageSize.width;    
       //var height = doc.internal.pageSize.height;

            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("order.pdf");
            }
        });
   };
   
    
 
});