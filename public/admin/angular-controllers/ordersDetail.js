var app = angular.module("mainApp", ['ngMaterial', 'ngMessages','ngAnimate', 'toastr']);
 
      app.factory('cartStorage', function() {
        var _cart = {
            products: [], 
        };
        var service = {
            get cart() {
                return _cart;
            }
        }
        return service;
    })
	
	
app.controller('orderDetailController', function($http,$scope,$window,$log, toastr) {
	
	var _this = this;
	
    $scope.customers = [
        {'name': 'Jane Stewart', 'city':'San Francisco'},
        {'name': 'Sam Jenkins', 'city':'Moscow'},
        {'name': 'Mark Andrews', 'city':'New York'}
    ];

 $scope.data2 =  {"order_id":$window.order_id,"user_id":$window.user_id};
 
 
   $('#loading').css('display', 'none');
	$scope.order_id =  {"order_id":$window.order_id,"user_id":$window.user_id};
	
	$scope.order_id_single =  $window.order_id;
 
 
	
	 var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/order_details?request_type=api',
                 data: $scope.data2,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.ordersTitle =  data;
				  $scope.orders =  data.blocks;
				  
				   $scope.orders1 =  data.blocks[0].data.data;
            
  $('#loading').css('display', 'none');

				 document.getElementById('res').value = JSON.stringify(data);
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
	



 $scope.deleteProduct = function(id){
	 
	 $scope.orderProductId = {"order_product_id":id};
	  
	    if (confirm("Are you sure?")) {
			
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/order_product_delete',
                 data: $scope.orderProductId,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			  
 
	  
	  
	 
	  			  $scope.data =  data;
				  
		  toastr.success(data.message,'Success!');
		 
        location.reload();
	 
            
				 document.getElementById('res').value = JSON.stringify(data);
				 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {     toastr.error('Error Occurs','Error!');    	document.getElementById('res').value = JSON.stringify(data);            }); 
			
		}
			
 }




	
	$('#add').hide();
	$('#loadVariant').hide();
	  $scope.ProductDataSearch = function(){ 
	    $scope.productSearch = function(query){
		    
				  
            return $http.get(APP_URL+"/search_product?search_text="+query, {params: {q: query}})
            .then(function(response){  
			  $scope.items = response.data.data;
              return response.data.data;
            }) 
          };
		  
		    $scope.selectedProductChange = function(item) { 
			$('.variantDiv').hide();
			$('#add').hide();
	$('#loadVariant').hide();
      $log.info('Item changed to ' + JSON.stringify(item)); 
	     $scope.product_value =  item;  
		 $scope.product_variants = item.variants_exist; 
		 
		 if($scope.product_variants == '0'){$('#add').show();}
		 else{$('#loadVariant').show();}
    }
	   
  };
  
       $('#variantAdd').hide();
        $scope.productArray = [];
  
        $scope.addNewProduct = function(){ 
	        $scope.productsValue = $scope.product_value; 
			$scope.id = $scope.productsValue.id; 
            $scope.product_id= {"product_id":$scope.id} 
          
            if($scope.product_value.variants_exist== '0'){
 
                $scope.product_value.discounted_price = $scope.product_value.base_price - ($scope.product_value.base_price * $scope.product_value.base_discount /100);
                $scope.product_value.quantity = $('#quantity').val(); 
			    $scope.productArray.push($scope.productsValue);
  		        $scope.newProduct = {"order_products": $scope.productArray};
				 
                var request = $http({
			        method: "POST",
			        url: APP_URL+'/edit_order_add_product/'+$scope.order_id_single,
			        data: $scope.newProduct,
			        headers: {
				        'Accept': 'application/json'
			        }
		        });

		        /* Check whether the HTTP Request is successful or not. */
		        request.success(function (data) { 
			        $scope.data = data;
					$("#addProductModal").modal('hide'); 
					 toastr.success(data.message,'Success!');
					  location.reload();
					 
			        document.getElementById("res1").value = JSON.stringify(data);
                }).error(function (data, status, headers, config) { 
			       document.getElementById("res1").value = JSON.stringify(data);
		        });
 
 
            }
            else{
				$('.variantDiv').show();
		        var request = $http({
			        method: "POST",
			        url: APP_URL+'/get_product_variants',
			        data: $scope.product_id,
			        headers: {
				        'Accept': 'application/json'
			        }
		        });

		        /* Check whether the HTTP Request is successful or not. */
		        request.success(function (data) {
			        $scope.variantsdata = data;
					$('#variantAdd').show();
					$('#loadVariant').hide();
						$scope.variantsArray = [];
			        document.getElementById("res1").value = JSON.stringify(data);
                }).error(function (data, status, headers, config) {
  
			       document.getElementById("res1").value = JSON.stringify(data);
		        });
 
            };
	 
		};	
		
		
		
		
		$scope.getVariantValue = function(variants){
	   
	 var alreadyInVariants = '';
	 var variantsIndex = 0;

  if($scope.variantsArray.length == 0) { $scope.variantsArray.push(variants); return;}
  
  for(var i=0; i<$scope.variantsArray.length; i++){
	
	 if($scope.variantsArray[i].id == variants.id){ alreadyInVariants = 'YES'; variantsIndex = i;}
	 
	 }
	 
	 if(alreadyInVariants == 'YES'){ 
			 $scope.variantsArray.splice(variantsIndex,1); return; 
	 }
	 else{
	
		  $scope.variantsArray.push(variants);
		   return;
		
       }  
}
		
		$scope.addVariants = function(){
			
			$scope.productsValue.variants = $scope.variantsArray; 
			 $scope.product_value.quantity = $('#quantity').val();
			 $scope.product_value.discounted_price = $scope.product_value.base_price - ($scope.product_value.base_price * $scope.product_value.base_discount /100);
			 
			 $scope.productArray.push($scope.product_value);
  		        $scope.newVariantProduct = {"order_products": $scope.productArray};
				
			  
			/*var price = $scope.product_value.base_price; 
			    for(var j=0; j<$scope.variantsArray.length; j++){
				    if($scope.variantsArray[j].price){
					   price = $scope.variantsArray[j].price; 
				    }
			    }
			 
			    var productToAdd = [angular.copy($scope.product_value)];
			    productToAdd.base_price = price; 
			    
				for(var j=0; j<$scope.variantsArray.length; j++){
				    if($scope.variantsArray[j].price_difference){
					   productToAdd.base_price = parseInt(productToAdd.base_price) + parseInt($scope.variantsArray[j].price_difference); 
				    }
		      	}
			 
			    productToAdd.discounted_price = productToAdd.base_price - (productToAdd.base_price * productToAdd.base_discount /100);
			    $scope.variantsProduct = productToAdd;
			    $scope.newVariantProduct = {"order_products": $scope.variantsProduct};*/
			  
			 
			  var request = $http({
			        method: "POST",
			        url: APP_URL+'/edit_order_add_product/'+$scope.order_id_single,
			        data: $scope.newVariantProduct,
			        headers: {
				        'Accept': 'application/json'
			        }
		        });

		        /* Check whether the HTTP Request is successful or not. */
		        request.success(function (data) { 
			        $scope.data = data;
					$("#addProductModal").modal('hide'); 
					 toastr.success(data.message,'Success!');
					  location.reload();
					 
			        document.getElementById("res1").value = JSON.stringify(data);
                }).error(function (data, status, headers, config) { 
			       document.getElementById("res1").value = JSON.stringify(data);
		        });
				
				 
			$("#addProductModal").modal('hide');
			
			
		}
		
 
 $scope.printInvoice = function(){
 
 location.url(APP_URL+'/bill.html?'+$scope.order_id);
	
}


 
 
 $scope.paymentReceive = function(){
	 
	  var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_payment_gateways',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.paymentReceiveData =  data;
             
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 }
 
 $scope.submitPaymentGateway = function(){
	// alert(JSON.stringify($scope.order_id));
	 
	 $scope.payment_method = $('#payment-method').val();
	 $scope.amount = $('#amount').val();
	 $scope.transactionId = $('#transactionId').val();
	  
	 $scope.paymentData =  {"order_id":$scope.order_id.order_id,"amount":$scope.amount,"payment_gateway":$scope.payment_method,"transaction_id":$scope.transactionId}
	   
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/add_transactions',
                 data: $scope.paymentData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			document.getElementById('res').value = JSON.stringify(data);
				//  alert('sfsdf'); 
             
              $("#paymentReceiveModal").modal('hide');
			  location.reload();
			   
			  
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
 
 
 
 $scope.orderUpdate = function(){
	 
	  var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_order_status_list',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			 	 //alert('sfsdf');
				  $scope.orderStatusdata =  data;
             
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {                	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 }
  
  
  
    $scope.IsVisible = false;
  $scope.driverDataChange = function(){
	    $scope.IsVisible = $scope.IsVisible ? false : true;
	    $scope.querySearch = function(query){
		 
			 
            return $http.get(APP_URL+"/search_users?search_text="+query+"&user_type=Driver", {params: {q: query}})
            .then(function(response){ 
              return response.data;
            }) 
          };
		  
		  
		  	  $scope.selectedItemChange = function(item) {
      $log.info('Item changed to ' + JSON.stringify(item)); 
	     $scope.customer_id_value =  item.id;
		 
    }
	   
  };
  
  
  $scope.submitDriverId = function(){
	  $scope.driver_id = $('#driver_id').val();
	  
	  $scope.driver = {"order_id":$scope.order_id.order_id,"driver_id":$scope.driver_id}
	   var request = $http({
                 method: "POST",
                 url: APP_URL+'/assign_driver',
                 data: $scope.driver ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			document.getElementById('res').value = JSON.stringify(data);
				   
	      $scope.IsVisible = false;
				   
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {              	document.getElementById('res').value = JSON.stringify(data);            }); 
	  
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  // assign vendor
  
   $scope.IsVisible1 = false;
  $scope.vendorDataChange = function(){
	    $scope.IsVisible1 = $scope.IsVisible1 ? false : true;
	    $scope.querySearch = function(query){
		 
			 
            return $http.get(APP_URL+"/search_users?search_text="+query+"&user_type=Vendor", {params: {q: query}})
            .then(function(response){ 
              return response.data;
            }) 
          };
		  
		  
		  	  $scope.selectedItemChangeVendor = function(item) {
      $log.info('Item changed to ' + JSON.stringify(item)); 
	   
	     $scope.vendor_id_value =  item.id; 
    }
	   
  };
  
  
  $scope.submitVendorId = function(){
	  $scope.vendor_id = $('#vendor_id2').val();
	  
	   
	  $scope.vendor = {"order_id":$scope.order_id.order_id,"vendor_id":$scope.vendor_id}
	   var request = $http({
                 method: "POST",
                 url: APP_URL+'/assign_vendor',
                 data: $scope.vendor ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			document.getElementById('res').value = JSON.stringify(data);
				   
	      $scope.IsVisible = false;
				   
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {              	document.getElementById('res').value = JSON.stringify(data);            }); 
	  
  }
  
  
  
  
  
  
 $scope.submitOrderStatus = function(){ 
	 
	 $scope.order_status = $('#order-status').val();  
	 $scope.orderStatusData =  {"order_id":$scope.order_id.order_id,"status":$scope.order_status}; 
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/order_status_update',
                 data: $scope.orderStatusData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				//  alert('sfsdf'); 
              $("#orderStatusModal").modal('hide');
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {               	document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
// apply discount 
  $scope.submitDiscount = function(){ 
	 
	 $scope.order_discount = $('#discount').val();  
	 $scope.discountData =  {"order_id":$scope.order_id.order_id,"discount":$scope.order_discount}; 
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/apply_discount',
                 data: $scope.discountData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				  
              $("#orderStatusModal").modal('hide');
			  toastr.success(data.message,'Success!');
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {           	 toastr.error('Error Occurs','Error!'); document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
 
 
 // apply shipping 
  $scope.submitShipping = function(){ 
	 
	 $scope.order_shipping = $('#shipping').val();  
	 $scope.shippingData =  {"order_id":$scope.order_id.order_id,"shipping":$scope.order_shipping}; 
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/apply_shipping',
                 data: $scope.shippingData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				  
              $("#shippingModal").modal('hide');
			  toastr.success(data.message,'Success!');
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {           	 toastr.error('Error Occurs','Error!'); document.getElementById('res').value = JSON.stringify(data);            }); 
			
			
 };
 
 
 
 
 
 
 
 
 
 




 
 
//apply shipping 
  $scope.submitExpressShipping = function(){ 
 
	 $scope.order_shipping = $('#expressShipping').val();  
	 $scope.shippingData =  {"order_id":$scope.order_id.order_id,"express_shipping":$scope.order_shipping}; 
	 
	 alert(JSON.stringify($scope.shippingData));
	  var request = $http({
                 method: "POST",
                 url: APP_URL+'/apply_express_shipping',
                 data: $scope.shippingData ,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				  
              $("#shippingModal").modal('hide');
			  toastr.success(data.message,'Success!');
			  
			  
			  location.reload();
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, header, config) {           	alert('failed');  toastr.error('Error Occurs','Error!'); document.getElementById('res').value = JSON.stringify(data);            }); 
};






 
 
 
 
 
 
 
 
    $scope.add = function() {
          d = {}
          d['name'] = $scope.name;
          d['city'] = $scope.city;
          $scope.message = $scope.name + " " + $scope.city
          $scope.customers.push(d);
          $scope.name = "";
          $scope.city = "";
          $scope.message = $scope.customers;
    };
	
	 
	
});





 

app.controller('invoiceController', function($http,$scope,$location,$window,toastr,$timeout) {
 
 var url = $location.absUrl();
 
	 
var n = url.lastIndexOf('#');
var result = url.substring(n + 1);

 
 $scope.abc = function(){
 $http.get(APP_URL+'/order_invoice_data/'+result)
            .then(function(response){ 
			 
			   $scope.invoice = response.data; 
			   
				setTimeout(function () {   
			 $scope.pppp();
			   }, 1);
            
            });  
 }
	 

 $scope.pppp = function(){
	// $timeout($window.print, 0);
 $window.print();
 }
	 
$scope.abc();




		/*download as pdf**/
   $scope.export = function(){
   
    
        html2canvas(document.getElementById('exportthis'), {
		
		//var doc = new jsPDF("p", "mm", "a4"); 
       //var width = doc.internal.pageSize.width;    
       //var height = doc.internal.pageSize.height;

            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("order.pdf");
            }
        });
   };
   
    
 
});























app.controller('productInvoiceController', function($http,$scope,$location,$window,$log, toastr,$timeout) {
	 
	 var url1 = $location.absUrl();
var url1 =  url1.replace(/[\s]/g, '');

var n1 = url1.lastIndexOf('#');
var result1 = url1.substring(n1 + 1);
 result1 = decodeURIComponent(result1);
 result1  = result1.replace(/\n/g, '<br/>');

 $scope.abc1 = function(){
	   $scope.orders = result1; 
				
				setTimeout(function () {   
			 $scope.printp();
			   }, 200);
 }
	 
 $scope.printp = function(){ 
  $window.print();
 }
	 
$scope.abc1();



});
