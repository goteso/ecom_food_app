var app = angular.module("mainApp", ['ngTagsInput', '720kb.datepicker'])
 
    app.directive('clockPicker', function() {
        return {
           restrict: 'A',
              link: function(scope, element, attrs) {
              element.clockpicker();
           }
        }
    })

	app.controller('itemController', function($http,$scope,$window) {
	
	    $scope.time = '';
	 
        /*get data from api**/      
	    $http.get('http://192.168.0.109/ecommerce/public/api/product_get_form_basic' )
		.success(function (data, status, headers, config) { 
            $scope.addItem =  data ;
			
	
        })
		.error(function (data, status, headers, config) {  
	         
        });     
	       
		$scope.add = function() { 
		
		document.getElementById("res").value = JSON.stringify( $scope.addItem);	 
 	 
	        var request = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/api/product_add',
                 data: $scope.addItem,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 
				document.getElementById("res").value = JSON.stringify(data);
			 
            
				 $scope.product_id = data["user_data"][0]["product_id"];
					 
		        $window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, headers, config) { 

 	 
				document.getElementById("res").value = JSON.stringify(data);
				return;
	   
        });   
         };

  
        // get json tags
        $scope.loadTags = function(query) {
           return $http.get('tags.json');
        };
  
  
  
  
  
 
 
 
    $scope.arrayText = [{"date_filter":"firstAdded","search_text":"","parent_id":"","limit":"10"}];
 
 
 
 //document.getElementById("res").value=JSON.stringify(angular.fromJson(angular.toJson($scope.Model)));  
// return false;
   
 
	/*get data from api**/    
	        var request = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/categories_list',
                 data:  $scope.arrayText,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
          $scope.allBrandList = data.data.data;
		 
			
			 
			 document.getElementById("res").value = JSON.stringify(data);	
				// $scope.product_id = data["user_data"][0]["product_id"];
				 document.getElementById("res").value = JSON.stringify(data);	 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, headers, config) { 

 		 
	        document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	
	
	
	
	//get brands
	
	    $scope.brands_post_data = [{"date_filter":"firstAdded","search_text":"","limit":"10"}];
 
 
 
 //document.getElementById("res").value=JSON.stringify(angular.fromJson(angular.toJson($scope.Model)));  
// return false;
   
 
	/*get data from api**/    
	        var request = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/brands_list',
                 data:  $scope.brands_post_data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				
				alert('categories_list fetched');
          $scope.allBrandList = data.data.data;
		 
			
			 
			 document.getElementById("res").value = JSON.stringify(data);	
				// $scope.product_id = data["user_data"][0]["product_id"];
				 document.getElementById("res").value = JSON.stringify(data);	 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, headers, config) { 

 		 
	        document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	
	
	
	
	
	
        //get tags data
        $scope.externalContacts = [];
        $scope.externalContacts1 = [];
 

        $scope.searchPeople = function(term) {
            var search_term = term.toUpperCase();
            $scope.people = [];
            angular.forEach($scope.allBrandList, function(item) {
                if (item.title.toUpperCase().indexOf(search_term) >= 0)
                  $scope.people.push(item);
            });

            return $scope.people;
        };
  
  
        // selected tabs  
        $scope.selected = 0;

  
    });





	
	app.controller('itemEditController', function($http,$scope,$window) {
   $scope.editItem =  $window._yourSpecialVar;
			
	 
		 
      
	     
	
	
 
	
	$scope.saveChanges = function() {
     /*  $http.get('addItem.json', $scope.addItem).then(function(data) {
      $scope.msg = 'Data saved';
    });  */  
    $scope.msg = 'Data sent: '+ angular.toJson($scope.editItem); 
	
	
	 document.getElementById("res").value = JSON.stringify($scope.editItem);
	 alert("data");
	 return;
	
	//document.getElementById("res").innerHTML =  $scope.editItem[0]["product_id"]; 
	var product_id = $scope.editItem[0]["product_id"];
	
	 
	var request1 = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/product_update/'+product_id,
                 data: $scope.editItem,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request1.success(function (data) {
               document.getElementById("res").value = JSON.stringify(data);
            }).error(function (data, status, headers, config) { 

 		
	        document.getElementById("res").value = JSON.stringify(data);
        }); 
    }; 
  
  
     
  
   $scope.add = function (index) { 
   var value = {};   
	 $scope.editItem[2].tabs[index].values.push(value);
			
   }
  
  
  
  	 
 
 
$scope.save_variant=function(index,tab_id)
{
 
	
	
$scope.price_difference = document.getElementsByName('price_difference'+index+tab_id)[0].value;


$scope.stock_count = document.getElementsByName('stock_count'+index+tab_id)[0].value;

$scope.price = document.getElementsByName('price'+index+tab_id)[0].value;

$scope.product_variant_type_id = document.getElementsByName('product_variant_type_id'+index+tab_id)[0].value;


 
$scope.variant_id = document.getElementsByName('id'+index+tab_id)[0].value;
 
$scope.product_id = document.getElementById('product_id').value;
$scope.title = document.getElementsByName('title'+index+tab_id)[0].value;

$scope.variant_data = {"product_id":$scope.product_id,"product_variant_type_id":$scope.product_variant_type_id,"title":$scope.title,"price_difference":$scope.price_difference,"stock_count":$scope.stock_count,"variant_id":$scope.variant_id};

 
 
	        var request = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/add_update_variant',
                 data:  $scope.variant_data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
          //$scope.allBrandList = data.data.data;
	 
		 $('input[name="'+'id'+index+tab_id+'"]').val(data)
		 
		 
			 document.getElementById("res").value = JSON.stringify(data);	
				// $scope.product_id = data["user_data"][0]["product_id"];
				 document.getElementById("res").value = JSON.stringify(data);	 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, headers, config) { 

 		 
	        document.getElementById("res").value = JSON.stringify(data);
        });  

		
	
}
 
 
 
   
	  $scope.delete_variant = function(index,tab_id){ 
 
	  document.getElementById("res").value = JSON.stringify($scope.editItem[2].tabs[index].values);
	   $scope.editItem[2].tabs[index].values.splice(index,1);
 
	  var variant_id = document.getElementsByName('id'+index+tab_id)[0].value;
	   alert(variant_id);
	   	    $http.get('http://192.168.0.109/ecommerce/public/delete_variant/'+variant_id )
		.success(function (data, status, headers, config) { 
           alert("deleted");
			
	
        })
		.error(function (data, status, headers, config) {  
	         alert("cannot delete");
			 
			 document.getElementById("res").value = JSON.stringify(data);
        }); 
		
		
	 }
	 
	 
 
 
  angular.forEach($scope.editItem, function(item) {
	  
	    angular.forEach(item.fields, function(field) { 
	 
		  
		  
		   if (field.type == 'multipleTag')
		   angular.forEach(field.value, function(data) { 
		  
		   $scope.externalContacts1 =  data ;[
    { text: 'Tag1' },
    { text: 'Tag2' },
    { text: 'Tag3' }
  ];
		    })
		})
		
		
		

    });
 
  
  
   // get json tags
   $scope.loadTags = function(query) {
    return $http.get('tags.json');
  };
  
  
  
  
  
  
  
   $scope.arrayText = [{"date_filter":"firstAdded","search_text":"","parent_id":"","limit":"10"}];
 
 
 
 //document.getElementById("res").value=JSON.stringify(angular.fromJson(angular.toJson($scope.Model)));  
// return false;
   
 
	/*get data from api**/    
	        var request = $http({
                 method: "POST",
                 url: 'http://192.168.0.109/ecommerce/public/categories_list',
                 data:  $scope.arrayText,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
          $scope.allBrandList = data.data.data;
		 
			
 
			 document.getElementById("res").value = JSON.stringify(data);	
				// $scope.product_id = data["user_data"][0]["product_id"];
				 document.getElementById("res").value = JSON.stringify(data);	 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            }).error(function (data, status, headers, config) { 

 		 
	        document.getElementById("res").value = JSON.stringify(data);
        });  
		
		
		
		
  //get tags data
  $scope.externalContacts = [];
   $scope.externalContacts1 = [];
  

  $scope.searchPeople = function(term) {
    var search_term = term.toUpperCase();
    $scope.people = [];
    angular.forEach($scope.allBrandList, function(item) {
      if (item.title.toUpperCase().indexOf(search_term) >= 0)
        $scope.people.push(item);

    });

    return $scope.people;
  };
  
  
 /** selected tabs**/ 
$scope.selected = 0;

    
 

 
 
	
  
});
