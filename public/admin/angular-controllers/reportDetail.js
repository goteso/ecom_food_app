var app = angular.module("mainApp", [])


app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	
 
app.controller('reportController',  function($http,$scope,Excel,$timeout,$location) {
	  $('#loading').css('display', 'block');
	var url = $location.absUrl();
	 
var n = url.lastIndexOf('/');
var result = url.substring(n + 1);

$scope.reportDetail = {"date_from":"2010-02-12","date_to":"2030-04-25","user_id":""}
	 
	
	      	   var request = $http({
                 method: "POST",
                 url: APP_URL+'/'+result,
                 data: $scope.reportDetail,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
				 $scope.reports =  data; 
               $('#loading').css('display', 'none');
            });  
			
			
 
       $scope.IsVisible = true;
       $scope.IsHidden = false;
       $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsHidden =  true;
			$scope.IsVisible = false;
        };
			
	
      /**Get notes values from json**/
       angular.forEach($scope.reports, function(data) {
         if(data.notes == '') { 		   
	   $scope.notes = 'There are no any notes';
		 }
		 else{
	      $scope.notes = data.notes; 
		 }
       });		

      /** Submit notes form data **/ 
		$scope.submit = function() { 
         if(this.notes) {   
           $scope.notes = this.notes;
		   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		   //$scope.notes = '';
         }
		 if(this.notes == ''){
			  $scope.notes = 'There are no any notes';
			   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		 }
		 
		};
		
		
		
		$scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        }
		
/*  $scope.refresh = function(){
    $http.get('/api/users')
          .success(function(data){
               $scope.users = data;
          });
} */ 





    $scope.printToCart = function(tableToExport) {
        var innerContents = document.getElementById('tableToExport').innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }
	
	
	
   $scope.export = function(){
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
        });
   };
  
});

