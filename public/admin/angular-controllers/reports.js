var app = angular.module("mainApp", ['ngRoute','ngMaterial', 'ngMessages','ngAnimate']);
 
  app.directive('hcChart', function () {
                return {
                    restrict: 'E',
                    template: '<div id="container" style="margin: 0 auto">not working</div',
                    scope: {
                        options: '='
                    },
                    link: function (scope, element) {
                        var chart = new Highcharts.chart(element[0], scope.options);
            $(window).resize(function () {
                chart.reflow();
            });
					}
                };
            })
	/*		
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'report-recomended.html',
            controller: 'reportDataController'
        })
       /* .when('/order', {
            templateUrl: 'report-orders.html',
            controller: 'reportDataController'
        }) 
		.when('/transactions', {
            templateUrl: 'report-transactions.html',
            controller: 'reportDataController'
        }) 
		.when('/customer', {
            templateUrl: 'report-customers.html',
            controller: 'reportDataController'
        })
		.when('/sales', {
            templateUrl: 'report-sales.html',
            controller: 'reportDataController'
        }) 
        .otherwise({
            redirectTo: '/'
        });
});
  */
 
app.controller('reportController', function($http,$scope,$window) {
	
   $('#loading').css('display', 'block');
$scope.main_data = $window.controller_data;
$scope.main_data = JSON.parse($scope.main_data);
    $('#loading').css('display', 'none');
$scope.total_users = $scope.main_data[0].total_users;
$scope.total_revenue = $scope.main_data[0].total_revenue;
$scope.total_orders = $scope.main_data[0].total_orders;


		   $scope.revenueReport = {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: $scope.main_data[0].months,
						labels: {
                          rotation: -45,
                          style: {
                            fontSize: '11px',
                            fontFamily: 'Open Sans, sans-serif'
                          }
                       }
                    },
					 yAxis: [{ // Primary yAxis
                        labels: {
                           format: ' {value}',
                           style: {
                              color: Highcharts.getOptions().colors[0]
                           }
                        },
                        title: {
                           text: '',
                           style: {
                             color: Highcharts.getOptions().colors[0]
                           }
                        }
                    }],
					 plotOptions: {
                         series: {
                            label: {
                             connectorAllowed: false
                           } 
                       }
                    },

					series: {
                       showInNavigator: true
                    },
					credits: {
                      enabled: false
                    },
					legend: {
                      enabled: true,
                      align: 'right',
                      verticalAlign: 'top',
                      layout: 'vertical',
                      x: 0,
                      y: 100					  
                     },
                    exporting: {
                       enabled: false
                    },
	
					// legend: { },
                    options: {
                        chart: {
                          color: Highcharts.getOptions().colors[0]
                        }
                    },
					rangeSelector: {
                       floating: true,
                       y: -65,
                       verticalAlign: 'bottom'
                    },

                    navigator: {
                       margin: 60
                    },

                    series: [{
						name:'Revenue',
						 type:'spline',
                          data:  $scope.main_data[0].revenues_y_array
                    }
					
					/**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/
					
					],
					 
					 colors: ['#4583EB', '#44a5f4', '#FF8080'],
 					
            }; 
			
			
			
			
					   
		   $scope.orderReport = {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: $scope.main_data[0].months,
						labels: {
                          rotation: -45,
                          style: {
                            fontSize: '11px',
                            fontFamily: 'Open Sans, sans-serif'
                          }
                       }
                    },
					 yAxis: [{ // Primary yAxis
                        labels: {
                           format: ' {value}',
                           style: {
                              color: Highcharts.getOptions().colors[0]
                           }
                        },
                        title: {
                           text: '',
                           style: {
                             color: Highcharts.getOptions().colors[0]
                           }
                        }
                    }],
					 plotOptions: {
                         series: {
                            label: {
                             connectorAllowed: false
                           } 
                       }
                    },

					series: {
                       showInNavigator: true
                    },
					credits: {
                      enabled: false
                    },
					legend: {
                      enabled: true,
                      align: 'right',
                      verticalAlign: 'top',
                      layout: 'vertical',
                      x: 0,
                      y: 100					  
                     },
                    exporting: {
                       enabled: false
                    },
	
					// legend: { },
                    options: {
                        chart: {
                          color: Highcharts.getOptions().colors[0]
                        }
                    },
					rangeSelector: {
                       floating: true,
                       y: -65,
                       verticalAlign: 'bottom'
                    },

                    navigator: {
                       margin: 60
                    },

                    series: [{
						name:'Orders',
						 type:'spline',
                          data:  $scope.main_data[0].orders_y_array
                    }
					
					/**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/
					
					],
					 
					 colors: ['#44a4f3', '#44a5f4', '#FF8080'],
 					
            }; 
			
			
			
			
			
			
			
			 
			
					   $scope.userReport = {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: $scope.main_data[0].months,
						labels: {
                          rotation: -45,
                          style: {
                            fontSize: '11px',
                            fontFamily: 'Open Sans, sans-serif'
                          }
                       }
                    },
					 yAxis: [{ // Primary yAxis
                        labels: {
                           format: ' {value}',
                           style: {
                              color: Highcharts.getOptions().colors[1]
                           }
                        },
                        title: {
                           text: '',
                           style: {
                             color: Highcharts.getOptions().colors[1]
                           }
                        }
                    }],
					 plotOptions: {
                         series: {
                            label: {
                             connectorAllowed: false
                           } 
                       }
                    },

					series: {
                       showInNavigator: true
                    },
					credits: {
                      enabled: false
                    },
					legend: {
                      enabled: true,
                      align: 'right',
                      verticalAlign: 'top',
                      layout: 'vertical',
                      x: 0,
                      y: 100					  
                     },
                    exporting: {
                       enabled: false
                    },
	
					// legend: { },
                    options: {
                        chart: {
                          color: Highcharts.getOptions().colors[1]
                        }
                    },
					rangeSelector: {
                       floating: true,
                       y: -65,
                       verticalAlign: 'bottom'
                    },

                    navigator: {
                       margin: 60
                    },

                    series: [{
						name:'Users',
						 type:'spline',
                          data:  $scope.main_data[0].users_y_array
                    }
					
					/**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/
					
					],
					 
					 colors: ['#4583EB', '#44a5f4', '#FF8080'],
 					
            }; 
			
			
			
			 
			
//	$scope.refresh_graph('revenues');		
  
});






app.controller('reportDataController', function($http,$scope,$window) {
 
 $scope.determinateValue = 0;
	  $scope.isLoading = true; 
		  $scope.isDisabled = false;
 var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_reports_list',
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
			
			 $scope.determinateValue = 50; 
		  
		    setTimeout(function () {  
	            $scope.isLoading = false;
			  $scope.isDisabled = true;
			   //alert($scope.isLoading);
			 }, 20);
			   
		  $scope.determinateValue = 100;	
			 $scope.isDisabled = true;
 
		   
				 $scope.reports =  data; 
            
				 //$scope.product_id = data["user_data"][0]["product_id"];
					 
		        //$window.location.href = 'product_edit/'+$scope.product_id;
            });  

});
