var app = angular.module("mainApp", ['ngRoute','ngAnimate', 'toastr','angular-jquery-locationpicker']);
 
app.config(function($routeProvider) {
    $routeProvider
        .when('/timeslots', {
            templateUrl: 'timeslots.html',
            controller: 'timeslotsController'
        })
        .when('/taxes', {
            templateUrl: 'taxes.html',
            controller: 'taxesController'
        })  
        .otherwise({
            redirectTo: '/timeslots'
        });
});
  
 
app.controller('timeslotsController', function($http,$scope,toastr) {
 
 
	   $('#loading').css('display', 'block');
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
	  
	  
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_profile_basic_details',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	
			$scope.profileData = data;
			
	   $('#loading').css('display', 'none');
			document.getElementById("res").value =JSON.stringify(data);
			
			
		 
      }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});




app.controller('taxesController', function($http,$scope,toastr) {
 
 
	   $('#loading').css('display', 'block');
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
	  
	  
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_profile_basic_details',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	
			$scope.profileData = data;
			
	   $('#loading').css('display', 'none');
			document.getElementById("res").value =JSON.stringify(data);
			
			
		 
      }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});





