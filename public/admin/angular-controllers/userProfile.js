var app = angular.module("mainApp", ['ngRoute','ngAnimate', 'toastr','angular-jquery-locationpicker']);
 
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'user-basicInfo.html',
            controller: 'profileController'
        })
        .when('/order', {
            templateUrl: 'user-order.html',
            controller: 'ordersController'
        }) 
		.when('/address', {
            templateUrl: 'user-address.html',
            controller: 'addressController'
        }) 
        .otherwise({
            redirectTo: '/'
        });
});
  
 
app.controller('profileController', function($http,$scope,toastr) {
 
 
	   $('#loading').css('display', 'block');
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
	  
	  
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_profile_basic_details',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	
			$scope.profileData = data;
			
	   $('#loading').css('display', 'none');
			document.getElementById("res").value =JSON.stringify(data);
			
			
		 
      }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});








 
 
app.controller('ordersController', function($http,$scope,toastr) {
 
 
 
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
		
 
		 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_orders',
                 data:    $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
		 	document.getElementById("res2").value =JSON.stringify(data);
			
	        $scope.app_url = data.app_url;
			
			 
			$scope.ordersData = data;
			
			 
      }).error(function (data, status, headers, config) { 
			 	
				document.getElementById("res2").value =JSON.stringify(data);
               
        });   
});




app.controller('addressController', function($http,$scope,toastr) {
 
  	 $scope.user_id =  document.getElementById("user_id").value ;
	   $scope.data = {"user_id":$scope.user_id}
		 
	 
		
		 // get addresses  get data from api**/
		
        $http.get(APP_URL+'/user_address/'+$scope.user_id+'?search_text=&limit=100&date_filter=lastAdded&fields=id,address_line1,address_line2,city,state,country,pincode,created_at')
            .success(function(data, status, headers, config) {
 
			$scope.addressData = data;
                $scope.columns_addresses = data.columns;
                $scope.data_addresses = data.data;
                $scope.currentpage_addresses = data.current_page;
                $scope.user_id = user_id;
 
            })
            .error(function(data, status, headers, config) {
0
                document.getElementById('res').value = JSON.stringify(data);
            });

 $scope.map = {
      center: {
        latitude: 21.0000,
        longitude: 78.0000
      },
      zoom: 4,
      events: {
        click: function(mapModel, eventName, originalEventArgs,ok) {
          var e = originalEventArgs[0]; 

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {

                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });


        }
      }
    };
	

  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address')
                        },
                        radius: 300,
                        enableAutocomplete: true
                    };
					

 
 	
    //get add address form
 
    $scope.add_address_form = function() {
		
		
		   
  
        /*get data from api**/
        $http.get(APP_URL+'/user_address_forms')
            .success(function(data, status, headers, config) {
 
                $scope.columns_add_address_data = data;

            })
            .error(function(data, status, headers, config) {
 
                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }

	
	



    $scope.add_address = function() {
	
	
	for(var i=0; i<$scope.columns_add_address_data[0].fields.length; i++){
		if($scope.columns_add_address_data[0].fields[i].identifier == 'latitude'){
			$scope.columns_add_address_data[0].fields[i].value = $('#us3-lat').val(); 
		}
		else if($scope.columns_add_address_data[0].fields[i].identifier == 'longitude'){
			$scope.columns_add_address_data[0].fields[i].value = $('#us3-lon').val(); 
		}
	}
		 $scope.user_id =  document.getElementById("user_id").value ;
   
	  
        /*get data from api**/
          var request = $http({
            method: "POST",
            url: APP_URL+'/user_address_store/' + $scope.user_id,
            data: $scope.columns_add_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
			 
    $('#myModal_add2').modal('hide');
        
            $scope.products = data;
		 
			toastr.success(data.message,'Success!');
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res2").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {
			 
         toastr.error('Error Occurs','Error!');
            document.getElementById("res2").value = JSON.stringify(data);
        });
    }
	
	
 
 
 
  $scope.user_address_forms = function(id) { 
  
  
	    $('#myModal').modal('hide');

        /*get data from api**/
        $http.get(APP_URL+'/api/user_address_forms/' + id)
            .success(function(data, status, headers, config) {

                $scope.columns_edit_address_data = data;
                $scope.address_id = id;

            })
            .error(function(data, status, headers, config) {

                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }
	
	
	    $scope.user_address_update = function() {

        var address_id = $scope.address_id; 
		  $('#myModal_edit').modal('hide');
  
        /*get data from api**/
        var request = $http({
            method: "POST",
            url: APP_URL+'/api/user_address_update/'+address_id,
            data: $scope.columns_edit_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
			  $scope.products = data;
			  toastr.success(data.message,'Success!');
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res2").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {
			toastr.error('Error Occurs','Error!');
             document.getElementById("res2").value = JSON.stringify(data);
        });


    }
	
	
		$scope.user_address_delete = function(itemId, index) {
     
	$scope.user_address_id = {user_address_id:itemId};
	
	   if (confirm("Are you sure?")) {
                 
				 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_address_delete',
                 data: $scope.user_address_id,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				
				$scope.data = data;
			   toastr.success(data.message,'Success!');
			   document.getElementById("res").value = JSON.stringify(data);
			    
			 }).error(function (data, status, headers, config) { 
			    toastr.error('Unknown Error Occurred','Error!');
			    document.getElementById("res").value = JSON.stringify(data);
				
        }); 
	   }
		
    };
	
	
});





