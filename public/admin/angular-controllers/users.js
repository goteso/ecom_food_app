var app = angular.module("mainApp", [ 'ngAnimate', 'toastr'])
 
app.factory('Excel', function($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function(s) {
            return $window.btoa(unescape(encodeURIComponent(s)));
        },
        format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
                return c[p];
            })
        };
    return {
        tableToExcel: function(tableId, worksheetName) {
            var table = $(tableId),
                ctx = {
                    worksheet: worksheetName,
                    table: table.html()
                },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
})

var itemApp = angular.module('itemApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


app.controller('userController', userController);







 
  function userController($http, $scope, Excel, $timeout , toastr) {
        var pro = this;
		


		


	 //Route 2 =========================================================================================================
     //=================================================================================================================		
	    $('#loading').css('display', 'block');
	   
	   	 var user_type_filter = $('#user_type_filter').val();
 
		 if(user_type_filter == null || user_type_filter == 'undefined' ) { user_type_filter = '';}
       /*get data from api**/
        $http.get(APP_URL+'/users?user_type_filter='+user_type_filter+'&search_text=&limit=100&date_filter=lastAdded&fields=id,first_name,last_name,email,mobile,photo,user_type')
        .success(function(data, status, headers, config) {
			  document.getElementById('res').value = JSON.stringify(data);
            pro.columns = data.columns;
            pro.data = data.data;
            pro.currentpage = data.current_page;
				 
	   $('#loading').css('display', 'none');
	   
	 
 
			  document.getElementById('res').value = JSON.stringify(data);
         })
        .error(function(data, status, headers, config) {
		    document.getElementById('res').value = JSON.stringify(data);
        });
     //=================================================================================================================	
	 //=================================================================================================================	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 //Route 2 =========================================================================================================
     //=================================================================================================================
		$scope.filter_users= function()
		{
		 
		 var date_filter = $('#date_filter').val();
		 var search_text = $('#search_text').val();
		 var user_type_filter = $('#user_type_filter').val();
	     if(date_filter == null || date_filter == 'undefined' ) { date_filter = '';}
		 if(search_text == null || search_text == 'undefined' ) { search_text = '';}
		 if(user_type_filter == null || user_type_filter == 'undefined' ) { user_type_filter = '';}
	   
	   $http.get(APP_URL+'/users?search_text='+search_text+'&limit=100&date_filter='+date_filter+'&fields=id,first_name,last_name,email,mobile,photo,user_type&user_type_filter='+user_type_filter)
        .success(function(data, status, headers, config) {
			
	
			      document.getElementById('res').value = JSON.stringify(data);
            pro.columns = data.columns;
            pro.data = data.data;
            pro.currentpage = data.current_page;
		
			document.getElementById('res').value = APP_URL+'/users?search_text='+search_text+'&limit=100&date_filter='+date_filter+'&fields=id,first_name,last_name,email,mobile,photo,user_type&user_type_filter='+user_type_filter;
           })
		   
        .error(function(data, status, headers, config) {
		 
			      document.getElementById('res').value = JSON.stringify(data);
          });
		}
	   //=================================================================================================================
	   //=================================================================================================================
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 	 //Route 21 ========================================================================================================
	 // its used to delete single user from delete icon from admin panel user  list
     //=================================================================================================================
       /**delete Row**/
		pro.removeChoice = removeChoice; 
		function removeChoice(itemId, index) {
     
	   if (confirm("Are you sure?")) {
 
	  			 	       var request = $http({
                 method: "POST",
                 url: APP_URL+'/users_delete',
                 data:  "["+JSON.stringify(pro.data.data[index])+"]",
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			   document.getElementById("res").value = JSON.stringify(data);
			   
			 
			   toastr.success(data.message,'Success!');
				  for (var i = 0; i < pro.data.data.length; i++) { 
	                       pro.data.data.splice(index, 1);
                           break;
	                 }
			    $scope.chckedIndexs = [];
			 }).error(function (data, status, headers, config) { 
			    toastr.error('Unknown Error Occurred','Error!');
			    document.getElementById("res").value = JSON.stringify(data);
                $scope.chckedIndexs = [];
        }); 
	   }
		
    };
	  //=================================================================================================================
      //=================================================================================================================
 
	
	
	
	 //Route 21 ========================================================================================================
	 //its used to delete multiple users from checkbox
     //=================================================================================================================
	/***Checkbox delete Row**/
	 $scope.chckedIndexs=[];

 
     $scope.checkedIndex = function (values) {
         if ($scope.chckedIndexs.indexOf(values) === -1) {
             $scope.chckedIndexs.push(values);
         }
         else {
             $scope.chckedIndexs.splice($scope.chckedIndexs.indexOf(values), 1);
         }
     }
      
      $scope.remove=function(index){
		 
		  angular.forEach($scope.chckedIndexs, function (value, index) { 
		  
 
		  for (var i = 0; i < pro.data.data.length; i++) { 
	 
            var index = pro.data.data.indexOf(value); 	

 		
		   pro.data.data.splice(index, 1);
          break;
		  }
       }); 
	   
	   
	   if (confirm("Are you sure?")) {
 
	   
	       var request = $http({
                 method: "POST",
                 url: APP_URL+'/users_delete',
                 data:  $scope.chckedIndexs,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				 
				 toastr.success(data.message,'Successfully Deleted!');
				$scope.chckedIndexs = [];
			 }).error(function (data, status, headers, config) { 
			 	
			document.getElementById("res").value =JSON.stringify(data);
                 $scope.chckedIndexs = [];
				  toastr.error('Error Occurred','Error!');
        });   
		
	   }
		
 	  //=================================================================================================================
      //=================================================================================================================       
		
     };


	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
		 
	 
	 
	 	$scope.changePassword =function(){ 
    
	
	var user_id = document.getElementById('user_id').value;
	
	
		var new_password = document.getElementById('new_password').value;
	$scope.data = {'user_id':user_id,'old_password':'pass','new_password':new_password};
	
	  
 
	  			 	       var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/change_password?request_type=api',
                data:  $scope.data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
			   document.getElementById("res").value = JSON.stringify(data);
			   
			 
			   toastr.success(data.message,'Success!');
			 
			 
				$scope.chckedIndexs = [];
			 }).error(function (data, status, headers, config) { 
			    toastr.error('Unknown Error Occurred','Error!');
			    document.getElementById("res").value = JSON.stringify(data);
                $scope.chckedIndexs = [];
        }); 
	   
		
    };
	
	
	
	
	
	
	
	
	
	
	
	
	
	

    $scope.selectedAll = {
        selectedLabelList: []
    }


    $scope.isSelectAll = function() {
        $scope.selectedAll.selectedLabelList = [];
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
            for (var i = 0; i < pro.users.length; i++) {
                var index = pro.users[i].data.indexOf(value);
                $scope.selectedAll.selectedLabelList.push(index);
            }
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach(pro.users[i].data, function(values) {
            values.selected = $scope.selectedAll;
        });
    }

	
	
	



//Route 10 - get list of all addresses added by user =================================================================== 
//====================================================================================================================== 
  

    $scope.get_addresses = function(user_id) {
 
        /*get data from api**/
        $http.get(APP_URL+'/user_address/' + user_id + '?search_text=&limit=100&date_filter=lastAdded&fields=id,address_line1,address_line2,city,state,country,pincode,created_at')
            .success(function(data, status, headers, config) {

                pro.columns_addresses = data.columns;
                pro.data_addresses = data.data;
                pro.currentpage_addresses = data.current_page;
                pro.user_id = user_id;
 
            })
            .error(function(data, status, headers, config) {

                document.getElementById('res').value = JSON.stringify(data);
            });
   }
//====================================================================================================================== 
//====================================================================================================================== 





	
	
    //get add address form
 
    $scope.add_address_form = function() {
 
        //$('#myModal').modal('hide');

        /*get data from api**/
        $http.get(APP_URL+'/user_address_forms')
            .success(function(data, status, headers, config) {

                pro.columns_add_address_data = data;

            })
            .error(function(data, status, headers, config) {

                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }




    $scope.add_address = function($user_id) {

        document.getElementById("res").value = JSON.stringify(pro.columns_add_address_data);
        /*get data from api**/
          var request = $http({
            method: "POST",
            url: APP_URL+'/user_address_store/' + pro.user_id,
            data: pro.columns_add_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {

            pro.products = data;
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {


            document.getElementById("res").value = JSON.stringify(data);
        });
    }




    $scope.user_address_forms = function(id) { 
	
        $('#myModal').modal('hide');

        /*get data from api**/
        $http.get(APP_URL+'/api/user_address_forms/' + id)
            .success(function(data, status, headers, config) {

                pro.columns_edit_address_data = data;
                pro.address_id = id;

            })
            .error(function(data, status, headers, config) {

                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }




    $scope.user_address_update = function(id) {

        var address_id = pro.address_id;
        $('#myModal').modal('hide');
 
        document.getElementById("res").value = JSON.stringify(pro.columns_edit_address_data);
        /*get data from api**/
        var request = $http({
            method: "POST",
            url: APP_URL+'/api/user_address_update/' + address_id,
            data: pro.columns_edit_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
           
            pro.products = data;
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {
 
            document.getElementById("res").value = JSON.stringify(data);
        });


    }




    /*download as excel**/
    $scope.exportToExcel = function(tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function() {
            location.href = exportHref;
        }, 100); // trigger download
    }

    /*download as pdf**/
    $scope.export = function() {
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function(canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
        });
    };


    /**pagination**/
    $scope.currentPage = 1;
    $scope.pageSize = 5;

    $scope.pageChangeHandler = function(num) {
        console.log('meals page changed to ' + num);
    };




    $scope.printToCart = function(tableToExport) {
        var innerContents = document.getElementById('tableToExport').innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }

	
	
	
	
	
	$scope.disabled = function() {
  if($scope.linkDisabled) { return false;}
}

};

app.controller('OtherController', OtherController);

function OtherController($scope) {
    $scope.pageChangeHandler = function(num) {
        console.log('going to page ' + num);
    };
}