               var data = [
				 {"categoryId":"2","categoryName":"Dry Cleaning","products":[
				 {"id":"1","categoryId":"2","title":"Ladies Salwar","price":"5","photo":"admin/images/products/product_images/salwar.jpg","discount":"506","qty":0},
				 {"id":"6","categoryId":"2","title":"Anarkali","price":"20","photo":"admin/images/products/product_images/anarkali.jpg","discount":"20","qty":0},
				 {"id":"7","categoryId":"2","title":"Saree","price":"66","photo":"admin/images/products/product_images/saree.jpg","discount":"66","qty":0},
				 {"id":"12","categoryId":"2","title":"Shorts","price":"20","photo":"admin/images/products/product_images/1497964986211.jpg","discount":"","qty":0},
				 {"id":"13","categoryId":"2","title":"Blazer","price":"40","photo":"admin/images/products/product_images/1497965027433.jpg","discount":"","qty":0},
				 {"id":"14","categoryId":"2","title":"Jean","price":"25","photo":"admin/images/products/product_images/1497965063132.jpg","discount":"","qty":0},
				 {"id":"17","categoryId":"2","title":"Pajama","price":"15","photo":"admin/images/products/product_images/1497965254944.jpg","discount":"","qty":0},
				 {"id":"18","categoryId":"2","title":"Overcoat","price":"40","photo":"admin/images/products/product_images/1497965306853.jpg","discount":"","qty":0},
				 {"id":"21","categoryId":"2","title":"Sweater","price":"35","photo":"admin/images/products/product_images/1497965494981.jpg","discount":"","qty":0},
				 {"id":"22","categoryId":"2","title":"Sherwani","price":"120","photo":"admin/images/products/product_images/1497965597299.jpg","discount":"","qty":0}
				 ]},
				 {"categoryId":"3","categoryName":"Carpet Cleanings","products":[
				 {"id":"2","categoryId":"3","title":"Stole","price":"10","photo":"admin/images/products/product_images/stole.jpg","discount":"50","qty":0},
				 {"id":"11","categoryId":"3","title":"Cushion","price":"15","photo":"admin/images/products/product_images/1497964930641.jpg","discount":"","qty":0}
				 ]},
				 {"categoryId":"4","categoryName":"Wash & Fold","products":[
				 {"id":"3","categoryId":"4","title":"Top","price":"18","photo":"admin/images/products/product_images/top.jpg","discount":"20","qty":0},
				 {"id":"8","categoryId":"4","title":"Dupatta","price":"6","photo":"admin/images/products/product_images/dupatta.jpg","discount":"6","qty":0},
				 {"id":"15","categoryId":"4","title":"Skirt","price":"20","photo":"admin/images/products/product_images/1497965100145.jpg","discount":"","qty":0},
				 {"id":"16","categoryId":"4","title":"Shirt","price":"15","photo":"admin/images/products/product_images/1497965138409.jpg","discount":"","qty":0},
				 {"id":"19","categoryId":"4","title":"Choli","price":"100","photo":"admin/images/products/product_images/1497965362766.jpg","discount":"","qty":0},
				 {"id":"20","categoryId":"4","title":"T-shirt","price":"20","photo":"admin/images/products/product_images/1497965467111.jpg","discount":"","qty":0}]}
				 ];