@extends('layout.auth')
@section('title', 'Create')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
	  <!---------- Static Sidebar Ends------------->	
      <div class="row">
         	    @include('flash::message')
         <div class="col-md-6 page-action text-right">
            <a href="{{ route('projects.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-10">
            {!! Form::open(['route' => ['projects.store'] ]) !!}
            @include('projects._form')
            <!-- Submit Form Button -->
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endsection