 

 

<input type="file" name="image" id="image">
 
<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', @$item->title, ['class' => 'form-control ', 'placeholder' => 'Category Title' ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
</div>


<div class="form-group @if ($errors->has('result')) has-error @endif">
{!! Form::label('result', 'Parent Category') !!}

 
  <select   class="form-control" id="parent_id" name="parent_id"  style="width:100%">
  <option value="">Select Parents Category(optional)</option>
      <?php 
	
	    $categories_level = \App\Settings::where('key_title','categories_level')->first(['key_value'])->key_value;
	  ?>
      @foreach($result as $ct)
	    <?php 
		  if($categories_level =='1' or $categories_level ==1)
		  {
			  if($ct->parent_id == '')
			  {
			  ?>
			   <option value="{{ $ct->id }}" <?php if(@$item->parent_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->title }} </option>
		 <?php }
		  }
		  
		  else
		  {?>
			   <option value="{{ $ct->id }}" <?php if(@$item->parent_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->title }} </option>
		 <?php }
		 ?>
	  @endforeach
</select>
</div>



 
 

<script type="text/javascript">
   $(document).ready(function() {
       $('#multi_select_box').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   
 });
</script>

 

 
<script type='text/javascript'>
$(window).load(function(){
$('.datetimepicker').datetimepicker({  format:'DD/MM/YYYY hh:mm A', });
});
</script>