<?php
   use App\Role;
?>
@extends('layout.auth')
@section('title', 'Categories' )
		 <link rel="stylesheet" href="{{ URL::asset('admin/css/custom.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
			
			 <div class="row">
			    <div class="col-sm-8">
				  	    @include('flash::message')
				  
					 </div>
					 <div class="col-sm-2 col-lg-2 text-right top-actions">
                        <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
						<!-- <a href="{{ URL::to('reports_page')}}"><i class="fa fa-print" onclicks="myFunction()"></i></a>-->
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
                                        
										
                      </div>
                  <div class="col-sm-2">
				  <div class="text-right">
                   @can('edit_categories') 
                     <button type="button" class="btn btn-success text-right"  data-toggle="modal" data-target="#add">Add New</button>
					 @endcan
                  </div>
					 </div>
					 </div>
					 
					 <br><br>
               <div class="row">
                  <div class="col-sm-12">
				   
                     <div class="tab-content  ">
                        <table class="table table-striped table-hover products-table" id="tableToExport">
						
						 
                           <thead>
                              <tr>
                                 <th>Id</th>
                                 <th>Image</th>
								 <th>Title</th>
                                 <th>Created At</th>
                               
                                @can('edit_categories')  <th class="text-center">Actions</th>@endcan
                             
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              @foreach($result as $item)
                              <?php $x++;?>
                              <tr>
                                 <td>#{{ $item['id'] }}</td>
								 <td>
								 <img src="<?php if($item['photo'] != '') {echo url('/')."/categories/".$item['photo'];}
								 else{echo url('/')."/admin/images/item-placeholder.png";}?>" style="max-height:200px" height="200">
								 
								 </td>
                                 <td>{{ $item['title'] }} </br><?php if($item['parent_category_title'] !='' or $item['parent_category_title'] !=null ){?> <small style="font-size:12px"><b>Parent :</b>{{ $item['parent_category_title'] }}</small><?php } ?></td>
                                 <td>{{ $item['created_at']->diffForHumans() }}</td>
                              @can('edit_categories')   
                                 <td class="text-center">
                                    @include($model_name.'._actions_with_modals', [
                                    'entity' => $model_name,
                                    'id' => $item['id']
                                    ])
                                 </td>
								 @endcan
							
                              
                              </tr>
                              <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                                 <div class="modal-dialog"  >
                                    <!-- Modal content-->
                                    <div class="modal-content"  >
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Update Category</h4>
                                       </div>
                                       <div class="modal-body "  >
									   
									 
                                          <div class="row">
                                             <div class="col-lg-10">
                                             
											   
											    <?php
			       echo Form::open(array('url' => '/update_category_form/'.$item['id'],'files'=>'true'));
				   
				 
			   ?>
											   
                                                @include($model_name.'._form')
                                                <!-- Submit Form Button -->
                                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                                {!! Form::close() !!}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default"  data-dismiss="modal" onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							  	 <?php $item=null;?>
                              <!-------inner model ends-------->
                              @endforeach
                           </tbody>
                        </table>
						  <div class="text-right">
                                 {{ $result->links() }}
                           </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>

</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog "  >
      <!-- Modal content-->
      <div class="modal-content "  >
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Category</h4>
         </div>
         <div class="modal-body "  >
          
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
               
			   <?php
			       echo Form::open(array('url' => '/store_category','files'=>'true'));
			   ?>
                  @include($model_name.'._form')
				  <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
				        <button class="btn btn-md btn-info"  onclick="this.form.submit()">Submit</button>
				   </form>
                  <!-- Submit Form Button -->
            
				 
                
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

 



<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
 
<!------>
@endsection