@extends('layout.auth')
@section('title', 'Dashboard' ) 
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
 <style>
 @media(min-width:766px){
	.card{height:330px;overflow-y:auto}
	.panel{height:400px;overflow-y:auto}
}
hc-chart {
padding-top:5px;
  width: 100%; 
  display: block;
} 
 
</style>
@section('content')
@section('header')
@include('includes.header')
@show
 
 

<div id="wrapper">
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	  
<script type="text/javascript">
    var controller_data = <?php echo json_encode($result);?>;
</script> 

 


 <textarea id="res" style="display:none" ></textarea>
	
	 <div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
 			
      <div ng-app="mainApp" ng-controller="dashboardController" ng-cloak>
        <div class="container-fluid" >
  <div class="row">
    <div class="col-sm-12" >
	   <h2 class="header">Dashboard</h2>
	</div>  
  </div>
  <div class="row" >
      <div class="col-sm-12 col-md-12 col-lg-12" >
	   <div class="row">
	  
	      <div class="col-sm-6 col-md-4" ng-repeat="items in dashboardData"  >
	         
			<div class="card" ng-show="items.type=='recentOrders'" >
			 <h3 class="block-title">Recent Orders</h3>
			  <table class="table items-detail"> 
		 <tbody>
		 <tr ng-repeat="items in items.data "> 
		  <td class="order-id">  <a href="{{ URL::to('order_detail_page')}}?order_id=@{{items.id}}">#@{{items.id}} </a> <span class="state-color" style="background-color:@{{items.color}}">@{{items.order_status | uppercase}}</span><br><pre class="order-cust"><a target="_blank" href="{{url::to('user_profile')}}?user_id=@{{items.custId}}">@{{items.cust}}</a></pre></td> 
		  <td class="order-total text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{items.total}}<br>@{{items.created_at_formatted  }}</td>
		 </tr> 
		 </tbody>
		 </table>
	       </div>
		 
	  <!-----Block Starts For Customer Information----->
	   <div class="card" ng-show="items.type=='sales'">  
	     <h3 class="block-title">Transactions</h3>
		  <h4 class="block-total">@{{items.total}}</h4>
		    <p class="block-desc">@{{items.desc}}</p>
            <hc-chart options="salesChartOptions">Placeholder for generic chart</hc-chart> 
	  </div>
	  <!-----Block Ends For Customer Information----->
	  
	  
	  
	  
	  <!-----Block Starts For Delivery Address----->
	   <div class="card" ng-show="items.type=='orders'">
	    <h3 class="block-title">Orders</h3>
		<h4>@{{items.desc}}</h4>
		    <hc-chart options="ordersChartOptions">Placeholder for generic chart</hc-chart> 	   
	  </div>
	  <!-----Block Ends For Delivery Address----->
	  
	 
	   <!-----Block Starts For Order Summary----->
	   <div class="card" ng-show="items.type=='newUsers'">
	    <h3 class="block-title">New Users</h3>
		
	 
	     <table class="table items-detail"> 
		 <tbody>
		 
		 
		 
		 <tr ng-repeat="item in items.data">
		 <td class="user-image"><img src="admin/images/user-placeholder.png"></td>
		  <td class="user-name"><a href="{{url::to('user_profile')}}?user_id=@{{item.id}}">@{{item.first_name}} @{{item.last_name}}</a><br><pre class="user-desc">@{{item.email}}</pre></td> 
		  <td class="user-createdTime text-right"><pre>@{{item.created_at }}</pre>
		  <span>@{{item.user_type_title }}</span>
		  </td>
		 </tr> 
		 </tbody>
		 </table>
	  </div>
	   <!-----Block Ends For Order Summary----->
	   
	   
	  
	   <!-----Block Starts For Payment Summary----->
	   <div class="card" ng-show="items.type=='newItems'">
	    <h3 class="block-title">New Items</h3>
		 <table class="table items-detail"> 
		 <tbody>
		 <tr ng-repeat="item in items.data ">
		 
		 	 <?php  $st = \App\FeaturesSettings::where("title",'photo')->first(['status'])->status;   ?>
		 <?php if($st =='1' or $st==1) { ?><td class="item-image"><img src="admin/images/item-placeholder.png"></td> <?php } ?>
		  <td class="item-title"><a href="{{url::to('product_edit')}}/@{{item.id}}">@{{item.title}}</a><br><pre class="item-desc">@{{item.desc}}</pre></td>
		  <td class="item-price"> <?php echo env("CURRENCY_SYMBOL", "");?> @{{item.priceDesc}}</td>
		  <td class="item-createdTime text-right"><pre>@{{item.created_at_formatted}}</pre>
		 
		  </td>
		 </tr> 
		 </tbody>
		 </table>
	  </div>
	  <!-----Block Ends For Payment Summary----->
	  
	 
	  </div>
    </div>
	
	
	
	</div>
    </div>

  </div>
</div>
      
	  <!--------------------------- Angular App Ends ---------------------------->
	  
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

 
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/dashboard.js')}}"></script> 
	  <!----assets for pdf download--------->
	 

<!------>
@endsection