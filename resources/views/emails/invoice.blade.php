<!DOCTYPE html>
<html>
<head>
	<!-- Define Charset -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Responsive Meta Tag -->
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

	<title>Invoice</title>
	
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	 
</head>
<body ng-app="mainApp">
<div  ng-controller="invoiceController">

<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="" >
  <tr>
     <td height="30" style="font-size: 50px; line-height: 30px;">&nbsp;</td>
  </tr>
  <tr >
     <td >
	  <table border="0" align="center" width="590" cellpadding="0" cellspacing="0"  border="0"   class="container590 bodybg_color" style="border:1px solid #f5f5f5;" id="exportthis">
			 <tr>
				 <td>
		 <table border="0" align="center" width="590" cellpadding="0" cellspacing="0"  border="0" bgcolor="f7f7f7" class="container590 bodybg_color"   >
			 <tr>
				 <td>
				     <table border="0" align="center" width="520" cellpadding="0" cellspacing="0" class="container590 "  >
								
								<tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
								
								<tr>
								 <td>
								 <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
								<tr>
								<td><img width="156" border="0" style="display: block; width: 156px;" ng-src="@{{invoice.order_barcode}}" alt="barcode" /></td>
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								<td style="font-family:'Open Sans',sans-serif;"><h2>INVOICE #@{{invoice.order_id}}</h2></td>
								</tr>
								</table>
								
								</td>
								</tr>
								
								
								<tr>
								 <td>
								 <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
								<tr>
								<td >
								  <pre style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;">@{{invoice.customer_details}}</pre>
								   
								</td>
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								<td align="right"  > 
								 <pre style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;">@{{invoice.vendor_details}}</pre>
								    
								</td>
								</tr>
								 
								 <tr>
								 <td style="font-family:'Open Sans',sans-serif;" align="right">
								   
								  <img width="156" border="0" style="display: block; width: 156px;" src="@{{invoice.app_logo}}" alt="logo" /> 
								   <p style="line-height:25px;font-size:16px;">Date : @{{invoice.date}}
								  </p>
								</td>
								
								 
								</tr>
								</table>
								
								</td>
								
								</tr>
					</table>
				 </td>
			 </tr>
		 </table>
		 
		 <table border="0" align="center" width="590" cellpadding="0" cellspacing="0"  border="0" bgcolor="ffffff" class="container590 bodybg_color"   >
		  <tr>
		    <td>
			   <table border="0" align="center" width="520" cellpadding="0" cellspacing="0" class="container590 "  >
								
					<tr><td height="5" style="font-size: 5px; line-height: 5px;">&nbsp;</td></tr>
								
					<tr>
						<td style="font-family:'Open Sans',sans-serif;">
						  <h4 style="margin:10px 0;">4 Items</h4>
						</td>
					</tr>
					
					<tr>
					   <td>
					    <table border="0" width="520" align="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7">
											
											<tr bgcolor="4a8fe0" >
												<td  align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.items.columns[0]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.items.columns[1]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.items.columns[2]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.items.columns[3]}}
												</td> 
											</tr>
											<tr ng-repeat="value in invoice.items.data" >
												<td  align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{value.title}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{invoice.currency_symbol}}@{{value.discounted_price}}/@{{value.unit}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{value.quantity}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{invoice.currency_symbol}}@{{value.discounted_price * value.quantity}}
												</td> 
											</tr>
										 
										</table>
					</td>
					</tr>
					
					<tr ><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
								
								<tr>
									<td >
										<table border="0"  bgcolor="#f7f7f7" align="right" width="250" cellpadding="0" cellspacing="0"style="padding:10px 15px;">
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Subtotal
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													@{{invoice.currency_symbol}}@{{invoice.sub_total}}
												</td>
											</tr>
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Coupon Discount
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													@{{invoice.coupon_discount}}
												</td>
											</tr>
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Shipping
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													@{{invoice.shipping}}
												</td>
											</tr>
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Tax
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													@{{invoice.tax}}
												</td>
											</tr>
											<tr  >
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													Total
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													@{{invoice.currency_symbol}}@{{invoice.total}}
												</td>
											</tr>
										</table>
									</td>
								</tr>
								
								
								
								<tr>
						<td style="font-family:'Open Sans',sans-serif;">
						  <h4  style="margin:10px 0;">Transactions</h4>
						</td>
					</tr>
					
					<tr>
					   <td>
					    <table border="0" width="520" align="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7">
											
											<tr bgcolor="4a8fe0">
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.order_transactions.columns[0]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.order_transactions.columns[1]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.order_transactions.columns[2]}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #ffffff; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #84d3e4;">
													@{{invoice.order_transactions.columns[3]}}
												</td> 
											</tr>
											<tr ng-repeat="value in invoice.order_transactions.data" >
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{value.created_at_formatted}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{value.payment_gateway}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{value.transaction_id}}
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-bottom: solid 1px #f1f1f1;">
													@{{invoice.currency_symbol}}@{{value.amount}}
												</td> 
											</tr> 
										</table>
					</td>
					</tr>
					
					
								
				</table>
				
				<table border="0"  bgcolor="#ffffff" align="right" width="160" cellpadding="0" cellspacing="0"  >
				<tr ><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
				<tr>
				  <td style="font-family:'Open Sans',sans-serif;">
				  
				  <p>Tool By | <span ><a style="text-decoration:none;color:;" href="https://www.goteso.com" target="_blank">Goteso</a></span></p></td>
				</tr> 
				</table>
				
			</td>
		  </tr> 
		 </table>
		  </td>
			 </tr>
		 </table>
		 
	  </td>
  </tr>
</table>

<div id="editor"></div>
<button id="pdf-button" type="button" value="Download PDF" ng-click="export()">Download as PDF</button>

</div>

 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
<script>
var app = angular.module("mainApp", []);
app.controller('invoiceController', function($http,$scope) {
  

$scope.invoice = <?php echo json_encode($d);?>;




		/*download as pdf**/
   $scope.export = function(){
   
    
        html2canvas(document.getElementById('exportthis'), {
		
		//var doc = new jsPDF("p", "mm", "a4"); 
       //var width = doc.internal.pageSize.width;    
       //var height = doc.internal.pageSize.height;

            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("order.pdf");
            }
        });
   };
 
});
</script>
 <script src="https://docraptor.com/docraptor-1.0.0.js"></script>
  <script>
    var downloadPDF = function() {
      DocRaptor.createAndDownloadDoc("YOUR_API_KEY_HERE", {
        test: true, // test documents are free, but watermarked
        type: "pdf",
        document_content: document.querySelector('html').innerHTML, // use this page's HTML
        // document_content: "<h1>Hello world!</h1>",               // or supply HTML directly
        // document_url: "http://example.com/your-page",            // or use a URL
        // javascript: true,                                        // enable JavaScript processing
        // prince_options: {
        //   media: "screen",                                       // use screen styles instead of print styles
        // }
      })
    }
  </script>
</body>
</html>