@include('emails.mail_header')				

		 
					
					<tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center" style="color: #293038; font-size: 24px; font-family: 'Questrial', sans-serif; mso-line-height-rule: exactly; line-height: 30px;" class="title_color main-header">
							
							<!-- ======= section header ======= -->
							
							<div style="line-height: 24px;">
	        					
	        					 
									
									{!!$email_body!!}
	        					
							</div>
        				</td>
					</tr>
					 
					
					<tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
					
					<tr>
						<td>
							<table border="0" width="360" align="center" cellpadding="0" cellspacing="0" class="container580">				
								<tr>
									<td align="center" style="color: #66717d; font-size: 14px; font-family: 'Questrial', sans-serif; mso-line-height-rule: exactly; line-height: 24px;" class="resize-text text_color">
										<div style="line-height: 24px">
											
											<!-- ======= section text ======= -->
											
				        					
				        					 
				        					
										</div>
			        				</td>	
								</tr>
							</table>
						</td>
					</tr>
					
@include('emails.mail_footer')	