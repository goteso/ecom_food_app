 
 
<div class="form-group @if ($errors->has('question')) has-error @endif">
    {!! Form::label('question', 'Question') !!}
    {!! Form::textarea('question', @$item->question, ['class' => 'form-control ', 'placeholder' => 'Question' ] ) !!}
    @if ($errors->has('question')) <p class="help-block">{{ $errors->first('question') }}</p> @endif
</div>




<div class="form-group @if ($errors->has('answer')) has-error @endif">
    {!! Form::label('answer', 'Answer') !!}
    {!! Form::textarea('answer', @$item->answer, ['class' => 'form-control ', 'placeholder' => 'Answer' ] ) !!}
    @if ($errors->has('answer')) <p class="help-block">{{ $errors->first('answer') }}</p> @endif
</div>
 