@extends('layout.auth')
@section('title', 'Features Settings')
@section('content')
@section('header')
@include('includes.header')
@show

<div id="wrapper"  >
   <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 

   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!-- Modal -->
 

                   <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid panel panel-default">
 
      	    @include('flash::message')
       
	  <?php $x1 = 0; $x2=0;  ?>
      <ul class="nav nav-tabs">
         @foreach($types as $type)
         <?php $x1++;  if($x1=='1') { $class1='active';} else { $class1='';}?>
         <li class="{{ $class1 }}" ><a data-toggle="tab" href="#fs{{ $x1 }}">{{ $type->type}} </a></li>
         @endforeach
      </ul>
	  
	  
 
	              {!! Form::model('', ['method' => 'POST', 'route' => ['update_features_settings' ], 'class' => 'm-b']) !!}
      <div class="tab-content">
         @foreach ($types as $type)
		 
		 <?php $settings = \App\FeaturesSettings::where("type",$type["type"])->get();   ?>
         <?php $x2++;  if($x2=='1') { $class2='in active';} else { $class2='';}?>
		 
		 
         <div id="fs{{ $x2 }}" class="tab-pane fade {{ $class2 }}">  
            @foreach ($settings as $s)
               <?php if($s->status == '1') { $stc = 'CHECKED';} else { $stc ='';}?>
		          <div class="col-md-3">
                        <div class="checkbox">
                            <label class="text-info">
					 
						 
							<div class="col-md-4 col-lg-4 col-xs-4">
							<input type="checkbox" {{$stc}} data-toggle="toggle" data-style="ios" data-size="normal" data-on="ON" data-off="OFF" class="switch switch-on toggle"  value='OFF'   id="c{{ $s->title }}"    >
							<input type="hidden"  name="{{ $s->title }}" id="{{ $s->title }}" value="{{ $s->status }}"> 
							</div>
							
							<div class="col-md-8 col-lg-8 col-xs-8">
							<h4>{{ $s["title"] }}</h4>
							</div>
							
                            </label>
                        </div>
                    </div> 
					     <script>
	 $(document).ready(function(){ 

 
	 
	 $("#c{{ $s['title'] }}").change(function() {
		 
         if ($(this).is(":checked")) {
      $("#{{ $s['title'] }}").val('1');
	 // alert($("#{{ $s['title'] }}").val());
  }
  else{
	    $("#{{ $s['title'] }}").val('0');
	 // alert($("#{{ $s['title'] }}").val());
  }
  
	 });
	 });
	 
	 </script>
			@endforeach		
         



 
         
         </div>
		 @endforeach
		 
		   
        
      </div>
	   
   </div>
   
   <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" class="text-left" style="padding-top:2%">
   {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
		    {!! Form::close() !!}
			</div>
   </section>
</div>

</div>

</div>



<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
 

<!----manage modal ends here--->
@endsection