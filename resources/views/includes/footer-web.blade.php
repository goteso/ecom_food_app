	  <!------------------FOOTER SECTION STARTS HERE--------------------->       
           	
         <footer class="footer grey darken-4   ">

         <div class="container-fluid" >
	       <div class="row  " style="padding:30px 0 20px;">
	         <div class="col-md-4 col-sm-7 col-xs-12 footer-logo">
			   <img src="{{ URL::asset('web/img/logo_foot.png')}}" class="img-responsive"><br>
			   <p class="text-white" style="padding:5px 20px 5px 0;color:#fff;">Launspace is a service you can trust. our attendants are highly trained, and your satisfaction is always guaranteed.</p>
			 </div>
			 <div class="col-md-2 col-sm-12">
			   <ul>
			    <li><a href="{{ URL::asset('site_index')}}">Home</a></li>
				<!--<li><a href="{{ URL::asset('site_login')}}">Login</a></li>-->
				<li><a href="{{ URL::asset('services')}}">Pricing</a></li>
				<li><a href="{{ URL::asset('terms_conditions')}}">Terms & Conditions</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3 col-sm-12">
			   <ul>
			    <li><a href="{{ URL::asset('about')}}">About</a></li>
				<li><a href="{{ URL::asset('faq')}}">FAQ</a></li>
				<li><a href="{{ URL::asset('privacy-policy')}}">Privacy Policy</a></li>
				<li><a href="{{ URL::asset('contact')}}">Contact</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3 col-sm-12">
			  <ul>
			    <h5 style="color:#fff">Connect with us:</h5>
			    <li><a href="https://www.facebook.com/" target="_blank">Facebook</a></li>
				<li><a href="https://twitter.com/" target="_blank">Twitter</a></li>
				<li><a href="https://www.linkedin.com/" target="_blank">Instagram</a></li>
			   </ul>
			 </div>
	 
	       </div>
          </div>
		</footer>
		
		
	 <!------------------- Footer ends here ---------------------->
	 <!------------------- Rights Reserved ---------------------->
	 <section>
	  <div class="container-fluid">
	 <div class="row">
	   <div class="col-lg-12 mt-1">
	     <p class="text-center copyright">All Rights Reserved © Copyright | Made with <img src="{{ URL::asset('web/img/heart.png')}}"> by <a href="https://www.goteso.com" target="_blank">Goteso</a></p>
	   </div>
	 </div>
	 </div>
	 </section>
	 <!------------------- Rights Reserved  ---------------------->