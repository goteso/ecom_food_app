 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu">
                    
				<li><a href="{{ URL::to('get_dashboard_data') }}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
					
					
				@can('view_orders')  	
		          <li><a href="{{ URL::to('orders_list') }}"><i class="fa fa-file"></i><span>Orders</span></a></li> 
				@endcan
				
				@can('view_products')  
				  <li><a href="{{ URL::to('product_list') }}"><i class="fa fa-list"></i><span>Products</span></a></li>
				@endcan
				 
				@can('view_categories')  
				  <li><a href="{{ URL::to('view_categories') }}"><i class="fa fa-file"></i><span >Categories</span></a></li>
				@endcan  
				
				
                @can('view_brands') 				
				  <?php $status = \App\FeaturesSettings::where('title','brand_ids')->first(['status'])->status;
				  if($status == '1')
				  {
				  ?>
				  <li><a href="{{ URL::to('brands') }}"><i class="fa fa-file"></i><span >Brands</span></a></li>
				  <?php } ?>
				@endcan  
				
				@can('view_users') 

                 <?php $roles = @\App\Role::where('sidebar_status','1')->get();
				 foreach($roles as $role)
				 {
				 ?>				
                  <li><a href="{{ URL::to('users') }}/{{ $role->id }} "><i class="fa fa-user"></i><span >{{ $role->name }} </span></a></li>
				  
				 <?php } ?>
		 
				 @endcan 
				  
				  
				  
				  
				  
				  
				  	 				
                  <li><a href="{{ URL::to('stores_list') }}"><i class="fa fa-shopping-cart"></i><span >Stores </span></a></li>
		 
				 
				 
				 @can('view_reports') 
				  <li><a href="{{ URL::to('reports_page') }}"><i class="fa fa-file"></i><span>Reports</span></a></li> 
				 @endcan
				 
				 
				 @can('view_roles') 
                  <li><a href="{{ URL::to('roles') }}"><i class="fa fa-user"></i><span>Roles & Permissions</span></a></li>
				 @endcan  
				  
				 @can('view_features_settings') 
				  <li><a href="{{ URL::to('features_settings') }}"><i class="fa fa-wrench"></i><span>Features Settings</span></a></li>
				 @endcan
				   
				 @can('view_transactions') 
				  <li><a href="{{ URL::to('transactions_list') }}"><i class="fa fa-list"></i><span>Transactions List</span></a></li>
				 @endcan
				   
				 @can('view_faqs') 
				  <li><a href="{{ URL::to('faqs_list') }}"><i class="fa fa-question-circle"></i><span>Faqs</span></a></li>
				 @endcan
				 
				   
				  <li><a href="{{ URL::to('upload_logo') }}"><i class="fa fa-upload"></i><span>Upload Logo</span></a></li>
				 
                  <li><a href="{{ URL::to('settings') }}"><i class="fa fa-cog"></i><span>Settings</span></a></li>
				  
               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>