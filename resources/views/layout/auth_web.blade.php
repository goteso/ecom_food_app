<!Doctype html>
<html>
   
   <head>
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>@yield('title')</title>
	    
			<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" /> 
 
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('web/css/compiled.min.css?ver=4.7.3')}}">
		 
	   
	   	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />  
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ URL::asset('web/css/style.css')}}">
		 
		<link rel="stylesheet" href="https://npmcdn.com/angular-toastr/dist/angular-toastr.css" />
		
		
		 
<style>
 toastr.options = {
  toastClass: 'alert',
  iconClasses: {
      error: 'alert-error',
      wait: 'alert-info',
      success: 'alert-success',
      warning: 'alert-warning'
  }
} 


	@font-face {
    font-family: 'Century Gothic';
    src: url('fonts/gothic.eot');
    src: url('fonts/gothic.eot?#iefix') format('embedded-opentype'),
         url('fonts/gothic.woff2') format('woff2'),
         url('fonts/gothic.woff') format('woff'),
         url('fonts/gothic.ttf') format('truetype'),
         url('fonts/gothic.svg#gothic') format('svg');
    
}

@font-face {
    font-family: 'Open Sans Light';
    src: url('fonts/OpenSans-Light.eot');
    src: url('fonts/OpenSans-Light.eot?#iefix') format('embedded-opentype'),
         url('fonts/OpenSans-Light.woff2') format('woff2'),
         url('fonts/OpenSans-Light.woff') format('woff'),
         url('fonts/OpenSans-Light.ttf') format('truetype'),
         url('fonts/OpenSans-Light.svg#gothic') format('svg');
    
}

@font-face {
  font-family:"Open Sans Condensed Light";
  src:url("fonts/OpenSans-CondLight.eot?") format("eot"),
      url("fonts/OpenSans-CondLight.woff") format("woff"),
	  url("fonts/OpenSans-CondLight.ttf") format("truetype"),
	  url("fonts/OpenSans-CondLight.svg#OpenSans-CondensedLight") format("svg");
  font-weight:normal;
  font-style:normal;}

@font-face {font-family:"Open Sans";
  src:url("fonts/OpenSans-Regular.eot?") format("eot"),
      url("fonts/OpenSans-Regular.woff") format("woff"),
	  url("fonts/OpenSans-Regular.ttf") format("truetype"),
	  url("fonts/OpenSans-Regular.svg#OpenSans") format("svg");
	font-weight:normal;
	font-style:normal;}

@font-face {
    font-family: 'Economica';
    src: url('fonts/Economica-Regular.eot');
    src: url('fonts/Economica-Regular.eot?#iefix') format('embedded-opentype'),
         url('fonts/Economica-Regular.woff2') format('woff2'),
         url('fonts/Economica-Regular.woff') format('woff'),
         url('fonts/Economica-Regular.ttf') format('truetype'),
         url('fonts/Economica-Regular.svg#economicaregular') format('svg');
    
}

h1,h2,h3,h4,h5,p{
font-family:"Open Sans";
}


#top_header {
  background-color:#653B7D;
}



#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}

 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}


	 .steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#fff!important;
	}

	</style>
 
		<meta name="csrf-token" content="{{ csrf_token() }}">
			
			
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular.js"></script>  
		
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
		 
		
		
       <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
       <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui.min.js"></script>
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.css">
   
		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="https://npmcdn.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-aria.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-messages.min.js"></script>
		  
        <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.js"></script>
  
  
		<script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		 
		
		
		<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCo0S5dcqwj11plZQyOn7Sx6VJPHQPVZko'></script>
   <script src="{{ URL::asset('admin/map/locationpicker.jquery.min.js')}}"></script>
   <script src="{{ URL::asset('admin/map/angularLocationpicker.jquery.js')}}"></script>
   
   
   
   
    
    <script src="{{ URL::asset('admin/js/angular-datepicker.js')}}"></script>
   
	       
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" /> 
            <link href="{{ URL::asset('admin/css/angular-datepicker.css')}}" rel="stylesheet" type="text/css" />   
			<!-----script files here------------> 

 
 
 
			 
		<meta name="csrf-token" content="{{ csrf_token() }}">
			
 
		
		<script> var APP_URL = '<?php echo env("APP_URL", "");?>';  </script>
        <script>
           window.Laravel = <?php echo json_encode([
               'csrfToken' => csrf_token(),
           ]); ?>
        </script>
   </head>
   <body>
 
  
       @yield('content')
   </body>
  
	  
</html>