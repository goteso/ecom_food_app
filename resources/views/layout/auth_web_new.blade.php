<!Doctype html>
<html>
   
   <head>
    
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>@yield('title')</title>
	   
	   
	   	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />  
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ URL::asset('web/css/style.css')}}">
		 
		<link rel="stylesheet" href="https://npmcdn.com/angular-toastr/dist/angular-toastr.css" />
		
		
		 
<style>
 toastr.options = {
  toastClass: 'alert',
  iconClasses: {
      error: 'alert-error',
      wait: 'alert-info',
      success: 'alert-success',
      warning: 'alert-warning'
  }
} 
.steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#fff;
	}
	   .steps h3{color:#B8152F}
   #works h4{color:#B8152F}
   .footer ul li a{color:#fff;}
   .footer h4{color:#fff}
   .footer ul li{padding:5px 0px}
   .nav { 
   display: inline-block; 
   } 
   .navbar{margin:0px}
   .navbar-default .navbar-nav>li>a {
   color:#fff!important;
   }
   .navbar{
   height:70px;
   }
   .navbar .navbar-right{
   padding-top:12px;
   }
   navbar .navbar-brand{
   padding-top:5px;
   }
   .copyright{
   font-size:16px;
   margin:20px 0;
   }
   .pt-2 {
   padding-top:4rem!important; 
   }
   .pb-2, .py-2 {
   padding-bottom: .5rem!important;
   }
   ul {
   padding: 0;
   list-style-type: none;
   }
   .footer ul li {
   padding: 5px 0px;
   }
   .footer ul li a {
   color: #fff;
   font-size: 16px;
   }
   .footer ul li a:hover{
   text-decoration:none;
   }
   .footer-logo img{
   float:left
   }
   .mt-1 {
   margin-top: 1rem!important;
   margin-bottom: 1rem!important;
   }
  .dropdown-menu, .input-group-addon, .pagination .page-item .page-link, .popover {
    border: 0;
}
.dropdown-menu{
    box-shadow: 0 2px 0px 0 rgba(0, 0, 0, .16), 0 0px 10px 0 rgba(0, 0, 0, .12);
}
.dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    color: #292b2c;
    text-align: left;
    background-color: #fff;
    background-clip: padding-box;
}

.dropdown-menu>li>a {
    display: block;
    padding: 10px 20px;
    clear: both;
    font-weight: 400;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
</style>
		<meta name="csrf-token" content="{{ csrf_token() }}">
			
			
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular.js"></script>  
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
       <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
       <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui.min.js"></script>
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.css">
   
		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="https://npmcdn.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-aria.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-messages.min.js"></script>
		  
        <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.8/angular-material.min.js"></script>
  
  
		<script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		 
		
		
		<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
   <script src="{{ URL::asset('admin/map/locationpicker.jquery.min.js')}}"></script>
   <script src="{{ URL::asset('admin/map/angularLocationpicker.jquery.js')}}"></script>
   
   
   
   
    
   
	       
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" /> 
            <link href="{{ URL::asset('admin/css/angular-datepicker.css')}}" rel="stylesheet" type="text/css" />   
			<!-----script files here------------> 
			  
	  
	  
	    
		
    
		<script src="{{ URL::asset('admin/js/angular-datepicker.js')}}"></script>  
		
		<script> var APP_URL = '<?php echo env("APP_URL", "");?>';  </script>
        <script>
           window.Laravel = <?php echo json_encode([
               'csrfToken' => csrf_token(),
           ]); ?>
        </script>
   </head>
   <body>
 
  
       @yield('content')
   </body>
   
   
   
    
    
	     
	 
 
 
	 
	  
</html>