@extends('layout.auth')
@section('title', 'Upload Logo' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
	  
	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
 
		
    <input type="file" id="file" style="display: none"/>
 
     
 
  <div ng-app="mainApp" ng-controller="logoController" ng-cloak>
 

     <!---------CONTENT SECTION STARTS HERE------>
		   
   <!--Section: Contact v.1-->
  
   <div class="container-fluid" >
    
  
     <div class="row mt-5 mb-4">
	  <div class="col-sm-12">
	    
		 <div class="row">
                                 <div class="col-sm-12" >
                                    <h2 class="header">Upload Logo</h2>
								 
									<textarea id="res" style="display:none;"></textarea>
                                 </div>
                              </div>
	    
			   
            
			 
            <div class="row" >
               <div class="col-sm-12" >
          
			   

				  <input type="hidden" name="_token" value="{{ csrf_token() }}">
			 
                     <div class="panel"  >
                        
                        <div class="row">
                           <div class="col-sm-12">
						    
                <div class="form-group"  >
                                 <label for="logo">Logo</label>
							 
									 
						<?php $app_logo = @\App\Settings::where('key_title','app_logo')->first(['key_value'])->key_value;  ?>			 
									 
									 
								     <input type="hidden"  value=" " id="photo" />
						 <div style="width:150px;height: 180px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
        <img width="100%" height="100%" id="preview_image" src="<?php echo env('APP_URL')."/logo/".@$app_logo;?>" />
        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
    </div>
	
			      <a href="javascript:changeProfile()" style="text-decoration: none;">
            <i class="fa fa-edit"></i> Change
        </a>&nbsp;&nbsp;
        <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
            <i class="fa fa-trash-o"></i>
            Remove
        </a>
                                
                              </div>
							  
							  </div>
							  
							   
                           </div>
						   
             <button   ng-click="upload_logo()" class="btn btn-success"  > Save </button> 
                        </div>
                   
					 
					 
				     </div>
               </div>
            </div>
         
				  
				  
				 
                <!--</form>-->
              </div>
            
			
			 
			</div>
			
			
			 </div>
 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">

	
	</div>
 
 
 
 
 <!--------message box ends-------------->
 
          </div>
	    </div>
			
      </div>
     </div>
       
   </div> 
  <!--/Section: Contact v.1-->
	</div>	
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		 
         </section>
      </div>
   </div>
  </div>
		
       
		
       
     
<!-- SCRIPTS -->
	<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/logo_upload.js')}}"></script>  
<!--
	<script src="{{URL::asset('web/js/bootstrap-fileupload.js')}}"></script> -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
    function changeProfile() { 
        $('#file').click();
		 
    }
    $('#file').change(function () {
		 
        if ($(this).val() != '') {
			 
            upload(this);

        }
    });
	
//	alert('<?php echo url('/')."/users/"; ?>'+photo);
//	$('#preview_image').attr('src', '{{asset('users')}}/'+$('#photo').val());
	
	
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-image-upload-logo')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
                      
	$('#photo').val(data).trigger("change");
 
	//$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
	 
	
					
					//document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '{{asset('logo')}}/' + data);  
                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('logo/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#photo').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-logo/" + $('#photo').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#photo').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        //alert(xhr.responseText);
                    }
                });
            }
    }
	

</script>
 
 
 
			
	
	@endsection
