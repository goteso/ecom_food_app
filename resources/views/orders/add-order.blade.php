@extends('layout.auth')
@section('title', 'Add Order' )
<link rel="stylesheet" href="{{ URL::asset('admin/css/main.css')}}">
<style>
   .autocomplete-custom-template li {
   border-bottom: 1px solid #ccc;
   height: auto;
   padding-top: 8px;
   padding-bottom: 8px;
   white-space: normal;
   }
   .autocomplete-custom-template li:last-child {
   border-bottom-width: 0;
   }
   .autocomplete-custom-template .item-title,
   .autocomplete-custom-template .item-metadata {
   display: block;
   line-height: 1.5;
   }
   .green{background:#309c20}
   
</style>

@section('content')
@section('header')
@include('includes.header')
@show
<div  ng-app="mainApp" style="margin-top:61px;">
<div  ng-controller="orderController as main" style="height:10px;">

   <md-progress-linear class="md-warn" md-mode="determinate" value="@{{main.determinateValue}}"  
      ng-show="isLoading" ng-disabled="isDisabled"></md-progress-linear>
</div>
<div  id="wrapper">
<div id="layout-static">
   <!---------- Static Sidebar Starts------->			
   @section('sidebar')
   @include('includes.sidebar')
   @show
   <!---------- Static Sidebar Ends------->
   <div class="static-content-wrapper">
      <section id="main-header">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  @include('flash::message')
                  <div class="text-right">
                  </div>
                  <div class="tab-content" >
                     <textarea id="res"  style="display:none   ;"></textarea>
 
                     <textarea id="res8"  style="display:none ;"></textarea>
 
                     <textarea id="res8"  style="display:none ;"></textarea>
 
                     <!--------------------------Angular App Starts ------------------------------------>
                     <div   >
					 
				 
					 
							 	 
                        <div class="container-fluid ">
                           <div class="row order-content" ng-controller="orderController as main" ng-cloak>
						  
                              <div class="col-sm-3 col-lg-2" >
                                 <div class="form-group"  >
                                    <div class="input-group">
                                       <input type="text" class="form-control"   ng-model="search" placeholder="Search by product" >
                                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                 </div>
                                 <nav role="navigation" class="sidebar-body" >
                                    <h4 class="sidebar-title">Categories</h4>
                                    <ul class=" ">
                                       <li ng-repeat="categories in main.addOrder" class="active " >
                                          <a  class="main" href="#@{{categories.category_title}}"> <span>@{{categories.category_title}}</span> </a>
                                          <ul class="acc-menu">
                                             <li ng-repeat="subCategories in categories.sub_categories ">
                                                <a  class="sub" href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a>
                                                <ul class="">
                                                   <li ng-repeat="subCategories in subCategories.sub_categories " >
                                                      <a class="subsub" href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a>
                                                      <ul class="acc-menu">
                                                         <li ng-repeat="subCategories in subCategories.sub"><a href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a></li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </nav>
                              </div>
							  

                              <div class="col-sm-5  col-lg-7 text-right" >
                                 <div class="tab-content">
                                    <div class="panel-group product-view" id="accordion">
                                       <div ng-repeat="categories in main.addOrder | filter:search" id ="@{{categories.category_title}}" class="tab-pane fade in active">
                                          <div ng-show="categories.products != '' ">
                                             <div class="panel ">
                                                <div class="panel-heading">
                                                   <h4 class="panel-title text-left">
                                                      @{{categories.category_title}} <span>(@{{categories.products.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#1@{{categories.category_title}}">  <i class="fa fa-angle-down"></i></a>
                                                   </h4>
                                                </div>
                                                <div id="1@{{categories.category_title}}" class="panel-collapse collapse in">
                                                   <table class="table items-detail">
                                                      <tbody>
                                                         <tr ng-repeat="product in categories.products | filter:search"  >
                                                            <td class="item-image ">
															      <img src="admin/images/item-placeholder.png" ng-show="product.photo == null">
															      <img src="{{URL::asset('/products')}}/@{{product.photo}}" ng-show="product.photo != null">
															   </td>
                                                            <td class="item-title"> @{{product.title}} </td>
                                                            <td class="item-price"><span ng-show='product.base_price > 0'><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>															</td>
                                                            <td class="item-createdTime  "><button class="btn-addCart" ng-click="main.addToCart(product)"><i class="fa fa-plus"></i></button></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                          <div ng-repeat="subCategories in categories.sub_categories" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                             <div ng-show="subCategories.products != ''" >
                                                <div class="panel ">
                                                   <div class="panel-heading">
                                                      <h4 class="panel-title text-left">
                                                         @{{subCategories.category_title}} <span>(@{{subCategories.products.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#2@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a>
                                                      </h4>
                                                   </div>
                                                   <div id="2@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                      <table  class="table items-detail">
                                                         <tbody>
                                                            <tr ng-repeat="product in subCategories.products | filter:search ">
                                                               <td class="item-image ">
															      <img src="admin/images/item-placeholder.png" ng-show="product.photo == null">
															      <img src="{{URL::asset('/products')}}/@{{product.photo}}" ng-show="product.photo != null">
															   </td>
                                                               <td class="item-title"> @{{product.title}} </td>
                                                               <td class="item-price"><span ng-show='product.base_price > 0'><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>  </td>
                                                               <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(product)" ng-disable="item.showAddToCart && !item.addedToCart"><i class="fa fa-plus"></i></button></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div ng-repeat="subCategories in subCategories.sub_categories" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                                <div ng-show="subCategories.products != ''">
                                                   <div class="panel ">
                                                      <div class="panel-heading">
                                                         <h4 class="panel-title text-left">
                                                            @{{subCategories.category_title}} <span>(@{{subCategories.products.length}} items)</span><a data-toggle="collapse" data-parent="#accordion" href="#3@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a>
                                                         </h4>
                                                      </div>
                                                      <div id="3@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                         <table  class="table items-detail">
                                                            <tbody>
                                                               <tr ng-repeat="product in subCategories.products | filter:search">
                                                                  <td class="item-image ">
															        <img src="admin/images/item-placeholder.png" ng-show="product.photo == null">
															        <img src="{{URL::asset('/products')}}/@{{product.photo}}" ng-show="product.photo != null">
															      </td>
                                                                  <td class="item-title"> @{{product.title}} </td>
                                                                  <td class="item-price"><span ng-show='product.base_price > 0'><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>  </td>
                                                                  <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(product)"><i class="fa fa-plus"></i></button></td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div ng-repeat="subCategories in subCategories.sub" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                                   <div ng-show="subCategories.products != ''">
                                                      <div class="panel ">
                                                         <div class="panel-heading">
                                                            <h4 class="panel-title text-left">
                                                               @{{subCategories.category_title}} <span>(@{{subCategories.products.length}} items)</span><a data-toggle="collapse"  data-parent="#accordion" href="#4@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a></span> 
                                                            </h4>
                                                         </div>
                                                         <div id="4@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                            <table  class="table items-detail">
                                                               <tbody>
                                                                  <tr ng-repeat="product in subCategories.products | filter:search ">
                                                                     <td class="item-image ">
															          <img src="admin/images/item-placeholder.png" ng-show="product.photo == null">
															          <img src="{{URL::asset('/products')}}/@{{product.photo}}" ng-show="product.photo != null">
															         </td>
                                                                     <td class="item-title"> @{{product.title}}  @{{products}}</td>
                                                                     <td class="item-price"><span ng-show='product.base_price > 0'><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>  </td>
                                                                     <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(product)"><i class="fa fa-plus"></i></button></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-lg-3" >
                                 <div class="panel panel-right">
                                    <div class="panel-header">
                                       <h4>Order Info</h4>
                                    </div>
                                 </div>
                                 <div ng-controller="cartController as cart" >
                                    <div class="panel" ng-show="cart.cartStorage.products">
                                       <div class="panel-body">
                                          <h5>Cart Items</h5>
                                        
										
										<!--<table class="cart_table table " style="padding-bottom:10px;border-bottom:1px solid #ccc;">
									 
										  <tr ng-repeat="product in cart.cartStorage.products track by $index" >
										     <td>   @{{product.title}}
												  <span ng-show="product.variants"  ><a href="#" data-toggle="tooltip" title="@{{product.variants[0].title}} @{{product.variants[1].title}} @{{product.variants[2].title}}">&#9432;</a></span> </td>
											<td>  <button ng-click="cart.increaseItemAmount(product)">
                                                   +
                                                   </button>
                                                   @{{product.quantity}} 
                                                   <button ng-click="cart.decreaseItemAmount(product)">
                                                   -
                                                   </button>
										   </td>
											<td><p><?php //echo env("CURRENCY_SYMBOL", "");?>@{{product.base_price}}</p></td>
											<td><button ng-click="cart.removeFromCart(product)"  >   <i class="fa fa-minus"></i> </button></td>
										  </tr>
										</table>-->
										
										
										    <div class="row cart_table" ng-repeat="product in cart.cartStorage.products track by $index" style="padding-bottom:10px;border-bottom:1px solid #ccc;" >
										   <div class="col-sm-12">
                                                <h4>@{{product.title}}
												  <span ng-show="product.variants != ''"  ><a href="#" data-toggle="tooltip" title="@{{variantsSelectedTitleData}} ">&#9432;</a></span> 
												  <!--<span ng-show="product.variants" ng-repeat="variant in product.variants">@{{variant.title}} </span>-->
												</h4>
												</div>
												 <div class="col-sm-6 text-center"><p>
                                                   <button ng-click="cart.increaseItemAmount(product)">
                                                   +
                                                   </button>
                                                   @{{product.quantity}} 
                                                   <button ng-click="cart.decreaseItemAmount(product)">
                                                   -
                                                   </button>
												   <p>
                                                </div>
                                                <div class="col-sm-2"><p><?php echo env("CURRENCY_SYMBOL", "");?>@{{product.discounted_price}}</p></div>
                                               
                                                <div class="col-sm-2">
                                                   <button ng-click="cart.removeFromCart(product)"  >
                                                   <i class="fa fa-minus"></i>
                                                   </button>
                                                </div>
                                             </div>
											 
											 
                                          <!--<span ng-bind="total"></span>-->
                                       </div>
                                    </div>
									
									  
								 <div class="panel cart" ng-show="cart.paymentData[0].type == 'payment_details'"> 
                                    <div class="panel-body">
                                        <div class="row  "    >
										    <div class="col-sm-12">
										       <h5>@{{cart.paymentData[0].data.title}}</h5> 
											</div>
											
											<table class="table paymentSummary" > 
		                                        <tbody >
		                                            <tr  ng-repeat="values in cart.paymentData[0].data.data ">
		                                               <td class="" style="font-size:14px;text-transform: capitalize">@{{values.title.replace('_', ' ')  }} </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo env('CURRENCY_SYMBOL');?>@{{values.value}} </td>
		                                            </tr>
 
													<tr>
		                                               <td class="" style="font-size:14px;text-transform: capitalize">Total </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo env('CURRENCY_SYMBOL');?>@{{cart.paymentData[0].order_total}} </td>
		                                            </tr>
 
 
                                                </tbody > 
								            </table>     
								        </div> 
                                       </div>
                                    </div>
									</div>
									
                               
								 
								  
								 <div class="panel main-cart" ng-show="main.paymentData[0].type == 'payment_details'">
								 
                                    <div class="panel-body">
                                        <div class="row  "    >
										    <div class="col-sm-12">
										       <h5>@{{main.paymentData[0].data.title}}</h5> 
											</div>
											
											<table class="table paymentSummary" > 
		                                        <tbody >
		                                            <tr  ng-repeat="values in main.paymentData[0].data.data ">
		                                               <td class="" style="font-size:14px;text-transform: capitalize">@{{values.title.replace('_', ' ')  }} </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo env('CURRENCY_SYMBOL');?>@{{values.value}} </td>
		                                            </tr>
 
													<tr>
		                                               <td class="" style="font-size:14px;text-transform: capitalize">Total </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo env('CURRENCY_SYMBOL');?>@{{main.paymentData[0].order_total}} </td>
		                                            </tr>
 
                                                </tbody > 
								            </table>     
								        </div> 
                                       </div>
                                    </div>
									 
									
									
                                 <div class="panel" ng-repeat="fields in main.addOrderMeta.fields"  >
                                    <div class="panel-body">
                                       <div ng-repeat="field in fields">
                                          <div class="" ng-show="field.type == 'select'">
										     <div ng-show="field.identifier == 'customer_id'" >
											 
											
										  
                                             <h5>@{{field.title}}</h5>
                                             <md-autocomplete ng-disabled="main.isDisabled" md-no-cache="main.noCache" md-selected-item="main.selectedItem" md-search-text-change="main.searchTextChange(main.searchText)" md-search-text="main.searchText" md-selected-item-change="main.selectedItemChange(item, field)" md-items="item in main.querySearch(main.searchText)" md-item-text="item.first_name" md-min-length="0" placeholder="Select a User" md-menu-class="autocomplete-custom-template"  id="customer_id" ng-required="data.required_or_not">
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> @{{item.first_name}} </span>
                                                   <span> @{{item.last_name}} </span>
                                                   </span>
                                                   <span class="item-metadata">
                                                   <span> @{{item.mobile}} </span>  
                                                   </span>
                                                </md-item-template>
                                             </md-autocomplete>
                                             <input type="text" ng-model="field.value" value="@{{customer_id_value}} == field.value " id="customer_id2" style="display:none;">
                                             <!-- <md-autocomplete flex
                                                md-selected-item="main.selectedItem()"
                                                md-search-text="main.searchText"
                                                md-items="item in main.querySearch(main.searchText)"
                                                md-item-text="item.first_name"
                                                md-delay="300"
                                                md-floating-label="Select Users">
                                                <div layout="row" class="item" layout-align="start center">
                                                 
                                                <span md-highlight-text="main.searchText"> @{{item.first_name}} </br>@{{item.last_name}} </span>  
                                                </div>
                                                
                                                </md-autocomplete>-->
                                             <!--  <select ng-model="field.value" class="form-control" id="customer_id">
                                                <option ng-selected="@{{options.value == field.value}}" ng-repeat="options in field.field_options" value="@{{options.value}}">@{{options.title}}@{{options.value}}</option>
                                                </select> 
                                                
                                                <input type="text" ng-model="field.value" id="customer_id2" style="display:none;">-->
                                          </div>
										  
										     <div ng-show="field.identifier == 'service_area_id'">
											 
											
                                             <h5>@{{field.title}}</h5>
                                             <md-autocomplete ng-disabled="main.isDisabled" md-no-cache="main.noCache" md-selected-item="main.selectedItem1" md-search-text-change="main.searchTextChange(main.searchText1)" md-search-text="main.searchText1" md-selected-item-change="main.selectedAreaChange(item, field)" md-items="item in main.areaSearch(main.searchText1)" md-item-text="item.area_title" md-min-length="0" placeholder="Select a Area" md-menu-class="autocomplete-custom-template"  id="service_area_id " ng-required="data.required_or_not">
                                                <md-item-template>
                                                        <span class="item-title"> 
                                                   <span> @{{item.area_title}} </span>
                                         
                                                   </span>
                                                 
                                                 
                                                </md-item-template>
                                             </md-autocomplete>
                                             <input type="text" ng-model="field.value" value="@{{service_area_id_value }} == field.value " id="service_area_id2" style="display:none;">
                                             <!-- <md-autocomplete flex
                                                md-selected-item="main.selectedItem()"
                                                md-search-text="main.searchText"
                                                md-items="item in main.querySearch(main.searchText)"
                                                md-item-text="item.first_name"
                                                md-delay="300"
                                                md-floating-label="Select Users">
                                                <div layout="row" class="item" layout-align="start center">
                                                 
                                                <span md-highlight-text="main.searchText"> @{{item.first_name}} </br>@{{item.last_name}} </span>  
                                                </div>
                                                
                                                </md-autocomplete>-->
                                             <!--  <select ng-model="field.value" class="form-control" id="customer_id">
                                                <option ng-selected="@{{options.value == field.value}}" ng-repeat="options in field.field_options" value="@{{options.value}}">@{{options.title}}@{{options.value}}</option>
                                                </select> 
                                                
                                                <input type="text" ng-model="field.value" id="customer_id2" style="display:none;">-->
                                          </div>
										  
										  
										  
										  
										  
										  </div>
                                          <div class="" ng-show="field.type == 'api'"  >
                                             <h5>@{{field.title}} 
                                                <button class="btn" ng-click="get_address(field.parent_identifier);"   style="background:transparent;"><i class="fa fa-edit"></i></button>
                                             </h5>
                                             <p>@{{selected_values.address_line1}}</p>
                                             <p>@{{selected_values.address_line2}}</p>
                                             <p>@{{selected_values.city}}</p>
                                             <p>@{{selected_values.state}}</p>
                                             <p>@{{selected_values.country}}</p>
                                             <input type="text" ng-model="field.value" value="@{{selected_values}} == field.value"  style="display: none;" id="user_address">
                                          </div>
                                          <div class="" ng-show="field.type == 'timeSlotsPicker'"  >
                                             <div class="" ng-show="field.identifier == 'pickup_time'">
                                                <h5>@{{field.title}}  <button class="btn"   ng-click="get_pickupTime()" style="background:transparent;"><i class="fa fa-edit"></i></button></h5>
                                                @{{pickupTimeShow}}
                                             </div>
                                             <div class="" ng-show="field.identifier == 'delivery_time'">
                                                <h5>@{{field.title}}  <button class="btn" ng-click="get_deliveryTime(field.parent_identifier,field.identifier);"  style="background:transparent;"><i class="fa fa-edit"></i></button>
                                                </h5>
                                                @{{deliveryTimeShow}}
                                             </div>
                                          </div>
                                          <div ng-show="field.type == 'textarea'">
                                             <h5>@{{field.title}}</h5>
                                             <div class="form-group" > 
                                                <textarea class="form-control" ng-model="field.value" ng-required="field.required_or_not == '1'" ></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--   <md-button class="md-raised md-primary" flex="50" ng-click="addOrder()">CREATE ORDER</md-button>-->
                                 <button class="btn btn-success btn-block" ng-click="addOrder();">CREATE ORDER</button>
                              </div>
                              
							  
		 				  
                              <!-------------------------------Modal For Product Variants Starts Here---------------------------------->
                              <div id="variantsModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:9999999;">
                                 <div class="modal-dialog modal-lg" style="z-index:9999999;width:80%;" data-backdrop="false">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Product Variants</h4>
                                       </div>
                                       <div class="modal-body">
                                          <div ng-repeat="data in main.variantsdata">
                                             @{{data.title}}  
                                             <div class="form-group"  ng-show="data.maximum_selection_needed > '1'"  >
                                                <label class="checkbox-inline" ng-repeat="variant in data.variants" >
                                                <input type="checkbox" id="@{{variant.id}}" ng-model="variant.value" value="@{{variant.title}}" ng-click="getVariantValue(variant);" >
                                                @{{variant.title}}<span ng-show="@{{variant.price_difference}}"> (+@{{variant.price_difference}})</span> <span ng-show="@{{variant.price}}">(@{{variant.price}})</span>
                                                </label>
                                             </div>
											 
											 
											 <div class="form-group">
                                                <label class="radio-inline" ng-repeat="variant in data.variants" ng-show="data.maximum_selection_needed =='0' || data.maximum_selection_needed =='1'">  
                                                <input type="radio" name="@{{data.title}}" id="@{{variant.id}}" ng-model="selectedVariant" value="@{{variant.title}}" ng-click="getVariantValue2(variant);" ng-required="data.minimum_selection_needed >= '1'">
                                                @{{variant.title}}<span ng-show="@{{variant.price_difference}}"> (+@{{variant.price_difference}})</span> <span ng-show="@{{variant.price}}">(@{{variant.price}})</span>
                                                </label>
                                             </div>
											 
                                          </div>
                                          <button class="btn btn-info" ng-click="main.addVariant();">Add Variant</button>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-------------------------------Modal For Product Variants Ends Here---------------------------------->
							  
							  
                              <!-- Modal -->
                              <div id="myModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:9999999;">
                                 <div class="modal-dialog modal-lg" style="z-index:9999999; " data-backdrop="false">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Select Address</h4>
                                       </div>
                                       <div class="modal-body">
                                          <!---<table class="table" id="exportthis "  >
                                             <thead>
                                                <tr>
                                                   <th>  </th>
                                                   <th ng-repeat="c in main.columns_addresses | orderBy:'index'"  ng-show="c.visible" ng-if="c.visible == 1" >@{{c.title}}</th>
                                                   <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody  >
                                                <tr  ng-repeat="values in main.data_addresses.data ">
                                                   <td >
                                                      <input type="checkbox" ng-checked=" " ng-model="selected_values" ng-click="checkedIndex(values)" data-dismiss="modal"> 
                                                      <h1></h1>
                                                   </td>
                                                   <td ng-repeat="c in main.columns_addresses" class="" ng-show="c.visible == 1" >
                                                      <span class="" ng-show="$index == '0'">@{{values.id}} </span>
                                                      <span class="" ng-show="$index == '1'">@{{values.address_line1}} </span>
                                                      <span class="name" ng-show="$index == '2'">@{{values.address_line2}} </span>
                                                      <span class="price" ng-show="$index == '3'">@{{values.city}} </span>
                                                      <span class="created" ng-show="$index == '4'">@{{values.state}} </span>
                                                      <span class="created" ng-show="$index == '5'">@{{values.country}} </span>
                                                      <span class="created" ng-show="$index == '6'">@{{values.pincode}} </span>
                                                      <span class="created" ng-show="$index == '7'">
                                                      @{{values.created_at}} 
                                                      </span>
                                                   </td>
                                                   <td class="actions">
                                                      <a class="btn btn-xs edit-product" ng-model="field.value" data-dismiss="modal"  ng-click="checkedIndex(values)"  >Select</a> 
                                                   </td>
                                                </tr>
                                                <tr ng-repeat="fields in main.addOrderMeta.fields " >
                                                   <td ng-repeat="field in fields" ng-show="field.identifier == 'delivery_address'" ng-if="isSelected(field)">@{{field.id}}</td>
                                                </tr>
                                             </tbody>
                                          </table>--->
										  
										  <div class="row addresses-row" ng-repeat="values in main.data_addresses.data ">
										  
										  
										   <p class="col-sm-8 col-lg-10">
										    
                                                      <span class=""  > Addreess Line1 : @{{values.address_line1}} </span><br>
                                                      <span class="name" >Addreess Line2 : @{{values.address_line2}} </span><br>
                                                      <span class="price" >City : @{{values.city}} </span><br>
                                                      <span class="created" >State : @{{values.state}} </span><br>
                                                      <span class="created"  >Country : @{{values.country}} </span><br>
                                                      <span class="created" >Pincode : @{{values.pincode}} </span><br>
                                                      
													  
										 
										  </p>
										  <p class="col-sm-4 col-lg-2">
										   <a class="btn btn-select-address edit-product" ng-model="field.value" data-dismiss="modal"  ng-click="checkedIndex(values)"  >Select</a> 
										   
										   </p>
										   
										   <div ng-repeat="fields in main.addOrderMeta.fields " >
                                                   <p ng-repeat="field in fields" ng-show="field.identifier == 'delivery_address'" ng-if="isSelected(field)">@{{field.id}}</p>
                                                </div>
												
										  </div>
										  
                                       </div>
                                    </div>
                                 </div>
                              </div>
							  
							  
							  
                              <!-- Modal -->
                              <div id="pickupModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
                                 <div class="modal-dialog  " style="z-index:99999;width:80%">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Select Pickup Time</h4>
                                       </div>
                                       <div class="modal-body"  >
                                         <ul class="nav nav-tabs picker-tabs">
                                            <li ng-class="{active: $index == selected}" ng-repeat="data in main.pickupDateData">
											   <a data-toggle="tab" href="#tab@{{$index}}" ng-click="getPickupD(data.display_date,data.date);  ">@{{data.display_date}}</a>
											</li>
	                                     </ul>
                                      <br> <div class="tab-content  ">
                                           <div ng-repeat="data in main.pickupDateData" id="tab@{{$index}}" class="tab-pane fade in" ng-class="{active: $index == selected}"> 
										   <div ng-repeat="  fields in main.addOrderMeta.fields">
                                             <div  ng-repeat=" field in fields" ng-show="field.identifier == 'pickup_time'">
											    <button class="btn btn-success btn-timeslots" ng-repeat="time in data.slots" ng-click="savePickup(field.identifier,time.id,field,time.from_time,time.to_time)" value="@{{time.id}}">@{{time.from_time}}- @{{time.to_time}}</button>
											    <input type="hidden" ng-model="field.value" value="@{{pickupTime}}" id="@{{field.identifier}}" > 
											</div>
                                          </div>
                                          </div>
                                           </div>
				                         </div>

                                             </div>
                                          </div>
										   
                                       </div>
                                     
                                 
                                 <!-- Modal -->
                                 <div id="deliveryModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
                                    <div class="modal-dialog  " style="z-index:99999;width:80%">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                             <h4 class="modal-title">Select Delivery Time</h4>
                                          </div>
                                          <div class="modal-body" >
										 
                                     
                                                   <ul class="nav nav-tabs picker-tabs">
                                                      <li ng-class="{active: $index == selected}" ng-repeat="data in main.deliveryDateData"><a data-toggle="tab" href="#tab2@{{$index}}" ng-click="getDeliveryD(data.display_date,data.date);  "  >@{{data.display_date}}</a></li>
                                                   </ul>
												   <br>
                                                   <div class="tab-content">  
                                                      <div ng-repeat="data in main.deliveryDateData" id="tab2@{{$index}}" class="tab-pane fade in" ng-class="{active: $index == selected}"> 
													   <div ng-repeat="fields in main.addOrderMeta.fields">
                                                      <div ng-repeat="field in fields" ng-show="field.identifier == 'delivery_time'">
                                                         <button class="btn btn-success btn-timeslots" ng-repeat="time in data.slots" ng-click="saveDelivery(field.identifier,time.id,field,time.from_time,time.to_time)" value="@{{time.id}}">@{{time.from_time}}- @{{time.to_time}}</button>
														 
                                                <input type="hidden" ng-model="field.value " value="@{{deliveryTime}}" id="@{{field.identifier}}" > 
														 </div>
                                            </div>
                                                      </div>
                                                   </div>
                                          
                                              
										   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              
                              <!--------------------------Angular App Ends ------------------------------------>
                           </div>
						   
						   
						   	  <script> 
$(function() {
	 //alert('1234'); 
	 $('a[href*=#]').click(function(e) {
					   
		           e.preventDefault(); 
		           $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top -100 }, 600, 'linear');
				   
		         });
		         });
	</script>			 
				
                        </div>
                     </div>
                  </div>
				   </div>
								 </div>
      </section>
      </div>
      </div>
      </div>
   </div>
</div>


<!------>

<!-- <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>-->
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/addOrder.js')}}"></script> 



 <script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

@endsection