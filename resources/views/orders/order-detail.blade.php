@extends('layout.auth')
@section('title', 'Order Details' )
<link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
 <style>
   @media(min-width:766px){
	.card{height:330px;  overflow-y: auto;}
	.panel{height:auto;  overflow-y: auto;}  
   
} 
.autocomplete-custom-template li {
  border-bottom: 1px solid #ccc;
  height: auto;
  padding-top: 8px;
  padding-bottom: 8px;
  white-space: normal;
}
.autocomplete-custom-template li:last-child {
  border-bottom-width: 0;
}
.autocomplete-custom-template .item-title,
.autocomplete-custom-template .item-metadata {
  display: block;
  line-height: 1.5;
}
</style>
@section('content')
@section('header')
@include('includes.header')
@show
 
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	  
<script type="text/javascript">
 var order_id = '<?php echo @$_GET['order_id'];?>';
 var user_id = '<?php echo @Auth::id();?>';
</script> 

 
 <textarea id="res" style=" display:none " ></textarea> 
	  <textarea id="res1" style=" display:none " ></textarea>

<div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">				 
					 <!--<i class="fa fa fa-circle-o-notch fa-spin  " style=" color:#309c20;font-size:40px;"></i>-->
					 <p >Calling all data...</p>
					 </div>
 			
       <div ng-app="mainApp" >
	  <div ng-controller="orderDetailController" ng-cloak>
	  <div class="container-fluid" >
  <div class="row">
    <div class="col-sm-12" >
	   <h2 class="header">#@{{ordersTitle.order_number}} Order Details <span><i> (@{{ordersTitle.order_date}})</i></span></h2>
	</div>  
  </div>
 
 
  <div class="row" >
      <div class="col-sm-7 col-md-8 col-lg-9 " >
	   <div class="row">
	      <div class="col-sm-12 "  ng-repeat="order in orders" ng-show="order.type=='items'">
	        
			
		
			<div class="card" ng-show="order.type=='items'" >
			 <h3 class="block-title" style="float:left;">@{{order.data.title}}</h3>
			 
			 	 <a class="btn btn-success" ng-click="ProductDataSearch()" data-toggle="modal" data-target="#addProductModal" style="float:right;">Add Product</a>
			  <table class="table items-detail"> 
			  
			  
		 <tbody>
		 <tr ng-repeat="items in order.data.data ">
		 <?php  $st = \App\FeaturesSettings::where("title",'photo')->first(['status'])->status; 
           $app_url = env('APP_URL'); 		 ?>
		<?php if($st =='1' or $st ==1) { ?> <td class="item-image">
		 	 
		<img src="admin/images/item-placeholder.png"><?php } ?></td>
		 
		 
		  <td class="item-title">@{{items.title}} <pre>@{{items.variants_desc}}</pre><br><span class="item-desc">@{{items.unit}}</span></td>
		  <td class="item-units">@{{items.quantity}} * @{{items.discounted_price}}</td>
		  <td class="item-total text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{items.discounted_price * items.quantity}}</td>
		  <td></td>
		  <td></td>
		   <td>
		     <a href="{{ URL::to('product_invoice')}}/@{{items.product_id}}/<?php echo @$_GET["order_id"];?>" ng-click="printProductInvoice(items.id)" target="_blank"><i class="fa fa-print"></i></a>
		     <a href="" ng-click="deleteProduct(items.id)" style="padding:3px 8px;"><i class="fa fa-trash-o"></i></a>
		   </td>
		  
		 </tr>
		  <tr class="total"> 
		  <td class="" width="40%">Sub Total</td>
		  <td></td>
		  <td></td>
		  <td class="text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{order.data.total}} </td>
		 </tr>
		 </tbody>
		 </table>
	       </div>
		 </div>
		 
		 
		  <div class="col-sm-12 col-md-6 col-lg-4"  ng-repeat="order in orders"  ng-show="order.type=='user'">
		 
		 
	  <!-----Block Starts For Customer Information----->
	   <div class="card"  > 
	     <h3 class="block-title">@{{order.data[0].title}}</h3>
	     <div >
		    <p class="name"><a href="#"></a>
			
			
          <a class=" "   ng-attr-href="{{(values.user_type != 'driver') ? '<?php echo @$app_url;?>/user_profile?user_id='+order.data[0].id : '#'}}"   >@{{order.data[0].name}}</a>
			</p>
			 <p><pre>@{{order.data[0].description}} </pre></p> 
	     </div>
	  </div>
	  <!-----Block Ends For Customer Information----->
	  
	  
	  </div>
	  
	  <!-----Block Starts For Delivery Address----->
	  <div class="col-sm-12 col-md-6 col-lg-4"  ng-repeat="order in orders"  ng-show="order.type=='delivery_address'">
		 
	   <div class="card" >
	    <h3 class="block-title">@{{order.data.title}}</h3>
		 <div >
		 
		   
		   		  <div ng-repeat="elem in order.data.data">
		 
	 
 
		  <p ng-repeat="(key, value) in elem" cellpadding="0"> 
		 
		  @{{value}} 
		  </p>
	 
	 
	 
		  </div>
		  
		  
		  
		  </div>
         </div>		   
	  </div>
	  <!-----Block Ends For Delivery Address----->
	  
 
	  
	  	   <!-----Block Starts For Payment Summary----->
	   <div class="col-sm-12 col-md-6 col-lg-4"  ng-repeat="order in orders" ng-show="order.type=='payment_details'"> 
	   <div class="card"  >
	    <h3 class="block-title">@{{order.data.title}}</h3>
		 
	 
		<table class="table paymentSummary" > 
		 <tbody >
		 
		 
		 <tr  ng-repeat="items in order.data.data ">
		  <td class="" style="font-size:14px;text-transform: capitalize">@{{items.title.replace('_', ' ')  }} </td>
		  <td class="text-right" style="font-size:14px"><pre>@{{items.value}}</pre></td>
		 </tr>
 
		 
		  <tr  style="font-weight:;border-top:1px solid #ccc">
		  <td class="">TOTAL</td>
		  <td class="text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{order.data.order_total}}</td>
		 </tr>
		 
		   <tr  style="font-weight: ">
		  <td class="">PAID</td>
		  <td class="text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{order.data.total_paid}}</td>
		 </tr>
		 
		 
		  <tr  style="font-weight:bold;border-top:1px solid #ccc">
		  <td class="">TO PAY</td>
		  <td class="text-right"><?php echo env("CURRENCY_SYMBOL", "");?>@{{order.data.order_total - order.data.total_paid}}</td>
		 </tr>
		 
		 
		<!-- <tr class="total">  
		  <td class="">Total</td>
		  <td class="text-right">@{{order.total}}</td>
		 </tr>-->
		 </tbody>
		</table>
	  </div>
	  
    </div>
	  <!-----Block Ends For Payment Summary----->
	
	   <!-----Block Starts For Order Summary----->
	   <div class="col-sm-12 col-md-6 col-lg-4"  ng-repeat="order in orders"  ng-show="order.type=='order_summary'">
	  
	   <div class="card" >
	    <h3 class="block-title">Order Summary</h3>
	 
	    <div ng-repeat="items in order.data.data " class="ordersummary">
	 
		  <h4 class="order-title">@{{items.display_title}}</h4>
		  
		  
		  <p class="order-desc" ng-hide="items.type == 'api'">@{{items.value}}</p> 
		  
		  
		 <div ng-repeat="elem in items.value" style="display:none;">
		   <p class="order-desc" ng-show="items.type == 'api'" >
		 
		  <table class="table" border="0" cellpadding="0">
		  
		    <tr>
		  <th style="50"></th>
		  <th  width="50"></th>
		  </tr>
		  
 
		  <tr ng-repeat="(key, value) in elem" cellpadding="0"> 
		  <td style="50">@{{key}}</td>
		  <td  width="50">@{{value}}</td>
		  </tr>
		  </table>
	 
	 
		  </p> 
		  </div>
		</div>
	  </div>
	  </div>
	   <!-----Block Ends For Order Summary----->
	   
	   
	   
	  

	  
	  
	</div>
    </div>
 
	
	<!-----------------Quick Actions Detail Starts Here---------------------->
 	<div class="col-sm-5 col-md-4 col-lg-3 "   >
	<div class="panel panel-action" ng-hide="!orders[0].data">
	<div class="panel-header ">
	   <h3 class=" ">Quick actions </h3>
	   
	 </div>
	  <div class="panel-body" > 
	  
		  <div ng-repeat="order in orders"  ng-show="order.type=='quickActions_links'" class="link-btn">  
	      <p  ng-repeat="links in order.data.data"class="action-link">
		    <a href="{{ URL::to('order_invoice')}}/a4/<?php echo @$_GET['order_id'];?>" ng-show="links.type == 'printInvoice'" ng-click="printInvoice();" target="_blank">@{{links.buttonTitle}}</a>
			
			 <a href="{{ URL::to('order_invoice')}}/thermal/<?php echo @$_GET['order_id'];?>" ng-show="links.type == 'printInvoiceThermal'" ng-click="printInvoice();" target="_blank">@{{links.buttonTitle}}</a>
			
			
			
			
			
		    <a href="#" ng-show="links.type != 'printInvoice' && links.type != 'printInvoiceThermal'" >@{{links.buttonTitle}}</a>
			
			  
		  </p> 
	  
		</div>
		
 
		 <table class="table actions"  ng-repeat="order in orders" ng-show="order.type=='quickActions_misc'"> 
		 <tbody>
		 <tr ng-repeat="misc in order.data.data">
		 <td class="">  
 
		   <h4 class="action-title">@{{misc.title}}</h4>
		   <p class="action-detail" ng-show="misc.buttonTitle != 'Driver' &&  misc.buttonTitle != 'Vendor'">@{{misc.value}}</p> 
		   <p class="action-detail" ng-show="misc.buttonTitle == 'Driver'">@{{misc.value[0].first_name}} @{{misc.value[0].last_name}}</p> 
		   <p class="action-detail" ng-show="misc.buttonTitle == 'Vendor'">@{{misc.value[0].first_name}} @{{misc.value[0].last_name}}</p> 
		    
		   
		  
		    

<!---- add driver --->
		  <div  ng-show="misc.buttonTitle == 'Driver'">
		    <div class="row" ng-show="IsVisible"  >
			<div class="col-sm-9">
 <md-autocomplete  md-selected-item="selectedItem" md-search-text-change="searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedItemChange(item, field)" md-items="item in querySearch(searchText)" md-item-text="item.first_name" md-min-length="0" placeholder="Select Driver" md-menu-class="autocomplete-custom-template"  md-autofocus=""  id="customer_id" style="min-width:100px;"> 
        <md-item-template>
          <span class="item-title"> 
            <span> @{{item.first_name}} </span>
			 <span > @{{item.last_name}} </span>
          </span>
          <span class="item-metadata">
            <span> @{{item.mobile}} </span>  
          </span>
        </md-item-template>
      </md-autocomplete>
	  </div>
	  <div class="col-sm-3">
 <input type="text" ng-model="customer_id_value"  id="driver_id" style="display:none;">
	   
      <button class="btn btn-info" ng-click="submitDriverId()">Save</button>	
	  </div>
	  </div>
	  </div>
	  
	<!---- add vendor --->  
	
	
	
	
			    

  
	
	
	

		  <div  ng-show="misc.buttonTitle == 'Vendor'">
		    <div class="row" ng-show="IsVisible1"  >
			<div class="col-sm-9">
			
			 
			
 <md-autocomplete  md-selected-item="selectedItem" md-search-text-change="searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedItemChangeVendor(item, field)" md-items="item in querySearch(searchText)" md-item-text="item.first_name" md-min-length="0" placeholder="Select Vendor" md-menu-class="autocomplete-custom-template"  md-autofocus=""  id="vendor_id" style="min-width:100px;"> 
        <md-item-template>
          <span class="item-title"> 
            <span> @{{item.first_name}} </span>
			 <span > @{{item.last_name}} </span>
          </span>
          <span class="item-metadata">
            <span> @{{item.mobile}} </span>  
          </span>
		  
		  
        </md-item-template>
      </md-autocomplete>
	  </div>
	  <div class="col-sm-3">
 <input type="text" ng-model="vendor_id_value"  id="vendor_id2" style="display:none;">
	   
      <button class="btn btn-info" ng-click="submitVendorId()">Save</button>	
	  </div>
	  </div>
	  </div>
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
		  </td>
		  <td class="text-right action-button">
	 
         <a href="#" ng-show="misc.buttonTitle == 'Driver'" ng-click="driverDataChange();">@{{misc.buttonTitle}}</a>
		  <a href="#" ng-show="misc.buttonTitle == 'Vendor'" ng-click="vendorDataChange();">@{{misc.buttonTitle}}</a> 
		
		  <a href="#" data-toggle="modal" data-target="#paymentReceiveModal" ng-show="misc.buttonTitle == 'Received'" ng-click="paymentReceive();"class="">@{{misc.buttonTitle}} </a>
		   <a href="#" ng-show="misc.buttonTitle == 'Update'" data-toggle="modal" data-target="#orderStatusModal" ng-click="orderUpdate();">@{{misc.buttonTitle}}</a>
		   <a href="#" ng-show="misc.buttonTitle == 'Discount'" data-toggle="modal" data-target="#discountModal" ng-click="discountUpdate();">@{{misc.buttonTitle}}</a>
		   <a href="#" ng-show="misc.buttonTitle == 'Shipping'" data-toggle="modal" data-target="#shippingModal" ng-click="shippingUpdate();">@{{misc.buttonTitle}}</a>
		   <a href="#" ng-show="misc.buttonTitle == 'Apply'" data-toggle="modal" data-target="#expressShippingModal" ng-click="expressShippingUpdate();">@{{misc.buttonTitle}}</a>
		   
		   
	  
		   </td>
		 </tr>
		 </tbody>
		</table>
		
		
 
		</div>
	  </div>
	</div>   
	<!-----------------Quick Actions Detail Ends Here---------------------->
	
  </div>
</div>

      
			    
				
				   <!-- Modal -->
<div id="addProductModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:1;margin-top:40px;">
  <div class="modal-dialog  "  >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Product</h4>
      </div>
      <div class="modal-body"  > 
	   
	   
	
	    <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedItem" md-search-text-change="searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedProductChange(item, field)" md-items="item in productSearch(searchText)" md-item-text="item.title" md-min-length="0" placeholder="Select a Product" md-menu-class="autocomplete-custom-template"  id="product_id">
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> @{{item.title}} </span> 
                                                   </span>
                                                   <span class="item-metadata">
                                                   <span> @{{item.base_price}} </span>  
                                                   </span>
                                                </md-item-template>
                                             </md-autocomplete>
	  <br>
         <div class="form-group"  >
               <label class="quantity"  > Quantity</label>
                      <input type="number" id="quantity" value="1" class="form-control" min="0"> 
                                               
          </div>
												
												
 						<input type="text" ng-model="product_value"	style="display:none;">								   
		 
		 
		 <br>
		 <button class="btn btn-info" id="add"  ng-click="addNewProduct()">Add Product</button>
		 <button class="btn btn-info" id="loadVariant" ng-click="addNewProduct()">Load Variants</button>
		 
		 
		
		<div class="variantDiv">
		  <div ng-repeat="data in variantsdata"  >
                                             @{{data.title}}
                                             <div class="form-group"  >
                                                <label class="checkbox-inline" ng-repeat="variant in data.variants" >
                                                <input type="checkbox" id="@{{variant.id}}" ng-model="variant.value" value="@{{variant.title}}" ng-click="getVariantValue(variant);">
                                                @{{variant.title}}<span ng-show="@{{variant.price_difference}}"> (+@{{variant.price_difference}})</span> <span ng-show="@{{variant.price}}">(@{{variant.price}})</span>
                                                </label>
                                             </div>
            </div> 
			  <button class="btn btn-info" ng-click="addVariants()"  id="variantAdd">Add Product</button>						  	
	</div>
	   
    </div>
</div>
  </div>
</div>




	   
	   
	   			   <!-- Modal -->
<div id="driverModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Driver Id</h4>
      </div>
      <div class="modal-body"  > 
	   
	   <div class="form-group"  >
	    <label for="">Select Driver id</label>
          <select class="form-control" id="payment-method"  >
		     <option ng-repeat="data in paymentReceiveData.data" value="@{{data.identifier}}">@{{data.title}}</option> 
		  </select>
 </div>
		
	   
    </div>
</div>
  </div>
</div>

			   <!-- Modal -->
<div id="paymentReceiveModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Receive</h4>
      </div>
      <div class="modal-body"  > 
	  <div class="" >
	   <div class="form-group"  >
	    <label for="">Select Payment Method</label>
          <select class="form-control" id="payment-method"  >
		     <option ng-repeat="data in paymentReceiveData.data" value="@{{data.identifier}}">@{{data.title}}</option> 
		  </select> 
		  </div>
		  
		  <div class="form-group"  >
                                 <label for="">Amount</label>
                                 <input type="text" value="" class="form-control"   id="amount" placeholder="Enter Amount" required>
                              </div>
							  
							  <div class="form-group"  >
                                 <label for="">Transaction Id</label>
                                 <input type="int" value="" class="form-control"   id="transactionId" placeholder="Enter Transaction ID" required>
                              </div>
							  
			
      <button class="btn btn-info" ng-click="submitPaymentGateway()">Submit</button>			
	  </div>
    </div>
</div>
  </div>
</div>







			   <!-- Modal -->
<div id="orderStatusModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Order Status</h4>
      </div>
      <div class="modal-body"  > 
	  <div class="" >
	   <div class="form-group"  >
	    <label for="">Select Order Status</label>
          <select class="form-control" id="order-status"  >
		     <option ng-repeat="data in ordersTitle.order_status" value="@{{data.title}}">@{{data.title}}</option> 
		  </select> 
		  </div>
		  
		 
							  
			
      <button class="btn btn-info" ng-click="submitOrderStatus()">Submit</button>			
	  </div>
    </div>
</div>
  </div>
</div>







			   <!-- Modal for discount -->
<div id="discountModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Discount</h4>
      </div>
      <div class="modal-body"  > 
	  <div class="" > 
		  
		  <div class="form-group"  >
                                 <label for="">Discount(in <?php echo env('CURRENCY_SYMBOL');?>)</label>
                                 <input type="text" value="" class="form-control"   id="discount" placeholder="Enter Amount" required>
                              </div>
							   
							  
			
      <button class="btn btn-info" ng-click="submitDiscount()">Submit</button>			
	  </div>
    </div>
</div>
  </div>
</div>
	  <!--------------------------- Angular App Ends ---------------------------->
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  			   <!-- Modal for discount -->
<div id="shippingModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Shipping</h4>
      </div>
      <div class="modal-body"  > 
	  <div class="" > 
		  
		  <div class="form-group"  >
                                 <label for="">Shipping(in <?php echo env('CURRENCY_SYMBOL');?>)</label>
                                 <input type="text" value="" class="form-control"   id="shipping" placeholder="Enter Amount" required>
                              </div>
							   
							  
			
      <button class="btn btn-info" ng-click="submitShipping()">Submit</button>			
	  </div>
    </div>
</div>
  </div>
</div>
	  <!--------------------------- Angular App Ends ---------------------------->
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  	  			   <!-- Modal for discount -->
<div id="expressShippingModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:99999;">
  <div class="modal-dialog  " style="z-index:99999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Express Shipping</h4>
      </div>
      <div class="modal-body"  > 
	  <div class="" > 
		  
		  <div class="form-group"  >
                                 <label for="">Express Shipping(in <?php echo env('CURRENCY_SYMBOL');?>)</label>
                                 <input type="text" value="" class="form-control"   id="expressShipping" placeholder="Enter Amount" required>
                              </div>
							   
							  
			
      <button class="btn btn-info" ng-click="submitExpressShipping()">Submit</button>			
	  </div>
    </div>
</div>
  </div>
</div>
	  <!--------------------------- Angular App Ends ---------------------------->
	  
	  
	  
	  
	  
	  </div>
</div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/ordersDetail.js')}}"></script>  

<!------>
@endsection