@extends('layout.auth')
@section('title', 'Orders' )
		 <link rel="stylesheet" href="{{ URL::asset('admin/css/custom.css')}}">
		  <style>
		@media(min-width:1200px) {.header-tab{width:19%!important;}}
		 </style>
@section('content')
@section('header')
@include('includes.header')
@show
 
 
 <div  ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div  ng-controller="orderController as ctrl" style="height:10px;">
   <md-progress-linear class="md-accent" md-mode="determinate" value="@{{ctrl.determinateValue}}"  
      ng-show="isLoading" ng-disabled="isDisabled"></md-progress-linear>
</div>


<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	<style type="text/css">
	img{
		 
	}
	th,td{ text-align:center;}
 
</style>

<?php
use App\FeaturesSettings;
use App\Settings;
?>  
<script type="text/javascript">
 var controller_data = <?php echo json_encode($result);?>;
</script>
 
 <textarea id="res" style="display:none;"></textarea>
 
            <div id="loading" class="loading" >
                <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">				 
		<p >Calling all data...</p>
	    </div>	
 		 
         <div class="container-fluid" ng-controller="orderController as ctrl" ng-cloak>
<!--
<div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">				 
					 <p >Calling all data...</p>
					 </div>	-->
		 <div class="row">
             <div class="col-sm-6 col-xs-6" >
	           <h2 class="header">Orders</h2>
	         </div> 
              <div class="col-sm-6  col-xs-6 text-right" >
			  <div class="dropdown add"> 
			   </div>
 
	         </div> 			 
           </div> 
		   
            <div class="row" >
               <div class="col-sm-12" >
			   
			   
			   
			   
			   
		<?php	   
			   /**
echo DNS1D::getBarcodeSVG("4445645656", "PHARMA2T",3,33,"green", true);
 
 
 echo DNS1D::getBarcodeSVG("4445645656", "PHARMA2T",3,33,"green", true);
echo DNS1D::getBarcodeHTML("4445645656", "PHARMA2T",3,33,"green", true);
echo '<img src="' . DNS1D::getBarcodePNG("4", "C39+",3,33,array(1,1,1), true) . '" alt="barcode"   />';
echo DNS1D::getBarcodePNGPath("4445645656", "PHARMA2T",3,33,array(255,255,0), true);
echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG("4", "C39+",3,33,array(1,1,1), true) . '" alt="barcode"   />';
**/
		?>	   
<!---	   



			   
			   <div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 70%;">
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" />

 
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('12', 'C39+')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('13', 'C39E')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('14', 'C39E+')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('15', 'C93')}}" alt="barcode" />
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('19', 'S25')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('20', 'S25+')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('21', 'I25')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('22', 'MSI+')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('23', 'POSTNET')}}" alt="barcode" />
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('16', 'QRCODE')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('17', 'PDF417')}}" alt="barcode" />
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('18', 'DATAMATRIX')}}" alt="barcode" />
</div>
-->



 

			    
                   
				      
				   <ul class="nav nav-tabs" >
				  <li ng-class="{active: $index == selected}"style="width:100%;" >
						 <div class="col-sm-4 col-md-3 col-lg-2 header-tab" ng-repeat="tab in ctrl.orders_count" style="background:@{{tab.color}}"> 
						    <button  class="btn" onclick="kmakeselected(this.value)"  value="@{{tab.title}}" style="width:100%;outline:none;background:transparent;color:white;text-align: left;padding: 0px 12px 8px;"  >
							  <h3 ><?php echo env('CURRENCY_SYMBOL','');?>@{{tab.orders_totals}}</h3>
							  <p>@{{tab.orders_count}} @{{tab.title}} Orders</p>
						   </button>
						   </div>
						 </li>
                    </ul>
							<br>
				 
					 <div class="row">
					   <div class="col-sm-6 col-lg-7"> 
					   
					     <div class="row">
				
	

<script>
function makeselected(v)
{
var city = "option[value="+v+"]";
$("#order_status").find(city).attr("selected", "selected");
//filter_orders();
}
</script>				           
							   
							   
						                        <div class="col-sm-6 col-md-6 col-lg-4" >
                                                   <div class="header-fliters-data">
                                                      <label>Sort by Date :</label>
                                                      <select  class="form-control" id="date_filter"  ng-model="date_filter" ng-change="filter_orders()">
                                                         <option value="">Select Type </option>
														 <option value="lastAdded">Last Added </option>
														 <option value="firstAdded">First Added </option>
														 
												      </select>
                                                   </div>
                                                </div>
												
												
												<div class="col-sm-6 col-md-6  col-lg-4" >
                                                   <div class="header-fliters-data">
                                                      <label>Sort by Price :</label>
                                                      <select  class="form-control" id="price_filter"  ng-model="price_filter" ng-change="filter_orders()">
                                                         <option value="">Select Type </option>
													     <option value="highToLow">Price(High to Low)</option>
														 <option value="lowToHigh">Price(Low to High)</option>
												      </select>
                                                   </div>
                                                </div>
												
												 <?php
												$orders_status = \App\OrdersStatus::get();
											    ?>
                                                <div class="col-sm-6 col-md-6 col-lg-4 ">
                                                  <div class="header-fliters-data">
                                                      <label>Filter By Status :</label>
                                                      <select  class="form-control" id="order_status"  ng-model="order_status" ng-change="filter_orders()">
                                                         <option value="">Select Status </option>
														 
														 <?php
														 foreach($orders_status as $c)
														 {
														 ?>
														 <option value="<?php echo $c->identifier;?>"><?php echo $c->title;?> </option>
														  
														 <?php } ?>
                                                      </select>
                                                   </div>
                                                </div> 
							 
</div>							 
					   </div>
					   <div class="col-sm-2 col-lg-2 text-right top-actions">
				 	<a href="{{ URL::to('orders_report_page')}}" target="_blank"><i class="fa fa-print" onclicks="printData('tableToExport')"></i></a>
			 
					    <div class="dropdown column-settings" style="display:none" >
                                    <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown" ><i class="fa fa-cog"></i> 
									</button>
                                    <ul class="dropdown-menu" >
                                         <li ng-repeat="c in product.columns"  ng-init="c.visible =@{{c.visible}}"> 
										   <input type="checkbox" ng-true-value="1" ng-false-value="'0'"   ng-model="c.visible"> @{{c.title}}
										 </li> 

<hr>										 
										 <li>
										 
						    <label for="search" class="pagesize-label">Rows</label>
                            <input type="number" min="1" max="100" class="form-control pagesize" ng-model="pageSize">
										 </li>
                                    </ul>
                                 </div> 
						 </div>
						  <div class="col-sm-4 col-lg-3 text-right">
						 <div class="input-group">
                            <input class="form-control" ng-model="search" placeholder="Search by id.." type="search" ng-change="search()" /> 
							<span class="input-group-addon" ng-click="SearchData()"> <span class="fa fa-search"></span> </span>
							
                         </div>

					   </div>
					   </div> 

					   <br> 
						<div id="tableToExport" class="products-table "  >
						<div class="table-responsive">
                        <table class="table table-responsive" id="exportthis" >
						 <thead >
                           <tr>
						     <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" /></th>
							 <th>ID</th>
							 
							 <?php $s=  FeaturesSettings::where('title','barcodes')->first(['status'])->status;
							 $currency_symbol   = env('CURRENCY_SYMBOL'); 
							 if($s=='1') { ?>
							 <th>BARCODE</th>
							 <?php } ?>
							 <th>CUSTOMER</th>
							 <th>TOTAL</th>
							  <th>STATUS</th>
							 <th>CREATED</th>
							 <th style="display:none">ACTIONS</th>
                           </tr>
						   </thead >
                           <tbody ng-repeat="order in ctrl.orders">
                              <tr dir-paginate="values in order.data | filter:search | orderBy : sortById : reverse | orderBy : sortByPrice : reverse | itemsPerPage: pageSize" current-page="currentPage"  >
							     <td><input type="checkbox" ng-model="values.selected" ng-click="checkedIndex(values)"></td>
                                 <td class="id"><a href="{{ URL::to('order_detail_page')}}?order_id=@{{values.id}}">#@{{values.id}} </a></td> 
									 
							 <?php $s=\App\FeaturesSettings::where('title','barcodes')->first(['status'])->status; if($s=='1') { ?> <td class="barcode">
							 
							 
							 <img src="<?php echo url('/').'/orders/';?>@{{values.id}}.png"  onerror="Image Not" style="" alt="" />
							 </td> <?php } ?>
								 <td class="name">@{{values.customer_details[0].first_name}}</td>
								 <td class="price"><pre><?php echo env("CURRENCY_SYMBOL", "");?>@{{values.total}}</pre></td> 
								 <td class="status"><span style="background-color:@{{values.label_color}}">
								   @{{values.order_status}} 
								 </span>
								 
								 </td>
								 <!-- <td class="status" ng-repeat="state in order.status"><span ng-repeat="value in state.data" style="background:#@{{value.color}}">@{{value.title}} </span></td>
								 
								  <td class="status" ng-repeat="state in order.status"> 
								  @{{state.enabled}}
								  <select  class="form-control"  ng-repeat="value in state.data" ng-true-value="1" ng-false-value="'0'"  ng-model=" " ng-options="value as value.title for value in state.data "  > 
									 <option style="background:#@{{value.color}}"></option>
                                  </select>
								  
								  
								  <div class="dropdown" >
                                    <button class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">Select Actions 
									<span class="caret"></span>
									</button>
                                    <ul class="dropdown-menu"> 
										 <li  ng-repeat="value in state.data"><a href=""  style="background:#@{{value.color}}">@{{value.title}}</a></li>  
                                    </ul>
                                 </div>
								  </td> -->
								  <!-- `counter` directive can be used as: Element / Class / Attribute -->

								 
								 <td class="created"><pre>@{{values.created_at_formatted}}</pre></td>
								 <td class="actions" style="display:none"> 
								  <a class="btn btn-xs edit-product" ><i class="fa fa-edit"></i></a>
								   <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash"> </i></a>
								   <a href="#" ng-click="printToCart('printSectionId')" >Print</a>
								 </td>
                              </tr> 
                           </tbody>
                        </table> 
						</div>
						 
	  
						</div> 
						
						
								  
				 
 <div id="printSectionId"  ng-repeat="values in order.data" style="display: none;">
<h1 class="visible">@{{values.name}}  </h1>
<h1 class="visible">@{{values.price}}  </h1>

<p>@{{values.status}}</p>
</div>
						
                  
                   
				   
               </div>
			   
			   
			     <div ng-controller="OtherController" class="other-controller">
				   <div>
						 <dir-pagination-controls
                            max-size="5"
                           direction-links="true"
                            boundry-links="true" on-page-change="pageChangeHandler(newPageNumber)">
                      </dir-pagination-controls> 
						  </div>
						  </div>
						  
            </div>
         </div>
       
	  
	  <!--------------------------- Angular App Ends ---------------------------->
	  
                    
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


  </div>
</div>

 

    <script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/orders.js')}}"></script> 
	  <!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
	   <script src="{{ URL::asset('admin/js/dirPagination.js')}}"></script>

<!------>
@endsection