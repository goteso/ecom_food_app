@extends('layout.auth')
@section('title', 'Products List' )
@section('content')
@section('header')
@include('includes.header')
@show
 
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	  
<script type="text/javascript">
 
</script>
 
 <textarea id="res" style="display:none" ></textarea>
	  
 			
      <div ng-app="mainApp" >
         <div class="container-fluid" >
		 <div class="row">
             <div class="col-sm-12" >
	           <h2 class="header">Products</h2>
	         </div>  
           </div>
            <div class="row" >
               <div class="col-sm-12" ng-controller="productController as ctrl" >
                  <div class="" > 
				  <br>
					 <div class="row">
					   <div class="col-sm-6 col-lg-7"> 
					   
					     <div class="row">
					            <div class="col-sm-6 col-md-4 " >
							     <div class="header-fliters-data">
								  <label>Batch Action :</label>
								  
								   <div class="dropdown" >
                                    <button class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">Select Actions 
									<span class="caret"></span>
									</button>
                                    <ul class="dropdown-menu"> 
										 <li><a href=""ng-click="remove($index)" >Delete All</a></li>  
                                    </ul>
                                 </div>
								  
								   
						       </div>
							   </div>
					           <div class="col-sm-6 col-md-4 " >
							     <div class="header-fliters-data">
							    <label>Filters :</label>
						          <select  class="form-control"  ng-model="employeeFilter" ng-options="employee as employee.first_name for employee in employees track by employee.id">
						             <option value="">Select Category </option> 
                                  </select>
						       </div>
							   </div>
							   
                               <div class="col-sm-6 col-md-4  ">
							     <div class="header-fliters-data">
							     <label>SortBy :</label> 
                          		 <div class="dropdown" >
                                    <button class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">Select Option
                                       <span class="caret"></span>
									</button>
                                    <ul class="dropdown-menu"> 
										 <li><a href=""ng-click="sortByName='name'; reverse = !reverse">Name</a></li> 
										 <li><a href=""ng-click="sortByPrice='price'; reverse = !reverse">Price</a></li>
                                    </ul>
                                 </div>
                               </div>
							   </div> 
                            </div>							 
					   </div>
					   
					   <div class="col-sm-2 col-lg-2 text-right top-actions">
					     <i class="fa fa-envelope-o" ng-click="export()"></i>
						<i class="fa fa-print" ng-click="printToCart('tableToExport')" ></i>
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')"></i> 
						<div class="dropdown column-settings" >
                                    <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown" ><i class="fa fa-cog"></i> 
									</button>
                                    <ul class="dropdown-menu" >
                                          <li ng-repeat="c in ctrl.products.columns"  ng-init="c.visible = @{{c.visible}}"> 
										   <input type="checkbox" ng-true-value="1" ng-false-value="'0'"   ng-model="c.visible"> @{{c.title}}
										 </li> 

                                            <hr>	 
										 <li>
										 
						    <label for="search" class="pagesize-label">Rows</label>
                            <input type="number" min="1" max="100" class="form-control pagesize" ng-model="pageSize">
										 </li>
                                    </ul>
                                 </div> 
						 </div>
						 
						 
						  <div class="col-sm-4 col-lg-3 text-right">
						  
						 <div class="input-group">
                            <input class="form-control" ng-model="search" placeholder="Search by id.." type="search" ng-change="search()" /> 
							<span class="input-group-addon"> <span class="fa fa-search"></span> </span>
                         </div>

					   </div>
					   </div> 

					   <br> 
						<div id="tableToExport" class="products-table table-responsive"  >
                        <table class="table" id="exportthis " >
						<thead>
                           <tr>
						     <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" /></th>
							 <th ng-repeat="c in ctrl.products.columns | orderBy:'index'"  ng-show="c.visible" ng-if="c.visible == 1" >@{{c.title}}</th> 
							 <th>ACTIONS</th>
                           </tr>
						   </thead>
                           <tbody > 
                              <tr  ng-repeat="values in ctrl.products.data.data|filter:search|orderBy:'index'| orderBy : sortByName : reverse | orderBy : sortByPrice : reverse ">
							     <td><input type="checkbox" ng-checked="" ng-model="values.selected" ng-click="checkedIndex(values)"></td>
								 
								  <td ng-repeat="c in ctrl.products.columns" class="" ng-show="c.visible == 1" >
								     <span class="id" ng-show="$index == '0'">@{{values.id}} </span>
									 <span class="" ng-show="$index == '1'"><img src="images/@{{values.photo}}" ng-hide="!values.photo"> <img src="images/@{{values.photo}}" ng-show="!values.photo"> </span>
									 <span class="name" ng-show="$index == '2'">@{{values.title}} </span>
									 <span class="price" ng-show="$index == '3'"><pre>@{{values.base_price}} </pre></span>
									 <span class="created" ng-show="$index == '4'"><pre>@{{values.created_at}}</pre> </span>
								  </td>
								   
								 <td class="actions"> 
								  <a class="btn btn-xs edit-product" ><i class="fa fa-edit"></i></a>
								   <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"></i></a>
								 </td>
                              </tr> 
                           </tbody>
						   
						     
                        </table> 
						
						
						   
						   
						   
	  
						</div> 
                     </div> 
                  </div>
              
			   <div class="col-sm-12 " ng-controller="OtherController">
			   <div class="other-controller">
						 <dir-pagination-controls
                            max-size="5"
                           direction-links="true"
                            boundry-links="true" on-page-change="pageChangeHandler(newPageNumber)">
                      </dir-pagination-controls> 
						  </div>
						  </div>
            </div>
			 </div>
			   
         </div> 
	   
	  <!--------------------------- Angular App Ends ---------------------------->
	  
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/products.js')}}"></script> 
	  <!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
	   <script src="{{ URL::asset('admin/js/dirPagination.js')}}"></script>

<!------>
@endsection