@extends('layout.auth')
@section('title', 'Edit Product' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	   
<script type="text/javascript">
    var _yourSpecialVar = <?php echo json_encode($result);?>;
</script>
 
  			     <input type="file" id="file" style="display:none "/>

     <input type="hidden"  id="photo" />
 <textarea id="res" style="display:none ;"></textarea>
	  
<div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">				  
					 <p >Calling all data...</p>
					 </div> 

      <div ng-app="mainApp" ng-controller="itemEditController" ng-cloak>
	   
         <div class="container-fluid" >
            <div class="row">
               <div class="col-sm-12" >
                  <h2 class="header">Edit Item</h2>
               </div>
		  
            </div>
			
			
 


            <div class="row" >
               <div class="col-sm-12" >
			   <input type="hidden" value="<?php echo $id;?>" name="product_id" id="product_id">
                  <div class="edit-panel" ng-repeat="items in editItem">
                     <h3 class="title">@{{items.title}}</h3>
                     <form >
                        <div class="row" ng-hide="items.title=='Items Variants'">
                           <div class="col-lg-5 col-sm-8" >
						   
						   
						   							  <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'file'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
									 
								
                                
								
								
								
                                     <div style="width:150px;height: 180px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
        <img width="100%" height="100%" id="preview_image" src="<?php echo env('APP_URL')."/products/";?>@{{data.value}}" />
        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
    </div>
    <p>
        <a href="javascript:changeProfile()" style="text-decoration: none;">
            <i class="fa fa-edit"></i> Change
        </a>&nbsp;&nbsp;
        <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
            <i class="fa fa-trash-o"></i>
            Remove
        </a>
    </p>
	
                              </div>
							  
							  
							  
                              <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'longText'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <textarea class="form-control" name="@{{data.identifier}}"ng-model="data.value"  id="@{{data.identifier}}" placeholder="Enter @{{data.title}}">@{{data.value}}</textarea>
                              </div>
                              <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'text'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" class="form-control" ng-model="data.value" id="@{{data.identifier}}"  placeholder="Enter @{{data.title}}" >
                              </div>
							  
							   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'string'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" class="form-control" ng-model="data.value" id="@{{data.identifier}}"  placeholder="Enter @{{data.title}}" >
                              </div>
							  
                              <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'multipleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
									 
                                 <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"  key-property="id" text="title" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="false" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  
							  
							  <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'singleTag'">
                                                   <label for="@{{data.identifier}}">@{{data.title}}</label> 
                                                   <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title" key-property="id" text="text" ng-blur="text=''">
                                                      <auto-complete min-length="1" highlight-matched-text="true" source="searchBrand($query)"></auto-complete>
                                                   </tags-input>
                                                </div>
												
												
                             <!-- <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'singleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <tags-input name="tags" display-property="value"  ng-model="data.value"></tags-input>
                              </div>-->
                              <div class="form-group" ng-repeat="data in items.fields" ng-show="data.symbol">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">@{{data.symbol}}</span>
                                    <input type="@{{data.type}}" class="form-control" name="@{{data.identifier}}"  id="@{{data.identifier}}" ng-model="data.value" placeholder="Enter @{{data.title}}">
                                 </div>
                              </div>
							  
							   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.message">
              <label for="@{{data.identifier}}">@{{data.title}}</label>
              <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}" placeholder="Enter @{{data.title}}" >
           </div> 
		  
		  <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'datePicker'">
		    <label for="@{{data.identifier}}">@{{data.title}}</label>
		  
			 
			  <datepicker
      date-format="yyyy-MM-dd"  
      button-prev='<i class="fa fa-arrow-circle-left"></i>'
      button-next='<i class="fa fa-arrow-circle-right"></i>'>
      <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3" placeholder="Enter @{{data.title}}"/>
    </datepicker> 
		   </div>
		   
		    <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'timePicker'">
		    <label for="@{{data.identifier}}">@{{data.title}}</label>
		    <div class="input-group clockpicker" 
       clock-picker 
       data-autoclose="true"   > 
    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
    <span class="input-group-addon">
      <span class="fa fa-clock-o"></span>
    </span>
  </div>
   </div>
							  
							   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'filessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'"> 
                                                 <label for="@{{data.identifier}}">@{{data.title}}</label> 
                                                 <input type="@{{data.type}}"  file-input="files" name="@{{data.identifier}}" ngf-pattern="'image/*'"  ngf-accept="'image/*'">
                                             </div>  
			
                           </div>
                           <div class="col-lg-12 col-sm-12 submit text-right">
                              <button ng-click="saveChanges()" class="btn btn-success"> Save Changes </button>
                           </div>
                        </div>
						

                        <div class="row" ng-show="items.title=='Items Variants'">
                           <div class="col-lg-12" >
						   
						   
						   <section class="tabbable" >
                              <ul class="nav nav-tabs" >
                                 <li ng-class="{active: $index == selected}" ng-repeat="tab in items.tabs"><a href="#@{{tab.title}}" data-toggle="tab" ng-click="tab = @{{$index}}" >@{{tab.title}}</a></li>
                              </ul>
							  
                              <div class="tab-content">
                                 <div id="@{{tab.title}}" class="tab-pane fade in  "  ng-class="{active: $index == selected}"  ng-repeat="tab in items.tabs"  >
							 
								 
                                    <table class="table tableAdd  ">
									
									 <tbody >
									 
                                        <tr ng-repeat="value in tab.values  " class="table-row" >
										
									 
								
		                                  
                                         <td ng-repeat="field in tab.fields" > 
										 	 
										     <div class="form-group" ng-show="field.type == 'text' && field.identifier=='title'">
                                               <label for="@{{field.identifier}}"> @{{field.title}}</label>
                                               <input type="@{{field.type}}" class="form-control" name="title@{{ $parent.$index }}@{{ tab.product_variant_type_id}}" id="title@{{ $index }}" ng-model="value.title"   style="width:80%" placeholder="Enter @{{field.title}}"required> 
                                             </div> 
											 
											 <div class="form-group" ng-show="field.symbol && field.type == 'float' && field.identifier=='price'  ">
                                               <label for="@{{field.identifier}}"> @{{field.title}}</label>
                                               <input type="number" class="form-control" name="price@{{ $parent.$index }}@{{ tab.product_variant_type_id}}" id="price@{{ $index }}" ng-model="value.price"   style="width:80%" placeholder="Enter @{{field.title}}"required> 
                                             </div> 
											 
											  <div class="form-group" ng-show="  field.type == 'float' && field.identifier=='stock_count'  ">
                                               <label for="@{{field.identifier}}"> @{{field.title}}</label>
                                               <input type="number" class="form-control" name="stock_count@{{ $parent.$index }}@{{ tab.product_variant_type_id}}" id="stock_count@{{ $index }}" ng-model="value.stock_count"  placeholder="Enter @{{field.title}}" style="width:80%"required> 
                                             </div> 
											
											 <div class="form-group " ng-show="field.symbol && field.type == 'float' && field.identifier=='price_difference'">
                                               <label for="@{{field.identifier}}">@{{field.title}}   </label>
                                               <div class="input-group ">
                                                 <span class="input-group-addon">@{{field.symbol}}</span>
                                                 <input type="number"    class="form-control" name="price_difference@{{ $parent.$index }}@{{ tab.product_variant_type_id}}"  id="price_difference@{{ $index }}"  ng-model="value.price_difference"  placeholder="Enter @{{field.title}}" style="width:80%"required> 
												</div>
												
												<!-------->
								 
										 
										 
                                             </div>
											 
										 <input type="text"   style="display:none;" class="form-control" name="id@{{ $parent.$index }}@{{ tab.product_variant_type_id}}"  id="id@{{ $index }}"  ng-model="value.id"      > 
										 <input type="text"   style="display:none;" class="form-control" name="product_variant_type_id@{{ $parent.$index }}@{{ tab.product_variant_type_id}}"  id="product_variant_type_id@{{ $index }}"  ng-model="tab.product_variant_type_id"   style="width:80% ;"  > 
											
			
											  
									     </td>    
                                         <td>
									      <label></label><br> 
									 
									      <button ng-click="save_variant($index, tab.product_variant_type_id)"   class="btn btn-save">Save</button>
									      <button ng-click="delete_variant($index, tab.product_variant_type_id,$parent.$index)" class="btn btn-delete"><i class="fa fa-trash-o"></i></button>
									     </td>
									   
                                        </tr>
										
										 
										  
										 
									  </tbody>
									</table>
										
									<div class="text-right">
							          <button ng-click="add($index,field)" class="btn btn-add btn-success"> Add New</button> 
									</div>
									  
                                 </div>
                              </div>
							  </section>
							  
							  
							  
							  
					 
			  
			  
                           </div>
                        </div>
						
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  
	  <!--------------------------- Angular App Ends ---------------------------->
	  
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>




<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/products_form.js')}}"></script> 


<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        //$('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-image-upload-products')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                   // $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
                      
	$('#photo').val(data).trigger("change");
   $('#preview_image').attr('src', '{{asset('products')}}/'+data);
 
				 
					//document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '{{asset('products')}}/'+data);
					
					
                }
                //$('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('users/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#photo').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-products/" + $('#photo').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#photo').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
</script>





<!------>
@endsection