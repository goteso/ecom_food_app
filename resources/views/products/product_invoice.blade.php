<!DOCTYPE html>
<html>
<head>
	<!-- Define Charset -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Responsive Meta Tag -->
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

	<title>Invoice</title>
	
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	 
</head>
<body ng-app="mainApp">
<div  ng-controller="invoiceController" ng-cloak>

 <?php 
 $app_logo = @\App\Settings::where('key_title','app_logo')->first(['key_value'])->key_value;
 ?>
 
 
				     <table border="0"     cellpadding="0" cellspacing="0"   >
							 
								<tr  >
								<td><img  border="0"   style="display: block; width: 156px;" src="{{URL::asset('orders')}}/{{ $d['order_id'] }}.png"  alt="barcode" /></td>
								 
								 </tr>
								 <tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
								  <tr> 
								  <td style="font-family:'Open Sans',sans-serif;">{{ $d['app_name']}} - #@{{invoice.basic_details[0].id}}</td>
								</tr>
								<tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
							     
								
								<tr>
							       
								  <td>{{ $d['basic_details'][0]['updated_at']->format('d M Y') }}</td>
								</tr>
								<tr>
							      <td>Order No. : </td>
								  <td>#{{ $d['order_id'] }}</td>
								</tr>
								<tr>
							      <td>Customer : </td>
								  <td>{{ $d['customer_id'] }}</td>
								</tr>
								
								<tr>
							      <td>Product:</td>
								  <td>{{ $d['basic_details'][0]['title'] }}</td>
								</tr>
							 
								
				 
								
								
						 
								
							 
								
							  
								
								
					 
					</table>
		 
		 
 
		  

 

 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
<script>
var app = angular.module("mainApp", []);
app.controller('invoiceController', function($http,$scope) {
  

$scope.invoice = <?php echo json_encode($d);?>;




		/*download as pdf**/
   $scope.export = function(){
   
    
        html2canvas(document.getElementById('exportthis'), {
		
		//var doc = new jsPDF("p", "mm", "a4"); 
       //var width = doc.internal.pageSize.width;    
       //var height = doc.internal.pageSize.height;

            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("order.pdf");
            }
        });
   };
 
});
</script>
 <script src="https://docraptor.com/docraptor-1.0.0.js"></script>
  <script>
    var downloadPDF = function() {
      DocRaptor.createAndDownloadDoc("YOUR_API_KEY_HERE", {
        test: true, // test documents are free, but watermarked
        type: "pdf",
        document_content: document.querySelector('html').innerHTML, // use this page's HTML
        // document_content: "<h1>Hello world!</h1>",               // or supply HTML directly
        // document_url: "http://example.com/your-page",            // or use a URL
        // javascript: true,                                        // enable JavaScript processing
        // prince_options: {
        //   media: "screen",                                       // use screen styles instead of print styles
        // }
      })
    }
  </script>
</body>
</html>