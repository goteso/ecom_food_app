@extends('layout.auth')
@section('title', 'Report Details' )
 
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
	  
	<!--  <script>
function myFunction() {
    window.print();
}
</script>-->


      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
					 <div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
      <div ng-app="mainApp" ng-controller="reportController" ng-cloak>
         <div class="container-fluid" >
            <div class="row" >
               <div class="col-sm-12" >
			    
                  <div class="panel reports-panel" ng-repeat="data in reports">
                     <div class="panel-header">
					 <div class="row">
					   <div class="col-sm-6">
					     <button class="btn btn-info edit" ng-click="ShowHide()" style="display:none">Edit Notes <i class="fa fa-pencil"></i></button>
					   </div>
					   <div class="col-sm-6 text-right">
					     <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
					     <i class="fa fa-print" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
					     <i class="fa fa-cog" style="display:none"></i>
						 <i class="fa fa-print" onclick="myFunction()" ></i>
					   </div>
					   </div>
                     </div>
                     <div class="panel-body" >
                        <h2 class="text-center title" >@{{data.title}}</h2>
                        <h4 class="text-center text-uppercase subtitle"> @{{data.subTitle}} </h4>
                        <h4 class="text-center date"> @{{data.dateRange}}</h4>
						<div id="tableToExport">
                        <table class="table" id="exportthis">
                           <tr>
                              <th ng-repeat="c in data.columns | orderBy:'index'" ng-show="c.visible == 1 " > @{{c.title}}</th>
                           </tr>
                           <tbody ng-repeat="values in data.data">
                              <tr>
                                 <td>
                                    <h4>@{{values.header}}</h4>
                                 </td>
                              </tr>
                              <tr ng-repeat="r in values.rows">
                                   <td  ng-repeat="c in data.columns"ng-show="c.visible == 1 " > <span ng-repeat="value in r | orderBy:'index'" ng-show="c.index== $index" >@{{value}}</span></td>  
								  
                              </tr>
                           </tbody>
                        </table>
						</div>
						
						<div class="row" > 
						 <div class="col-sm-12" ng-show= "IsVisible"> 
						  <p >@{{notes}}</p>
						  </div>
						 <form ng-show = "IsHidden" ng-submit="submit()"> 
						  <div class="col-sm-8 col-md-9 col-lg-10"> 
						      <textarea class="form-control" ng-model="notes" id="notes" name="notes">@{{data.notes}}</textarea>
						  </div>
						   <div class="col-sm-4 col-md-3 col-lg-2">
						     <button type="submit" class="btn btn-success">Save</button>
						   </div>
						 </form>
						</div>
						
                     </div>
                     <div class="panel-footer">
                        <p class="text-center">@{{data.footer}}</p>
                     </div>
                  </div> 
               </div>
            </div>
         </div>
      </div>
	  
	                      
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/reportDetail.js')}}"></script> 
@endsection

