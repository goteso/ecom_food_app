var app = angular.module("mainApp", [])


app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	
 
app.controller('reportController',  function($http,$scope,Excel,$timeout) {
$scope.reports = [{
  "title": "Goteso",
  "subTitle": "General Report",
  "dateRange": "January - March 2018",
  "columns": [
    {
      "title": "DATE",
      "index": 0,
      "visible": 1
    },
    {
      "title": "NAME",
      "index": 1,
      "visible": 1
    },
    {
      "title": "DESCRIPTION",
      "index": 2,
      "visible": 1
    },
    {
      "title": "AMOUNT",
      "index": 3,
      "visible": 1
    }
  ],
  "data": [
    {
      "header": "Sales in November",
      "rows": [
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ],
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ],
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ],
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ]
      ]
    },
    {
      "header": "Sales in December",
      "rows": [
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ],
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ],
        [
          "06/01/2018",
          "Yugal Kishore",
          "Escrow payment for #5635",
          "₹20,000.00"
        ]
      ]
    }
  ],
  "notes": "",
  "footer": " Friday, 23 March, 2018 01:28 PM GMT+05:30"
}
];

       $scope.IsVisible = true;
       $scope.IsHidden = false;
       $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsHidden =  true;
			$scope.IsVisible = false;
        };
			
	
      /**Get notes values from json**/
       angular.forEach($scope.reports, function(data) {
         if(data.notes == '') { 		   
	   $scope.notes = 'There are no any notes';
		 }
		 else{
	      $scope.notes = data.notes; 
		 }
       });		

      /** Submit notes form data **/ 
		$scope.submit = function() { 
         if(this.notes) {   
           $scope.notes = this.notes;
		   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		   //$scope.notes = '';
         }
		 if(this.notes == ''){
			  $scope.notes = 'There are no any notes';
			   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		 }
		 
		};
		
		
		
		$scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        }
		
/*  $scope.refresh = function(){
    $http.get('/api/users')
          .success(function(data){
               $scope.users = data;
          });
} */ 

   $scope.export = function(){
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
        });
   };
  
});

