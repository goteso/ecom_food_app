 
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', $item['name'], ['class' => 'form-control ', 'placeholder' => 'Role Name' ] ) !!}
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>



<div class="form-group @if ($errors->has('name')) has-error @endif">
   {!! Form::label('name', 'Admin Panel Login ?') !!}</br>
  <input type="radio" name="admin_panel_login" value="1" <?php if($item['admin_panel_login'] == 1 or $item['admin_panel_login'] == '1') { echo 'checked'; } ?>> Yes<br>
  <input type="radio" name="admin_panel_login" value="0" <?php if($item['admin_panel_login'] == 0 or $item['admin_panel_login'] == '0') { echo 'checked'; } ?>> No<br>
</div>


 
    {!! Form::hidden('id', @$item['id'], [] ) !!}
 
