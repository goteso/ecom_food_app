@extends('layout.auth')
@section('title', 'Roles & Permissions')
<link rel="stylesheet" href="{{ URL::asset('admin/css/style.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
 
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
     
	  

                   <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid">
 
 
  <div class="row">
                  <div class="col-sm-12 roles">
				   
					 
      	    @include('flash::message')
       
	  <?php $x1 = 0; $x2=0;?>
      <ul class="nav nav-tabs nav-roles">
         @foreach($roles as $role1)
         <?php $x1++;  if($x1=='1') { $class1='active';} else { $class1='';}?>
         <li class="{{ $class1 }}" ><a data-toggle="tab" href="#{{ $role1['id'] }}">{{ $role1->name}} </a></li>
         @endforeach
      </ul>
	  
	  <br>
	  <div class="row roles-header" > 
		 <div class="col-sm-5 header-left">
       <h3 class="text-left">Roles</h3>
	  </div>
         <div class="col-md-7 page-action text-right header-right">
             
            <button type="button" class="btn btn-role"  data-toggle="modal" data-target="#myModal">Add New</button>
          
			
			  
            <button type="button" class="btn btn-role"  data-toggle="modal" data-target="#view">Manage</button>
       
         </div>
      </div>
	  
      <div class="tab-content">
         @forelse ($roles as $role)
         <?php $x2++;  if($x2=='1') { $class2='in active';} else { $class2='';}?>
         <div id="{{ $role['id'] }}" class="tab-pane fade {{ $class2 }}">  
            {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']) !!}
            @include('shared._permissions', [
            'title' => $role->name .' Permissions',
            'model' => $role ])
         
            {!! Form::submit('Save', ['class' => 'btn btn-role']) !!}
          
            {!! Form::close() !!}
         </div>
		 
         @empty
         <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
         @endforelse
      </div>
   </div>
   </div> 
   </section>
</div>

</div>

</div>






 <!-- Modal -->
	 
      <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
         <div class="modal-dialog" role="document">
            {!! Form::open(['method' => 'post']) !!}
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title text-center" id="roleModalLabel">Role</h3>
               </div>
               <div class="modal-body">
                  <!-- name Form Input -->
				 
				  <div class="row">
				  <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                  <div class="form-group @if ($errors->has('name')) has-error @endif"  >
                     {!! Form::label('name', 'Name') !!}
                     {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
                     @if ($errors->has('name')) 
                     <p class="help-block">{{ $errors->first('name') }}</p>
                     @endif
                  </div>
				  
				  
				  
<div class="form-group @if ($errors->has('name')) has-error @endif">
   {!! Form::label('name', 'Admin Panel Login ?') !!}</br>
  <input type="radio" name="admin_panel_login" value="1" checked> Yes<br>
  <input type="radio" name="admin_panel_login" value="0"> No<br>
</div>


				   <!-- Submit Form Button -->
                  {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
				  </div>
				  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 
               </div>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
	  
	  
	  
	  
	  
<!----- Manage Modal Starts----->
<!------------- View Modal Starts ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog text-center"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage Roles</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th class="text-center">Sr</th>
                           <th class="text-center">Role</th>
						   <th class="text-center">Guard</th>
                           <th class="text-center">Created At</th>
                           @can('edit_roles', 'delete_roles')
                           <th class="text-center">Actions</th>
                           @endcan
                        </tr>
                     </thead>
                     <tbody>
					     <?php $x=0;?>
                        @foreach($roles as $item)
						<?php $x++;?>
                        <tr>
                           <td>{{ $x }}</td>
                           <td>{{ $item['name'] }}</td>
						   <td>{{ $item['guard_name'] }}</td>
                           <td>{{ $item['created_at'] }}</td>
                           @can('edit_users')
                           <td class="text-center">
						   <?php
                                       $model_contains_role = \DB::table('model_has_roles')->where('role_id', '=', $item['id'])->get()->count();
									   if($model_contains_role < 1)
									   {
                            ?>
                              @include('role._actions_roles', [
                              'entity' => 'roles',
                              'id' => $item['id']
                              ])
									   <?php } else { echo 'Users Exists';}?>
                           </td>
                           @endcan
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-left">Update Role</h4>
                                 </div>
                                 <div class="modal-body text-left"  >
                                   
                                    <div class="row text-left">
                                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                          {!! Form::model($item['id'], ['method' => 'PUT', 'route' => ['roles.update',  $item['id'] ] ]) !!}
                                          @include('role._form_edit_roles')
                                          <!-- Submit Form Button -->
                                          {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
                                          {!! Form::close() !!}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
<!--
<script>
function manageClose(){
	$('#view').modal('hide');
}-->
</script>

<!----manage modal ends here--->
@endsection