@extends('layout.auth')
@section('title', 'Settings')
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show

<div id="wrapper"  > 
 

   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!-- Modal -->
 

                   <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid  ">
  
       
	  <div class="row">
               <div class="col-sm-12">
                  @include('flash::message')
                  <div class="text-right">
                  </div>
                  <div class="tab-content" >
				   <div ng-app="mainApp">
				  <div ng-view></div>
				  </div>
		</div>
	  </div>
	  </div> 
	   
   </div>
    
   </section>
</div>

</div>

</div>



		
		
		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/settings.js')}}"></script> 
 

<!----manage modal ends here--->
@endsection