@extends('layout.auth')
@section('title', 'Driver Profile' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
  <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
   
   
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
					 
					 <textarea id="res2"  style="display:  none;"></textarea>
					 <input type="hidden" id="user_id" id="user_id" value="<?php echo @$_GET["user_id"];?>">
                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
						
						<!--<div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>-->
      <div ng-app="mainApp">
         <div class="container-fluid"  ng-controller="profileController" ng-cloak>
   <div class="row" > 
	     <div class="col-sm-8 col-md-9 user-profile-data" >
		 
		 <div class="row"  > 
	      <div class="col-sm-12 col-md-6 col-lg-5"   >
		    <div class="card info"  ng-repeat="values in profileData.user_data" >
		     <img src="admin/images/user-placeholder.png" style="display:block;margin-right:0">
			 <div>
		       <h4>@{{values.first_name}} @{{values.last_name}} <a href="{{ URL::to('user_edit_form') }}/@{{values.id}}"><i class="fa fa-edit"></i></a></h4>
			   <h5>@{{values.email}}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		<!--   <div class="col-sm-6 col-md-3"   >
		    <div class="card"  ng-repeat="values in profileData.user_data" >
			<div class="card-data">
		       <h4>@{{values.orders_count}}</h4>
			   <h5>Total Orders</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"  ng-repeat="values in profileData.user_data" >
		   <div class="card-data">
		      <h4><?php echo env("CURRENCY_SYMBOL", "");?>@{{values.total_revenues}}</h4>
			 <h5>Total Revenues</h5>
			 </div>
		   </div>
		   </div> -->
		 
		 
		 </div>
		 </div>
		  <div class="col-sm-4 col-md-3 text-right" >
		    <!--<a class="btn btn-create" href="{{ URL::to('order_add') }}">Create<br> Order</a>-->
		 </div> 
  </div>
   </div>  
    
	
	 
	<!-- <div ng-view></div>-->
	    <div class="row" > 
	       <div class="col-sm-12 user-profile-data" >
		   <div class="tab-content " style="padding: 10px 15px;">
		   <div class=" panel" style="padding: 10px;">
					 <div id="map" style="width: 100%;  height: 700px;  position: relative; overflow: hidden;"></div>
					 </div>
				</div>	 
			  </div>
          </div>
		  
		  
	 </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
					 
					 
					 
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


<script>
var simpleGoogleMapsApiExample = simpleGoogleMapsApiExample || {};
 
simpleGoogleMapsApiExample.map = function (mapDiv, latitude, longitude, accuracy) {
  "use strict";
 
  var createMap = function (mapDiv, coordinates) {
    var mapOptions = {
      center: coordinates,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 15
    };
 
    return new google.maps.Map(mapDiv, mapOptions);
  };
 
  var addMarker = function (map, coordinates) {
    var markerOptions = {
      clickable: false,
      map: map,
      position: coordinates
    };
 
    return new google.maps.Marker(markerOptions);
  };
 
  var addCircle = function (map, coordinates, accuracy) {
    var circleOptions = {
      center: coordinates,
      clickable: false,
      fillColor: "blue",
      fillOpacity: 0.15,
      map: map,
      radius: accuracy,
      strokeColor: "blue",
      strokeOpacity: 0.3,
      strokeWeight: 2
    };
 
    return new google.maps.Circle(circleOptions);
  };
 
  var infoWindowVisible = (function () {
    var currentlyVisible = false;
 
    return function (visible) {
      if (visible !== undefined) {
        currentlyVisible = visible;
      }
 
      return currentlyVisible;
    };
  }());
 
  var addInfoWindowListeners = function (map, marker, infoWindow) {
    google.maps.event.addListener(marker, "click", function () {
      if (infoWindowVisible()) {
        infoWindow.close();
        infoWindowVisible(false);
      } else {
        infoWindow.open(map, marker);
        infoWindowVisible(true);
      }
    });
 
    google.maps.event.addListener(infoWindow, "closeclick", function () {
      infoWindowVisible(false);
    });
  };
 
  var addInfoWindow = function (map, marker, address) {
    var infoWindowOptions = {
      content: address,
      maxWidth: 200
    };
    var infoWindow = new google.maps.InfoWindow(infoWindowOptions);
 
    addInfoWindowListeners(map, marker, infoWindow);
 
    return infoWindow;
  };
 
  var initialize = function (mapDiv, latitude, longitude, accuracy) {
    var coordinates = new google.maps.LatLng(latitude, longitude);
    var map = createMap(mapDiv, coordinates);
    var marker = addMarker(map, coordinates);
    var geocoder = new google.maps.Geocoder();
 
    addCircle(map, coordinates, accuracy);
 
    geocoder.geocode({
      location: coordinates
    }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK && results[0]) {
        marker.setClickable(true);
        addInfoWindow(map, marker, results[0].formatted_address);
      }
    });
  };
 
  initialize(mapDiv, latitude, longitude, accuracy);
};
 
$(document).ready(function () {
  "use strict";
    
		 setInterval(function(){	
		  
		 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
             
				var m =JSON.parse(xmlhttp.responseText);
				document.getElementById('res2').value = JSON.stringify(m);
				
				 
	     var t1,t2;
		 
		 t1 = m[0].latitude;
		  t2 = m[0].longitude;
	    
		 
     simpleGoogleMapsApiExample.map($("#map")[0], t1, t2);
				 
            }
        };
        xmlhttp.open("GET",  APP_URL+"/get_driver_locations_track?user_id=94", true);
       xmlhttp.send(); },3000);
	   
	   
 // simpleGoogleMapsApiExample.map($("#map")[0], -25.363, 131.044, 70);
});
</script>

<!--
 <script>
 
      function initMap() { 
		 
	   var myLatlng1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var myLatlng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map2'), {
          zoom: 6,
          center: myLatlng1
        });

        var marker = new google.maps.Marker({
          position: myLatlng1,
          map: map,
          title: 'Click to zoom'
        });

        map.addListener('center_changed', function() {
          // 3 seconds after the center of the map has changed, pan back to the
          // marker.
          window.setTimeout(function() {
            map.panTo(marker.getPosition());
			 
          }, 3000);
        });

        marker.addListener('click', function() {
          map.setZoom(8);
          map.setCenter(marker.getPosition());
        });
      }
    </script>
	
	 -->

    <script   type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCo0S5dcqwj11plZQyOn7Sx6VJPHQPVZko&callback=initMap '></script>
	
	
		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/driverProfile.js')}}"></script> 
@endsection  

