@extends('layout.auth')
@section('title', 'Edit Store' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
   
   
  
  

      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
					 
                     <div class="text-right">
                     </div>
					 
				 
					 <textarea id="res"  style="display: none ;"></textarea>
					 
					 
					  

    <input type="file" id="file" style="display: none"/>




                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
      <div ng-app="mainApp" ng-controller="editStoreController" ng-cloak>
         <div class="container-fluid" >
            <div class="row">
               <div class="col-sm-12" >
                  <h2 class="header">Edit Store</h2>
               </div>
            </div>
			 
            <div class="row" >
               <div class="col-sm-12" >
                
                     <div class="panel" >
                         
                        <div class="row">
                           <div class="col-lg-7 col-sm-8">
						  


 {!! Form::model('', ['method' => 'PUT', 'route' => ['store_update'] ]) !!}
						  
						  
						                          <div class="row">
						 
						 
						 <input type="hidden" class="form-control" name="store_id" id="store_id" value="{{ @$result->id }}">
				
 <div class="form-horizontal"  >
   

   
	<div class="form-group">
	
        <label class="col-sm-2 control-label">Location:</label>

        <div class="col-sm-8">
            <input type="text" class="form-control" id="us3-address"/>
        </div> 
		
    </div> 
	
	
    <!--Map by element. Also it can be attribute-->
    <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;"></locationpicker>

    <div class="clearfix" >&nbsp;</div>
    <div class="m-t-small">
	  <div class="col-sm-6">
        <label class="p-r-small  control-label">Latitude:</label>  
            <input type="text" class="form-control"  name="latitude"  id="latitude" value="{{ @$result->latitude }} " />
        </div>
		
        <div class="col-sm-6">
        <label class="p-r-small  control-label">Longitude:</label> 
            <input type="text" class="form-control" name="longitude"   id="longitude" value="{{ @$result->longitude }} " />
        </div>
    </div>
    <div class="clearfix"></div>
</div>
				</div>
				
				
				
			
				 <br>


							  
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"   >
                                 <label for="store_name">Store Name</label>
                                 <input type="text"   class="form-control"  name="store_name"  id="store_name" value="{{ @$result->store_name }}"   > 
                              </div>
							    
							  
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"  >
                                 <label for="address1">Address1</label>
                                 <textarea class="form-control" name="address1"  id="address1" placeholder="Enter address1">{{ @$result->address1 }}  </textarea>
                              </div>
							  
							   <div class="form-group"  >
                                 <label for="address1">Address2</label>
                                 <textarea class="form-control" name="address2"  id="address2" placeholder="Enter address2">{{ @$result->address2 }}</textarea>
                              </div>
							  
							  
							  <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"   >
                                 <label for="city">City</label>
                                 <input type="text"   class="form-control"  name="city"  id="city" placeholder="Enter city " value="{{ @$result->city }}"  > 
                              </div>
							  
							  <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"   >
                                 <label for="state">State</label>
                                 <input type="text"   class="form-control"  name="state"  id="state" placeholder="Enter state"  value="{{ @$result->state }}"     > 
                              </div>
							  
							  <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"   >
                                 <label for="country">Country</label>
                                 <input type="text"   class="form-control"  name="country"  id="country" placeholder="Enter country name"  value="{{ @$result->country }}"   > 
                              </div>
							  
							  <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"   >
                                 <label for="pincode">Pincode</label>
                                 <input type="text"   class="form-control"  name="pincode"  id="pincode" placeholder="Enter pincode"  value="{{ @$result->pincode }}"   > 
                              </div>
							  
                           </div>
                           <div class="col-lg-7 col-sm-4 submit text-right">
                           </div>
                        </div>
                     </div>
					 
					 
                              <button   ng-click="add(data.value)" class="btn btn-success" > Svae Chnages </button>
                 
				   {!! Form::close() !!}	
				  
				   
               </div>
            </div>
         </div>
      </div>   
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>












 
		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/addStore.js')}}"></script> 
		
		
		
@endsection

