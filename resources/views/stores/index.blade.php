@extends('layout.auth')
@section('title', 'Stores' )
		 <link rel="stylesheet" href="{{ URL::asset('admin/css/custom.css')}}">
		 <style>.disabled {
  cursor: not-allowed;
}</style>
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <script type="text/javascript"></script>
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
	  
	  <script>
	   
	  </script>
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
                     <textarea id="res" style="display: none; "></textarea>
                     <div class="tab-content" >
 
                        <!--------------------------Angular App Starts ------------------------------------>
                        <div ng-app="mainApp" >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-12" >
                                    <h2 class="header">Stores List</h2>
								  </div>
                              </div>
							  
							  
						 <div><a href="{{ URL::to('add_store') }}" class="btn btn-success btn-md">Add Store</a></div>
							  

							  
							  <br>
							  
							  
							  
							  
							  
							  
							  
				 <?php @$manager_role_id = @\App\Role::where('name','Manager')->first(['id'])->id; 
				   $vendors = @\App\User::where('user_type',@$manager_role_id)->get();
				   
				 
				 ?>
							  
							  
							  
							  
							  
							  
							                 <div class="row">
                  <div class="col-sm-12">
				   
                     <div class="tab-content products-table">
                            <table class="table" class="table table-striped" id="exportthis" >
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <th>Store Name</th>
								 <th>Assigned Vendor</th>
								 <th>Address</th>
								 <th>City</th>
								 <th>State</th>
								 <th>Country</th>
                                 <th>Created At</th>
                               <th>Action</th>
                                 
                             
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;  $currency_symbol   = env('CURRENCY_SYMBOL'); ?>
                              @foreach($result as $item)
                              <?php $x++;?>
                              <tr>
                                 <td>{{ $x }}</td>
								 <td>{{ @$item->store_name }}</td>
								 <td>
								 
							
								     {!! Form::model($item->id, ['method' => 'PUT', 'route' => ['assign_vendor_to_store',  $item->id ] ]) !!}
                                          <select id="" name="vendor_id" class="form-control" onchange="this.form.submit()" required>
										  <option value="">Select Vendor  </option>
										  	 <?php if(sizeof($vendors) > 0)
											 {
												foreach($vendors as $v)
												{		

 												
												   ?>
										                  <option value="<?php echo $v->id;?>" <?php if($v->id == $item->vendor_id) { echo 'SELECTED';} ?>><?php echo $v->first_name." ".$v->last_name;?></option>
											       <?php 
											    } 
											 }?>
										  </select>
                                         
                                          {!! Form::close() !!}								  
								 </td>
								 <td>{{ @$item->address1 }} 
								      {{ @$item->address2 }} 
									  {{ @$item->pincode }}
								 </td>
								 <td>{{ @$item->city }}</td>
								 <td>{{ @$item->state }}</td>
								 <td>{{ @$item->country }}</td>
								 <td>{{ @$item['created_at']->diffForHumans() }}</td>
                                
                                 <td><a href="{{url('store_edit')}}/{{ @$item->id }} "><i  class="fa fa-edit"></i></a></td>
							
                              
                              </tr>
    
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							   
                              <!-------inner model ends-------->
                              @endforeach
                           </tbody>
                        </table>
						  <div class="text-right">
                                 {{ $result->links() }}
                           </div>
                     </div>
                  </div>
               </div>
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
		   
		   
		   
		   
                                 </div>
                        
                              </div>
                           </div>
                        </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>

 <script>
 document.getElementById('loading').innerHTML = '';
 </script>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/users.js')}}"></script> 
@endsection