@extends('layout.auth')
@section('title', 'User Profile' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
   
   
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
					 
					 <textarea id="res2"  style="display:none;"></textarea>
					 <input type="hidden" id="user_id" id="user_id" value="<?php echo @$_GET["user_id"];?>">
                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
						
						
      <div ng-app="mainApp">
         <div class="container-fluid"  ng-controller="profileController" ng-cloak>
   <div class="row" > 
	     <div class="col-sm-8 col-md-9 user-profile-data" >
		 
		 <div class="row"  > 
	      <div class="col-sm-12 col-md-6 col-lg-5"   >
		    <div class="card info"  ng-repeat="values in profileData.user_data" >
		     <img src="admin/images/user-placeholder.png" style="display:block;margin-right:0">
			 <div>
		       <h4>@{{values.first_name}} @{{values.last_name}} <a href="{{ URL::to('user_edit_form') }}/@{{values.id}}"><i class="fa fa-edit"></i></a></h4>
			   <h5>@{{values.email}}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		   <div class="col-sm-6 col-md-3"   >
		    <div class="card"  ng-repeat="values in profileData.user_data" >
			<div class="card-data">
		       <h4>@{{values.orders_count}}</h4>
			   <h5>Total Orders</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"  ng-repeat="values in profileData.user_data" >
		   <div class="card-data">
		      <h4><?php echo env("CURRENCY_SYMBOL", "");?>@{{values.total_revenues}}</h4>
			 <h5>Total Revenues</h5>
			 </div>
		   </div>
		   </div> 
		 
		 
		 </div>
		 </div>
		  <div class="col-sm-4 col-md-3 text-right" >
		    <a class="btn btn-create" href="{{ URL::to('order_add') }}">Create<br> Order</a>
		 </div> 
  </div>
   </div>  
   

	  
	 
	 
	
	 <div ng-view></div>
	 </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/userProfile.js')}}"></script> 
@endsection

