 
 
<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', @$item->title, ['class' => 'form-control ', 'placeholder' => 'Category Title' ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
</div>


<div class="form-group @if ($errors->has('result')) has-error @endif">
{!! Form::label('result', 'Parent Category') !!}
  <select   class="form-control" id="parent_id" name="parent_id"  style="width:100%">
  <option value="">Select Parent Category(optional)</option>
  
      @foreach($result as $ct)
	   <?php  // $count1 = \App\Categories::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		//if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="{{ $ct->id }}" <?php // echo $s1;?> >{{ $ct->title }}</option>
     @endforeach
</select>
</div>



 
 

<script type="text/javascript">
   $(document).ready(function() {
       $('#multi_select_box').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   
 });
</script>

 

 
<script type='text/javascript'>
$(window).load(function(){
$('.datetimepicker').datetimepicker({  format:'DD/MM/YYYY hh:mm A', });
});
</script>