<?php
   use App\Role;
?>
@extends('layout.auth')
@section('title', 'Transactions' )
		 <link rel="stylesheet" href="{{ URL::asset('admin/css/custom.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
			
			 <div class="row">
			    <div class="col-sm-8">
				  	    @include('flash::message')
				  <h3>Transactions</h3>
					 </div>
					 <div class="col-sm-2 col-lg-2 text-right top-actions">
                        <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
					 
						<!-- <a href="{{ URL::to('reports_page')}}" target="_blank"><i class="fa fa-print" onclicks="myFunction()"></i></a>-->
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
                                        
										
                      </div>
            
					 </div>
			 
					 <br><br>
               <div class="row">
                  <div class="col-sm-12">
				   
                     <div class="tab-content products-table">
                            <table class="table" class="table table-striped" id="exportthis" >
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <th>OrderId</th>
								 <th>Customer</th>
								 <th>Amount</th>
								 <th>Payment Gateway</th>
								 <th>TransactionId</th>
                                 <th>Created At</th>
                               
                                 
                             
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;  $currency_symbol   = env('CURRENCY_SYMBOL'); ?>
                              @foreach($result as $item)
                              <?php $x++;?>
                              <tr>
                                 <td>{{ $x }}</td>
                                 <td> <a target="_blank" href="{{ URL::to('order_detail_page')}}?order_id={{$item['order_id']}}">#{{$item['order_id']}} </a></td>
								 <td><a target="_blank" href="{{ URL::to('user_profile')}}?user_id={{$item['customer_id']}}">{{ $item['customer_name'] }}  </a> </td>
							 
								 <td>{{$currency_symbol}}{{ $item['amount'] }}</td>
								 <td>{{ $item['payment_gateway'] }}</td>
								 <td>{{ $item['transaction_id'] }}</td>
                                 <td>{{ $item['created_at']->diffForHumans() }}</td>
                                
                      
							
                              
                              </tr>
    
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							   
                              <!-------inner model ends-------->
                              @endforeach
                           </tbody>
                        </table>
						  <div class="text-right">
                                 {{ $result->links() }}
                           </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
 
<!--Add New Model Starts--> 
<style>

</style>
<!-- Modal -->
<!------------- Add New User ----->
 


<!------>
@endsection