@extends('layout.auth')
@section('title', 'Add User' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
   
    <script type="text/javascript">
    var controller_data = <?php echo json_encode($result);?>;
</script>  
  
  

      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
					 
                     <div class="text-right">
                     </div>
					 
				 
					 <textarea id="res"  style="display: none ;"></textarea>
					 
					 
					  

    <input type="file" id="file" style="display: none"/>




                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
      <div ng-app="mainApp" ng-controller="userController" ng-cloak>
         <div class="container-fluid" >
            <div class="row">
               <div class="col-sm-12" >
                  <h2 class="header">Add User</h2>
               </div>
            </div>
			 
            <div class="row" >
               <div class="col-sm-12" >
                  <form name="myForm">
                     <div class="panel" ng-repeat="users in addUser">
                        <h3>@{{users.title}}</h3>
                        <div class="row">
                           <div class="col-lg-5 col-sm-8">
						  



                              


 <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'file'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
									 
								     <input type="hidden" ng-model="data.value" value="@{{data.value}}" id="@{{data.identifier}}" />
                                
								
								
								
                                     <div style="width:150px;height: 180px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
        <img width="100%" height="100%" id="preview_image" src="{{asset('images/noimage.jpg')}}"/>
        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
    </div>
    <p>
        <a href="javascript:changeProfile()" style="text-decoration: none;">
            <i class="fa fa-edit"></i> Change
        </a>&nbsp;&nbsp;
        <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
            <i class="fa fa-trash-o"></i>
            Remove
        </a>
    </p>
	
                              </div>


							  
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'text'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}" placeholder="Enter @{{data.title}}"  ng-required="data.required_or_not" > 
                              </div>
							  
							  <!----- For Email Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'email'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}" placeholder="Enter @{{data.title}}"  ng-required="data.required_or_not">  
                              </div>
							  
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'password'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}" placeholder="Enter @{{data.title}}"  ng-required="data.required_or_not">
                              </div>
							  
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'radio'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> <br> 
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" name="@{{data.identifier}}" >@{{options.title}}
								  </label> 
                              </div>
							  
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'checkbox'">
                                 <label for="@{{data.identifier}}">@{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="@{{data.type}}"  ng-model="data.value[options.value]" id="@{{options.value}}"  >@{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" >@{{options.title}}
								 </label>
                              </div>
							  
							  
				 
         				  			  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'Selectbox'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> <br> 
									 
							 		  
							  <select ng-model="data.value" class="form-control"  ng-required="data.required_or_not">
							  	 <option value="" selected>Select User Type</option>
                             <option  ng-repeat="options in data.field_options" value="@{{options.value}} ">@{{options.title}}</option>
						
                     </select>
								  
                              </div>
							  
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'multipleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'singleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.symbol">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">@{{data.symbol}}</span>
                                    <input type="@{{data.type}}" class="form-control"   ng-model="data.value" id="@{{data.identifier}}" placeholder="Enter @{{data.title}}" >
                                 </div>
                              </div>
							  
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'datePicker'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  placeholder="Enter @{{data.title}}"/>
                                 </datepicker>
                              </div>
							  
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'timePicker'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter @{{data.title}}" ng-required="data.required_or_not">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
								 
								 <div class='input-group date' id='datetimepicker1'>
                                   <input type='text' class="form-control"  ng-model="data.value"/>
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
                              </div>
							  
							  
							   <!----- For DateTimepicker Input Field----------->
                              <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'dateTimePicker'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter @{{data.title}}">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  
							  <!----- For Textarea Input Field----------->
							  <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'longText'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <textarea class="form-control" name="@{{data.identifier}}"ng-model="data.value"  id="@{{data.identifier}}" placeholder="Enter @{{data.title}}">@{{data.value}}</textarea>
                              </div>
							  
                           </div>
                           <div class="col-lg-7 col-sm-4 submit text-right">
                           </div>
                        </div>
                     </div>
					 
					 
                              <button   ng-click="add(data.value)" class="btn btn-success" > Add User </button>
                  </form>
				  
				   
               </div>
            </div>
         </div>
      </div>   
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>














<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-image-upload')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
                      
	$('#photo').val(data).trigger("change");
 
	//$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
	 
	
					
					//document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '{{asset('users')}}/' + data);
                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('users/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#photo').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image/" + $('#photo').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#photo').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
</script>
		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/addUser.js')}}"></script> 
		
		
		
@endsection

