@extends('layout.auth')
@section('title', 'Driver Profile' )
 <link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
   
   
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     @include('flash::message')
                     <div class="text-right">
                     </div>
					 
					 <textarea id="res2"  style="display:  none;"></textarea>
					 <input type="hidden" id="user_id" id="user_id" value="<?php echo @$_GET["user_id"];?>">
                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
						
						<!--<div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/images/89.svg')}}" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>-->
      <div ng-app="mainApp">
         <div class="container-fluid"  ng-controller="profileController" ng-cloak>
   <div class="row" > 
	     <div class="col-sm-8 col-md-9 user-profile-data" >
		 
		 <div class="row"  > 
	      <div class="col-sm-12 col-md-6 col-lg-5"   >
		    <div class="card info"  ng-repeat="values in profileData.user_data" >
		     <img src="admin/images/user-placeholder.png" style="display:block;margin-right:0">
			 <div>
		       <h4>@{{values.first_name}} @{{values.last_name}} <a href="{{ URL::to('user_edit_form') }}/@{{values.id}}"><i class="fa fa-edit"></i></a></h4>
			   <h5>@{{values.email}}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		<!--   <div class="col-sm-6 col-md-3"   >
		    <div class="card"  ng-repeat="values in profileData.user_data" >
			<div class="card-data">
		       <h4>@{{values.orders_count}}</h4>
			   <h5>Total Orders</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"  ng-repeat="values in profileData.user_data" >
		   <div class="card-data">
		      <h4><?php echo env("CURRENCY_SYMBOL", "");?>@{{values.total_revenues}}</h4>
			 <h5>Total Revenues</h5>
			 </div>
		   </div>
		   </div> -->
		 
		 
		 </div>
		 </div>
		  <div class="col-sm-4 col-md-3 text-right" >
		    <!--<a class="btn btn-create" href="{{ URL::to('order_add') }}">Create<br> Order</a>-->
		 </div> 
  </div>
   </div>  
    
	 
	
	 <div ng-view></div>
	 
	 
	 </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/driverProfile.js')}}"></script> 
		  <script type="text/javascript">
				   
        var markers = [
    {
        "title": 'Aksa Beach',
        "lat": '67.020441',
        "lng": '15.501716',
        "description": 'Aksa Beach is a popular beach and a vacation spot in Aksa village'
    } 
    ];
	    var m = [];
        window.onload = function () {
            LoadMap();
	    };

        var map;
        var marker;
        function LoadMap() {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            //SetMarker(2);
        };
		
		
 
 
  
 
 function SetMarker(data) {
             alert('1');
            if (marker != null) { marker.setMap(null); }
            var bounds = new google.maps.LatLngBounds();
		 

           for(var y=0;y<data.length;y++)
			{
	        var myLatlng = new google.maps.LatLng(data[y]["latitude"], data[y]["longitude"]);
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data[y]["firstName"]
            });
			
 m.push(marker);			
	          //Create and open InfoWindow.
               var infoWindow = new google.maps.InfoWindow();
               infoWindow.setContent("<span>" +data[y]["firstName"]+ "</span>");
               infoWindow.open(map, marker);
			   bounds.extend(myLatlng);
           }
map.fitBounds(bounds);
 };
 
  function changeMarkerPosition(marker , lat , lng , name ) {
    var latlng = new google.maps.LatLng(lat, lng);
    marker.setPosition(latlng);
 marker.setTitle(name);
}
 
 
 
 
  
 function SetMarker2(data) {
            
 alert('2');
           for(var y=0;y<data.length;y++)
			{
				 alert(y);
				
			changeMarkerPosition(m[y] , data[y]["latitude"] , data[y]["longitude"] , data[y]["firstName"]);
			}
 };
 
 
		
		
		
		 setInterval(function(){	
		  
		  
		 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
             
			 
				var m =JSON.parse(xmlhttp.responseText);
				document.getElementById('res2').value = JSON.stringify(m);
				var t;
				t["longitude"] = '30.6425';
				t["latitude"] = '30.6425';
				
				alert(t);
				SetMarker2(m);
            }
        };
        xmlhttp.open("GET",  APP_URL+"/get_driver_locations", true);
       xmlhttp.send(); },3000);
  
    </script>
@endsection

