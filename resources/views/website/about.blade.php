@extends('layout.auth_web')
@section('title', 'About' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
  

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		<div class="container " >
		 <div class="row mt-5 mb-4">
		  <div class="col-sm-12">
		   <div class="card">
            <div class="card-block">

             <!--Header-->
             <div class="form-header" style="background-color:#B8152F;">
              <h3><i class=""></i> ABOUT US</h3>
             </div>

             <!--Body-->
		     <div class="row">
		      <div class="col-sm-12 ">
		        
			   <h4 class="about-head mb-3"  > Try <?php //echo $project_title; ?> by scheduling the pickup and get your laundry and dry clean at the tap of a button. We also do carpet dry cleaning, shoe dry cleaning, sofa cover dry cleaning and so on. </h4>
           
			   <div class="row">
				 <div class="col-md-5">
				  <div class="flex-center">
                   <img class="img-responsive mx-auto d-block" alt="cup and logo" src="{{URL::asset('web/img/about_us.jpg')}}">
                  </div>
			     </div>
			 
				 
						<div class="col-md-7 mt-1 text-justify" data-delay="400">	 
							<p><strong>About Launspace</strong></p>
							<p>We are here to make laundry convenient, quick&amp; happy experience. Our simple system and incredible customer service will take the thinking out of your laundry hassle, so that you can focus on more important things.</p>
							<p><strong>Mission</strong></p>
							<p>To provide a safe and efficient laundry experience like nobody else, through concierge customer service, strategic marketing, innovative technology, and superior cleanliness.</p>
							<p><strong>Vision</strong></p>
							<p>To be #1 recommended laundry service by listening and responding to the needs of our customers.</p>
							<p>&nbsp;</p>
							
						</div>
               </div>
								 
              </div>
             </div>
			 
	        </div>
           </div>
      <!--/Form with header-->
	  
		  </div>
         </div>
		</div>   
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		 <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
		
       
	 @include('includes.footer-web')
			
	
	@endsection




