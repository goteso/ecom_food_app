@extends('layout.auth_web')
@section('title', 'Login' )
@section('content')
@section('header')
@include('includes.header-web')
@show
  


<div ng-app="myApp" ng-controller="changePassController">

  
 
     <!---------CONTENT SECTION STARTS HERE------>
	 
		   <div class="container mt-5 mb-4" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
     <div class="card-block">

        <!--Header-->
        <div class="form-header" style="background-color:#B8152F;">
            <h3><i class="fa fa-lock" style="color:#fff"></i> Change Password</h3>
        </div>

        <!--Body-->
		<div class="row">
		<div class="col-sm-10 offset-sm-1">
		
		<form action="" method="post"  >  
        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="password" id="form2" ng-model="oldPassword" class="form-control validate" required>
            <label for="form2" data-error="wrong" >Current Password</label>
        </div>
         <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="password" id="password" ng-model="newPassword" class="form-control validate" required>
            <label for="form2" data-error="wrong" >New Password</label>
        </div>
		 <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="password" id="confirm_password" ng-model="newPassword2" class="form-control validate" required>
            <label for="form2" data-error="wrong">Retype New Password</label>
        </div>
       

		 <div class="md-form">
        <div class="text-center">
            <button type="submit" class="btn" style="background-color:#B8152F" ng-click="changePass()">Save</button>
			 <p>Forgot <a href="{{URL::asset('forgot_password')}}" style="color:#B8152F" >Password?</a></p>
        </div>
		</div>
		</form>
		
		
</div>
    </div>
	
 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">

	
	</div>
 
 
 <!--------message box ends-------------->
</div>
</div>
<!--/Form with header-->
		    </div>
    </div>
		</div>   
		
		</div>
		
		<!-------------CONTENT SECTION ENDS HERE-------------->
		
		
       
       
 
 
 
<!-- SCRIPTS -->
    <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.js"></script>
	<script>
	
	  var app = angular.module('myApp',['ngStorage']);
	  
	  app.controller('changePassController',function($scope,$http){
		  
		   $scope.changePass = function(){
			  
			$scope.user_id = '65';
			
			$scope.$watch('oldPassword', function(val) {
			$scope.oldPassword = val;
			
		})
		  $scope.$watch('newPassword', function(val) {
            $scope.newPassword = val;
		})
		
		 $scope.changePassData = {"user_id":$scope.user_id, "old_password":$scope.oldPassword, "new_password":$scope.newPassword};
		   
    var request = $http({
                method: "POST",
                url: "http://192.168.0.109/ecommerce/public/api/change_password",
                data: $scope.changePassData,
                headers: { 'Accept':'application/json'  }
           }).then(function (data, status, headers, config) { 
		   
		   $scope.info = data;
		   
                 //document.getElementById("res").value = JSON.stringify(data);      
           },function (data, status, headers, config) { 
            
       });
	   };
	   
		  
	  })
	</script>
   
<script type="text/javascript">
   var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword; 
</script>
 


 @include('includes.footer-web')
			
	
	@endsection