@extends('layout.auth_web')
@section('title', 'Contact' )
@section('content')
@section('header')
@include('includes.header-web')
@show 
 
 
 
 
   

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		<!--Section: Contact v.1-->
<section  >
<div class="container">
  
     <div class="row mt-4 mb-4">
	  <div class="col-sm-12">
	    
		 <div class="card">

                <div class="card-block">
	            <div class="form-header"  style="background-color:#B8152F;">
                        <h3 class=""><i class="fa fa-envelope" style="color:#fff"></i> Contact Us</h3>
                    </div>
	    <!--Google map-->
            <div id="map-container" class="z-depth-1-half map-container" style="height:400px"></div>
 
			
		<!--	<p class="mt-2 font-weight-normal " style="color:#00E676;">We have received your enquiry and will respond to you within 24 hours.<p>-->
	 
    <div class="row mt-2">

        <!--First column-->
        <div class="col-md-7 col-sm-12">

            <!--Form with header-->
           
                    <!--Header-->
                   

                    <div class="row">
		<div class="col-md-10 offset-md-1 col-xs-10 offset-xs-1">
		
		<h3 class="mb-2 "  style="color:#B8152F;"><i class="fa fa-envelope"></i> Contact Us</h3>
             
			 <form action="" method="post">  
        
                    <!--Body-->
                    <div class="md-form">
                        <i class="fa fa-user prefix"></i>
                        <input type="text" id="form3" class="form-control validate" required>
                        <label for="form3">Your name</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="text" id="form2" class="form-control validate" required>
                        <label for="form2">Your email</label>
                    </div>
                    
					<div class="md-form">
					<i class="fa fa-phone prefix"></i>
					<input type="text" id="form2" name="phone" class="form-control validate" required>
					<label for="form2" data-error="wrong" >Phone Number</label>
				   </div>
		
                    <div class="md-form">
                        <i class="fa fa-tag prefix"></i>
                        <input type="text" id="form32" class="form-control validate" required>
                        <label for="form32">Subject</label>
                    </div>

                    
                    <div class="">
                        <button class="btn btn-large btn-danger">Send Message</button>
                    </div>

                </div>
</div>
 </div>
	 
	 
	  <div class="col-md-5 my-auto"  >
             <p style="font-weight:bold"> <a class="btn-floating btn-small btn-purple"><i class="fa fa-phone"></i></a> T:  8427-675-675</p>
                                <p style="font-weight:bold"> <a class="btn-floating btn-small btn-purple"><i class="fa fa-map-marker"></i></a> A: 78/1,  Malbar Hill Road, Mumbai.  </p>
                                <p style="font-weight:bold"><a class="btn-floating btn-small btn-purple"><i class="fa fa-envelope"></i></a> E: care@launspace.com
 </p>
                         	</article>
                           
							
					
                        </div>
	 
	 
	 
	 
   
            </div>
            </div>
            <!--/Form with header-->

        </div>
        <!--/First column-->

       </div>

    </div>
</div>
</section>
<!--/Section: Contact v.1-->
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		
		
        

  <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
 
  
  <script>
  function regular_map() {
    var var_location = new google.maps.LatLng(40.725118, -73.997699);

    var var_mapoptions = {
        center: var_location,
        zoom: 14
    };

    var var_map = new google.maps.Map(document.getElementById("map-container"),
        var_mapoptions);

    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "New York"
    });
}

google.maps.event.addDomListener(window, 'load', regular_map);
  </script>
   

	 @include('includes.footer-web')
			
	
	@endsection



