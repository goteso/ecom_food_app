@extends('layout.auth_web')
@section('title', 'Login' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
	
<div ng-app="myApp" ng-controller="forgotPasswordCtrl">
   
<!-- Modal -->
					
		      <!-- Modal Subscription -->
               <div class="modal fade modal-ext" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <!--Content-->
                     <div class="modal-content">
                       <!--Header-->
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title w-100" id="myModalLabel">OTP Verification</h4>
                       </div>
					<!--Body-->
					<div class="modal-body">
					
						<form action="" method = "POST">
							<div class="col-sm-12 md-form">
								<input type="password" id="form4" class="form-control validate" required>
								<label for="form4">Enter OTP here</label>
							</div>
							
							
								<div class="col-sm-12 text-center md-form" >
								      <button type="button" class="btn green "ng-click="verifyOtp()" >Verify</button>
								</div>
							
						</form>
					</div>
						
                     </div>
                     <!--/.Content-->
                  </div>
               </div>
			  <!-- Modal Subscription ends-->
			  
			  
			  
			  <!-- Modal Subscription -->
               <div class="modal fade modal-ext" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <!--Content-->
                     <div class="modal-content">
                       <!--Header-->
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title w-100" id="myModalLabel">Change Password</h4>
                       </div>
					<!--Body-->
					<div class="modal-body">
					
						<form action="" method = "POST">
							<div class="col-sm-12 md-form">
								<input type="password" id="form4" ng-model="newPass" class="form-control validate" required>
								<label for="form4">Enter New Password</label>
							</div>
							<div class="col-sm-12 md-form">
								<input type="password" id="form4" ng-model="confirmPass" class="form-control validate" required>
								<label for="form4">Confirm Password</label>
							</div>
							
							
								<div class="col-sm-12 text-center md-form" >
								      <button type="button" class="btn green " ng-click="changePassword()">Submit</button>
								</div>
							
						</form>
					</div>
						
                     </div>
                     <!--/.Content-->
                  </div>
               </div>
			  <!-- Modal Subscription ends-->
			  
			  

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		   
		   
		   <div class="container mt-5 mb-4" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
    <div class="card-block">

        <!--Header-->
        <div class="form-header" style="background-color:#B8152F;">
            <h3><i class="fa fa-lock" style="color:#fff"></i> Forgot Password?</h3>
        </div>

        <!--Body-->
		<div class="row">
		<div class="col-sm-10 offset-sm-1">
		<form action="" method="post" >  
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="email" id="form2" class="form-control validate" ng-model="email" required>
            <label for="form2" data-error="wrong" >Your email</label>
        </div>

       

		 <div class="md-form">
        <div class="text-center">
            <button type="button"  class="btn btn-danger" ng-click="getEmail()">Get Password</button>
        </div>
		</div>
		</form>
</div>
    </div>
	
  
  
  
 
</div>
</div>
<!--/Form with header-->
		    </div>
    </div>
		</div>   
		
		
		</div>
		<!-------------CONTENT SECTION ENDS HERE-------------->
		
		
     
 
<!-- SCRIPTS --> 
   <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script>

 <script type="text/javascript">
   /*  $(window).on('load',function(){
        $('#myModal').modal('show');
    }); */
</script>
<script>
 var app = angular.module('myApp',[]);
 app.controller('forgotPasswordCtrl',function($scope,$http){
	
  $scope.getEmail = function(){
		   $scope.$watch('email', function(val) {
            $scope.email = val;
		})
		
		 $scope.emailData = {"email":$scope.email};
	   
		
		//alert("sending data "+JSON.stringify($scope.emailData));
		
		
	     $('#myModal').modal('show');
		   
		var request = $http({
					method: "POST",
					url: "http://192.168.0.109/ecommerce/public/api/forgot_password",
					data: $scope.emailData,
					headers: { 'Accept':'application/x-www-form-urlencoded'  }
			   });

			   /* Check whether the HTTP Request is successful or not. */
			   request.success(function (data, status, headers, config) { 
		    
			   $scope.data=data;
			   
			
			   //alert(JSON.stringify($scope.data));
			   $scope.otp = data.otp;
			   
			   
			   }).error(function (data, status, headers, config) { 
			  
		   });
		   };	
		   
		/*=================================================*/
		
		   $scope.verifyOtp = function(){
		   $scope.$watch('otp', function(val) {
            $scope.otp = val;
		}) 
		
		 $scope.verifyData = {"email":$scope.email,"one_time_code":$scope.otp};
		 alert(JSON.stringify($scope.verifyData));
		   
		var request = $http({
					method: "POST",
					url: "http://192.168.0.109/ecommerce/public/api/verify_otp",
					data: $scope.verifyData,
					headers: { 'Accept':'application/x-www-form-urlencoded'  }
			   });

			   /* Check whether the HTTP Request is successful or not. */
			   request.success(function (data, status, headers, config) { 
		    
			   $scope.data=data;
			   //alert(JSON.stringify($scope.data));
			   $scope.statuscode=data.status_code;
			   //alert(JSON.stringify($scope.statuscode));
			   if($scope.statuscode==1){
				   $('#myModal1').modal('show');
			   }
			   $scope.id = data.id;
			   
			   }).error(function (data, status, headers, config) { 
			   $scope.errordata=data;
			  //alert(JSON.stringify($scope.errordata));
		   });
		   };	
		   
		   /*=================================================*/
	 
	 $scope.changePassword = function(){
		   $scope.$watch('newPass', function(val) {
            $scope.newPass = val;
			//alert($scope.newPass);
		}) 
		
		$scope.$watch('confirmPass', function(val) {
            $scope.confirmPass = val;
		}) 
		
		 $scope.Changepwd = {"password":$scope.newPass,"user_id":"65"};
		 //alert(JSON.stringify($scope.Changepwd));
		   
		var request = $http({
					method: "POST",
					url: "http://192.168.0.109/ecommerce/public/api/forgot_password_change",
					data: $scope.Changepwd,
					headers: { 'Accept':'application/x-www-form-urlencoded'  }
			   });

			   /* Check whether the HTTP Request is successful or not. */
			   request.success(function (data, status, headers, config) { 
		    
			   $scope.data=data;
			  // alert("success");
			   $('#myModal').modal('hide');
			   
			   
			   }).error(function (data, status, headers, config) { 
			   $scope.errordata=data;
			  //alert("error"+JSON.stringify(data));
		   });
		   };	
 })
</script>
 
 @include('includes.footer-web')
			
	
	@endsection