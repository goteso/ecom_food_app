@extends('layout.auth_web')
@section('title', 'Home' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
	 
	 <!----------------Paralax Image------------->
	 <div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div id="background-image">
					<!---<div><img src="{{ URL::asset('web/img/logo-slider.png')}}" class="center-block img-responsive logo" style="width:130px;"></div>-->
	   
						<h1 style="color:#fff" class="text-center logo-name">Laundry & Lot at your Doorstep</h1>
	   
					
					   <h3 class="p-4 my-auto text-center text-white">Schedule a free Pickup or Call us at <b><?php //echo $doorDryCleanContact;?>8427-675-675</b></h3>
					 <div class="col-md-12 text-center">
						<!--<a href="{{URL::asset('add-new-order')}}" class="btn btn-danger btn-lg white-text mt-1" ><h5 class="flex-center my-auto ">SCHEDULE PICKUP</h5> </a>-->
						<a href="#" class="btn btn-danger btn-lg white-text mt-1" ><h5 class="flex-center my-auto ">SCHEDULE PICKUP</h5> </a>
					   
					 </div>
			 </div>
	 <!----------------Paralax Image------------->
	 
	 
	 <!----------------How it works-------------->
	 
	 <section id="works">
	 <div class="container-fluid">
	    <div class="row">
		  <div class="col-md-12">
		     <h4 class="text-center works-title heading">Dry Cleaning & Laundry Made Simple</h4>
			 <p class="text-center font-weight-normal description">How it works:</p>
			 <span class="border-center"></span>
		  </div>
		</div>
		<div class="row mt-2">
		<div class="col-sm-12 col-xs-12 col-md-10 col-md-offset-1">
		<div class="row">
		<div class="col-md-2 col-sm-2 hidden-xs">
		 <img src="{{ URL::asset('web/img/works.png')}}" class="img-responsive center-block" style="height:250px">
		</div>
			   <div class="col-sm-6 col-md-6 col-xs-6 steps">
			      <h4 class="mb-1"> Book via Our Phone or Online</h4>
				  <p class="font-weight-normal" > We're a mobile, door-to-door dry Cleaning and Laundry Service.</p>
				  
				  <h4 class="mb-1"> Meet our Pickup Pilot</h4>
				  <p class="font-weight-normal"> We collect your dry cleaning & laundry from your doordtep.</p>
				  
				  <h4 class="mb-1"> Receive your fresh clothes</h4>
				  <p class="font-weight-normal" > We bring back fresh clothes in 48 hours!</p>

				  </div>
			   
			   <div class="col-md-4 col-sm-4 col-xs-4">
			      <img src="{{ URL::asset('web/img/circle.png')}}" class="img-responsive center-block">
			   </div>
			 </div> 
		   </div>  
		</div>
	  </div>
	 </section>
	 
	 
	 <!----------------How it works-------------->
	 
	 <Section id="prices" style="background-color:#EFEFEF">
	    <div class="container mt-2">
		  <div class="row justify-content-sm-center">
			<div class="col-sm-12">
		     <h4 class="text-center font-weight-normal pt-2 heading" >Stay Crisp &amp; Clean at your Convenience</h4>
			  <!--<p class="text-center description">Stay <strong>Crisp</strong> &amp; <strong>Clean</strong> at your <strong>Convenience</strong> </p>-->
			 <span class="border-center"></span>
			</div>
		  </div>
			 
		  <div class="row text-center mt-2 convenience">
	 
	        <div class="col-sm-12 col-md-4">
			 <div class="card mb-2">
			   <div class="row card-block">
			     <div class="col-sm-4 col-md-12 col-lg-4  pb-1">
				  <img src="{{URL::asset('web/img/crisp.png')}}" class="mx-auto d-block img-responsive ">
				 </div>
				 <div class="col-sm-8 col-md-12 col-lg-8 text-left"  >
				   <p class=" "> <strong>Crisp</strong></p>
				   <p class="">Launspace laundering is usually followed up by professional ironing or pressing in order to give your garments a shiny and crisp finish.</p>
				 </div>
			   </div>
			 </div>
			</div>
			
			
			
			<div class="col-sm-12 col-md-4">
			 <div class="card mb-2">
			   <div class="row card-block">
			      <div class="col-sm-4  col-md-12 col-lg-4   pb-1">
				  <img src="{{URL::asset('web/img/clean.png')}}" class="mx-auto d-block img-responsive ">
				 </div>
				 <div class="col-sm-8 col-md-12 col-lg-8 text-left">
				   <p><strong>Clean</strong></p>
					<p class="">Laundry is a service built on trust &ndash; the trust consumers give to a cleaner for proper care of their best apparel. Because some fabrics require special attention and offer no room for compromise on quality.</p>
				 </div>
			   </div>
			 </div>
			</div>
			
			<div class="col-sm-12 col-md-4">
			 <div class="card mb-2">
			   <div class="row card-block"> 
			     <div class="col-sm-4  col-md-12 col-lg-4  pb-1">
				 <img src="{{URL::asset('web/img/convenience.png')}}" class="mx-auto d-block img-responsive ">
				 </div>
				 <div class="col-sm-8 col-md-12 col-lg-8  text-left">
				   <p><strong>Convenience</strong></p>
							<p class=" ">Web booking, on-demand pick-up, store, helpline, home-delivery-all at your service.</p>
				 </div>
			   </div>
			 </div>
			</div>
			
			<!--<div class="col-sm-6">
			 <div class="card mb-2">
			   <div class="row">
			     <div class="col-sm-4 col-md-3 my-auto">
				 <img src="{{URL::asset('web/img/satisfaction.png')}}" class="mx-auto d-block img-responsive ">
				 </div>
				 <div class="col-sm-8  col-md-9 pt-1  text-left">
				   <p><strong>We care, you wear</strong></p>
							<p class=" pr-1">Launspace is a service you can trust. our attendants are highly trained, and your satisfaction is always guaranteed.</p>
				 </div>
			   </div>
			 </div>
			</div>-->
			
	 <p></p>
							
							
							
							
							
					 </div> 
		   </div>   
	 </section>			
	  <!----------------OFFERS SECTION STARTS HERE------------------>
	  
	  <Section id="prices">
	    <div class="container mt-2">
		  <div class="row justify-content-sm-center">
			<div class="col-sm-12">
		     <h4 class="text-center font-weight-normal pt-2 heading" >Affordable Prices</h4>
			  <p class="text-center description">Download <?php //echo $project_title;?>Laundry App today to discover special offers in your area! </p>
			 <span class="border-center"></span>
			</div>
		  </div>
			 
		  <div class="row text-center mt-2">
            <!-- Nav tabs -->
<ul class="nav nav-tabs nav-justified danger-color-dark">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#top" role="tab">Top/Bottom</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#unisex" role="tab">Unisex</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#household" role="tab">Household</a>
    </li>
</ul> 
 
<!-- Tab panels -->
<div class="tab-content card" style="width:100%">
    <!--Panel 1-->
    <div class="tab-pane fade in show active" id="top" role="tabpanel">
	<br>
  <table class="table"> 
    <thead class=" ">
        <tr >
		<th >#</th>
            <th class="text-center">Item Name</th>
				<th class="text-center">Dry Cleaning(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				<th class="text-center">Wash & Press(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				<th class="text-center">Iron Only(<?php echo env('CURRENCY_SYMBOL');?>)</th>
        </tr>
    </thead> 
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>T - Shirt</td>
            <td>69 </td>
            <td>49 </td>
			 <td>29</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td> Jeans</td>
            <td>69</td>
			 <td>49</td>
            <td>29</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>Skirt</td>
            <td>69</td>
			 <td>49</td>
            <td>29</td>
        </tr>
		 <tr>
            <th scope="row">4</th>
            <td>Silk/Leather</td>
            <td>99</td>
			 <td>-</td>
            <td>-</td>
        </tr>
    </tbody> 
</table>  
    </div>
    <!--/.Panel 1-->
    <!--Panel 2-->
    <div class="tab-pane fade" id="unisex" role="tabpanel">
        <br>
          <table class="table"> 
    <thead class=" ">
        <tr >
		<th >#</th>
            <th class="text-center">Item Name</th>
				<th class="text-center">Dry Cleaning(<?php echo env('CURRENCY_SYMBOL');?>)</th> 
        </tr>
    </thead> 
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Jacket / Coat Regular</td>
            <td> 209</td> 
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Shawl Regular </td>
            <td>169</td> 
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>Gloves</td>
            <td>69</td> 
        </tr>
    </tbody> 
</table>
    </div>
    <!--/.Panel 2-->
    <!--Panel 3-->
    <div class="tab-pane fade" id="household" role="tabpanel">
        <br>
        <table class="table"> 
    <thead class=" ">
        <tr >
		<th >#</th>
            <th class="text-center">Item Name</th>
				<th class="text-center">Dry Cleaning(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				<th class="text-center">Wash & Press(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				<th class="text-center">Iron Only(<?php echo env('CURRENCY_SYMBOL');?>)</th>
        </tr>
    </thead> 
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Bedsheet(Single/Double)</td>
            <td> 129</td>
            <td> 109</td>
			 <td>79</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Towel </td>
            <td>59</td>
			 <td>49</td>
            <td>-</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>Curtain-Regular (Per sqft)</td>
            <td>11</td>
			 <td>7</td>
            <td>-</td>
        </tr>
    </tbody> 
</table>
    </div>
    <!--/.Panel 3-->
</div>
          </div>
			 
	      <div class="row text-center mt-3 ">
			 <a href="{{URL::asset('services')}}" class="btn btn-danger font-weight-bold mx-auto white-text mb-2" > <h4 class="flex-center">SEE ALL</h4> </a>
		  </div>
	
	    </div>
		</section>
	 <!----------------OFFERS SECTION ENDS HERE---------------------> 
	 
	   
	  
	  
	  <!---------------CUSTOMERS VIEWS SECTION STARTS HERE----------------
	  
	    <div class="container white mt-2 mb-4">
          <div class="row justify-content-sm-center">
		   <div class="col-sm-12">
		    <h4 class="text-center font-weight-normal heading"> We make you smile!</h4>
			<p class="text-center font-weight-normal description">What our customers say:</p>
			<span class="border-center"></span>
		   </div>
		  </div>
			
		  <div class="row white mt-2 mb-4">
		  
			<div class="col-md-7 testimonial" >
				   
			  <div class="row">
				<div class="flex-center col-md-3 col-sm-3"  >
				  <img src="{{ URL::asset('web/img/c1.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
                </div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted">“I could never find time for laundry, and let’s not even talk about ironing. With <?php //echo $project_title;?>, laundry and ironing are off my to-do list for good. And so is dry cleaning.”</p>
				  <h3><span>  <b>Chris</b></span><b>, Developer</b></h3>
                </div>
              </div>					

              <div class="row mt-2">
                <div class="flex-center col-md-3 col-sm-3">
				   <img src="{{ URL::asset('web/img/c2.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
                </div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted" > “I can’t even remember the amount of expensive clothes I’ve ruined. With <?php //echo $project_title;?>, my silk shirts, cashmere sweaters, and blouses are all in good hands, finally!”</p>
				  <h3 ><span> <b>Bella</b></span><b>, CA</b></h3>
				</div>	
			  </div>

              <div class="row mt-2 ">
                <div class="flex-center  col-sm-3 col-md-3">
				  <img src="{{ URL::asset('web/img/c3.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
				</div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted">“My family goes through so many clothes a week, there’s always mountains of laundry to do. Now that I use <?php //echo $project_title;?>, I spend Sundays with the kids rather than the washing machine.”</p>
				  <h3 ><span> <b>Ellena</b></span><b>, Lecturer</b></h3>
                </div>	
              </div>					
			
			</div>
				 
			<div class="col-md-5 mt-2 ">
			  <div class="flex-center">
                  <img src="{{ URL::asset('web/img/satisfied-customers.png')}}" class="img-responsive mx-auto d-block" width="50%">
              </div> 
			</div>
				 
		  </div>
             
		</div>
		   
	 <!---------------CUSTOMERS VIEWS SECTION STARTS HERE----------------->
	  
	 <!------------Second last section-------------->
	  
	  <div class="container-fluid"  style="background-color:#EFEFEF">
	    <div class="row">
		  <div class="col-lg-10 col-lg-offset-1">
		    <div class="row mb-2 mt-0 readmore">
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			     <div class="row laundry">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <img src="{{ URL::asset('web/img/faq.png')}}" class="img-responsive">
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>FAQ</h3>
					  <p>Find out about all your queries.</p>
					  <a href="{{URL::asset('faq')}}">Read More</a>
					</div>
				 </div>
			  </div>
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			  <div class="row laundry ">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <img src="{{ URL::asset('web/img/help.png')}}" class="img-responsive">
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>Need Help!</h3>
					  <p>Discover the care for your Garments</p>
					  <a href="{{URL::asset('contact')}}">Read More</a>
					</div>
				 </div>
			  </div>
			</div>
		  </div>
		</div>
		</div>
	  
	 
	 <!------------Second last section-------------->
	 
	 </div>
		</div>
	</div>
	
  <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
	 @include('includes.footer-web')
			
	
	@endsection