@extends('layout.auth_web')
@section('title', 'Login' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
<style>

.md-form label{
	font-weight:200;
}


</style>




 
     <!---------Navbar section starts here--------->
           
     <!---------Navbar section ends here--------->
 
 
 
 
     <!---------CONTENT SECTION STARTS HERE------>
	<div ng-app="mainApp" ng-controller="loginController">	   
		    
		   
		   <div class="container mt-5 mb-4" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
    <div class="card-block">

        <!--Header-->
        <div class="form-header "  style="background-color:#B8152F;">
            <h3><i class="fa fa-lock" style="color:#fff"></i> Login</h3>
        </div>

        <!--Body-->
		<div class="row">
		<div class="col-sm-10 offset-sm-1">
		
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="email" id="form2" name="userName" class="form-control validate" ng-model="userName" required>
            <label for="form2" data-error="wrong" >Your email / Phone</label>
        </div>

        <div class="md-form mt-2">
            <i class="fa fa-lock prefix"></i>
            <input type="password" name="password" id="form4" class="form-control validate" ng-model="password" required>
            <label for="form4" data-error="wrong" >Your password</label>
        </div>

		 <div class="md-form">
        <div class="text-center">
            <button type="button"  class="btn btn-danger" ng-click="login()">Login</button>
        </div>
		</div>
		
		
</div>
    </div>
	 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">
	
	
	</div>
  
 
 <!--------message box ends-------------->

    <!--Footer-->
    <div class="modal-footer">
        <div class="options"> 
            <p>Forgot <a href="{{URL::asset('forgot_password')}}" style="color:#653B7D;">Password?</a></p>
			<span>Not a member? <a href="{{URL::asset('register')}}" style="color:#653B7D;">Register Here </a></span>
			 
        </div>
    </div>
</div>
</div>
<!--/Form with header-->
		    </div>
    </div>
		</div>   
		
		</div>
		
		<!-------------CONTENT SECTION ENDS HERE-------------->
	 

 
 
 
 
<!-- SCRIPTS -->
    <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script>   
	
	<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/web_login.js')}}"></script> 
   <script>
  
   var app = angular.module('myApp',['ngRoute','ngStorage']);
 

   app.controller('loginController',function($scope, $http, $window, $location, $rootScope){
	
	  
		
})

   
   </script>

 @include('includes.footer-web')
			
	
	@endsection