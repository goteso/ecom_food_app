@extends('layout.auth_web_new')
@section('title', 'Order Detail' )

	<style>
	 .steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#000;
	}
	.navbar{
		height:70px;
	}
	.navbar .navbar-right{
		padding-top:12px;
	}
	navbar .navbar-brand{
		padding-top:5px;
	}
 
	</style>
@section('content')
@section('header')
@include('includes.header-web-new')
@show 
     
 

     
<div ng-app="mainApp" ng-controller="orderDetailController" ng-init="orderDetail()" style="background-color:">
 
   <div class="container">
      <div class="row">
	    <div class="col-lg-12">
		  <h3 class="text-center">Order Details</h3>
		  <span class="border-center"></span>
			<div class="panel panel-default">
                <div class="panel-body">
				<div class="" style="margin:10px"><h4><b>@{{BlockDetail.title}}</b></h4></div>
				  <table class="table">
				   <thead>
				   <tr>
				   </tr>
				   </thead>
				   <tbody>
				   
				   <tr ng-repeat="o in BlockDetail.data">
				     <td>@{{o.title}}</td>
					 <td>@{{o.quantity }} * @{{o.discounted_price}}</td>
					 <td><?php echo env('CURRENCY_SYMBOL');?>@{{ o.discounted_price}}</td>
					 <!--<td class="actions">
                        <a class="btn btn-xs edit-product"  data-toggle="modal" data-target="#myModal_edit" ng-click="user_address_forms(values.id)" ><i class="fa fa-edit"></i></a>
                        <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"> </i></a>  
                     </td>-->
				   </tr>
				   
				   <tr>
				   <td>Sub total</td>
				   <td></td>
				   <td><?php echo env('CURRENCY_SYMBOL');?>@{{ BlockDetail.total }}</td>
				   <td></td>
				   </tr>
				   </tbody>
				  </table>
				</div>
			</div>
		</div>
	  </div>
	  <div class="row">
	  <!-- <div class="col-sm-4 col-lg-4">
	      <div class="panel panel-default">
		
                <div class="panel-body" ng-repeat="c in CutomerDetail">
				 <p><b>@{{c.title}}</b></p>
				 <p><pre style="border:none;background-color:transparent">@{{c.description}}</pre></p><br>
				</div>
		  </div>
	   </div>-->
	   <div class="col-sm-4 col-lg-4">
	      <div class="panel panel-default">
                <div class="panel-body" ng-repeat="a in AddressDetail.data">
				 <p><b>@{{AddressDetail.title}}</b></p>
				 <p>@{{a.address_line1}}</p>
				  <p>@{{a.address_line2}}</p>
				   <p>@{{a.city}}</p>
				    <p>@{{a.state}}</p>
					 <p>@{{a.pincode}}</p>
				</div>
		  </div>
	   </div>
	   <div class="col-sm-4 col-lg-4">
	      <div class="panel panel-default">
                <div class="panel-body" >
				<p><b>@{{PaymentDetail.title}}</b></p>
				<table class="table"> 
                <tr  ng-repeat="p in PaymentDetail.data">
				<td>
				<p>@{{p.title}}</p>
				</td>
				<td class="text-right">
				<p><?php echo env('CURRENCY_SYMBOL');?>@{{p.value}}</p>
				</td>
				</tr>
				<tr>
				 <td>TOTAL</td>
				 <td class="text-right"><?php echo env('CURRENCY_SYMBOL');?>@{{PaymentDetail.order_total}}</td>
				</tr>
				
				<tr>
				 <td>PAID</td>
				 <td class="text-right"><?php echo env('CURRENCY_SYMBOL');?>@{{PaymentDetail.total_paid}}</td>
				</tr>
				
				<tr>
				 <td>TO PAY</td>
				 <td class="text-right"><?php echo env('CURRENCY_SYMBOL');?>@{{PaymentDetail.order_total - PaymentDetail.total_paid}}</td>
				</tr>
				
				</table>
				</div>
		  </div>
	   </div>
	   <div class="col-sm-4 col-lg-4">
	      <div class="panel panel-default">
                <div class="panel-body">
				<p><b>@{{OrderSummary.title}}</b></p>
				<table class="table">
				  
				  <tr  ng-repeat="o in OrderSummary.data">
				   <th>@{{o.display_title}}</th>
				    <td>@{{o.value}}</td>
				   </tr>

				 
				   
				</table>
				</div>
		  </div>
	   </div>
	  </div>
   </div>
   </div>
 
   
   <script>
   var app = angular.module('mainApp',[]);
   app.controller('orderDetailController',function($scope, $http){
	   $scope.orderDetail = function(){
		   <?php $id = $_GET['id'];
		   ?>
		   
		   $scope.fetchedId = <?php echo $id; ?>
		   
	 $scope.orderId = {"order_id":$scope.fetchedId}
	 var request = $http({
		method: "POST",
		url: APP_URL+'/api/order_details?order_id='+$scope.fetchedId,
		data: $scope.orderId,
		headers: { 'Accept':'application/json'  }
		}).then(function (data, status, headers, config) { 

		$scope.OrederDetailData = data;
		$scope.BlockDetail = data.data.blocks[0].data;
		$scope.CutomerDetail = data.data.blocks[1].data;
		$scope.AddressDetail = data.data.blocks[2].data;
		$scope.PaymentDetail = data.data.blocks[3].data;
		$scope.OrderSummary = data.data.blocks[4].data;
		
		
		},function(data, status, headers, config) {
		alert(JSON.stringify(data));
		});
 }
   })
   app.filter('newlines', function () {
    return function(text) {
        return text.replace(/\n/g, '<br/>');
    }
})
	app.filter('noHTML', function () {
		return function(text) {
			return text
					.replace(/&/g, '&amp;')
					.replace(/>/g, '&gt;')
					.replace(/</g, '&lt;');
		}
	});
   
   

     
   </script>
 @include('includes.footer-web')
			
	
	@endsection