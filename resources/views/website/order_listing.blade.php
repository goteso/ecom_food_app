@extends('layout.auth_web_new')
@section('title', 'Orders' )
@section('content')
@section('header')
@include('includes.header-web-new')
@show   
     
	<style>
	 .steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#000;
	}
	.navbar{
		height:70px;
	}
	.navbar .navbar-right{
		padding-top:12px;
	}
	navbar .navbar-brand{
		padding-top:5px;
	}
 
	</style>
 

      
     

<div ng-app="mainApp" ng-controller="addressController" ng-init="onload()">
 
  <div class="container mt-2">
  
	<div class="row">
	<div class="col-sm-12">
	   <p class="text-center" style="margin:15px;font-size:20px;text-align:center!important"><b>Order Listing</b></p>
	   <span class="border-center "></span>
	   </div>
	   
	   <div class="col-sm-12 text-right right-add-btn"  style="padding:5px 30px;">
            <a href="{{URL::asset('add-new-order')}}"><button class="btn btn-danger btn-add-new" style="margin-top:30px">Add Order</button> </a>
         </div>
		 
	</div>	
   
   <div class="row">
	<div class="col-sm-12">
		<div class="panel">
			<div class="panel-body user-info">
				<table class="table table-bordered" id="exportthis "  >
					<thead>
						<tr class="text-center" >
						<th class="text-center"> Sr. No.</th>
						<th class="text-center">ID</th>
						<th class="text-center">Order Status</th>
						<th class="text-center">Total</th>
						<th class="text-center">Created At</th>

						<!--<th class="text-center">Action</th>-->
						</tr>
					</thead>
					<tbody>
						<tr   ng-repeat="c in columns_addresses"  class="text-center" >
						<td>@{{$index+1}}</td>
						<td><a ng-href="{{URL::asset('order_detail')}}?id=@{{c.id}}" ng-click="orderDetail()" target="_blank">@{{c.id}}</a></td>
						<td>@{{c.order_status}}</td>
						<td><?php echo env('CURRENCY_SYMBOL');?>@{{c.total}} </td>
						<td>@{{c.created_at_formatted}}</td>
 

						<!--<td class="actions">
						<span>
						<!--<a class="btn btn-xs edit-product"  data-toggle="modal" data-target="#myModal_edit" ng-click="user_address_forms(values.id)" ><i class="fa fa-edit"></i></a>--
						<a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"> </i></a> </span>
						</td>-->
						</tr>
					</tbody>
				</table>
			</div> 					 
		</div>		

 

		</div>
</div></div>

   
   
  
   </div>
           
 
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script> 
 <script>
var app = angular.module("mainApp", ['ngRoute']); 
app.controller('addressController', function($http,$scope) {
 $scope.onload = function(){
	  
		$scope.id = '33';
	 
		$scope.Data = {"user_id":$scope.id,"order_status":"Pending"}
      
		var request = $http({
		method: "POST",
		url: APP_URL+'/customers_orders_list',
		data: $scope.Data,
		headers: { 'Accept':'application/json' }
		}).then(function (data, status, headers, config) { 

		$scope.addressData = data;
		if($scope.addressData == ""){
			alert('There are no orders yet!');
		}
		else{
			$scope.columns_addresses = data.data.order_data;
		}
		
		
		
		//alert(JSON.stringify($scope.addressData ));
		//$scope.columns_addresses = data.data.order_data;
		//alert(JSON.stringify($scope.columns_addresses));
		
		
		},function(data, status, headers, config) {
		//alert(JSON.stringify(data));
		});
 } 

	
	
});
</script>
 @include('includes.footer-web')
			
	
	@endsection