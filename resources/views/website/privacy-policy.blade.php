@extends('layout.auth_web')
@section('title', 'About' )
@section('content')
@section('header')
@include('includes.header-web')
@show
  
 
   

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		<div class="container mt-5 mb-4" >
		 <div class="row">
		  <div class="col-sm-10 offset-sm-1">
		   <div class="card">
            <div class="card-block">

             <!--Header-->
             <div class="form-header" style="background-color:#B8152F;">
              <h3><i class=""></i> PRIVACY POLICY</h3>
             </div>

             <!--Body-->
		     <div class="row">
		      <div class="col-sm-10 offset-sm-1 text-justify">
		       
			
					<p>By using the website and/or registering yourself at launspace.com you authorize us, our affiliates &amp; our associate partners to contact you via email or phone call or SMS and offer you their services for the product you have opted for, imparting product knowledge, offer promotional offers running on website &amp; offers offered by the associated third parties, for which reasons, as well as web aggregation, personally identifiable information may be collected as detailed below. And irrespective of the fact if also you have registered yourself under DND or DNC or NCPR service, you still authorize us to give you a call from the Launspace services Pvt Ltd for the above mentioned purposes for your registration with us. This Privacy Policy covers Launspace services Pvt Ltd.'s treatment of personally identifiable information that Launspace services Pvt Ltd collects when you are on the Launspace services Pvt Ltd site, and when you use Launspace services Pvt Ltd.'s services. This policy also covers Launspace services Pvt Ltd.'s treatment of any personally identifiable information that Launspace services Pvt Ltd.'s business partners share with Launspace services Pvt Ltd. This policy does not apply to the practices of companies that Launspace services Pvt Ltd. does not own or control or to people that Launspace services Pvt Ltd. does not employ or manage.</p>
					
					<p>Launspace services Pvt Ltd. collects personally identifiable information when you register for a Launspace services Pvt Ltd. account, when you use certain Launspace services Pvt Ltd. products or services, when you visit Launspace services Pvt Ltd. pages, and when you enter promotions or sweepstakes. Launspace services Pvt Ltd. may also receive personally identifiable information from our business partners. When you register with Launspace services Pvt Ltd., we ask for your first name, last name, state, city, address, email, birth date, and gender. Once you register with Launspace services Pvt Ltd. and sign in to our services, you are not anonymous to us. Also, during registration, you may be requested to register your mobile phone and email idor other device to receive text messages, notifications, and other services to your wireless device. By registration you authorize us to send sms/email alerts to you for your login details and any other service requirements or some advertising messages/emails from us.</p>
					
					<p><strong>Information Sharing and Disclosure</strong></p>
					
					<p>Launspace services Pvt Ltd. will not sell or rent your personally identifiable information to anyone other than as specifically noted herein. Notwithstanding the foregoing, Launspace services Pvt Ltd. may share, sell and/or transfer your personally identifiable information to an affiliate and/or associate partner. Launspace services Pvt Ltd. may also share, sell, and/or transfer your personally identifiable information to any successor-in-interest as a result of a sale of any part of Launspace services Pvt Ltd.'s business or upon the merger, reorganization or consolidation of Launspace services Pvt Ltd. with another entity on a basis that Launspace services Pvt Ltd. is not the surviving entity. For the purposes of this paragraph, &ldquo;affiliate&rdquo; means any person directly, or indirectly through one or more intermediaries, that controls, is controlled by or is under common control with Launspace services Pvt Ltd. The term &ldquo;control,&rdquo; as used in the immediately preceding sentence, shall mean with respect to any person, the possession, directly or indirectly, of the power, through the exercise of voting rights, contractual rights or otherwise, to direct or cause the direction of the management or policies of the controlled person. As used in this Privacy Policy, the term &ldquo;person&rdquo; includes any natural person, corporation, partnership, limited liability company, trust, unincorporated association or any other entity.</p>
					
					<p>&ldquo;If you are no longer interested in receiving e-mail announcements and other marketing information from us, or you want us to remove any PII that we have collected about you, please e-mail your request to care@launspace.com</p>
					
					<p>&ldquo;We use third-party service providers to serve ads on our behalf across the Internet and sometimes on this site. They may collect anonymous information about your visits to our website, and your interaction with our products and services. They may also use information about your visits to this and other websites to target advertisements for goods and services. This anonymous information is collected through the use of a pixel tag, which is industry standard technology used by most major websites. No personally identifiable information is collected or used in this process. They do not know the name, phone number, address, email address, or any personally identifying information about the user.&rdquo;</p>
					
					<p><strong>Legal</strong></p>
					
					<p>We strictly follow the norms of non disclosure agreement. It is our endeavour to ensure that the websites linked to us are safe and secure. However we have no control over 3rd party websites. Given our legal responsibilities to legal authorities and regulators, there may be situations when we need to share your information with legal authorities or processes or need to protect our right or comply by terms and conditions.</p>
					
					<p><strong>Privacy Policy Changes</strong></p>
					
					<p>Launspace services Pvt Ltd. may amend this policy from time to time, at our discretion. If we make any substantial changes in the way we use your personal information we will notify you by posting the changes here on these Privacy Policy pages.</p>
					
					<p><strong>Disclaimer</strong></p>
					
					<p>&ldquo;We use cookies and other technologies such as pixel tags and clear gifs to store certain types of information each time you visit any page on our website. Cookies enable this website to recognize the information you have consented to give to this website and help us determine what portions of this website are most appropriate for your professional needs. We may also use cookies to serve advertising banners to you. These banners may be served by us or by a third party on our behalf. These cookies will not contain any personal information.&rdquo;</p>
      
	          </div>
             </div>
			 
			
	       </div>
          </div>
      <!--/Form with header-->
		  </div>
         </div>
		</div>   
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		 <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
 @include('includes.footer-web')
			
	
	@endsection
 

