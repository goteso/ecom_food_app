@extends('layout.auth_web')
@section('title', 'Services' )
@section('content')
@section('header')
@include('includes.header-web')
@show
    
    
     
	<style>
	 .steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#000;
	}
	
	
	h1,h2,h3,h4,h5,p{font-family:"Open Sans"
}


#top_header {
background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}
[ng-cloak]{
    display:none; important!
}
 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}

b{font-weight:700;}
  

	</style>


<div ng-app="myApp"> 

		<div  ng-controller="cartForm" >
		
		
		 
	  <!---
		<div class="container-fluid  p-0" >
		<div class="row"> 
          <img src="{{URL::asset('web/img/top_section.jpg')}}" class="img-responsive" width="100%"> 
		 </div> 
		 </div> -->
		 
		 <div class="container" >
		   <div class="row">
		   <div class="col-sm-12  " style="padding:15px 0;">
		 <div class="panel-group product-view" id="accordion">                         
                                       <div ng-repeat="categories in addOrder " id ="@{{categories.category_title}}"  >  
                                          <div ng-show="categories.products != '' " style="border:1px solid #ccc;margin-bottom:8px;">
                                             <div class="panel ">
                                                <div class="panel-heading">
                                                   <h4 class="panel-title text-left">
                                                      <b>@{{categories.category_title | uppercase}}</b> <span>(@{{categories.products.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#1@{{categories.category_title}}">  <i class="fa fa-angle-down" style="float:right; position: relative;"></i></a>
                                                   </h4>
                                                </div>
                                                <div id="1@{{categories.category_title}}" class="panel-collapse collapse in">
                                                   <table class="table items-detail">
												     <thead>
													  <tr>
													  <th class="text-center">S.No.</th>
													     <th class="text-center">Item Name</th>
				                                         <th class="text-center">Dry Cleaning(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				                                         <th class="text-center">Wash & Press(<?php echo env('CURRENCY_SYMBOL');?>)</th>
				                                         <th class="text-center">Iron Only(<?php echo env('CURRENCY_SYMBOL');?>)</th>
													  </tr>
												     </thead>
                                                      <tbody>
                                                         <tr ng-repeat="product in categories.products | filter:search"  >
                                                            <td class="item-title text-center">@{{$index+1}}  </td>					 
                                                            <td class="item-title text-center">  @{{product.title}} </td>
															<td class="text-center"  ng-show="product.variants[0].title == 'Dry Cleaning' " >@{{product.variants[0].price_difference}}</td>
															<td class="text-center"  ng-show="product.variants[0].title == 'Wash & Press' ">@{{product.variants[0].price_difference}}</td>
															<td class="text-center"  ng-show="product.variants[0].title == 'Only Press' ">@{{product.variants[0].price_difference}}</td>
															<td> 
															</td> 
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div> 
	         </div>
		 
	   </div>
	      </div>
		   </div>
	   </div>
	   
</div>
	</div>
	  
 <script>
    var app = angular.module('myApp', []);
    app.controller('cartForm', function ($scope,$http) {
        
		 $http.get(APP_URL+'/add_order_form_site')
		 .then(function (response) { 
		
		 $scope.addOrder = response.data.categories_products;
		  //alert(JSON.stringify($scope.addOrder));
		 });
	
 });

 
      </script> 




	 @include('includes.footer-web')
			
	
	@endsection