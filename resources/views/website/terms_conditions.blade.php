@extends('layout.auth_web')
@section('title', 'About' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 

 
 

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		<div class="container " >
		 <div class="row mt-5 mb-4">
		  <div class="col-sm-12">
		   <div class="card">
            <div class="card-block">

             <!--Header-->
             <div class="form-header" style="background-color:#B8152F;">
              <h3><i class=""></i> Terms & Conditions</h3>
             </div>

             <!--Body-->
		     <div class="row">
		      <div class="col-sm-12 ">
		        
			   
           
			   <div class="row">
				 
						<div class="col-md-10 col-md-offset-1 mt-1 text-justify" data-delay="400">	 
							<!--<p><strong>BUSINESS TERMS</strong></p>-->
							
							<p><strong>Terms &amp; conditions</strong></p>
							<p>Launspace strives to provide the best quality of cleaning as per standardized methods and care; however, all articles are accepted at customers&rsquo; risk.Launspace services Pvt Ltd. will not be liable for damage arising due to the normal cleaning process. Launspace accepts no responsibility of colour bleed and/or shrinkage.</p>
							<p>Any damage must be brought to notice within 24 hrs of receipt of garment.</p>
							<p>In case of misplaced or damaged garment, the maximum liability will be up to 5 times of the garment billing value in form of service credits.</p>
							<p>Unclaimed garments would be safely kept with us for a maximum period of 30 days after which Launspace services Pvt Ltd. cannot be held liable for loss or any article. Also, a Safe Keeping Charge of Rs.5/- per piece per day will be charged post 15 days from the delivery date.</p>
							<p>Launspace is not responsible for loss or damage to any personal or non cleaning articles left in the clothing or bags such as money, jewellery or an article.</p>
							<p>Launspace accepts garments with stains on a best effort basis. There is no assurance for removal of these stains.</p>
							<p>Home convenience fees as per our price list and Order cancellation charges of 15% will apply.</p>
							<p>These Terms and conditions are subject to change without prior notice.</p>
							<p>Please check all the garments at time of delivery.</p>
							<p>All Taxes as applicable will be levied.</p>
							<p>For any feedback, please write in to us at care@launspace.com or call us on 8427-675-675.</p>
							
							<p><strong>Refund Policy</strong></p>
							<p>No refunds will be eligible if the cleaning service not up to the satisfaction or expectation of any individual.</p>
							<p>If the customer pays twice for one transaction, the one additional transaction amount will be refunded via the same source within 15 to 20 working days.</p>
							<p><strong>Cancellation Policy</strong></p>
							<p>Cancellations are permissible 24 hours before garment processing. Logistic charges may apply on cancellations. The entire amount or any deduction will be refunded via original method of payment within 10 to 15 working days.</p>
							
						</div>
               </div>
								 
              </div>
             </div>
			 
	        </div>
           </div>
      <!--/Form with header-->
	  
		  </div>
         </div>
		</div>   
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		 <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
		
       
 @include('includes.footer-web')
			
	
	@endsection
 




