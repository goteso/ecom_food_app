
<?php
	include_once("header.php");
?>

<body ng-app="mainApp" ng-controller="addressController" ng-init="onload()">
  <div class="container-fluid">
  <div class="row">
  
     <!-- <div class="col-sm-10" > 
	     <ul class="nav nav-tabs" >
			 <li  class="" ><a href="#/" data-toggle="tab"   >Basic Detail</a></li>
			 <li  class="" ><a href="#/order" data-toggle="tab"   >Orders</a></li>
			 <li  class="active" ><a href="#/address" data-toggle="tab"   >Address</a></li> 
         </ul>
			</div> -->
<div class="col-sm-2 right-add-btn"  >
   <button class="btn  btn-add-new" data-toggle="modal" data-target="#myModal_add" ng-click="add_address_form()">Add Address</button> 
</div> 			 
			 
      </div>			
          <!--<div class="tab-conten  tab-content-datat">
             <div id="/" class="tab-pane fade in  active" > -->
			  
   
			 <div class="panel">
			 <div class="panel-body user-info">
                  
				 
				  <table class="table" id="exportthis "  >
                                                   <thead>
                                                      <tr>
                                                         <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" /></th>
                                                         <th ng-repeat="c in columns_addresses | orderBy:'index'"  ng-show="c.visible" ng-if="c.visible == 1" >{{c.title}}</th> 
                                                         <th>ACTIONS</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody >
                                                      <tr  ng-repeat="values in data_addresses.data ">
                                                         <td>
                                                            <input type="checkbox" ng-checked="" ng-model="values.selected" ng-click="checkedIndex(values)"> 
                                                            <h1></h1>
                                                         </td>
                                                         <td ng-repeat="c in columns_addresses" class="" ng-show="c.visible == 1" >
														  <span class="" ng-show="$index == '0'">{{values.id}} </span>
                                                            <span class="" ng-show="$index == '1'">{{values.address_line1}} </span>
                                                            <span class="name" ng-show="$index == '2'">{{values.address_line2}} </span>
                                                            <span class="price" ng-show="$index == '3'">{{values.city}} </span>
                                                            <span class="created" ng-show="$index == '4'">{{values.state}} </span>
                                                            <span class="created" ng-show="$index == '5'">{{values.country}} </span>
                                                            <span class="created" ng-show="$index == '6'">{{values.pincode}} </span>
                                                            <span class="created" ng-show="$index == '7'">
                                                               <pre>{{values.created_at}}</pre>
                                                            </span>
                                                         </td>
                                                         
                                                         <td class="actions">
                                                            <a class="btn btn-xs edit-product"  data-toggle="modal" data-target="#myModal_edit" ng-click="user_address_forms(values.id)" ><i class="fa fa-edit"></i></a>
                                                           <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"> </i></a>  
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
												
												
			 
			 
                  </div> 					 
				</div>		

 			
              <!--</div>--> 
			  <!--</div>--> 
               
     </div>
   </div>
   
   
   
   
    <!------------------------------------------- Add address  Modal starts ------------------------------------------------>
                                    <div id="myModal_add" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"  style="width:80%;z-index:9999999999999999999" data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Add New Address</h4>
                                             </div>
                                             <div class="modal-body" >
                                               
								 <form name="myForm">
                     
                        <div class="row">
                           <div class="col-lg-6 col-sm-12" ng-repeat="data in columns_add_address_data[0].fields">
						   
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'text'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Email Type Input Field----------->
                              <div class="form-group " ng-show="data.type == 'email'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required> 
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'password'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'radio'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'checkbox'">
                                 <label for="{{data.identifier}}">{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="{{data.type}}"  ng-model="data.value[options.value]" id="{{options.value}}"  >{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" >{{options.title}}
								 </label>
                              </div>
							  
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'Selectbox'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'multipleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'singleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  </div>
							 <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-show="data.symbol">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">{{data.symbol}}</span>
                                    <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}"  >
                                 </div>
                              </div>
							  </div>
							<div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'datePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  />
                                 </datepicker>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'timePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  </div>
						<div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"ng-show="data.type == 'longText'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <textarea class="form-control" name="{{data.identifier}}"ng-model="data.value"  id="{{data.identifier}}" >{{data.value}}</textarea>
                              </div>
							  
                           </div>
                           
						   
                        </div>
                   
					 
					 
                              <button   ng-click="add_address()" class="btn btn-success"> Add </button>
                  </form>
				  
										   
										   
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
           <!------------------------------------ Edit addresses ends -----------------------------------> 
		   
		   
   
      
		   <!------------------------------------------- Edit address  Modal starts ------------------------------------------------>
                                    <div id="myModal_edit" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"  style="width:80%;z-index:9999999999999999999" data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Address</h4>
                                             </div>
                                             <div class="modal-body" >
                                      
										   
								 <form name="myForm">
                     
                        <div class="row"> 
                           <div class="col-lg-6 col-sm-12" ng-repeat="data in columns_edit_address_data[0].fields">
						   
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'text'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Email Type Input Field----------->
                              <div class="form-group " ng-show="data.type == 'email'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required> 
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'password'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'radio'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">@{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'checkbox'">
                                 <label for="{{data.identifier}}">{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="{{data.type}}"  ng-model="data.value[options.value]" id="{{options.value}}"  >{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" >{{options.title}}
								 </label>
                              </div>
							  
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'Selectbox'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">@{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'multipleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'singleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  </div>
							 <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-show="data.symbol">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">{{data.symbol}}</span>
                                    <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}"  >
                                 </div>
                              </div>
							  </div>
							<div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'datePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  />
                                 </datepicker>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'timePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  </div>
						<div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"ng-show="data.type == 'longText'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <textarea class="form-control" name="{{data.identifier}}"ng-model="data.value"  id="{{data.identifier}}" >{{data.value}}</textarea>
                              </div>
							  
                           </div>
                           
						   
                        </div>
                   
					 
					 
                              <button   ng-click="user_address_update()" class="btn btn-success"> Save Changes </button>
                  </form>
				  
										   
										   
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
           <!------------------------------------ Add addresses ends -----------------------------------> 
   </div>

<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
<script src="https://code.angularjs.org/1.2.0/angular-animate.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>
 <script>
var app = angular.module("mainApp", ['ngRoute']); 
app.controller('addressController', function($http,$scope) {
 $scope.onload = function(){
  	 $scope.user_id =  "66";
	   $scope.data = {"user_id":66}
		 
	 
		
		 // get addresses  get data from api**/
		
        $http.get('http://192.168.0.109/ecommerce/public/user_address_list/'+$scope.user_id+'?request_type=api')
            .then(function(data, status, headers, config) {
            alert("success");
			$scope.addressData = data;
			alert(JSON.stringify($scope.addressData))
                $scope.columns_addresses = data.columns;
                $scope.data_addresses = data.data;
                $scope.currentpage_addresses = data.current_page;
                $scope.user_id = user_id;
 
            }),(function(data, status, headers, config) {
             alert(JSON.stringify(data));
                document.getElementById('res').value = JSON.stringify(data);
            });
 }

 

 
 
 	
    //get add address form
 
    $scope.add_address_form = function() {
  

        /*get data from api**/
        $http.get('http://192.168.0.109/ecommerce/public/user_address_forms')
            .success(function(data, status, headers, config) {
 
                $scope.columns_add_address_data = data;

            })
            .error(function(data, status, headers, config) {
 
                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }




    $scope.add_address = function() {
	
		 $scope.user_id =  document.getElementById("user_id").value ;
  
		 
	 
        /*get data from api**/
          var request = $http({
            method: "POST",
            url:'http://192.168.0.109/ecommerce/public/api/user_address_store/' + $scope.user_id,
            data: $scope.columns_add_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
    $('#myModal_add').modal('hide');
        
            $scope.products = data;
			//toaster.success(data.message,'Success!');
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res2").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {
         //toaster.error('Error Occurs','Error!');
            document.getElementById("res2").value = JSON.stringify(data);
        });
    }
	
	
 
  $scope.user_address_forms = function(id) { 
	    $('#myModal').modal('hide');

        /*get data from api**/
        $http.get('http://192.168.0.109/ecommerce/public/api/api/user_address_forms/' + id)
            .success(function(data, status, headers, config) {

                $scope.columns_edit_address_data = data;
                $scope.address_id = id;

            })
            .error(function(data, status, headers, config) {

                document.getElementById('res').value = JSON.stringify(data);
            });
 
    }
	
	
	    $scope.user_address_update = function() {

        var address_id = $scope.address_id; 
		  $('#myModal_edit').modal('hide');
  
        /*get data from api**/
        var request = $http({
            method: "POST",
            url: 'http://192.168.0.109/ecommerce/public/api/api/user_address_update/'+address_id,
            data: $scope.columns_edit_address_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
			  $scope.products = data;
			  toaster.success(data.message,'Success!');
            // $scope.product_id = data["user_data"][0]["product_id"];
            document.getElementById("res2").value = JSON.stringify(data);
            //$window.location.href = 'product_edit/'+$scope.product_id;
        }).error(function(data, status, headers, config) {
			//toaster.error('Error Occurs','Error!');
             document.getElementById("res2").value = JSON.stringify(data);
        });


    }
	
	
});
</script>
   </body>
   </html>