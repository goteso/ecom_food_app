@extends('layout.auth_web')
@section('title', 'About' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
  

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		<div class="container " >
		 <div class="row mt-5 mb-4">
		  <div class="col-sm-12">
		   <div class="card">
            <div class="card-block">

             <!--Header-->
             <div class="form-header" style="background-color:#B8152F;">
              <h3><i class=""></i> ABOUT US</h3>
             </div>

             <!--Body-->
		     <div class="row">
		      <div class="col-sm-12 ">
		        
			   <h4 class="mb-3"> Try <?php //echo $project_title; ?> by scheduling the pickup and get your laundry and dry clean at the tap of a button. We also do carpet dry cleaning, shoe dry cleaning, sofa cover dry cleaning and so on. </h4>
           
			   <div class="row">
				 <div class="col-md-6">
				  <div class="flex-center">
                   <img class="img-responsive mx-auto d-block" alt="cup and logo" src="{{URL::asset('web/img/about_us.png')}}">
                  </div>
			     </div>
						
                 <!--<div class="col-md-6 mt-1" data-delay="400">
                  <h2 class="text-center" style="color:#B8152F;">About <?php //echo $project_title; ?></h2>
                   <p >See our big range of departaments, who offer a lot of attention to our patients see what fits you and give us a call</p>
				   <h2 class="text-center mt-2" style="color:#B8152F;">The Story Of Our Success</h2>
                   <p><?php //echo $project_title; ?> provides premium washing and dry cleaning service leveraging mobile based technology. We pick up your dirty duds from your doorstep and deliver fresh, clean clothes back at your doorstep.</p>
                   <p><?php //echo $project_title; ?> provides affordable and convenient way of getting your wash, laundry and dryclean done with prime quality. Our instant pickup at a slot chosen by you with a turnaround time of 48 hours provides you laundry and dry cleaning with best quality. The processing of washing, laundry and dry cleaning is done in best-class setups with Italian equipment and German chemicals. We also do laundry with antiseptic wash, fabric softener and hygienic detergents. You can also visit our Live Processing Centre after scheduling a visit with us.</p>
                 </div>-->
				 
						<div class="col-md-6 mt-1 text-justify" data-delay="400">	 
							<p><strong>About Launspace</strong></p>
							<p>We are here to make laundry convenient, quick&amp; happy experience. Our simple system and incredible customer service will take the thinking out of your laundry hassle, so that you can focus on more important things.</p>
							<p><strong>Mission</strong></p>
							<p>To provide a safe and efficient laundry experience like nobody else, through concierge customer service, strategic marketing, innovative technology, and superior cleanliness.</p>
							<p><strong>Vision</strong></p>
							<p>To be #1 recommended laundry service by listening and responding to the needs of our customers.</p>
							<p>&nbsp;</p>
							<p>Stay <strong>Crisp</strong>&amp;<strong>Clean</strong> at your <strong>Convenience</strong></p>
							<p><strong>Crisp</strong></p>
							<p>Launspace laundering is usually followed up by professional ironing or pressing in order to give your garments a shiny and crisp finish.</p>
							<p><strong>Clean</strong></p>
							<p>Laundry is a service built on trust &ndash; the trust consumers give to a cleaner for proper care of their best apparel. Because some fabrics require special attention and offer no room for compromise on quality.</p>
							<p><strong>Convenience</strong></p>
							<p>Web booking, on-demand pick-up, store, helpline, home-delivery-all at your service.</p>
							<p><strong>We care, you wear</strong></p>
							<p>Launspace is a service you can trust. our attendants are highly trained, and your satisfaction is always guaranteed.</p>
						</div>
               </div>
								 
              </div>
             </div>
			 
	        </div>
           </div>
      <!--/Form with header-->
	  
		  </div>
         </div>
		</div>   
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		
		
       
	 @include('includes.footer-web')
			
	
	@endsection




