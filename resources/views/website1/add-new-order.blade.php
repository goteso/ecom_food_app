@extends('layout.auth_web')
@section('title', 'Add Order' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
<style>

h1 ,h2 ,h3 ,h4 ,h5 ,p{font-family:"Open Sans"
}


#top_header {
 background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}

 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}


</style>
	 

<div ng-app="myApp" ng-controller="addOrder"  >
 
     <!---------Navbar section ends here--------->
 
 
 
   

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		   
		   
		   <div class="container mt-5" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
    <div class="card-block">

        <!--Header-->
        <div class="form-header" style="background-color:#B8152F;">
            <h3> Add Order</h3>
        </div>

		
		<div class="row">
		<div class="col-sm-12">
		
	<div class="panel" ng-repeat="fields in addOrderMeta.fields"  >
                                    <div class="panel-body">
                                       <div ng-repeat="field in fields">
                                          <div class="" ng-show="field.type == 'select'">
                                             <h5>@{{field.title}}</h5>
                                             <md-autocomplete ng-disabled="main.isDisabled" md-no-cache="main.noCache" md-selected-item="main.selectedItem" md-search-text-change="main.searchTextChange(main.searchText)" md-search-text="main.searchText" md-selected-item-change="main.selectedItemChange(item, field)" md-items="item in main.querySearch(main.searchText)" md-item-text="item.first_name" md-min-length="0" placeholder="Select a User" md-menu-class="autocomplete-custom-template"  id="customer_id">
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> @{{item.first_name}} </span>
                                                   <span> @{{item.last_name}} </span>
                                                   </span>
                                                   <span class="item-metadata">
                                                   <span> @{{item.mobile}} </span>  
                                                   </span>
                                                </md-item-template>
                                             </md-autocomplete>
                                             <input type="text" ng-model="field.value" value="@{{customer_id_value}} == field.value " id="customer_id2" style="display:none;">
                                            
                                          </div>
                                          <div class="" ng-show="field.type == 'api'"  >
                                             <h5>@{{field.title}} 
                                                <button class="btn" ng-click="get_address(field.parent_identifier);"   style="background:transparent;"><i class="fa fa-edit"></i></button>
                                             </h5>
                                             <p>@{{selected_values.address_line1}}</p>
                                             <p>@{{selected_values.address_line2}}</p>
                                             <p>@{{selected_values.city}}</p>
                                             <p>@{{selected_values.state}}</p>
                                             <p>@{{selected_values.country}}</p>
                                             <input type="text" ng-model="field.value" value="@{{selected_values}} == field.value"  style="display: none;" id="user_address">
                                          </div>
                                          <div class="" ng-show="field.type == 'timeSlotsPicker'"  >
                                             <div class="" ng-show="field.identifier == 'pickup_time'">
                                                <h5>@{{field.title}}  <button class="btn"   ng-click="get_pickupTime()" style="background:transparent;"><i class="fa fa-edit"></i></button></h5>
                                                @{{pickupTimeShow}}
                                             </div>
                                             <div class="" ng-show="field.identifier == 'delivery_time'">
                                                <h5>@{{field.title}}  <button class="btn" ng-click="get_deliveryTime(field.parent_identifier,field.identifier);"  style="background:transparent;"><i class="fa fa-edit"></i></button>
                                                </h5>
                                                @{{deliveryTimeShow}}
                                             </div>
                                          </div>
                                          <div ng-show="field.type == 'textarea'">
                                             <h5>@{{field.title}}</h5>
                                             <div class="form-group" > 
                                                <textarea class="form-control" ng-model="field.value" ng-required="field.required_or_not == '1'" ></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <button class="btn btn-danger " ng-click="addOrder();">CREATE ORDER</button>
		</div>
		 
	
    </div>
	
</div>
 
 <!--------message box starts--------------
 
    <div class="col-sm-12 text-center" style="padding:2%;">
	
	
	</div>
 
 
 <!--------message box ends-------------->
 
<!--/Form with header-->
		    </div><br></br>
    </div>
		</div>   
		 </div>
		</div>
		
		
		<!-------------CONTENT SECTION ENDS HERE-------------->
        
      <script>
   var app = angular.module('myApp',[]);
   app.controller('addOrder',function($scope, $http){
	   
    $http.get(APP_URL+'/add_order_form')
		 .then(function (response) { 
		 
		 $scope.addOrderMeta = response.data.order_meta_fields; 
				//$scope.UserListData = data.order_meta_fields.fields.fields;
		  alert(JSON.stringify($scope.addOrderMeta));
		 });
   });
   </script>
 @include('includes.footer-web')
			
	
	@endsection
 



