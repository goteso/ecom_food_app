<?php session_start();?>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

     <title> Edit Profile</title>
 <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/compiled.min.css?ver=4.7.3">
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet" />
 


 
<style>
@font-face {
    font-family: 'Century Gothic';
    src: url('fonts/gothic.eot');
    src: url('fonts/gothic.eot?#iefix') format('embedded-opentype'),
         url('fonts/gothic.woff2') format('woff2'),
         url('fonts/gothic.woff') format('woff'),
         url('fonts/gothic.ttf') format('truetype'),
         url('fonts/gothic.svg#gothic') format('svg');
    
}

@font-face {
    font-family: 'Open Sans Light';
    src: url('fonts/OpenSans-Light.eot');
    src: url('fonts/OpenSans-Light.eot?#iefix') format('embedded-opentype'),
         url('fonts/OpenSans-Light.woff2') format('woff2'),
         url('fonts/OpenSans-Light.woff') format('woff'),
         url('fonts/OpenSans-Light.ttf') format('truetype'),
         url('fonts/OpenSans-Light.svg#gothic') format('svg');
    
}

@font-face {
  font-family:"Open Sans Condensed Light";
  src:url("fonts/OpenSans-CondLight.eot?") format("eot"),
      url("fonts/OpenSans-CondLight.woff") format("woff"),
	  url("fonts/OpenSans-CondLight.ttf") format("truetype"),
	  url("fonts/OpenSans-CondLight.svg#OpenSans-CondensedLight") format("svg");
  font-weight:normal;
  font-style:normal;}

@font-face {font-family:"Open Sans";
  src:url("fonts/OpenSans-Regular.eot?") format("eot"),
      url("fonts/OpenSans-Regular.woff") format("woff"),
	  url("fonts/OpenSans-Regular.ttf") format("truetype"),
	  url("fonts/OpenSans-Regular.svg#OpenSans") format("svg");
	font-weight:normal;
	font-style:normal;}

@font-face {
    font-family: 'Economica';
    src: url('fonts/Economica-Regular.eot');
    src: url('fonts/Economica-Regular.eot?#iefix') format('embedded-opentype'),
         url('fonts/Economica-Regular.woff2') format('woff2'),
         url('fonts/Economica-Regular.woff') format('woff'),
         url('fonts/Economica-Regular.ttf') format('truetype'),
         url('fonts/Economica-Regular.svg#economicaregular') format('svg');
    
}

h1,h2,h3,h4,h5,p{
font-family:"Open Sans";
}


#top_header {
 background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}

 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}
#loading{
  position:absolute;
  top:0px;
  right:0px;
  width:100%;
  height:80%;
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity: 0.2;
  cursor:pointer
}

</style>
	
</head>

<body ng-app="myApp" ng-controller="userProfileController">
     <!---------Navbar section starts here--------->
         <?php
	            include_once("header.php");
	        ?>
     <!---------Navbar section ends here--------->
 
 
   

     <!---------CONTENT SECTION STARTS HERE------>
		   
   <!--Section: Contact v.1-->
  <section>
   <div class="container" >
   
	
     <div class="row mt-4" id="hide">
	  <div class="col-sm-12">
	    
		<div class="card">
          <div class="card-block">
	        <div class="form-header" style="background-color:#B8152F;">
              <h2 class="">Edit Profile</h2>
			  <toaster-container>
			  </toaster-container>
            </div>
		    <div class="row mt-2">
              <div class="col-sm-12">
			  
		        
				 <div class="row">
				  <div class="col-sm-4"> 
				   <div class="flex-center">
				     <div class="">
					 
                            <div class="fileupload fileupload-new text-center" data-provides="fileupload">
                             
                                <div class="fileupload-preview fileupload-exists thumbnail m-b-20"   style="max-width:150px; max-height:150px; line-height:20px;border:1px solid #CCCCCC;">
								
							
								</div>
									<img src="img/c3.png" id="imgViewer">
                                <div class="text-center ">
								
                                 <label for="sub_category_image_file" >  <span class="fileupload-new ">Photo</span></label>
									
									
									 <!----test------>
                    
				     <input type="file" name="sub_category_image_file" id="sub_category_image_file" class="form-control" style="  background-color:red;display:none"   onchange="uploadFile1()" required  > 
                              <!--<form   name="upload" id="upload" method='post' action='' enctype="multipart/form-data" class="form-horizontal form-bordered"> 	-->
						   		 <progress id="progressBar" class="progress-bar" role="progressbar" value="0" max="100" style="border:none;width:100%;visibility:hidden; height:6px;margin-top:2%;margin-bottom:2%" ></progress>
						         		
                                  <div id="myDiv"></div>
			                    
 <!---test ends--->
 
                                    <!--<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>-->
                                </div>
                            </div>
                        </div>

				  </div>
				 </div>
				 <div class="col-sm-8">
				 
					<div id="loading">
					  <loading></loading>
					</div>

			 
				    <input type="hidden" id="sub_category_image_name"   class="form-control" name="userImage"   > 	
				  <div class="row">
				 <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-user prefix icons-sm" ></i>
                    <input type="text" id="form2" name="firstName" value = "" ng-model="firstName" class="form-control">
                    <label for="form2" data-error="wrong" data-success="right">First Name</label>
                  </div>
				  </div>
				  <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-user prefix icons-sm"></i>
                    <input type="text" id="form3" name="lastName" value ="" class="form-control">
                    <label for="form3"data-error="wrong" data-success="right">Last Name</label>
                  </div>
				</div>
				 </div>
				 
				  <div class="row">
				 <div class="col-md-6">
		          <div class="md-form">
                    <i class="fa fa-envelope prefix icons-sm"></i>
                    <input type="email" id="form2" name="email" value = "" class="form-control">
                    <label for="form2"data-error="wrong" data-success="right">Email</label>
                  </div>
				  </div>
				  
				  <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-phone prefix icons-sm"></i>
                    <input type="text" id="form15" name="mobile" value = "" class="form-control">
                    <label for="form15" data-error="wrong" data-success="right">Mobile Number</label>
                  </div>
				  </div>
				  </div>
				  
				  <div class="row ">
                   <!--First column-->
                  <div class="col-md-6">
                     <div class="md-form">
                    <i class="fa fa-home prefix icons-sm"></i>
                    <input type="text" id="date-picker" name="dob" value="" class="form-control datepicker">
                    <label for="date-picker" data-error="wrong" data-success="right">Date Of Birth</label>
                  </div>
                   </div>
                   <!--Second column-->
                   <div class="col-md-6 ">
                     
                     <h6 class="form-group" style="font-size:14px;color:#666;margin-top:-8px">Gender</h6>
				    <div class="form-inline">
					
				<fieldset class="form-group">
                 <input name="gender" type="radio" class="with-gap" id="radio4"  value = "0" required <?php  //if($gender == '0') {echo 'checked="checked"';}?> >
                 <label for="radio4">Male</label>
                 </fieldset>

                <fieldset class="form-group">
                 <input name="gender" type="radio" class="with-gap" id="radio5" value = "1" required <?php  //if($gender == '1') { //echo 'checked="checked"';}?> >
                  <label for="radio5">Female</label>
                 </fieldset>
				 
				 </div>
                   </div>
                  </div>
				  
				 </div>
				  </div>
				 
				 <div class="row">
				   <div class="col-sm-12 text-center mt-2">
                    <a href=""> <button type="button" class="btn" style="background-color:#B8152F" ng-click="pop()">Save</button></a>
                  </div>
				  </div>
				  
				  <div ng-repeat="post in posts">{{first_name}}</div>
				  </div>
                <!--</form>-->
              </div>
            
			
			<div id="res"></div>
			</div>
			
			
			 
 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">

	
	</div>
 
 
 <!--------message box ends-------------->
 
 <script>
function image_set()
{
var image = "<?php //echo $photo; ?>";
document.getElementById("imgViewer").src = "img/c3.png";
 
}
image_set();
</script>
 
 
 
 
 
 
 
 
 
 

          </div>
	    </div>
			
      </div>
     </div>
       
   </div>
  </section>
  <!--/Section: Contact v.1-->
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		
		
       
		
       
     <!------------------FOOTER SECTION STARTS HERE--------------------->       
           	
         <footer class="footer grey darken-4 pt-2 pb-2" style="margin-top:12%" >

         <div class="container-fluid ">
	       <div class="row ">
	         <div class="col-md-4 footer-logo">
			   <img src="img/logo_foot.png" class="img-responsive center-block">
			 </div>
			 <div class="col-md-2">
			   <ul>
			    <li><a href="index.php">Home</a></li>
				<li><a href="login.php">Login</a></li>
				<li><a href="services.php">Pricing</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3">
			   <ul>
			    <li><a href="about.php">About</a></li>
				<li><a href="faq.php">FAQ</a></li>
				<li><a href="privacy-policy.php">Privacy Policy</a></li>
				<li><a href="contact.php">Contact</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3">
			  <ul>
			    <h4>Connect with us:</h4>
			    <li><a href="#">Facebook</a></li>
				<li><a href="#">Twitter</a></li>
				<li><a href="#">Instagram</a></li>
			   </ul>
			 </div>
	 
	       </div>
          </div>
		</footer>
		
		
	 <!------------------- Footer ends here ---------------------->
	 <!------------------- Rights Reserved ---------------------->
	 <section>
	  <div class="container-fluid">
	 <div class="row">
	   <div class="col-lg-12 mt-1">
	     <p class="text-center copyright">All Rights Reserved © Copyright | Made with <img src="img/heart.png"> by <a href="https://www.goteso.com" target="_blank">Goteso</a></p>
	   </div>
	 </div>
	 </div>
	 </section>
	 <!------------------- Rights Reserved  ---------------------->
 
 
 
 
<!-- SCRIPTS -->
    <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script>
	<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
 <script src="js/bootstrap-fileupload.js"></script>
<script src="https://code.angularjs.org/1.2.0/angular-animate.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>
 <script>
$('.datepicker').pickadate();
</script>

<script>
   var app = angular.module('myApp',['ngRoute','toaster', 'ngAnimate']);
  
   
  app.directive('loading', function () {
    return {
        restrict: 'E',
        replace:true,
        template: '<p><img src="img/spinner.gif"/></p>', // Define a template where the image will be initially loaded while waiting for the ajax request to complete
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                val = val ? $(element).show() : $(element).hide();  // Show or Hide the loading image   
            }); 
        }
    }
});
  
   app.controller('userProfileController',function($scope, $http,toaster){

		$scope.pop = function(){
		$scope.loading = true; // Show loading 
		 $scope.user_id = {"profile_user_id":"33"};
		 
		 //alert(JSON.stringify($scope.user_id));  

		   
    var request = $http({
                method: "POST",
                url: "http://192.168.0.109/ecommerce/public/api/get_user_profile",
                data: $scope.user_id,
                headers: { 'Accept':'application/json'  }
           });
		 

           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data, status, headers, config) { 
		     
		 
		   //alert('success');
		   $scope.posts = data;
		   /* var fname = $scope.name = data.data.first_name;
		   var lname = $scope.name = data.data.last_name;
		   $scope.name = data.data.email;
		   $scope.name = data.data.mobile;*/
		   $scope.loading = false; 
		  
     		  toaster.pop('success', "Success !", "Data is inserted successfully");
		  
		   //alert(JSON.stringify($scope.posts));
                      
           }).error(function (data, status, headers, config) { 
            alert('error');
			toaster.pop('error', "title", "text");
           
       });
		   }
		  
   })

   </script>

   
</body>
</html>


