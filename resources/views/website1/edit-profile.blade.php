<?php
  session_start();
  $var = $_GET['userid'];
  $_SESSION['userid'] = $var;?>

<title>Edit Profile</title>


 
<style>
@font-face {
    font-family: 'Century Gothic';
    src: url('fonts/gothic.eot');
    src: url('fonts/gothic.eot?#iefix') format('embedded-opentype'),
         url('fonts/gothic.woff2') format('woff2'),
         url('fonts/gothic.woff') format('woff'),
         url('fonts/gothic.ttf') format('truetype'),
         url('fonts/gothic.svg#gothic') format('svg');
    
}

@font-face {
    font-family: 'Open Sans Light';
    src: url('fonts/OpenSans-Light.eot');
    src: url('fonts/OpenSans-Light.eot?#iefix') format('embedded-opentype'),
         url('fonts/OpenSans-Light.woff2') format('woff2'),
         url('fonts/OpenSans-Light.woff') format('woff'),
         url('fonts/OpenSans-Light.ttf') format('truetype'),
         url('fonts/OpenSans-Light.svg#gothic') format('svg');
    
}

@font-face {
  font-family:"Open Sans Condensed Light";
  src:url("fonts/OpenSans-CondLight.eot?") format("eot"),
      url("fonts/OpenSans-CondLight.woff") format("woff"),
	  url("fonts/OpenSans-CondLight.ttf") format("truetype"),
	  url("fonts/OpenSans-CondLight.svg#OpenSans-CondensedLight") format("svg");
  font-weight:normal;
  font-style:normal;}

@font-face {font-family:"Open Sans";
  src:url("fonts/OpenSans-Regular.eot?") format("eot"),
      url("fonts/OpenSans-Regular.woff") format("woff"),
	  url("fonts/OpenSans-Regular.ttf") format("truetype"),
	  url("fonts/OpenSans-Regular.svg#OpenSans") format("svg");
	font-weight:normal;
	font-style:normal;}

@font-face {
    font-family: 'Economica';
    src: url('fonts/Economica-Regular.eot');
    src: url('fonts/Economica-Regular.eot?#iefix') format('embedded-opentype'),
         url('fonts/Economica-Regular.woff2') format('woff2'),
         url('fonts/Economica-Regular.woff') format('woff'),
         url('fonts/Economica-Regular.ttf') format('truetype'),
         url('fonts/Economica-Regular.svg#economicaregular') format('svg');
    
}

h1,h2,h3,h4,h5,p{
font-family:"Open Sans";
}


#top_header {
 background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}

 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}

</style>
	


<body ng-app="myApp" ng-controller="userProfileController" ng-init="pop()">
     <!---------Navbar section starts here--------->
         <?php
	            include_once("header.php");
	        ?>
     <!---------Navbar section ends here--------->
 
 
    <?php echo $_SESSION['userid']?>

     <!---------CONTENT SECTION STARTS HERE------>
		   
   <!--Section: Contact v.1-->
  <section>
   <div class="container" >
   
	
  
     <div class="row mt-4">
	  <div class="col-sm-12">
	    
		<div class="card">
          <div class="card-block">
	        <div class="form-header" style="background-color:#B8152F;">
              <h2 class="">Edit Profile</h2>
			  
            </div>
	   
		    <div class="row mt-2">
              <div class="col-sm-12">
			  
		        
				 <div class="row">
				  <div class="col-sm-4"> 
				   <div class="flex-center">
				     <div class="">
					 
                            <div class="fileupload fileupload-new text-center" data-provides="fileupload">
                             
                                <div class="fileupload-preview fileupload-exists thumbnail m-b-20"   style="max-width:150px; max-height:150px; line-height:20px;border:1px solid #CCCCCC;">
								
							
								</div>
									<img src="img/c3.png" id="imgViewer">
                                <div class="text-center ">
								
                                 <label for="sub_category_image_file" >  <span class="fileupload-new ">Photo</span></label>
									
									
									 <!----test------>
                    
				     <input type="file" name="sub_category_image_file" ng-model="profile" id="sub_category_image_file" class="form-control" style="  background-color:red;display:none"   ng-change="uploadFile1()" required  > 
                              <!--<form   name="upload" id="upload" method='post' action='' enctype="multipart/form-data" class="form-horizontal form-bordered"> 	-->
						   		 <progress id="progressBar" class="progress-bar" role="progressbar" value="0" max="100" style="border:none;width:100%;visibility:hidden; height:6px;margin-top:2%;margin-bottom:2%" ></progress>
						         		
                                  <div id="myDiv"></div>
			                    
 <!---test ends--->
 
                                    <!--<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>-->
                                </div>
                            </div>
                        </div>

				  </div>
				 </div>
				 <div class="col-sm-8">
			 
				    <input type="hidden" id="sub_category_image_name"   class="form-control" name="userImage"   > 	
				  <div class="row">
				 <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-user prefix icons-sm" ></i>
                    <input type="text" id="form2" ng-model="firstName" class="form-control">
                    <label for="form2" data-error="wrong" data-success="right">First Name</label>
                  </div>
				  </div>
				  <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-user prefix icons-sm"></i>
                    <input type="text" id="form3" ng-model="lastName" class="form-control">
                    <label for="form3"data-error="wrong" data-success="right">Last Name</label>
                  </div>
				</div>
				 </div>
				 
				  <div class="row">
				 <div class="col-md-6">
		          <div class="md-form">
                    <i class="fa fa-envelope prefix icons-sm"></i>
                    <input type="email" id="form2" ng-model="email" class="form-control">
                    <label for="form2"data-error="wrong" data-success="right">Email</label>
                  </div>
				  </div>
				  
				  <div class="col-md-6">
				  <div class="md-form">
                    <i class="fa fa-phone prefix icons-sm"></i>
                    <input type="text" id="form15" ng-model="mobile" class="form-control">
                    <label for="form15" data-error="wrong" data-success="right">Mobile Number</label>
                  </div>
				  </div>
				  </div>
				  
				  <div class="row ">
                   <!--First column-->
                  <div class="col-md-6">
                     <div class="md-form">
                    <i class="fa fa-home prefix icons-sm"></i>
                    <input type="text" id="date-picker" name="dob" ng-value="<?php //echo $dob; ?>" class="form-control datepicker">
                    <label for="date-picker" data-error="wrong" data-success="right">Date Of Birth</label>
                  </div>
                   </div>
                   <!--Second column-->
                   <div class="col-md-6 ">
                     
                     <h6 class="form-group" style="font-size:14px;color:#666;margin-top:-8px">Gender</h6>
				    <div class="form-inline">
					
				<fieldset class="form-group">
                 <input name="gender" type="radio" class="with-gap" id="radio4"  value = "0" required <?php  //if($gender == '0') {echo 'checked="checked"';}?> >
                 <label for="radio4">Male</label>
                 </fieldset>

                <fieldset class="form-group">
                 <input name="gender" type="radio" class="with-gap" id="radio5" value = "1" required <?php  //if($gender == '1') { //echo 'checked="checked"';}?> >
                  <label for="radio5">Female</label>
                 </fieldset>
				 
				 </div>
                   </div>
                  </div>
				  
				 </div>
				  </div>
				 
				 <div class="row">
				   <div class="col-sm-12 text-center mt-2">
                    <a href=""> <button type="button" class="btn" style="background-color:#B8152F" ng-click="updateInfo()">Save</button></a>
                  </div>
				  </div>
				  
				 
                <!--</form>-->
              </div>
            
			
			<div id="res"></div>
			</div>
			
			
			 
 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">

	
	</div>
 
 
 <!--------message box ends-------------->
 
 <script>
function image_set()
{
var image = "<?php //echo $photo; ?>";
document.getElementById("imgViewer").src = "img/c3.png";
 
}
image_set();
</script>
 
 
 
 
 
 
 
 
 
 

          </div>
	    </div>
			
      </div>
     </div>
       
   </div>
  </section>
  <!--/Section: Contact v.1-->
		
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		
		
       
		
       
     <!------------------FOOTER SECTION STARTS HERE--------------------->       
           	
         <footer class="footer grey darken-4 pt-2 pb-2" style="margin-top:12%" >

         <div class="container-fluid ">
	       <div class="row ">
	         <div class="col-md-4 footer-logo">
			   <img src="img/logo_foot.png" class="img-responsive">
			 </div>
			 <div class="col-md-2">
			   <ul>
			    <li><a href="index.php">Home</a></li>
				<li><a href="login.php">Login</a></li>
				<li><a href="services.php">Pricing</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3">
			   <ul>
			    <li><a href="about.php">About</a></li>
				<li><a href="faq.php">FAQ</a></li>
				<li><a href="privacy-policy.php">Privacy Policy</a></li>
				<li><a href="contact.php">Contact</a></li>
			   </ul>
			 </div>
			 <div class="col-md-3">
			  <ul>
			    <h4>Connect with us:</h4>
			    <li><a href="#">Facebook</a></li>
				<li><a href="#">Twitter</a></li>
				<li><a href="#">Instagram</a></li>
			   </ul>
			 </div>
	 
	       </div>
          </div>
		</footer>
		
		
	 <!------------------- Footer ends here ---------------------->
	 <!------------------- Rights Reserved ---------------------->
	 <section>
	  <div class="container-fluid">
	 <div class="row">
	   <div class="col-lg-12 mt-1">
	     <p class="text-center copyright">All Rights Reserved © Copyright | Made with <img src="img/heart.png"> by <a href="https://www.goteso.com" target="_blank">Goteso</a></p>
	   </div>
	 </div>
	 </div>
	 </section>
	 <!------------------- Rights Reserved  ---------------------->
 
 
 
 
<!-- SCRIPTS -->
	<script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script>
	<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
	<script src="js/bootstrap-fileupload.js"></script>
	<script src="https://code.angularjs.org/1.2.0/angular-animate.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.js"></script>
	<script>
$('.datepicker').pickadate();
</script>

<script>
   var app = angular.module('myApp',['ngRoute','toaster', 'ngAnimate','ngStorage']);

   app.controller('userProfileController',function($scope, $http, toaster, $sessionStorage){
		$scope.pop = function(){
			//alert(JSON.stringify($sessionStorage.id));
		 $scope.userid = <?php echo $_SESSION['userid'];?>;
		 alert($scope.userid);
		 $scope.user_id = {"profile_user_id":$scope.userid}; 
         alert(JSON.stringify($scope.user_id ));		 
        var request = $http({
                method: "POST",
                url: 'http://192.168.0.109/ecommerce/public/user_edit_form/'+ $scope.userid + '?request_type=api',
                data: $scope.user_id,
                headers: { 'Accept':'application/json'  }
           })
           /* Check whether the HTTP Request is successful or not. */
          .then(function (data, status, headers, config) { 
		   alert('success');
		   $scope.posts = data;
		   alert($scope.posts);
		   $scope.firstName = data.data.first_name;
		   $scope.lastName = data.data.last_name;
		   $scope.email = data.data.email;
		   $scope.mobile = data.data.mobile;
		   
			  
		   //alert(JSON.stringify($scope.posts));
		  },function (data, status, headers, config) {
			  
            alert('error');
           $scope.posts = url;
		   console.log(url);
		   alert(JSON.stringify($scope.posts));
       });
	   
	   /* var url = window.location.href;
    var a = url.indexOf("?");
    var b =  url.substring(a);
    var c = url.replace(b,"");
    url = c; */
		   }
		   
		   <!--------------------------------------------->
		   
		  $scope.updateInfo = function(){
			$scope.$watch('firstName', function(val) {
            $scope.firstName = val;
			alert(JSON.stringify(firstName));
			})
			$scope.$watch('lastName', function(val) {
            $scope.lastName = val;
			alert(JSON.stringify(lastName));
			})
			$scope.$watch('email', function(val) {
            $scope.email = val;
			})
			$scope.$watch('mobile', function(val) {
            $scope.mobile = val;
			})
			$scope.$watch('profile', function(val) {
            $scope.profile = val;
			})
			 
			
		  $scope.updated_info = {"user_id":$sessionStorage.id,"first_name":$scope.firstName,"last_name":$scope.lastName,"mobile":$scope.mobile,"profile_image":$scope.profile}; 
		  alert(JSON.stringify($scope.updated_info));
		 
		 
         var request = $http({
                method: "POST",
                url: "http://192.168.0.105/ecommerce/public/api/update_profile",
                data: $scope.updated_info,
                headers: { 'Accept':'application/json'  }
           });
           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data, status, headers, config) { 
		     
		 
		   //alert('success');
		   $scope.p = data;
		   /* $scope.firstName = data.data.first_name;
		   $scope.lastName = data.data.last_name;
		   $scope.email = data.data.email;
		   $scope.mobile = data.data.mobile; */
		   
			  
		   //alert(JSON.stringify($scope.posts));
                      
           }).error(function (data, status, headers, config) { 
            alert('Error:'+'error');
           
       });
		   }
		   <!-------------------------------------------------->
		   /* $scope.uploadFile1 = function(){
		   $scope.uploadProfile= {"profile_image":"","user_id":"33"};  
           var request = $http({
                method: "POST",
                url: "http://192.168.0.109/ecommerce/public/api/get_user_profile",
                data: $scope.uploadProfile,
                headers: { 'Accept':'application/json'  }
           });
           /* Check whether the HTTP Request is successful or not. *
           request.success(function (data, status, headers, config) { 
		     
		 
		   alert('success');
		   $scope.posts = data;
		   /* $scope.firstName = data.data.first_name;
		   $scope.lastName = data.data.last_name;
		   $scope.email = data.data.email;
		   $scope.mobile = data.data.mobile;
		    *
			  
		   alert(JSON.stringify($scope.posts));
                      
           }).error(function (data, status, headers, config) { 
            alert('error');
           
       });
		   } */
   })

   </script>

   
</body>
</html>

