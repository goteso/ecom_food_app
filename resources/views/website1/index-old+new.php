
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?php //echo $project_title; ?> - Home</title>
    
    <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon">


	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/compiled.min.css?ver=4.7.3">
	<link rel="stylesheet" href="css/style.css">

 
 </head>

<body> 

 
     <!---------Navbar section starts here--------->
           <?php
	            include_once("header.php");
	        ?>
     <!---------Navbar section ends here--------->
	 
	 <!----------------Paralax Image------------->
	 <div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="parallax">
					<div><img src="img/goteso-logo-white.png" class="center-block img-responsive logo"></div>
	   
						<p style="color:#fff" class="text-center logo-name">Laundry & Lot at your Doorstep</p>
	   
			<div class="row justify-content-sm-center ">
             <div class="col-md-12 flex-center m-1" style="color:#fff;">
               <h4 class="p-3 my-auto text-center">Schedule a free Pickup or Call us at <b><?php //echo $doorDryCleanContact;?>+97466176662</b></h4>
             </div>
			 </div>
			 <div class="row">
             <div class="col-md-12 text-center">
			    <a href="checkout.php" class="btn btn-danger btn-lg white-text " ><h5 class="flex-center my-auto">SCHEDULE PICKUP</h5> </a>
			   </h5>
             </div>
			 </div>
           
	 </div>
	 <!----------------Paralax Image------------->
	 
	 
	 <!----------------How it works-------------->
	 
	 <section id="works">
	 <div class="container-fluid">
	    <div class="row">
		  <div class="col-lg-12">
		    
		     <h4 class="text-center works-title">Dry Cleaning & Laundry Made Simple</h4>
			 <h5 class="text-center">How it works:</h5>
			 <p class="text-center">Order on the website or simply call us at 8080-454-454. Once you schedule a pickup, we shall collect, process and deliver it back to you within 48 hours at your convenient place and timings. You can track your present order, view your existing and past orders, pay online and earn reference discounts too.</p>
			 <div style="text-align:center">
			 <button class="btn btn-danger">BOOK NOW</button>
			 </div>
			 <img src="img/laundry-work.png" class="center-block img-responsive">
		  </div>
		</div>
	  </div>
	 </section>
	 
	 
	 <!----------------How it works-------------->
	 <hr>
	 
	  <!----------------OFFERS SECTION STARTS HERE------------------>
	 
	    <div class="container white mt-2">
		  <div class="row justify-content-sm-center">
			<div class="col-sm-12">
		     <h2 class="text-center font-weight-normal " style="color:#653B7D;">Affordable Prices</h2>
			  <h4 class="text-center" style="color: #A0A0A0 ;">Download <?php //echo $project_title;?>Laundry App today to discover special offers in your area! </h4>
			</div>
		  </div>
			 
		  <div class="row text-center mt-2">
           <div class="col-sm-4" >
             <img src="img/shirt.png" class="img-responsive mx-auto d-block" width="99%">
		     <h4 class="text-center" style="color:#505050  ;">From: $ 20 / Shirt </h4>
              <h5 class="grey-text text-center">Wash & Iron</h5>
           </div>
           <div class="col-sm-4" style="border-left:0.5px solid #AFB6BF;border-right:0.5px solid #AFB6BF;">
             <img src="img/trouser.png" class="img-responsive mx-auto d-block" width="99%">
		      <h4 class="text-center">From  : $ 20 / Trouser </h4>
              <h5 class="grey-text">Wash & Iron</h5>
           </div>
           <div class="col-sm-4" >
             <img src="img/kurta.png" class="img-responsive mx-auto d-block" width="99%">
		     <h4 class="text-center">From : $ 15 / Kurta </h4>
              <h5 class="grey-text">Wash & Fold</h5>
           </div>
          </div>
			 
	      <div class="row text-center mt-3 ">
			 <a href="services.php" class="btn btn-purple font-weight-bold mx-auto white-text" > <h4 class="flex-center">SEE ALL</h4> </a>
		  </div>
	
	    </div>
	 <!----------------OFFERS SECTION ENDS HERE---------------------> 
	 
	 
	 <!-----------------Testimonial Section------------------------->
	 <section>
	 <div class="container-fluid">
	 <div class="row">
	 <div class="col-md-12">
	 <div class="parallax">
	  <h2 class="text-center " style="color:#fff;padding:20px">Enjoy our hassle free ecrerience</h2>
	  <p class="text-center "  style="color:#fff">Happy clothes and our Satisfied customers</p>
	  
	 
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators --
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="img/c1.png" alt="Los Angeles" style="width:10%;" class="center-block img-responsive">
        <div class="carousel-caption  ">
          <p class="text-center laundry">It was such a nice feeling to get clean & crisp clothes without having to move an inch, keep up the good work highly recommended.</p>
          <p class="text-center laundry"> - Anushree Mittal, Senior Manager at IFA</p>
        </div>
      </div>

      <div class="item">
        <img src="img/c1.png" alt="Los Angeles" style="width:10%;" class="center-block img-responsive">
        <div class="carousel-caption  ">
          <p class="text-center laundry">It was such a nice feeling to get clean & crisp clothes without having to move an inch, keep up the good work highly recommended.</p>
          <p class="text-center laundry"> - Anushree Mittal, Senior Manager at IFA</p>
        </div>
      </div>
    
      <div class="item">
        <img src="img/c1.png" alt="Los Angeles" style="width:10%;" class="center-block img-responsive">
        <div class="carousel-caption  ">
          <p class="text-center laundry">It was such a nice feeling to get clean & crisp clothes without having to move an inch, keep up the good work highly recommended.</p>
          <p class="text-center laundry"> - Anushree Mittal, Senior Manager at IFA</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>
  </div></div></div>
  </section>

	 
	 <!-----------------Testimonial Section------------------------->
	 <!------------Second last section-------------->
	  <section style="height:150px;" class="laundry">
	  <div class="container-fluid">
	    <div class="row" style="background-color:#F9F9F7;border-top:1px solid #ccc">
		  <div class="col-lg-10 col-lg-offset-1">
		    <div class="row">
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			     <div class="row laundry">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <i class="fa fa-file"></i>
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>FAQ</h3>
					  <p>Find out about all your queries.</p>
					  <a href="#">Read More</a>
					</div>
				 </div>
			  </div>
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			  <div class="row laundry">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <i class="fa fa-file"></i>
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>Know more</h3>
					  <p>Discover the care for your Garments</p>
					  <a href="#">Read More</a>
					</div>
				 </div>
			  </div>
			</div>
		  </div>
		</div>
		</div>
	  </section>
	 
	 <!------------Second last section-------------->
	 
	  <!------------------FOOTER SECTION STARTS HERE--------------------->       
           	
        <footer class="footer grey darken-4 " style="margin-top:10%;">

         <div class="container-fluid ">
	       <div class="row ">
	
	       <!-----left starts--------->
            <div class="col-md-3 col-sm-12  mt-1"  > 
	          <a href="index.php" class="flex-center ">
		       <img src="img/logo.png" class="img-responsive mx-auto d-block" style="max-height:60px;">
		      </a>
	        </div>
		   <!-----left ends--------->
	 
	      <!----middle starts------>
	        <div class="col-sm-12 col-md-7 col-xs-12 col-lg-6"> 
	       
		     <div class="row mt-1">
		      <div class="col-sm-4 col-md-4 col-xs-12 mt-2">
		       <div class="row ">
	            <div class="col-md-4 col-lg-3 col-sm-12 col-xs-12" >
				  <img src="img/1.png" class="img-responsive mx-auto d-block" style="max-height:40px">
				</div>
				<div class="col-md-8 col-lg-6 col-sm-12 col-xs-12 text-center my-auto" >
				  <span class="white-text" ><h5> Convenient</h5></span>
				</div>
			   </div>
	          </div>
	 
	          <div class="col-sm-4 col-md-5 col-xs-12 text-center mt-2">
			   <div class="row">
	            <div class=" col-md-4 col-lg-3 col-sm-12 ">
				  <img src="img/2.png" style="max-height:40px" class=" img-responsive mx-auto d-block">
				</div>
				<div class="col-sm-12 col-lg-6 col-md-8 text-center my-auto">
				  <span class="white-text "> <h5>Professional</h5> </span>
				</div>
			   </div>
	          </div>
	 
	          <div class="col-sm-4 col-md-3 col-xs-12 mt-2">
		       <div class="row">
	            <div class="col-md-4 col-lg-3 col-sm-12" >
				  <img src="img/3.png" style="max-height:40px" class="img-responsive mx-auto d-block">
				</div>
				<div class="col-md-8 col-lg-6 col-sm-12 text-center my-auto">
				  <span class="white-text"> <h5>Fast</h5> </span>
				</div>
	           </div>
		      </div>
		     </div>
		
		    <div class="row mb-1">
		       <hr class="white" width="60%">
		     </div>
		
		     <div class="footer-link row"  >
		      <div class="col-sm-10 offset-sm-1">
          	    <ul class="text-center inline-ul" >
				  <li class="active white-text"><a href="index.php" class="white-text" >Home</a></li>
				  <li><a href="login.php" class="ml-2 white-text">Login</a></li>
				  <a href="services.php" class="ml-2 white-text">Pricing</a>
				  <a href="about.php" class="ml-2 white-text">About</a>
				  <a href="faq.php" class="ml-2 white-text">Faq</a>
				  <a href="privacy_policy.php" class="ml-2 white-text">Privacy Policy</a>
				  <a href="contact.php"  class="ml-2 white-text">Contact</a>
	            </ul>
		      </div>
		     </div>
		
		     <div class="row">
		      <div class="col-sm-10 offset-sm-1 text-center" >
     	        <p class="footer-links" style="font-family:'Open Sans';">
				  <a href="#" class=" white-text"><?php //echo $doorDryCleanContact;?></a>
				  <a href="mailto:info@doorDryClean.com?subject=The%20subject%20of%20the%20mail" class="ml-2 white-text"><?php// echo $doorDryCleanEmail;?></a>
			    </p>
		      </div>
	         </div>
	 
	         <div class="row footer-icons text-center">
		      <div class="col-sm-10 offset-sm-1 ">
		        <a href="https://www.facebook.com/" type="button" class="btn-floating btn-fb"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/" type="button" class="btn-floating btn-tw"><i class="fa fa-twitter"></i></a>
                <a href="https://google.com" type="button" class="btn-floating btn-gplus"><i class="fa fa-google-plus"></i></a>
                <a href="https://www.linkedin.com" type="button" class="btn-floating btn-li"><i class="fa fa-linkedin"></i></a>
                <a href="https://www.instagram.com" type="button" class="btn-floating btn-ins"><i class="fa fa-instagram"></i></a>
		      </div>
	         </div>
			 
	        </div> 
		 <!----middle ends------>
	 
	    <!-----right starts------>
            <div class="col-md-2  col-lg-3 hidden-md-down"> 
		     <!--<img src="img/footer_lady.png" class="img-responsive pull-right " style="margin-top:-210px;">-->
            </div> 
		 <!-----right ends------->
	 
	       </div>
          </div>
		</footer>
		
	 <!------------------- Footer ends here ---------------------->
	 
	 
			</div>
		</div>
	</div>

	 
	  <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>