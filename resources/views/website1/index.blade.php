@extends('layout.auth_web')
@section('title', 'Home' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
	 
	 <!----------------Paralax Image------------->
	 <div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div id="background-image">
					<div><img src="{{ URL::asset('web/img/logo-slider.png')}}" class="center-block img-responsive logo" style="width:130px;"></div>
	   
						<p style="color:#fff" class="text-center logo-name">Laundry & Lot at your Doorstep</p>
	   
					
					   <h4 class="p-3 my-auto text-center text-white">Schedule a free Pickup or Call us at <b><?php //echo $doorDryCleanContact;?>+97466176662</b></h4>
					 <div class="col-md-12 text-center">
						<a href="checkout.php" class="btn btn-danger btn-lg white-text " ><h5 class="flex-center my-auto">SCHEDULE PICKUP</h5> </a>
					   
					 </div>
			 </div>
	 <!----------------Paralax Image------------->
	 
	 
	 <!----------------How it works-------------->
	 
	 <section id="works">
	 <div class="container-fluid">
	    <div class="row">
		  <div class="col-md-12">
		     <h4 class="text-center works-title heading">Dry Cleaning & Laundry Made Simple</h4>
			 <p class="text-center font-weight-normal description">How it works:</p>
			 <span class="border-center"></span>
		  </div>
		</div>
		<div class="row mt-2">
		<div class="col-sm-12 col-xs-12 col-md-10 col-md-offset-1">
		<div class="row">
		<div class="col-md-2 col-sm-2 hidden-xs">
		 <img src="{{ URL::asset('web/img/works.png')}}" class="img-responsive center-block" style="height:250px">
		</div>
			   <div class="col-sm-6 col-md-6 col-xs-6 steps">
			      <h4 class="mb-1"> Book via Our App or Online</h4>
				  <p class="font-weight-normal" > We're a mobile, door-to-door dry Cleaning and Laundry Service.</p>
				  
				  <h4 class="mb-1"> Meet our Pickup Pilot</h4>
				  <p class="font-weight-normal"> We collect your dry cleaning & laundry from your doordtep.</p>
				  
				  <h4 class="mb-1"> Receive your fresh clothes</h4>
				  <p class="font-weight-normal" > We bring back fresh clothes in 48 hours!</p>

				  </div>
			   
			   <div class="col-md-4 col-sm-4 col-xs-4">
			      <img src="{{ URL::asset('web/img/circle.png')}}" class="img-responsive center-block">
			   </div>
			 </div> 
		   </div>  
		</div>
	  </div>
	 </section>
	 
	 
	 <!----------------How it works-------------->
	 
	 
	  <!----------------OFFERS SECTION STARTS HERE------------------>
	  
	  <Section id="prices" style="background-color:#EFEFEF">
	    <div class="container mt-2">
		  <div class="row justify-content-sm-center">
			<div class="col-sm-12">
		     <h4 class="text-center font-weight-normal pt-2 heading" >Affordable Prices</h4>
			  <p class="text-center description">Download <?php //echo $project_title;?>Laundry App today to discover special offers in your area! </p>
			 <span class="border-center"></span>
			</div>
		  </div>
			 
		  <div class="row text-center mt-2">
           <div class="col-sm-4" >
             <img src="{{ URL::asset('web/img/shirt.png')}}" class="img-responsive mx-auto d-block" width="99%">
		     <h4 class="text-center" style="color:#505050  ;">From: $ 20 / Shirt </h4>
              <h5 class="grey-text text-center">Wash & Iron</h5>
           </div>
           <div class="col-sm-4" style="border-left:0.5px solid #AFB6BF;border-right:0.5px solid #AFB6BF;">
             <img src="{{ URL::asset('web/img/trouser.png')}}" class="img-responsive mx-auto d-block" width="99%">
		      <h4 class="text-center">From  : $ 20 / Trouser </h4>
              <h5 class="grey-text">Wash & Iron</h5>
           </div>
           <div class="col-sm-4" >
             <img src="{{ URL::asset('web/img/kurta.png')}}" class="img-responsive mx-auto d-block" width="99%">
		     <h4 class="text-center">From : $ 15 / Kurta </h4>
              <h5 class="grey-text">Wash & Fold</h5>
           </div>
          </div>
			 
	      <div class="row text-center mt-3 ">
			 <a href="services.php" class="btn btn-danger font-weight-bold mx-auto white-text mb-2" > <h4 class="flex-center">SEE ALL</h4> </a>
		  </div>
	
	    </div>
		</section>
	 <!----------------OFFERS SECTION ENDS HERE---------------------> 
	  <!---------------CUSTOMERS VIEWS SECTION STARTS HERE----------------->
	  
	    <div class="container white mt-2 mb-4">
          <div class="row justify-content-sm-center">
		   <div class="col-sm-12">
		    <h4 class="text-center font-weight-normal heading"> We make you smile!</h4>
			<p class="text-center font-weight-normal description">What our customers say:</p>
			<span class="border-center"></span>
		   </div>
		  </div>
			
		  <div class="row white mt-2 mb-4">
		  
			<div class="col-md-7 testimonial" >
				   
			  <div class="row">
				<div class="flex-center col-md-3 col-sm-3"  >
				  <img src="{{ URL::asset('web/img/c1.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
                </div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted">“I could never find time for laundry, and let’s not even talk about ironing. With <?php //echo $project_title;?>, laundry and ironing are off my to-do list for good. And so is dry cleaning.”</p>
				  <h3><span>  <b>Chris</b></span><b>, Developer</b></h3>
                </div>
              </div>					

              <div class="row mt-2">
                <div class="flex-center col-md-3 col-sm-3">
				   <img src="{{ URL::asset('web/img/c2.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
                </div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted" > “I can’t even remember the amount of expensive clothes I’ve ruined. With <?php //echo $project_title;?>, my silk shirts, cashmere sweaters, and blouses are all in good hands, finally!”</p>
				  <h3 ><span> <b>Bella</b></span><b>, CA</b></h3>
				</div>	
			  </div>

              <div class="row mt-2 ">
                <div class="flex-center  col-sm-3 col-md-3">
				  <img src="{{ URL::asset('web/img/c3.png')}}"  class="img-responsive img-fluid rounded-circle" style="border:1px solid #653B7D;">
				</div>	
                <div class="col-md-7 offset-sm-1 col-sm-8"  >
				  <p class="text-muted">“My family goes through so many clothes a week, there’s always mountains of laundry to do. Now that I use <?php //echo $project_title;?>, I spend Sundays with the kids rather than the washing machine.”</p>
				  <h3 ><span> <b>Ellena</b></span><b>, Lecturer</b></h3>
                </div>	
              </div>					
			
			</div>
				 
			<div class="col-md-5 mt-2 ">
			  <div class="flex-center">
                  <img src="{{ URL::asset('web/img/satisfied-customers.png')}}" class="img-responsive mx-auto d-block" width="50%">
              </div> 
			</div>
				 
		  </div>
             
		</div>
		   
	 <!---------------CUSTOMERS VIEWS SECTION STARTS HERE----------------->
	  
	 <!------------Second last section-------------->
	  
	  <div class="container-fluid" style="background-color:#EFEFEF";>
	    <div class="row">
		  <div class="col-lg-10 col-lg-offset-1">
		    <div class="row mb-2 mt-0 readmore">
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			     <div class="row laundry">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <img src="{{ URL::asset('web/img/faq.png')}}" class="img-responsive">
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>FAQ</h3>
					  <p>Find out about all your queries.</p>
					  <a href="#">Read More</a>
					</div>
				 </div>
			  </div>
			  <div class="col-lg-6 col-sm-6 col-xs-6">
			  <div class="row laundry ">
				    <div class="col-lg-2 col-sm-2 col-xs-2">
					  <img src="{{ URL::asset('web/img/help.png')}}" class="img-responsive">
					</div>
					<div class="col-lg-10 col-sm-10 col-xs-10">
					  <h3>Need Help!</h3>
					  <p>Discover the care for your Garments</p>
					  <a href="#">Read More</a>
					</div>
				 </div>
			  </div>
			</div>
		  </div>
		</div>
		</div>
	  
	 
	 <!------------Second last section-------------->
	 
	 </div>
		</div>
	</div>
	
 
	 @include('includes.footer-web')
			
	
	@endsection