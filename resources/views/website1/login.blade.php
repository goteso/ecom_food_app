@extends('layout.auth_web')
@section('title', 'Login' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
<style>

.md-form label{
	font-weight:200;
}


</style>




 
     <!---------Navbar section starts here--------->
           
     <!---------Navbar section ends here--------->
 
 
 
 
     <!---------CONTENT SECTION STARTS HERE------>
	<div ng-app="myApp" ng-controller="loginController">	   
		    
		   
		   <div class="container mt-5 mb-4" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
    <div class="card-block">

        <!--Header-->
        <div class="form-header "  style="background-color:#B8152F;">
            <h3><i class="fa fa-lock" style="color:#fff"></i> Login</h3>
        </div>

        <!--Body-->
		<div class="row">
		<div class="col-sm-10 offset-sm-1">
		
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="email" id="form2" name="userName" class="form-control validate" ng-model="userName" required>
            <label for="form2" data-error="wrong" >Your email / Phone</label>
        </div>

        <div class="md-form mt-2">
            <i class="fa fa-lock prefix"></i>
            <input type="password" name="password" id="form4" class="form-control validate" ng-model="password" required>
            <label for="form4" data-error="wrong" >Your password</label>
        </div>

		 <div class="md-form">
        <div class="text-center">
            <button type="button"  class="btn btn-danger" ng-click="login()">Login</button>
        </div>
		</div>
		
		
</div>
    </div>
	 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">
	
	
	</div>
  
 
 <!--------message box ends-------------->

    <!--Footer-->
    <div class="modal-footer">
        <div class="options">
		     <!-------<p><a href="login_with_otp.php" style="color:#653B7D;">Login With OTP</a></p>------->
            <p>Forgot <a href="{{URL::asset('forgot_password')}}" style="color:#653B7D;">Password?</a></p>
			<span>Not a member? <a href="{{URL::asset('register')}}" style="color:#653B7D;">Register Here </a></span>
			<!--<p>Not a member? <a href="register.php">Register Here</a></p>-->
        </div>
    </div>
</div>
</div>
<!--/Form with header-->
		    </div>
    </div>
		</div>   
		
		</div>
		
		<!-------------CONTENT SECTION ENDS HERE-------------->
	 

 
 
 
 
<!-- SCRIPTS -->
    <script type="text/javascript" src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/compiled.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-local-storage/0.7.1/angular-local-storage.js"></script>--> 
   <script>
  
   var app = angular.module('myApp',['ngRoute','ngStorage']);
 
	app.config(function($routeProvider){
	$routeProvider
	.when("/", {
	templateUrl: "login.php"
	})

	.when("/edit", {
	templateUrl: "edit-profile.php"
	})
	
	.when("/logout", {
	templateUrl: "index.php"
	})

	})
	

   app.controller('loginController',function($scope, $http, $window, $location, $rootScope){
	
	   $scope.login = function(){
		   $scope.$watch('userName', function(val) {
            $scope.userName = val;
			//alert($scope.userName);
		});
		  $scope.$watch('password', function(val) {
            $scope.password = val;
			//alert($scope.password);
		});
		
		 $scope.loginData = {"email":$scope.userName, "password":$scope.password};
	   
		  //alert($scope.loginData);
		  
		// alert(JSON.stringify($scope.loginData));
		//$location.path('/edit');
	
		   
    var request = $http({
                method: "POST",
                url: APP_URL+'/api/login',
                data: $scope.loginData,
                headers: { 'Accept':'application/json'  }
           }).then(function (response) { 
		  // alert('success');
		   $scope.info = response.data;
		   //alert(JSON.stringify($scope.info));
		   $scope.status = data.data.status_code;
		   $scope.id  = data.data.user_data.id;
		   
		   //$sessionStorage = $scope.status;
		   //$rootScope = $scope.status;
		   //alert('Rootscope'+JSON.stringify($rootScope));
		   //$sessionStorage.id = data.data.user_data.id;
		   //$localStorage.id = data.data.user_data.id;
		   //alert(JSON.stringify($sessionStorage.id))
		    //alert(JSON.stringify($scope.status));
//alert(JSON.stringify($scope.id));
		   if($scope.status==1){
           $window.location.href = 'edit-profile.php?userid='+ $scope.id;
		   }
		   else{
			   alert('You have entered Wrong Credentials. Try with Valid Credentials!');
		   }
		   
                 //document.getElementById("res").value = JSON.stringify(data);      
           },function (data, status, headers, config) { 
             alert('error');
            //document.getElementById("res").value = JSON.stringify(data);
       });
	   };
		
})

   
   </script>

 @include('includes.footer-web')
			
	
	@endsection