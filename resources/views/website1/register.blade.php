@extends('layout.auth_web')
@section('title', 'About' )
@section('content')
@section('header')
@include('includes.header-web')
@show
 
<style>

h1 ,h2 ,h3 ,h4 ,h5 ,p{font-family:"Open Sans"
}


#top_header {
 background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}

 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}


</style>
	 

<div ng-app="myApp" ng-controller="signUpController" ng-init="add_address_form()">
 
     <!---------Navbar section ends here--------->
 
 
 
   

     <!---------CONTENT SECTION STARTS HERE------>
		   
		   
		   
		   
		   <div class="container mt-5" >
		   <div class="row">
		   <div class="col-sm-10 offset-sm-1">
		   <div class="card">
    <div class="card-block">

        <!--Header-->
        <div class="form-header" style="background-color:#B8152F;">
            <h3><i class="fa fa-user" style="color:#fff"></i> Sign Up</h3>
        </div>

		
		<div class="row">
		<div class="col-sm-10 offset-sm-1">
		
		<div class="row">  
		<div class="col-lg-6 col-sm-12" ng-repeat="data in columns_add_address_data[0].fields">
						   
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'text'" >
                                 <label for="@{{data.identifier}}">@{{data.title}} </label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  required>
                              </div>
							  
							  <!----- For Email Type Input Field----------->
                              <div class="form-group " ng-show="data.type == 'email'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  required> 
                              </div>
							  
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'password'" >
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  required>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields">
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'radio'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" name="@{{data.identifier}}">@{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5"  ng-repeat="data in columns_add_address_data[0].fields">
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'checkbox'">
                                 <label for="@{{data.identifier}}">@{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="@{{data.type}}"  ng-model="data.value[options.value]" id="@{{options.value}}"  >@{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" >@{{options.title}}
								 </label>
                              </div>
							  
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields" >
							  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'Selectbox'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" name="@{{data.identifier}}">@{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields">
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'multipleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields">
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'singleTag'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  </div>
							 <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields" >
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-show="data.symbol">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">@{{data.symbol}}</span>
                                    <input type="@{{data.type}}" class="form-control"   ng-model="data.value" id="@{{data.identifier}}"  >
                                 </div>
                              </div>
							  </div>
							<div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields" >
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'datePicker'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  />
                                 </datepicker>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in columns_add_address_data[0].fields" >
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'timePicker'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  </div>
						<div class="col-lg-5 col-sm-5"  ng-repeat="data in columns_add_address_data[0].fields" >
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"ng-show="data.type == 'longText'">
                                 <label for="@{{data.identifier}}">@{{data.title}}</label>
                                 <textarea class="form-control" name="@{{data.identifier}}"ng-model="data.value"  id="@{{data.identifier}}" >@{{data.value}}</textarea>
                              </div>
							  
                           </div>
							
                    
		</div>
		  <button ng-click="add_address()" class="btn btn-danger"> Add </button>
							<br></br>
		</div>
		 
	
    </div>
	
</div>
 
 <!--------message box starts--------------
 
    <div class="col-sm-12 text-center" style="padding:2%;">
	
	
	</div>
 
 
 <!--------message box ends-------------->
 
<!--/Form with header-->
		    </div><br></br>
    </div>
		</div>   
		 </div>
		</div>
		
		
		<!-------------CONTENT SECTION ENDS HERE-------------->
        
      <script>
   var app = angular.module('myApp',[]);
   app.controller('signUpController',function($scope, $http){
	   
	$scope.add_address_form = function() {
  

        /*get data from api**/
        $http.get(APP_URL+'/api/user_forms')
            .then(function(data, status, headers, config) {

                $scope.columns_add_address_data = data.data; 
            },function(data, status, headers, config) {
               
            });
 
    }
	
 
   });
   </script>
 @include('includes.footer-web')
			
	
	@endsection
 



