@extends('layout.auth_web_new')
@section('title', 'Services' )
@section('content')
@section('header')
@include('includes.header-web-new')
@show
    
    
     
	<style>
	 .steps h3{color:#B8152F}
	 #works h4{color:#B8152F}
	 .footer ul li a{color:#fff;}
	 .footer h4{color:#fff}
	 .footer ul li{padding:5px 0px}
	 
	.nav { 
	display: inline-block; 
	} 
	.navbar{margin:0px}
	.navbar-default .navbar-nav>li>a {
	color:#000;
	}
	
	
	h1,h2,h3,h4,h5,p{font-family:"Open Sans"
}


#top_header {
background-color:#653B7D;
}


#banner{
position:relative;
margin-top:-25%;
}

#banner .sec1{
opacity:0.8;
}
[ng-cloak]{
    display:none; important!
}
 #divider_div
 {
	 
    background-image: url("img/divider.png");
    background-repeat: no-repeat;
	  background-position: center center;
 
}


  

	</style>


<div ng-app="myApp"> 

		<div  ng-controller="cartForm" >
		
		
		 
	 
		<!--
		<div class="container-fluid  p-0" >
          <img src="{{URL::asset('web/img/top_section.jpg')}}" class="img-responsive" width="100%">
         </div>-->
		 
		 <div class="container" >

<div class="row">
         <p class="text-center"style="margin:30px 15px 10px;font-size:20px"><b>Pricing</b></p>
         <span class="border-center "></span>
      </div>

		   <div class="row">
		   <div class="col-sm-12  " style="padding:15px 0;">
		 <div class="panel-group product-view" id="accordion"> 
                                       <div ng-repeat="categories in addOrder " id ="@{{categories.category_title}}"  >
                                          <div ng-show="categories.products != '' " style="border:1px solid #ccc;margin-bottom:8px;">
                                             <div class="panel ">
                                                <div class="panel-heading">
                                                   <h4 class="panel-title text-left">
                                                      <b>@{{categories.category_title | uppercase}}</b> <span>(@{{categories.products.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#1@{{categories.category_title}}">  <i class="fa fa-angle-down" style="float:right; position: relative;"></i></a>
                                                   </h4>
                                                </div>
                                                <div id="1@{{categories.category_title}}" class="panel-collapse collapse in">
                                                   <table class="table items-detail">
                                                      <tbody>
                                                         <tr ng-repeat="product in categories.products | filter:search"  > 
                                                            <td class="item-title"> @{{product.title}} </td>
                                                           <!-- <td class="item-price"><span ng-show="product.base_price > 0"><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>	 </td> -->
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                          <!--<div ng-repeat="subCategories in categories.sub_categories" id ="@{{subCategories.category_title}}"  >
                                             <div ng-show="subCategories.products != ''" >
                                                <div class="panel ">
                                                   <div class="panel-heading">
                                                      <h4 class="panel-title text-left">
                                                         @{{subCategories.category_title}} <span>(@{{subCategories.products.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#2@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a>
                                                      </h4>
                                                   </div>
                                                   <div id="2@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                      <table  class="table items-detail">
                                                         <tbody>
                                                            <tr ng-repeat="product in subCategories.products  "> 
                                                               <td class="item-title"> @{{product.title}} </td>
                                                               <td class="item-price"><span ng-show='product.base_price > 0'><?php echo env('CURRENCY_SYMBOL');?>@{{product.base_price}} </span>
                                                            <span ng-show='product.base_price == 0'>Price on Selection</span>  </td> 
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
		                                   </div>-->
	         </div>
		 
	   </div>
	      </div>
		   </div>
	   </div>
	   
</div>
	</div>
	  
 <script>
    var app = angular.module('myApp', []);
    app.controller('cartForm', function ($scope,$http) {
        
		 $http.get(APP_URL+'/add_order_form')
		 .then(function (response) { 
		
		 $scope.addOrder = response.data.categories_products;
		   
		 });
	
 });

 
      </script> 




	 @include('includes.footer-web')
			
	
	@endsection