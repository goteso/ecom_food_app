<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


 
// Dashboard 
Route::get('get_dashboard_data', array('as' => 'get_dashboard_data', 'uses' => 'DashboardController@get_dashboard_data') )->middleware('cors');


// products 
 

Route::post('product_delete', array('as' => 'product_delete', 'uses' => 'ProductsController@destroy') )->middleware('cors');

Route::get('product_edit/{product_id}', array('as' => 'product_edit', 'uses' => 'ProductsController@edit') )->middleware('cors');
Route::post('product_update/{product_id}', array('as' => 'product_edit', 'uses' => 'ProductsController@update_product') )->middleware('cors');

Route::post('product_list', array('as' => 'product_list', 'uses' => 'ProductsController@index') )->middleware('cors');

Route::post('product_details', array('as' => 'product_details', 'uses' => 'ProductsController@product_details') )->middleware('cors');
Route::post('add_update_variant', array('as' => 'add_update_variant', 'uses' => 'ProductsController@add_update_variant') )->middleware('cors');


//Categories
Route::post('categories_list', array('as' => 'categories_list', 'uses' => 'CategoriesController@index') )->middleware('cors');

Route::post('categories_add', array('as' => 'categories_add', 'uses' => 'CategoriesController@store') )->middleware('cors');

Route::get('categories_delete/{category_id}', array('as' => 'categories_delete', 'uses' => 'CategoriesController@destroy') )->middleware('cors');

Route::get('categories_edit/{category_id}', array('as' => 'categories_edit', 'uses' => 'CategoriesController@edit') )->middleware('cors');
Route::post('categories_update', array('as' => 'categories_update', 'uses' => 'CategoriesController@update') )->middleware('cors');




//users 
 
Route::get('user_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_forms') )->middleware('cors');
Route::post('user_store', array('as' => 'user_add', 'uses' => 'UserController@user_store') )->middleware('cors');

Route::get('user_edit_form/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@edit') )->middleware('cors');;

Route::post('user_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@update_user') )->middleware('cors');
Route::post('user_delete', array('as' => 'user_delete', 'uses' => 'UserController@destroy') )->middleware('cors');



Route::get('user_address_list/{user_id}', array('as' => 'user_list', 'uses' => 'UserController@user_address_list') )->middleware('cors');

Route::get('user_address_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_address_forms') )->middleware('cors');
Route::post('user_address_store', array('as' => 'user_add', 'uses' => 'UserController@user_address_store') )->middleware('cors');

Route::get('user_address_edit_form/{user_id}', array('as' => 'user_address_edit_form', 'uses' => 'UserController@user_address_edit_form') )->middleware('cors');

Route::post('user_address_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@user_address_update') )->middleware('cors');
Route::post('user_address_delete', array('as' => 'user_delete', 'uses' => 'UserController@destroy') )->middleware('cors');

Route::post('user_profile_basic_details', array('as' => 'user_profile_basic_details', 'uses' => 'UserController@user_profile_basic_details') )->middleware('cors');
Route::post('user_orders/', array('as' => 'user_orders', 'uses' => 'UserController@user_orders') )->middleware('cors');



Route::get('/api/hello',function() { return 'hello';})->middleware('cors');




// orders 
Route::get('add_order_form/', array('as' => 'add_order_form', 'uses' => 'OrdersController@add_order_form') )->middleware('cors');
Route::post('place_order/', array('as' => 'place_order', 'uses' => 'OrdersController@place_order') )->middleware('cors');
Route::post('order_details/', array('as' => 'order_details', 'uses' => 'OrdersController@order_details') )->middleware('cors');
Route::get('orders_list/', array('as' => 'orders_list', 'uses' => 'OrdersController@orders_list') )->middleware('cors');
Route::post('order_quick_actions', array('as' => 'order_quick_actions', 'uses' => 'OrdersController@order_quick_actions') )->middleware('cors');


// Reports Sections Starts 
Route::post('reports_sales/', array('as' => 'reports_sales', 'uses' => 'ReportsController@reports_sales') )->middleware('cors');
Route::post('reports_sales_by_customer/', array('as' => 'reports_sales_by_customer', 'uses' => 'ReportsController@reports_sales_by_customer') )->middleware('cors');
Route::post('reports_all_customers/', array('as' => 'reports_all_customers', 'uses' => 'ReportsController@reports_all_customers') )->middleware('cors');




 

 Route::post('delete_products_multiple', array('as' => 'delete_products_multiple', 'uses' => 'ProductsController@delete_products_multiple') )->middleware('cors');

Route::post('order_details234/', array('as' => 'order_details', 'uses' => 'OrdersController@order_details') )->middleware('cors');

 
 
  // web services
 Route::group(array('namespace'=>'Api'), function()
 {
    Route::post('login', array('as' => 'login', 'uses' => 'UserController@login') )->middleware('cors');
	Route::post('logout', array('as' => 'login', 'uses' => 'UserController@logout') )->middleware('cors');
  	Route::post('/forgot_password', array('as' => 'forgot_password', 'uses' => 'UserController@forgotPasswordEmail'));
	Route::post('/verify_otp', array('as' => 'verify_otp', 'uses' => 'UserController@verify_otp'));
	Route::post('/forgot_password_change', array('as' => 'forgot_password_change', 'uses' => 'UserController@forgotPasswordChange'));
	Route::post('/change_password', array('as' => 'change_password', 'uses' => 'UserController@newPassword'));
    Route::post('/get_user_profile', array('as' => 'get_user_profile', 'uses' => 'UserController@get_user_profile'));
	Route::post('/get_other_user_profile', array('as' => 'get_other_user_profile', 'uses' => 'UserController@get_other_user_profile'));
	Route::post('/get_other_user_sales_posts', array('as' => 'get_other_user_sales_posts', 'uses' => 'UserController@get_other_user_sales_posts'));
    Route::post('/upload_profile_image', array('as' => 'upload_profile_image', 'uses' => 'UserController@upload_profile_image'));
	Route::post('/update_profile', array('as' => 'update_profile', 'uses' => 'UserController@update_profile'));
 });
 
 
 

