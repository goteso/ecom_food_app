<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('get_dashboard_data', array('as' => 'get_dashboard_data', 'uses' => 'DashboardController@get_dashboard_data') )->middleware('cors');
 
 
Route::get('upload_logo', function () { return view('logo.upload_logo'); })->middleware('cors');


// route to show the login form for admin section
Route::group(array('namespace'=>'Admin'), function()
{
//=======================================================PRODUCTS===============================================================================================================
//==============================================================================================================================================================================

Route::get('product_get_form_basic/{product_id?}', array('as' => 'product_get_form_basic', 'uses' => 'ProductsController@add') )->middleware('cors'); // Get Form to Add Basic Info of product //clean done
Route::post('product_add', array('as' => 'product_add', 'uses' => 'ProductsController@store') )->middleware('cors'); //clean done
Route::get('product_edit/{product_id}', array('as' => 'product_edit', 'uses' => 'ProductsController@edit') )->middleware('cors'); //clean done
Route::post('product_delete', array('as' => 'product_delete', 'uses' => 'ProductsController@destroy') )->middleware('cors');
Route::post('product_update/{product_id}', array('as' => 'product_edit', 'uses' => 'ProductsController@update_product') )->middleware('cors'); //clean done
Route::get('product_invoice/{product_id}/{order_id?}', array('as' => 'product_invoice', 'uses' => 'ProductsController@product_invoice') )->middleware('cors'); //clean done
Route::get('product_list', array('as' => 'product_list', 'uses' => 'ProductsController@index', 'middleware' => 'auth') )->middleware('cors');
//Route::post('product_list', array('as' => 'product_list', 'uses' => 'ProductsController@get_products_list') )->middleware('cors');
Route::get('get_product_list', array('as' => 'get_product_list', 'uses' => 'ProductsController@get_products_list') )->middleware('cors');
Route::post('product_details', array('as' => 'product_details', 'uses' => 'ProductsController@product_details') )->middleware('cors');
Route::post('add_update_variant', array('as' => 'add_update_variant', 'uses' => 'ProductsController@add_update_variant') )->middleware('cors');
Route::get('delete_variant/{variant_id}', array('as' => 'delete_variant', 'uses' => 'ProductsController@delete_variant') )->middleware('cors');
Route::post('delete_products_multiple', array('as' => 'delete_products_multiple', 'uses' => 'ProductsController@delete_products_multiple') )->middleware('cors');
Route::post('get_product_variants', array('as' => 'get_product_variants', 'uses' => 'ProductsController@get_product_variants') )->middleware('cors');
Route::get('search_product', array('as' => 'search_product', 'uses' => 'ProductsController@search_product') )->middleware('cors'); //clean done

 

 
//=======================================================GET TIME SLOTS=========================================================================================================
//==============================================================================================================================================================================
Route::get('get_time_slots', array('as' => 'get_time_slots', 'uses' => 'TimeSlotsController@get_time_slots') )->middleware('cors');


 
 
 
 
 
//=======================================================TRANSACTIONS=========================================================================================================
//============================================================================================================================================================================
Route::get('transactions_list', 'TransactionsController@index'  )->name('transactions_list');
Route::post('add_transactions', 'TransactionsController@add_transactions')->name('add_transactions');





 
//=======================================================PAYMENT GATEWAYS=======================================================================================================
//==============================================================================================================================================================================
Route::get('get_payment_gateways', 'PaymentGatewaysController@get_payment_gateways')->name('get_payment_gateways');






//=======================================================CATEGORIES=============================================================================================================
//==============================================================================================================================================================================
Route::post('categories_list', array('as' => 'categories_list', 'uses' => 'CategoriesController@index', 'middleware' => 'auth') )->middleware('cors');
Route::post('categories_add', array('as' => 'categories_add', 'uses' => 'CategoriesController@store') )->middleware('cors');
Route::get('categories_delete/{category_id}', array('as' => 'categories_delete', 'uses' => 'CategoriesController@destroy') )->middleware('cors');
Route::get('categories_edit/{category_id}', array('as' => 'categories_edit', 'uses' => 'CategoriesController@edit') )->middleware('cors');
Route::post('categories_update', array('as' => 'categories_update', 'uses' => 'CategoriesController@update') )->middleware('cors');
Route::get('view_categories', 'CategoriesController@view_categories')->name('view_categories');







//=======================================================FAQS===================================================================================================================
//==============================================================================================================================================================================
Route::get('faqs_list', array('as' => 'faqs_list', 'uses' => 'FaqsController@index', 'middleware' => 'auth') )->middleware('cors');
Route::post('faqs_add', array('as' => 'faqs_add', 'uses' => 'FaqsController@store') )->middleware('cors');
Route::get('faqs_delete/{faq_id}', array('as' => 'faqs_delete', 'uses' => 'FaqsController@destroy') )->middleware('cors');
Route::get('faqs_edit/{category_id}', array('as' => 'faqs_edit', 'uses' => 'FaqsController@edit', 'middleware' => 'auth') )->middleware('cors');
Route::post('faqs_update', array('as' => 'faqs_update', 'uses' => 'FaqsController@update') )->middleware('cors');
Route::get('get_faqs/', array('as' => 'barcode', 'uses' => 'FaqsController@get_faqs') )->middleware('cors');








//=======================================================DASHBOARD==============================================================================================================
//==============================================================================================================================================================================
Route::get('get_dashboard_data', array('as' => 'get_dashboard_data', 'uses' => 'DashboardController@get_dashboard_data', 'middleware' => 'auth') )->middleware('cors');





 
//=======================================================REPORTS================================================================================================================
//==============================================================================================================================================================================
Route::get('reports_page/', array('as' => 'reports_page', 'uses' => 'ReportsController@reports_page', 'middleware' => 'auth') )->middleware('cors');
Route::get('get_reports_list/', array('as' => 'get_reports_list', 'uses' => 'ReportsController@get_reports_list') )->middleware('cors'); // done

Route::get('reports_all_customers/', array('as' => 'reports_all_customers_page', 'uses' => 'ReportsController@reports_all_customers_page', 'middleware' => 'auth') )->middleware('cors');
Route::post('reports_all_customers/', array('as' => 'reports_all_customers', 'uses' => 'ReportsController@reports_all_customers', 'middleware' => 'auth') )->middleware('cors'); //done

Route::get('reports_all_drivers/', array('as' => 'reports_all_drivers_page', 'uses' => 'ReportsController@reports_all_drivers_page', 'middleware' => 'auth') )->middleware('cors');
Route::post('reports_all_drivers/', array('as' => 'reports_all_drivers', 'uses' => 'ReportsController@reports_all_drivers' ) )->middleware('cors'); //done

Route::get('sales_report_page/', array('as' => 'sales_report_page', 'uses' => 'ReportsController@sales_report_page', 'middleware' => 'auth') )->middleware('cors'); 
Route::post('sales_report_page/', array('as' => 'reports_sales', 'uses' => 'ReportsController@reports_sales' ) )->middleware('cors');

Route::post('reports_sales_by_customer/', array('as' => 'reports_sales_by_customer', 'uses' => 'ReportsController@reports_sales_by_customer' ) )->middleware('cors');

Route::get('orders_report_page/', array('as' => 'orders_report_page', 'uses' => 'ReportsController@orders_report_page', 'middleware' => 'auth') )->middleware('cors');
Route::post('orders_report_page/', array('as' => 'reports_orders', 'uses' => 'ReportsController@reports_orders' ) )->middleware('cors');

Route::get('cancelled_orders_report_page/', array('as' => 'cancelled_orders_report_page', 'uses' => 'ReportsController@cancelled_orders_report_page', 'middleware' => 'auth') )->middleware('cors'); 
Route::post('cancelled_orders_report_page/', array('as' => 'reports_cancelled_orders', 'uses' => 'ReportsController@reports_cancelled_orders' ) )->middleware('cors');

Route::get('transactions_report_page/', array('as' => 'transactions_report_page', 'uses' => 'ReportsController@transactions_report_page', 'middleware' => 'auth') )->middleware('cors');

Route::get('customers_most_sales_report_page/', array('as' => 'customers_most_sales_report_page', 'uses' => 'ReportsController@customers_most_sales_report_page', 'middleware' => 'auth') )->middleware('cors');
Route::post('customers_most_sales_report_page/', array('as' => 'reports_all_customers_most_sales', 'uses' => 'ReportsController@reports_all_customers_most_sales' ) )->middleware('cors'); //done
 
Route::get('drivers_most_sales_report_page/', array('as' => 'drivers_most_sales_report_page', 'uses' => 'ReportsController@drivers_most_sales_report_page', 'middleware' => 'auth') )->middleware('cors');
Route::post('drivers_most_sales_report_page/', array('as' => 'reports_all_drivers_most_sales', 'uses' => 'ReportsController@reports_all_drivers_most_sales') )->middleware('cors'); //done
 
 
 
 
 
 
 
  
//=======================================================ORDERS================================================================================================================
//=============================================================================================================================================================================

 
 //=======================================================USERS==================================================================================================================
//==============================================================================================================================================================================

//Route 1. ========================================
Route::get('user_list_page/{user_type?}', function($user_type = ''){  return view('users.users')->with('user_type',$user_type); })->middleware('cors');

//Route 2. ========================================
Route::get('user_list', array('as' => 'user_list', 'uses' => 'UserController@user_list' ) )->middleware('cors'); 

<<<<<<< HEAD
//Route 3.============ return view = users.add-user , with basic and meta form data========== used in [admin_panel]
=======
//Route 3.=========================================
>>>>>>> 0f78c19bdeee22da87f69c0c6b990ebc30f7710b
Route::get('user_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_forms') )->middleware('cors');

//Route 4.=========================================
Route::post('user_store', array('as' => 'user_add', 'uses' => 'UserController@user_store') )->middleware('cors');

//Route 5.=========================================
Route::get('user_edit_form/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@user_edit_forms') )->middleware('cors');;

//Route 6.=========================================
Route::post('user_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@update_user') )->middleware('cors');

//Route 7.=========================================
Route::post('user_delete', array('as' => 'user_delete', 'uses' => 'UserController@destroy') )->middleware('cors');

//Route 8.=========================================
Route::post('get_driver_locations', array('as' => 'get_driver_locations', 'uses' => 'UserController@get_driver_locations') )->middleware('cors');

//Route 9.=========================================
Route::post('update_driver_locations', array('as' => 'update_driver_locations', 'uses' => 'UserController@update_driver_locations') )->middleware('cors');

//Route 10.=========================================
Route::get('user_address_list/{user_id}', array('as' => 'user_list', 'uses' => 'UserController@user_address_list') )->middleware('cors');

//Route 11.=========================================
Route::get('user_address_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_address_forms') )->middleware('cors');

//Route 12.=========================================
Route::post('user_address_store/{user_id}', array('as' => 'user_add', 'uses' => 'UserController@user_address_store') )->middleware('cors');

//Route 13.=========================================
Route::get('user_address_edit_form/{user_id}', array('as' => 'user_address_edit_form', 'uses' => 'UserController@user_address_edit_form') )->middleware('cors');

//Route 14.=========================================
Route::post('user_address_delete', array('as' => 'user_address_delete', 'uses' => 'UserController@user_address_delete') )->middleware('cors');

//Route 15.=========================================
Route::post('user_address_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@user_address_update') )->middleware('cors'); 

//Route 16.=========================================
Route::get('user_profile', array('as' => 'user_profile', 'uses' => 'UserController@user_profile') )->middleware('cors');
 
//Route 17.=========================================
Route::get('driver-profile', function () { return view('users.driver-profile'); })->middleware('cors');

//Route 18.=========================================
Route::get('search_users', array('as' => 'search_users', 'uses' => 'UserController@search_users') )->middleware('cors');

//Route 19.=========================================
Route::get('search_areas', array('as' => 'search_areas', 'uses' => 'UserController@search_areas') )->middleware('cors');

//Route 20.=========================================
Route::post('user_profile_basic_details', array('as' => 'user_profile_basic_details', 'uses' => 'UserController@user_profile_basic_details') )->middleware('cors');

//Route 21.=========================================
Route::post('user_orders/', array('as' => 'user_orders', 'uses' => 'UserController@user_orders') )->middleware('cors');

//Route 21.=========================================
Route::post('delete_users_multiple', array('as' => 'delete_users_multiple', 'uses' => 'UserController@delete_users_multiple') )->middleware('cors');

//Route 23.=========================================
Route::get('get_vendors_list', 'UserController@get_vendors_list')->name('get_vendors_list'); 
 
//Route 24.=========================================
Route::get('user-address', function () {  return view('website.user-address'); })->middleware('cors');
 
 
 
 
 
  
//=======================================================ORDERS================================================================================================================
//==============================================================================================================================================================================

//Route 1.=========================================
 
Route::get('barcode/', array('as' => 'barcode', 'uses' => 'OrdersController@barcode') )->middleware('cors');

//Route 2.=========================================
Route::get('add_order_form/', array('as' => 'add_order_form', 'uses' => 'OrdersController@add_order_form') )->middleware('cors');

//Route 3.=========================================
Route::get('add_order_form_site/', array('as' => 'add_order_form_site', 'uses' => 'OrdersController@add_order_form_site') )->middleware('cors');

//Route 4.=========================================
Route::post('assign_driver/', array('as' => 'assign_driver', 'uses' => 'OrdersController@assign_driver') )->middleware('cors');

//Route 5.=========================================
Route::post('assign_vendor/', array('as' => 'assign_vendor', 'uses' => 'OrdersController@assign_vendor') )->middleware('cors');

//Route 6.=========================================
Route::post('order_status_update', 'OrdersController@order_status_update')->name('order_status_update');

//Route 7.=========================================
Route::get('get_order_status_list', 'OrdersController@get_order_status_list')->name('get_order_status_list'); 

//Route 8.=========================================
Route::get('order_add/', array('as' => 'order_add', 'uses' => 'OrdersController@order_add', 'middleware' => 'auth') )->middleware('cors');

//Route 9.=========================================
Route::get('orders_list_detail', array('as' => 'orders_list_detail', 'uses' => 'OrdersController@orders_list_detail') )->middleware('cors'); 

//Route 10.=========================================
Route::get('order_detail_page/{order_id?}', array('as' => 'order_detail', 'uses' => 'OrdersController@order_detail') )->middleware('cors');

Route::post('place_order/', array('as' => 'place_order', 'uses' => 'OrdersController@place_order') )->middleware('cors');

Route::post('place_order_calculate/', array('as' => 'place_order_calculate', 'uses' => 'OrdersController@place_order_calculate') )->middleware('cors');

Route::get('get_nearest_store', array('as' => 'get_nearest_store', 'uses' => 'OrdersController@get_nearest_store' ))->middleware('cors');

Route::post('order_details/', array('as' => 'order_details', 'uses' => 'OrdersController@order_details') )->middleware('cors');

Route::get('order_details/{order_id}', array('as' => 'order_details', 'uses' => 'OrdersController@order_details') )->middleware('cors');
Route::get('orders_list/', array('as' => 'orders_list', 'uses' => 'OrdersController@orders_list') )->middleware('cors');
Route::get('orders_list_filtered/', array('as' => 'orders_list_filtered', 'uses' => 'OrdersController@orders_list_filtered') )->middleware('cors');

Route::post('order_quick_actions', array('as' => 'order_quick_actions', 'uses' => 'OrdersController@order_quick_actions') )->middleware('cors');

Route::post('edit_order', array('as' => 'edit_order', 'uses' => 'OrdersController@edit_order') )->middleware('cors'); // added on 20 april

Route::post('send_order_invoice', array('as' => 'send_order_invoice', 'uses' => 'NotificationsController@send_order_invoice') )->middleware('cors'); // added on 20 april
  
Route::post('drivers_orders_list', array('as' => 'drivers_orders_list', 'uses' => 'OrdersController@drivers_orders_list') )->middleware('cors'); // added on 20 april
Route::post('customers_orders_list', array('as' => 'customers_orders_list', 'uses' => 'OrdersController@customers_orders_list') )->middleware('cors'); // added on 20 april
  
Route::post('order_product_delete', array('as' => 'order_product_delete', 'uses' => 'OrdersController@order_product_delete') )->middleware('cors'); // added on 20 april
 
Route::post('get_order_product_variants', array('as' => 'get_order_product_variants', 'uses' => 'OrdersController@get_order_product_variants') )->middleware('cors'); // added on 20 april
 
Route::post('update_product_variants', array('as' => 'update_product_variants', 'uses' => 'OrdersController@update_product_variants') )->middleware('cors'); // added on 20 april
 
Route::post('get_categories_products', array('as' => 'get_categories_products', 'uses' => 'OrdersController@get_categories_products') )->middleware('cors'); // added on 20 april
  
Route::post('get_order_subtotal/{order_id?}', array('as' => 'get_order_subtotal', 'uses' => 'OrdersController@get_order_subtotal') )->middleware('cors'); // added on 20 april
  
Route::post('edit_order_add_product/{order_id?}', array('as' => 'edit_order_add_product', 'uses' => 'OrdersController@edit_order_add_product') )->middleware('cors'); // added on 20 april
  
Route::post('edit_order_delete_product/{order_id?}', array('as' => 'edit_order_delete_product', 'uses' => 'OrdersController@edit_order_delete_product') )->middleware('cors'); // added on 20 april
 
Route::get('get_order_invoice', function () {   return view('orders.order_invoice'); });
 
 
 
 
 
 
  
//======================================================STORES==================================================================================================================
//==============================================================================================================================================================================
Route::get('stores_list', array('as' => 'stores_list', 'uses' => 'StoresController@stores_list') )->middleware('cors'); // added on 20 april
Route::put('assign_vendor_to_store/{id?}', array('as' => 'assign_vendor_to_store', 'uses' => 'StoresController@assign_vendor_to_store') )->middleware('cors'); // added on 20 april 
  Route::get('add_store', function () {
    return view('stores.add-store');
});
Route::post('add_store', array('as' => 'add_store', 'uses' => 'StoresController@add_store') )->middleware('cors'); 
Route::get('store_edit/{store_id?}', array('as' => 'store_edit', 'uses' => 'StoresController@store_edit') )->middleware('cors'); 
Route::put('store_update', array('as' => 'store_update', 'uses' => 'StoresController@store_update') )->middleware('cors'); 
Route::get('search_stores', array('as' => 'search_stores', 'uses' => 'StoresController@search_stores') )->middleware('cors');

 
 
 
 
 
 
 
 
 
//=======================================================USERS==================================================================================================================
//==============================================================================================================================================================================
Route::get('user_list_page/{user_type?}', function($user_type = ''){  return view('users.users')->with('user_type',$user_type); })->middleware('cors');

Route::get('user_list', array('as' => 'user_list', 'uses' => 'UserController@user_list' ) )->middleware('cors');

Route::get('user-address', function () {  return view('website.user-address'); })->middleware('cors');

Route::get('user_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_forms') )->middleware('cors');

Route::post('user_store', array('as' => 'user_add', 'uses' => 'UserController@user_store') )->middleware('cors');

Route::get('user_edit_form/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@user_edit_forms') )->middleware('cors');;

Route::post('user_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@update_user') )->middleware('cors');
Route::post('user_delete', array('as' => 'user_delete', 'uses' => 'UserController@destroy') )->middleware('cors');

Route::post('get_driver_locations', array('as' => 'get_driver_locations', 'uses' => 'UserController@get_driver_locations') )->middleware('cors');
Route::post('update_driver_locations', array('as' => 'update_driver_locations', 'uses' => 'UserController@update_driver_locations') )->middleware('cors');

Route::get('user_address_list/{user_id}', array('as' => 'user_list', 'uses' => 'UserController@user_address_list') )->middleware('cors');

Route::get('user_address_forms/{user_id?}', array('as' => 'user_register_basic', 'uses' => 'UserController@user_address_forms') )->middleware('cors');

Route::post('user_address_store/{user_id}', array('as' => 'user_add', 'uses' => 'UserController@user_address_store') )->middleware('cors');

Route::get('user_address_edit_form/{user_id}', array('as' => 'user_address_edit_form', 'uses' => 'UserController@user_address_edit_form') )->middleware('cors');

Route::post('user_address_delete', array('as' => 'user_address_delete', 'uses' => 'UserController@user_address_delete') )->middleware('cors');

Route::post('user_address_update/{user_id}', array('as' => 'user_edit', 'uses' => 'UserController@user_address_update') )->middleware('cors'); 

Route::get('user_profile', array('as' => 'user_profile', 'uses' => 'UserController@user_profile') )->middleware('cors');
 
Route::get('driver-profile', function () { return view('users.driver-profile'); })->middleware('cors');

Route::get('search_users', array('as' => 'search_users', 'uses' => 'UserController@search_users') )->middleware('cors');

Route::get('search_areas', array('as' => 'search_areas', 'uses' => 'UserController@search_areas') )->middleware('cors');

Route::post('user_profile_basic_details', array('as' => 'user_profile_basic_details', 'uses' => 'UserController@user_profile_basic_details') )->middleware('cors');

Route::post('user_orders/', array('as' => 'user_orders', 'uses' => 'UserController@user_orders') )->middleware('cors');

Route::post('delete_users_multiple', array('as' => 'delete_users_multiple', 'uses' => 'UserController@delete_users_multiple') )->middleware('cors');

Route::get('get_vendors_list', 'UserController@get_vendors_list')->name('get_vendors_list'); 
 
 
 
 
 
 
   
//=======================================================PRODUCTS==================================================================================================================
//=================================================================================================================================================================================
Route::post('add_products', 'ProductsController@add_products')->name('add_products');
Route::post('products/create', 'ProductsController@create');
	
Route::get('/products/{id}/add_product_meta', 'ProductsController@add_product_meta')->name('add_product_meta');
Route::post('products/store_product_meta', 'ProductsController@store_product_meta')->name('products.store_product_meta');
	
Route::get('/products/{id}/add_product_variants', 'ProductsController@add_product_variants')->name('add_product_variants');
Route::post('products/store_product_variants', 'ProductsController@store_product_variants')->name('products.store_product_variants');

Route::get('/products/{id}/edit', 'ProductsController@edit')->name('product_edit');
Route::post('/products/update_meta', 'ProductsController@update_product_meta')->name('update_product_meta');

Route::post('/products/update_product_variants', 'ProductsController@update_product_variants')->name('update_product_variants');
	
Route::post('/products/delete_product_variants/{product_variant_id}', 'ProductsController@delete_product_variants')->name('delete_product_variants');
Route::post('/products/delete_product_image/{product_meta_values_id}', 'ProductsController@delete_product_image')->name('delete_product_image');

 
 
 
 
 
 
  
//=======================================================MISC==================================================================================================================
//==============================================================================================================================================================================
Route::post('app_data_update/', array('as' => 'app_data_update', 'uses' => 'AppDataController@app_data_update') )->middleware('cors');
Route::get('app_data/', array('as' => 'app_data', 'uses' => 'AppDataController@index') )->middleware('cors');
 
Route::post('get_init_data/', array('as' => 'get_init_data', 'uses' => 'UserController@get_init_data') )->middleware('cors');

Route::get('get_brands_list', 'BrandsController@index')->name('brands_list'); //api
 
Route::post('update_admin_payments_received/', array('as' => 'update_admin_payments_received', 'uses' => 'TransactionsController@update_admin_payments_received') )->middleware('cors');
Route::post('get_admin_payments_received/', array('as' => 'get_admin_payments_received', 'uses' => 'TransactionsController@get_admin_payments_received') )->middleware('cors');
  
Route::post('get_driver_payments_received/', array('as' => 'get_driver_payments_received', 'uses' => 'TransactionsController@get_driver_payments_received') )->middleware('cors');

Route::post('upload_app_logo', array('as' => 'upload_app_logo', 'uses' => 'DashboardController@upload_app_logo') )->middleware('cors'); //clean done
 
});



 
 
 
 
 
//=======================================================CATEGORIES=============================================================================================================
//==============================================================================================================================================================================
   Route::resource('categories', 'CategoriesController');
   Route::post('update_category_form/{category_id}', 'CategoriesController@update_category')->name('update_category');
   Route::post('store_category', 'CategoriesController@store_category')->name('store_category');
   Route::post('update_category/{category_id}', 'CategoriesController@update_category')->name('update_category');
   Route::post('add_categories', 'CategoriesController@add_categories')->name('add_categories');


   
   
   
	
	
	
	
//=======================================================FAQS==================================================================================================================
//==============================================================================================================================================================================
    Route::resource('faqs', 'FaqsController');
	Route::post('store_faq', 'FaqsController@store_faq')->name('store_faq');
	Route::post('update_faq/{faq_id}', 'FaqsController@update_faq')->name('update_faq');

	
	
	

	
	

//=======================================================MISC==================================================================================================================
//==============================================================================================================================================================================
     Route::get('order_invoice/{type}/{order_id?}', array('as' => 'order_invoice', 'uses' => 'OrdersController@order_invoice') )->middleware('cors');
     Route::post('apply_discount', 'OrdersController@apply_discount')->name('apply_discount');
     Route::get('/get_tags', 'TutorialsController@get_tags')->name('get_tags');
     Route::get('/get_auth_skills', 'UserController@get_auth_skills')->name('get_auth_skills');
     Route::get('/logout', 'HomeController@logout')->name('logout');

	
	
	
	
	

	
//=======================================================BRANDS==================================================================================================================
//==============================================================================================================================================================================	
    
 	 Route::resource('brands', 'BrandsController');
     Route::get('brands_list', 'BrandsController@brands_list')->name('brands_list'); //api
     Route::post('store_brand', 'BrandsController@store_brand')->name('store_brand');
     Route::post('update_brand/{brand_id}', 'BrandsController@update_brand')->name('update_brand');

	 
     //Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'UserController@getDashboard', 'as' => 'user.dashboard',]);
     //Route::group( ['middleware' => ['auth']], function() {
	 Route::get('/users/{role}', 'UserController@users_by_role')->name('users_by_role');
     Route::resource('users', 'UserController');

     Route::resource('roles', 'RoleController');
     Route::resource('posts', 'PostController');
	 Route::resource('contact_types', 'ContactTypeController');
     Route::resource('products_list', 'ProductsController');
	
    //product variant types section 
     Route::resource('product_variant_types', 'ProductVariantTypesController');
     Route::resource('brands', 'BrandsController');	
	 Route::get('dropzone', 'HomeController@dropzone');
     Route::post('dropzone/store', ['as'=>'dropzone.store','uses'=>'HomeController@dropzoneStore']);
	 Route::resource('features_settings', 'FeaturesSettingsController');
	 Route::post('features_settings/update', 'FeaturesSettingsController@update_features_settings')->name('update_features_settings');
     Route::get('/get_auth_skills', 'UserController@get_auth_skills')->name('get_auth_skills');
 
//});

 






 
 


//=======================================================MAIN WEBSITE==================================================================================================================
//=====================================================================================================================================================================================


Route::get('register', ['middleware' => 'guest', 'uses' => 'UserController@getRegister', 'as' => 'auth.register',]);
Route::post('register', ['middleware' => 'guest', 'uses' => 'UserController@postRegister',]);
Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'UserController@getDashboard', 'as' => 'user.dashboard',]);
Route::get('profile', ['middleware' => 'auth', 'uses' => 'UserController@getProfile', 'as' => 'user.profile',]);
Route::get('activity', ['middleware' => 'auth', 'uses' => 'UserActivityController@getUserActivity', 'as' => 'user.activity',]);

Route::post('postBoard', ['middleware' => 'auth', 'uses' => 'BoardController@postBoard',]);
Route::post('update-board-favourite', ['middleware' => 'auth', 'uses' => 'BoardController@updateBoardFavourite',]);

 

/**
 * Password Reset
 */
Route::group(
    ['prefix' => 'password'],
    function () {
        // Password reset link request routes...
        Route::get('/email', 'Auth\PasswordController@getEmail');
        Route::post('/email', 'Auth\PasswordController@postEmail');
        // Password reset routes...
        Route::get('/reset/{token}', 'Auth\PasswordController@getReset');
        Route::post('/reset', 'Auth\PasswordController@postReset');
    }
);


Route::post('create-user-activity', ['uses' => 'UserActivityController@createUserActivity']);


Route::get('/ddd', function(){
    $status= \App\OnOff::feature('discount');
	return $status;
});

 

Route::get('email-test', function(){
	$details['email'] = 'harvindersingh@goteso.com';
    dispatch(new App\Jobs\SendEmailTest($details));
    dd('done');
});

Route::match(['get', 'post'], 'ajax-image-upload', 'ImageController@ajaxImage');

Route::delete('ajax-remove-image/{filename}', 'ImageController@deleteImage');

Route::match(['get', 'post'], 'ajax-image-upload-products', 'ImageController@ajaxImageProducts');

Route::delete('ajax-remove-image-products/{filename}', 'ImageController@deleteImageProducts');

Route::match(['get', 'post'], 'ajax-image-upload-logo', 'ImageController@ajaxImageLogo');

Route::delete('ajax-remove-image-logo/{filename}', 'ImageController@deleteImageLogo');
 
Route::get('/settings', function () {   return view('settings.index');});

Route::get('/site_login', function () {
    return view('website.index');
});


Route::get('/site_login', function () {
    return view('website.login');
});


Route::get('/services', function () {
    return view('website.services');
});


Route::get('/about', function () {
    return view('website.about');
});


Route::get('/faq', function () {
    return view('website.faq');
});


Route::get('/privacy-policy', function () {
    return view('website.privacy-policy');
});


Route::get('/register', function () {
    return view('website.register');
});


Route::get('/contact', function () {
    return view('website.contact');
});

Route::get('/terms_conditions', function () {
    return view('website.terms_conditions');
});
  
Route::get('add-new-order', function () { return view('website.add-new-order'); })->middleware('cors');

Route::get('/edit-profile', function () {
    return view('website.edit-profile');
});

Route::get('/order_detail', function () {
    return view('website.order_detail');
});

Route::get('/order_listing', function () {
    return view('website.order_listing');
});

Route::get('/edit-profile', function () {
    return view('website.edit-profile');
});

Route::get('/change-password', function () {
    return view('website.change-password');
});

Route::get('/forgot_password', function () {
    return view('website.forgot_password');
});