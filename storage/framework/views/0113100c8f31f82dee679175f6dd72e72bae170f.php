<?php
   use App\Role;
?>

<?php $__env->startSection('title', 'Categories' ); ?>
		 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/custom.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
			
			 <div class="row">
			    <div class="col-sm-8">
				  	    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				  
					 </div>
					 <div class="col-sm-2 col-lg-2 text-right top-actions">
                        <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
						<!-- <a href="<?php echo e(URL::to('reports_page')); ?>"><i class="fa fa-print" onclicks="myFunction()"></i></a>-->
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
                                        
										
                      </div>
                  <div class="col-sm-2">
				  <div class="text-right">
                   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_categories')): ?> 
                     <button type="button" class="btn btn-success text-right"  data-toggle="modal" data-target="#add">Add New</button>
					 <?php endif; ?>
                  </div>
					 </div>
					 </div>
					 
					 <br><br>
               <div class="row">
                  <div class="col-sm-12">
				   
                     <div class="tab-content  ">
                        <table class="table table-striped table-hover products-table" id="tableToExport">
						
						 
                           <thead>
                              <tr>
                                 <th>Id</th>
                                 <th>Image</th>
								 <th>Title</th>
                                 <th>Created At</th>
                               
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_categories')): ?>  <th class="text-center">Actions</th><?php endif; ?>
                             
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php $x++;?>
                              <tr>
                                 <td>#<?php echo e($item['id']); ?></td>
								 <td>
								 <img src="<?php if($item['photo'] != '') {echo url('/')."/categories/".$item['photo'];}
								 else{echo url('/')."/admin/images/item-placeholder.png";}?>" style="max-height:200px" height="200">
								 
								 </td>
                                 <td><?php echo e($item['title']); ?> </br><?php if($item['parent_category_title'] !='' or $item['parent_category_title'] !=null ){?> <small style="font-size:12px"><b>Parent :</b><?php echo e($item['parent_category_title']); ?></small><?php } ?></td>
                                 <td><?php echo e($item['created_at']->diffForHumans()); ?></td>
                              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_categories')): ?>   
                                 <td class="text-center">
                                    <?php echo $__env->make($model_name.'._actions_with_modals', [
                                    'entity' => $model_name,
                                    'id' => $item['id']
                                    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                 </td>
								 <?php endif; ?>
							
                              
                              </tr>
                              <div id="edit<?php echo e($item['id']); ?>" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                                 <div class="modal-dialog"  >
                                    <!-- Modal content-->
                                    <div class="modal-content"  >
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Update Category</h4>
                                       </div>
                                       <div class="modal-body "  >
									   
									 
                                          <div class="row">
                                             <div class="col-lg-10">
                                             
											   
											    <?php
			       echo Form::open(array('url' => '/update_category_form/'.$item['id'],'files'=>'true'));
				   
				 
			   ?>
											   
                                                <?php echo $__env->make($model_name.'._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                <!-- Submit Form Button -->
                                                <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                                                <?php echo Form::close(); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default"  data-dismiss="modal" onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							  	 <?php $item=null;?>
                              <!-------inner model ends-------->
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </tbody>
                        </table>
						  <div class="text-right">
                                 <?php echo e($result->links()); ?>

                           </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>

</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog "  >
      <!-- Modal content-->
      <div class="modal-content "  >
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Category</h4>
         </div>
         <div class="modal-body "  >
          
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
               
			   <?php
			       echo Form::open(array('url' => '/store_category','files'=>'true'));
			   ?>
                  <?php echo $__env->make($model_name.'._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				  <input type="hidden"   value="<?php echo e(Session::token()); ?>" name="_token" id="token" >
				        <button class="btn btn-md btn-info"  onclick="this.form.submit()">Submit</button>
				   </form>
                  <!-- Submit Form Button -->
            
				 
                
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

 



<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
 
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>