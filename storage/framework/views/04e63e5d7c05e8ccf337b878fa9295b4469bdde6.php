<?php $__env->startSection('title', 'Reports' ); ?>
 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

<div  ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div  ng-controller="reportDataController" style="height:10px;">
   <md-progress-linear class="md-accent" md-mode="determinate" value="{{determinateValue}}"  
      ng-show="isLoading" ng-disabled="isDisabled"></md-progress-linear>
</div>
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
	  
	  <script>
	  var controller_data = '<?php echo json_encode($result); ?>';
	  </script>
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
					 <div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/images/89.svg')); ?>" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
      <div  ng-controller="reportController" ng-cloak>
          
		  <div class="container-fluid">
		    <div class="row">
			<div class="col-sm-12">
		  <div class="panel report-panel">
		    <div class="row">
			
			
			
			
			 <div class="col-sm-3">
		      <ul class="nav nav-pills nav-stacked nav-reprt">
                 <li class="active revenue"><a data-toggle="pill" ng-click="refresh_graph('revenues')" href="#revenue"><?php echo env('CURRENCY_SYMBOL');?>{{total_revenue}}<br><span>Revenue (Last 30 Days)</span></a></li>
                 <li class="order"><a  data-toggle="pill" ng-click="refresh_graph('orders')" href="#order">{{total_orders}}<br> <span>Orders (Last 30 Days)</span></a></li>
                 <li class="revenue"><a data-toggle="pill" ng-click="refresh_graph('users')"  href="#users">{{total_users}} <br><span>Users Joined (Last 30 Days)</span></a></li> 
             </ul>
			 </div>
			 
			 
			 <div class="col-sm-9">
			   <div class="tab-content">
			    <div id="revenue" class="tab-pane fade in  active" >  
			      <hc-chart options="revenueReport">Placeholder for generic chart</hc-chart> 
				 </div>
				 
				  <div id="order" class="tab-pane fade " >  
			      <hc-chart options="orderReport">Placeholder for generic chart</hc-chart> 
				 </div>
				 
				  <div id="users" class="tab-pane fade  " >  
			      <hc-chart options="userReport">Placeholder for generic chart</hc-chart> 
				 </div>
			   </div>
			 </div>
			</div>
			
          </div>
		  </div>
		  </div>
	  </div>
	  
	 </div>
	 
	 
	  <div ng-controller="reportDataController" ng-cloak>
	 <div class="container-fluid">
  <div class="row">
     <div class="col-sm-12" > 
	    <ul class="nav nav-tabs report-tabs" >
			<li  class="active" ><a href="#recomended" data-toggle="tab"   >Recommended</a></li>
			<li  class="" ><a href="#report-order" data-toggle="tab"   >Orders</a></li>
			<li  class="" ><a href="#transactions" data-toggle="tab"   >Transactions</a></li>
			<li  class="" ><a href="#customer" data-toggle="tab"   >Customers </a></li> 
			<li  class="" ><a href="#driver" data-toggle="tab"   >Drivers </a></li> 
			<li  class="" ><a href="#sales" data-toggle="tab"   >Sales </a></li> 
        </ul>
							  
        <div class="tab-content tab-content-data">
          <div id="recomended" class="tab-pane fade in  active" >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row"> 
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.recommended == '1'">
				     <h4 class="report-title">{{data.title}} <span> <a target="_blank" href=" <?php echo e(URL::asset('')); ?>{{data.url}}">Generate</a></span></h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
		  <div id="report-order" class="tab-pane fade " >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row">
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.identifier == 'Orders'">
				     <h4 class="report-title">{{data.title}} <span>   <a target="_blank" href="<?php echo e(URL::asset('')); ?>{{data.url}}">Generate</a> 
					 </span></h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
		   <div id="transactions" class="tab-pane fade " >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row">
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.identifier == 'Transactions'">
				     <h4 class="report-title">{{data.title}} <span> 
					     <a target="_blank" href="<?php echo e(URL::asset('')); ?>{{data.url}}">Generate</a>
					</span></h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
		  <div id="customer" class="tab-pane fade " >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row">
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.identifier == 'Customers'">
				     <h4 class="report-title">{{data.title}} 
					   <span > 
					     <a target="_blank" href="<?php echo e(URL::asset('')); ?>{{data.url}}" >Generate</a> 
					   </span>
					 </h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
		  
		  
		  		  <div id="driver" class="tab-pane fade " >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row">
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.identifier == 'Drivers'">
				     <h4 class="report-title">{{data.title}} 
					   <span > 
					     <a target="_blank" href="<?php echo e(URL::asset('')); ?>{{data.url}}" >Generate</a> 
					   </span>
					 </h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
		   <div id="sales" class="tab-pane fade " >  
			<div class="panel">
			  <div class="panel-body ">
                <div class="row">
				  <div class="col-sm-4" ng-repeat="data in reports.reports_list" ng-show="data.identifier == 'Sales'">
				     <h4 class="report-title">{{data.title}} <span ng-show="data.url"> 
					    <a target="_blank" href="<?php echo e(URL::asset('')); ?>{{data.url}}">Generate</a>
					</span></h4>
					 <p class="report-desc">{{data.description}}</p>
				  </div>
				</div>
              </div> 					 
			</div>					 
          </div>
		  
        </div> 
		
     </div>
   </div>
</div>

	 </div>
	                      
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>
</div>

		<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/reports.js')); ?>"></script> 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>