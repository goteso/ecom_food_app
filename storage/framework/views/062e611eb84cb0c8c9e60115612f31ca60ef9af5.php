<?php $__env->startSection('title', 'Products List' ); ?> 
		 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/custom.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
 
 <div  ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div  ng-controller="productController as ctrl" style="height:10px;">
   <md-progress-linear class="md-accent" md-mode="determinate" value="{{ctrl.determinateValue}}"  
      ng-show="isLoading" ng-disabled="isDisabled"></md-progress-linear>
</div>

<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
	  <!--------------------------- Angular App Starts ---------------------------->
	  
<script type="text/javascript">
 
</script>
 
 <textarea id="res" style="display:none ;" ></textarea>
	   <div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/images/89.svg')); ?>" class="img-responsive center-block">				 
					 <p >Calling all data...</p>
					 </div>  
 			 
         <div class="container-fluid" >
		 <div class="row">
             <div class="col-sm-12" >
	           <h2 class="header">Products</h2>
	         </div>  
           </div>
            <div class="row" >
               <div class="col-sm-12" ng-controller="productController as ctrl" ng-cloak>
                  <div class="" > 
				  <br>
					  <div class="row">
					   <div class="col-sm-8 col-lg-9"> 
					   <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
					     <div class="row">
					            <div class="col-sm-6 col-md-6  col-lg-3" >
							     <div class="header-fliters-data">
								  <label>Batch Action :</label>
								  
								   <div class="dropdown" >
                                    <button class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">Select Actions 
									<span class="caret"></span>
									</button>
                                    <ul class="dropdown-menu"> 
										 <li><a href=""ng-click="remove($index)" >Delete All</a></li>  
                                    </ul>
                                 </div>
								 
					 
								  
								   
						       </div>
							   </div>
				                               <div class="col-sm-6 col-md-6 col-lg-3" >
                                                   <div class="header-fliters-data">
                                                      <label>Sort by Date :</label>
                                                      <select  class="form-control" id="date_filter"  ng-model="date_filter" ng-change="filter_products()">
                                                         <option value="">Select Type </option>
														 <option value="lastAdded">Last Added </option>
														 <option value="firstAdded">First Added </option>
														 
												      </select>
                                                   </div>
                                                </div>
												
												
												<div class="col-sm-6 col-md-6  col-lg-3" >
                                                   <div class="header-fliters-data">
                                                      <label>Sort by Price :</label>
                                                      <select  class="form-control" id="price_filter"  ng-model="price_filter" ng-change="filter_products()">
                                                         <option value="">Select Type </option>
													     <option value="highToLow">Price(High to Low)</option>
														 <option value="lowToHigh">Price(Low to High)</option>
												      </select>
                                                   </div>
                                                </div>
												
												<?php
												$categories = \App\Categories::get();
											    ?>
                                                <div class="col-sm-6 col-md-6 col-lg-3 ">
                                                  <div class="header-fliters-data">
                                                      <label>Filter By Category :</label>
                                                      <select  class="form-control" id="category_filter"  ng-model="category_filter" ng-change="filter_products()">
                                                         <option value="">Select Category </option>
														 
														 <?php
														 foreach($categories as $c)
														 {
														 ?>
														 <option value="<?php echo $c->id;?>"><?php echo $c->title;?> </option>
														  
														 <?php } ?>
                                                      </select>
                                                   </div>
                                                </div> 
												
												<?php
												   $status = \App\FeaturesSettings::where('title','brand_ids')->first(['status'])->status;
												if($status == '1')
												{
												 
												$brands = \App\Brands::get();
											    ?>
												<div class="col-sm-6 col-md-6 col-lg-4  ">
                                                  <div class="header-fliters-data">
                                                      <label>Filter By Brands :</label>
                                                      <select  class="form-control" id="brand_filter"  ng-model="brand_filter" ng-change="filter_products()">
                                                         <option value="">Select Brand </option>
														 
														 <?php
														 foreach($brands as $c)
														 {
														 ?>
														 <option value="<?php echo $c->id;?>"><?php echo $c->title;?> </option>
														  
														 <?php } ?>
                                                      </select>
                                                   </div>
                                                </div> 
												 <?php } ?>
                            </div>							 
					   </div>
					   
					   <div class="col-sm-2 col-lg-2 text-right top-actions" style="display:none">
					     <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
						<!--<a href="<?php echo e(URL::to('reports_page')); ?>"><i class="fa fa-print" onclicks="printData('tableToExport')"></i></a>-->
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
						<div class="dropdown column-settings" style="display:none" >
                                    <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown" ><i class="fa fa-cog"></i> 
									</button>
                                    <ul class="dropdown-menu" >
                                          <li ng-repeat="c in ctrl.products.columns"  ng-init="c.visible = {{c.visible}}"> 
										   <input type="checkbox" ng-true-value="1" ng-false-value="'0'"   ng-model="c.visible"> {{c.title}}
										 </li> 

                                            <hr>	 
										 <li style="display:none">
										 
						    <label for="search" class="pagesize-label">Rows</label>
                            <input type="number" min="1" max="100" class="form-control pagesize" ng-model="pageSize">
										 </li>
                                    </ul>
                                 </div> 
						 </div>
						 
						 
				          <div class="col-sm-4 col-lg-3 text-right">
                                             <div class="input-group">
                                                <input class="form-control"  id="search_text"  placeholder="Search Product" ng-model="search_text"  /> 
                                                <span type="button" class="input-group-addon" ng-click="filter_products()" > <span class="fa fa-search"></span> </span>
                                             </div>
                                          </div>
					   </div> 

					   <br> 
					   
					   		 <?php  $st = \App\FeaturesSettings::where("title",'photo')->first(['status'])->status;
                                    $base_price_settings = \App\FeaturesSettings::where("title",'base_price')->first(['status'])->status;							 ?>
						<div id="tableToExport" class="products-table table-responsive"  >
                        <table class="table" class="table table-striped" id="exportthis" >
						<thead>
                           <tr>
						     <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" style="display:none"/></th>
							 <th>ID</th>
							 <th>TITLE</th>
							 <?php if($base_price_settings =='1' or $base_price_settings == 1) { ?><th>PRICE</th><?php } ?>
							 <?php if($st == '1' or $st == 1) { ?> <th>PHOTO</th><?php } ?>
							 <th>CREATED</th>							 
							  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_products')): ?><th>ACTIONS</th><?php endif; ?>
                           </tr>
						   </thead>
                           <tbody > 
                              <tr  ng-repeat="values in ctrl.products.data.data|filter:search|orderBy:'index'| orderBy : sortByName : reverse | orderBy : sortByPrice : reverse ">
							     <td><input type="checkbox" ng-checked="" ng-model="values.selected" ng-click="checkedIndex(values)"></td>
								 
								 <td><a href="<?php echo e(URL::to('product_edit')); ?>/{{values.id}}" ><b>#{{values.id}}</b></a> </td>
								 
								 
								 <td>{{values.title}}</td>
								 
								 		 
							
									 
									 
								 <?php if($base_price_settings =='1' or $base_price_settings == 1) { ?><td ><span ng-show='values.base_price > 0'><?php echo env("CURRENCY_SYMBOL", "");?>{{values.base_price}}</span></td><?php } ?>
								 
								 		 <?php     
									 if($st == '1' or $st == 1)
									 {
									 ?>
									 <td><img src="<?php echo url('/');?>/products/{{values.photo}}" style="height:40px;width:40px" ng-hide="!values.photo"> <img class="img-responsive" style="height:40px;width:40px" src="<?php echo url('/').'/admin/images/item-placeholder.png';?>" ng-show="!values.photo"> </td>
									 <?php } ?>
									 
								 <td>{{values.created_at_formatted}}</td>
				 
							  
								  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_products')): ?>  
								 <td class="actions"> 
								  <a class="btn btn-xs edit-product" href="<?php echo e(URL::to('product_edit')); ?>/{{values.id}}" ><i class="fa fa-edit"></i></a>
								   <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"></i></a>
								   <!--  <a class="btn btn-xs edit-product" href="<?php echo e(URL::to('product_edit')); ?>/{{values.id}}" ><img src="<?php echo e(url::to('admin/images/edit.png')); ?> " style="height:16px;width:16px"/></a>
								   <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><img src="<?php echo e(url::to('admin/images/bin.png')); ?> " style="height:16px;width:13px"/></a>-->
								 </td>
								 <?php endif; ?>
                              </tr> 
                           </tbody>
						   
						     
                        </table> 
						
						
						   
						   
						   
	  
						</div> 
                     </div> 
                  </div>
              
			   <div class="col-sm-12 " ng-controller="OtherController">
			   <div class="other-controller">
						 <dir-pagination-controls
                            max-size="5"
                           direction-links="true"
                            boundry-links="true" on-page-change="pageChangeHandler(newPageNumber)">
                      </dir-pagination-controls> 
						  </div>
						  </div>
            </div>
			 </div>
			   
         </div> 
	   
	  <!--------------------------- Angular App Ends ---------------------------->
	  
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/products.js')); ?>"></script> 
	  <!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
	   <script src="<?php echo e(URL::asset('admin/js/dirPagination.js')); ?>"></script>

<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>