<?php $__env->startSection('title', 'Settings'); ?>
 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

<div id="wrapper"  > 
 

   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!-- Modal -->
 

                   <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid  ">
  
       
	  <div class="row">
               <div class="col-sm-12">
                  <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <div class="text-right">
                  </div>
                  <div class="tab-content" >
				   <div ng-app="mainApp">
				  <div ng-view></div>
				  </div>
		</div>
	  </div>
	  </div> 
	   
   </div>
    
   </section>
</div>

</div>

</div>



		
		
		<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/settings.js')); ?>"></script> 
 

<!----manage modal ends here--->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>