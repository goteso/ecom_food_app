<?php $__env->startSection('title', 'Report Details' ); ?>
 
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
	  
	<!--  <script>
function myFunction() {
    window.print();
}
</script>-->


      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
					 <div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/images/89.svg')); ?>" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
      <div ng-app="mainApp" ng-controller="reportController" ng-cloak>
         <div class="container-fluid" >
            <div class="row" >
               <div class="col-sm-12" >
			    
                  <div class="panel reports-panel" ng-repeat="data in reports">
                     <div class="panel-header">
					 <div class="row">
					   <div class="col-sm-6">
					     <button class="btn btn-info edit" ng-click="ShowHide()" style="display:none">Edit Notes <i class="fa fa-pencil"></i></button>
					   </div>
					   <div class="col-sm-6 text-right">
					     <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
					     <i class="fa fa-print" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
					     <i class="fa fa-cog" style="display:none"></i>
						 <i class="fa fa-print" onclick="myFunction()" ></i>
					   </div>
					   </div>
                     </div>
                     <div class="panel-body" >
                        <h2 class="text-center title" >{{data.title}}</h2>
                        <h4 class="text-center text-uppercase subtitle"> {{data.subTitle}} </h4>
                        <h4 class="text-center date"> {{data.dateRange}}</h4>
						<div id="tableToExport">
                        <table class="table" id="exportthis">
                           <tr>
                              <th ng-repeat="c in data.columns | orderBy:'index'" ng-show="c.visible == 1 " > {{c.title}}</th>
                           </tr>
                           <tbody ng-repeat="values in data.data">
                              <tr>
                                 <td>
                                    <h4>{{values.header}}</h4>
                                 </td>
                              </tr>
                              <tr ng-repeat="r in values.rows">
                                   <td  ng-repeat="c in data.columns"ng-show="c.visible == 1 " > <span ng-repeat="value in r | orderBy:'index'" ng-show="c.index== $index" >{{value}}</span></td>  
								  
                              </tr>
                           </tbody>
                        </table>
						</div>
						
						<div class="row" > 
						 <div class="col-sm-12" ng-show= "IsVisible"> 
						  <p >{{notes}}</p>
						  </div>
						 <form ng-show = "IsHidden" ng-submit="submit()"> 
						  <div class="col-sm-8 col-md-9 col-lg-10"> 
						      <textarea class="form-control" ng-model="notes" id="notes" name="notes">{{data.notes}}</textarea>
						  </div>
						   <div class="col-sm-4 col-md-3 col-lg-2">
						     <button type="submit" class="btn btn-success">Save</button>
						   </div>
						 </form>
						</div>
						
                     </div>
                     <div class="panel-footer">
                        <p class="text-center">{{data.footer}}</p>
                     </div>
                  </div> 
               </div>
            </div>
         </div>
      </div>
	  
	                      
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


		<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/reportDetail.js')); ?>"></script> 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>