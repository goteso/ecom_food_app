 

 

<input type="file" name="image" id="image">
 
<div class="form-group <?php if($errors->has('title')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('title', 'Title'); ?>

    <?php echo Form::text('title', @$item->title, ['class' => 'form-control ', 'placeholder' => 'Category Title' ] ); ?>

    <?php if($errors->has('title')): ?> <p class="help-block"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>
</div>


<div class="form-group <?php if($errors->has('result')): ?> has-error <?php endif; ?>">
<?php echo Form::label('result', 'Parent Category'); ?>


 
  <select   class="form-control" id="parent_id" name="parent_id"  style="width:100%">
  <option value="">Select Parents Category(optional)</option>
      <?php 
	
	    $categories_level = \App\Settings::where('key_title','categories_level')->first(['key_value'])->key_value;
	  ?>
      <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	    <?php 
		  if($categories_level =='1' or $categories_level ==1)
		  {
			  if($ct->parent_id == '')
			  {
			  ?>
			   <option value="<?php echo e($ct->id); ?>" <?php if(@$item->parent_id == $ct->id ) { echo 'SELECTED';}?> ><?php echo e($ct->title); ?> </option>
		 <?php }
		  }
		  
		  else
		  {?>
			   <option value="<?php echo e($ct->id); ?>" <?php if(@$item->parent_id == $ct->id ) { echo 'SELECTED';}?> ><?php echo e($ct->title); ?> </option>
		 <?php }
		 ?>
	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
</div>



 
 

<script type="text/javascript">
   $(document).ready(function() {
       $('#multi_select_box').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   
 });
</script>

 

 
<script type='text/javascript'>
$(window).load(function(){
$('.datetimepicker').datetimepicker({  format:'DD/MM/YYYY hh:mm A', });
});
</script>