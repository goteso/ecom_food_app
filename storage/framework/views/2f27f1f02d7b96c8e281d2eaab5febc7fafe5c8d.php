 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu">
                    
				<li><a href="<?php echo e(URL::to('get_dashboard_data')); ?>"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
					
					
				<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_orders')): ?>  	
		          <li><a href="<?php echo e(URL::to('orders_list')); ?>"><i class="fa fa-file"></i><span>Orders</span></a></li> 
				<?php endif; ?>
				
				<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_products')): ?>  
				  <li><a href="<?php echo e(URL::to('product_list')); ?>"><i class="fa fa-list"></i><span>Products</span></a></li>
				<?php endif; ?>
				 
				<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_categories')): ?>  
				  <li><a href="<?php echo e(URL::to('view_categories')); ?>"><i class="fa fa-file"></i><span >Categories</span></a></li>
				<?php endif; ?>  
				
				
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_brands')): ?> 				
				  <?php $status = \App\FeaturesSettings::where('title','brand_ids')->first(['status'])->status;
				  if($status == '1')
				  {
				  ?>
				  <li><a href="<?php echo e(URL::to('brands')); ?>"><i class="fa fa-file"></i><span >Brands</span></a></li>
				  <?php } ?>
				<?php endif; ?>  
				
				<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_users')): ?> 

                 <?php $roles = @\App\Role::where('sidebar_status','1')->get();
				 foreach($roles as $role)
				 {
				 ?>				
                  <li><a href="<?php echo e(URL::to('user_list_page')); ?>/<?php echo e($role->id); ?> "><i class="fa fa-user"></i><span ><?php echo e($role->name); ?> </span></a></li>
				  
				 <?php } ?>
		 
				 <?php endif; ?> 
				  
				  
				  
				  
				  
				  
				  	 				
                  <li><a href="<?php echo e(URL::to('stores_list')); ?>"><i class="fa fa-shopping-cart"></i><span >Stores </span></a></li>
		 
				 
				 
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_reports')): ?> 
				  <li><a href="<?php echo e(URL::to('reports_page')); ?>"><i class="fa fa-file"></i><span>Reports</span></a></li> 
				 <?php endif; ?>
				 
				 
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_roles')): ?> 
                  <li><a href="<?php echo e(URL::to('roles')); ?>"><i class="fa fa-user"></i><span>Roles & Permissions</span></a></li>
				 <?php endif; ?>  
				  
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_features_settings')): ?> 
				  <li><a href="<?php echo e(URL::to('features_settings')); ?>"><i class="fa fa-wrench"></i><span>Features Settings</span></a></li>
				 <?php endif; ?>
				   
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_transactions')): ?> 
				  <li><a href="<?php echo e(URL::to('transactions_list')); ?>"><i class="fa fa-list"></i><span>Transactions List</span></a></li>
				 <?php endif; ?>
				   
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_faqs')): ?> 
				  <li><a href="<?php echo e(URL::to('faqs_list')); ?>"><i class="fa fa-question-circle"></i><span>Faqs</span></a></li>
				 <?php endif; ?>
				 
				   
				  <li><a href="<?php echo e(URL::to('upload_logo')); ?>"><i class="fa fa-upload"></i><span>Upload Logo</span></a></li>
				 
                  <li><a href="<?php echo e(URL::to('settings')); ?>"><i class="fa fa-cog"></i><span>Settings</span></a></li>
				  
               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>