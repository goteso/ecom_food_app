<?php $__env->startSection('title', 'Users' ); ?>
		 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/custom.css')); ?>">
		 <style>.disabled {
  cursor: not-allowed;
}</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
      <script type="text/javascript"></script>
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
	  
	  <script>
	   
	  </script>
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="text-right">
                     </div>
                     <textarea id="res" style="display:  ; "></textarea>
                     <div class="tab-content" >
 <div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/images/89.svg')); ?>" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
                        <div ng-app="mainApp" >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-12" >
                                    <h2 class="header"><?php echo @\App\Role::where('id',@$user_type )->first(['name'])->name;?></h2>
									
									    
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" ng-controller="userController as ctrl"  ng-cloak>
                                    <div class="" >
                                       <br>
                                       <div class="row">
                                          <div class="col-sm-6 col-lg-7">
                                             <div class="row">
                                                <div class="col-sm-6 col-md-4 " >
                                                   <div class="header-fliters-data">
                                                      <label>Batch Action :</label>
                                                      <div class="dropdown" >
                                                         <button class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">Select Actions 
                                                         <span class="caret"></span>
                                                         </button>
                                                         <ul class="dropdown-menu">
                                                            <li><a href=""ng-click="remove($index)" >Delete All</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4 " >
                                                   <div class="header-fliters-data">
                                                      <label>Sort :</label>
                                                      <select  class="form-control" id="date_filter"  ng-model="date_filter" ng-change="filter_users()">
                                                         <option value="">Select Type </option>
														 <option value="lastAdded">Last Added </option>
														 <option value="firstAdded">First Added </option>
                                                      </select>
                                                   </div>
                                                </div>
                                      
											 
												
												<input type='hidden' id="user_type_filter" value="<?php echo @$user_type;?>" />
                                             </div>
                                          </div>
                                          <div class="col-sm-2 col-lg-2 text-right top-actions">
                                               <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
						<a href="<?php echo e(URL::to('reports_all_customers')); ?>" target="_blank"><i class="fa fa-print" onclicks="printData('tableToExport')"></i></a>
					    <i class="fa fa-share" ng-click="exportToExcel('#tableToExport')" style="display:none"></i> 
                                             <div class="dropdown column-settings" style="display:none"  >
                                                <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown" ><i class="fa fa-cog"></i> 
                                                </button>
                                                <ul class="dropdown-menu" >
                                                   <hr>
                                                   <li>
                                                      <label for="search" class="pagesize-label">Rows</label>
                                                      <input type="number" min="1" max="100" class="form-control pagesize" ng-model="pageSize">
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="col-sm-4 col-lg-3 text-right">
                                             <div class="input-group">
                                                <input class="form-control"  id="search_text"    ng-model="search_text"  /> 
                                                <span type="button" class="input-group-addon" ng-click="filter_users()" > <span class="fa fa-search"></span> </span>
                                             </div>
                                          </div>
                                       </div>
                                       <br> 
                                       <div id="tableToExport" class="products-table table-responsive" id="exportthis" >
									    
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr>
                                                   <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" style="display:none"/></th>
                                                   <th ng-repeat="c in ctrl.columns | orderBy:'index'"  ng-show="c.visible" ng-if="c.visible == 1" >{{c.title.replace('_', ' ')  | uppercase }}</th>
                                                  <!-- <th>Addresses</th>-->
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?>  <th>ACTIONS</th><?php endif; ?>
                                                </tr>
                                             </thead>
                                             <tbody >
											 
                                                <tr  ng-repeat="values in ctrl.data.data |   orderBy:'index'| orderBy : sortByName : reverse | orderBy : sortByPrice : reverse  ">
                                                   <td>
                                                      <input type="checkbox" ng-checked="" ng-model="values.selected" ng-click="checkedIndex(values)"> 
                                                      <h1></h1>
                                                   </td>
                                                   <td ng-repeat="c in ctrl.columns" class="" ng-show="c.visible == 1" >
                                                    <!---  <span class="id" ng-show="$index == '0'" ><a class=" " href="<?php echo e(URL::to('user_edit_form')); ?>/{{values.id}}">#{{values.id}}</a> </span> --->
													 
													 
													  <span class="id" ng-show="$index == '0'"  ><a class=" "  ng-show="values.user_type_title == 'Driver' || values.user_type_title == 'Customer'" ng-attr-href="{{(values.user_type_title != 'Driver') ? '<?php echo env("APP_URL", "");?>/user_profile?user_id='+values.id : '<?php echo env("APP_URL", "");?>/driver-profile?user_id='+values.id}}"   >#{{values.id}}</a> <span  ng-show="values.user_type_title != 'Driver' && values.user_type_title != 'Customer'" >{{values.id}}</span>
													      </span>
                                                      <span class="" ng-show="$index == '1'">{{values.first_name}} </span>
                                                      <span class="name" ng-show="$index == '2'">{{values.last_name}} </span>
                                                      <span class="price" ng-show="$index == '3'">{{values.email}} </span>
                                                      <span class="created" ng-show="$index == '4'">{{values.mobile}} </span>
                                                      <span class="created" ng-show="$index == '5'">
                                                        <img class="img-responsive" style="height:40px;width:40px" src="<?php echo url('/').'/admin/images/user-placeholder.png';?>" ng-show="!values.photo">
							<img class="img-responsive" style="height:40px;width:40px" src="<?php echo url('/').'/users/';?>{{values.photo}}" ng-show="values.photo">
                                                      </span> 
													  
													      <span class="created" ng-show="$index == '6'">
														  {{values.user_type_title}}
														  </span>
                                                      <span class="created" ng-show="$index == '7'">
                                                         <pre>{{values.created_at}}</pre>
                                                      </span>
													  
													 
                                                   </td>
                  
                                                   <td style="display:  none">
                                                      <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" ng-click="get_addresses(values.id)">Addresses</button>
                                                   </td>
												   
												    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?> 
												     <td class="actions">
                                                      <button type="button" class="btn btn-info btn-sm" data-toggle="modal" value="{{values.id}}" data-target="#changePassword" onclick="setPassword(this.value)" value="values.id"  ><i class="fa fa-key"></i></button>
                                                
                                                  
                                                      <a class="btn btn-xs edit-product" href="<?php echo e(URL::to('user_edit_form')); ?>/{{values.id}}"><i class="fa fa-edit"></i></a>
                                                       <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"> </i></a>  
                                                   </td>
												   <?php endif; ?>
												   
												   
												   
								
									
									
									
									
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
									
									<script>
									function setPassword(v)
									{
										document.getElementById("user_id").value = v;
										return false;
									}
									</script>
           <!------------------------------------------- view address  Modal starts ------------------------------------------------>
                                    <div id="myModal" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"  style="width:80%;z-index:9999999999999999999" data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
											 
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
												 <button type="button" data-toggle="modal" data-target="#myModal_add" ng-click="add_address_form()" >Add New</button>
                                                <h4 class="modal-title">View Addresses</h4>
                                             </div>
                                             <div class="modal-body" >
                                                <p>List of Addresses.</p>
                                                <table class="table" id="exportthis1 "  >
                                                   <thead>
                                                      <tr>
                                                         <th> <input type="checkbox" ng-model="selectedAll" ng-click="isSelectAll()" /></th>
                                                         <th ng-repeat="c in ctrl.columns_addresses | orderBy:'index'"  ng-show="c.visible" ng-if="c.visible == 1" >{{c.title}}</th>
                                                         <th>Addresses</th>
														 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?> 
                                                         <th>ACTIONS</th>
														 <?php endif; ?>
                                                      </tr>
                                                   </thead>
                                                   <tbody >
                                                      <tr  ng-repeat="values in ctrl.data_addresses.data ">
                                                         <td>
                                                            <input type="checkbox" ng-checked="" ng-model="values.selected" ng-click="checkedIndex(values)"> 
                                                            <h1></h1>
                                                         </td>
                                                         <td ng-repeat="c in ctrl.columns_addresses" class="" ng-show="c.visible == 1" >
                                                            <span class="" ng-show="$index == '0'">{{values.address_line1}} </span>
                                                            <span class="name" ng-show="$index == '1'">{{values.address_line2}} </span>
                                                            <span class="price" ng-show="$index == '2'">{{values.city}} </span>
                                                            <span class="created" ng-show="$index == '3'">{{values.state}} </span>
                                                            <span class="created" ng-show="$index == '4'">{{values.country}} </span>
                                                            <span class="created" ng-show="$index == '5'">{{values.pincode}} </span>
                                                            <span class="created" ng-show="$index == '6'">
                                                               <pre>{{values.created_at}}</pre>
                                                            </span>
                                                         </td>
                                                         <td>
														 
											 
															<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" ng-click="get_addresses(values.id)">Addresses</button>
                                                         </td>
														 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?> 
                                                         <td class="actions">
                                                            <a class="btn btn-xs edit-product"  data-toggle="modal" data-target="#myModal_edit" ng-click="user_address_forms(values.id)"  ><i class="fa fa-edit"></i></a>
                                                           <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><i class="fa fa-trash-o"> </i></a>  
                                                         </td>
														 <?php endif; ?>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
           <!------------------------------------ view addresses ends -----------------------------------> 
		   
		   
		   
		   
		   
		   				                       <div id="changePassword" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"   data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
											 
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
								 
                                                <h4 class="modal-title">Enter New Password </h4>
                                             </div>
                                             <div class="modal-body" >
                                                 
			                                 <input type="hidden" ng-model="values.id" value='values.id' class="form-control"   id="user_id"  required> 
							  
                                             <div class="form-group "   >
                                                  <label> Password</label>
                                                  <input type="password"  class="form-control" id="new_password"  required> 
                                             </div>
											 
											  <div class="form-group "   >
                                                 <button type="button" class="btn btn-success" ng-click="changePassword()" >Update</button>
                                             </div>
						 
                                        
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
		 
									
									
		   
		   <!------------------------------------------- Edit address  Modal starts ------------------------------------------------>
                                    <div id="myModal_edit" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"  style="width:80%;z-index:9999999999999999999" data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Address</h4>
                                             </div>
                                             <div class="modal-body" >
                                      
										   
								 <form name="myForm">
                     
                        <div class="row">
                           <div class="col-lg-6 col-sm-12" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
						   
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'text'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Email Type Input Field----------->
                              <div class="form-group " ng-show="data.type == 'email'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required> 
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'password'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'radio'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'checkbox'">
                                 <label for="{{data.identifier}}">{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="{{data.type}}"  ng-model="data.value[options.value]" id="{{options.value}}"  >{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" >{{options.title}}
								 </label>
                              </div>
							  
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'Selectbox'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'multipleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields">
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'singleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  </div>
							 <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-show="data.symbol">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">{{data.symbol}}</span>
                                    <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}"  >
                                 </div>
                              </div>
							  </div>
							<div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'datePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  />
                                 </datepicker>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'timePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  </div>
						<div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_edit_address_data[0].fields" >
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"ng-show="data.type == 'longText'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <textarea class="form-control" name="{{data.identifier}}"ng-model="data.value"  id="{{data.identifier}}" >{{data.value}}</textarea>
                              </div>
							  
                           </div>
                           
						   
                        </div>
                   
					 
					 
                              <button   ng-click="user_address_update()" class="btn btn-success"> Save Changes </button>
                  </form>
				  
										   
										   
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
           <!------------------------------------ Add addresses ends -----------------------------------> 
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   		   
		   <!------------------------------------------- Add address  Modal starts ------------------------------------------------>
                                    <div id="myModal_add{{values.id}}" class="modal fade" role="dialog" style="z-index:9999999999999999999" data-backdrop="false">
                                       <div class="modal-dialog modal-lg"  style="width:80%;z-index:9999999999999999999" data-backdrop="false">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Add New Address</h4>
                                             </div>
                                             <div class="modal-body" >
                                              
											  {{values.id}}
										   
								 <form name="myForm">
                     
                        <div class="row">
                           <div class="col-lg-6 col-sm-12" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
						   
						      <!----- For Simple Text Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'text'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Email Type Input Field----------->
                              <div class="form-group " ng-show="data.type == 'email'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required> 
                              </div>
							  </div>
							   <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Password Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'password'" >
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  required>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Radio Button Type Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'radio'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Checkbox Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'checkbox'">
                                 <label for="{{data.identifier}}">{{data.title}} </label> <br>
								 
								 <div class="checkbox" ng-repeat="options in data.field_options">
                                    <label><input type="{{data.type}}"  ng-model="data.value[options.value]" id="{{options.value}}"  >{{options.title}}</label>
                                 </div>


								 <label class="checkbox-inline" ng-repeat="options in data.field_options">
								   <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" >{{options.title}}
								 </label>
                              </div>
							  
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Select Box Type Input Field----------->
                              <div class="form-group" ng-show="data.type == 'Selectbox'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> <br>
                                  <label  class="radio-inline" ng-repeat="options in data.field_options">
								     <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}">{{options.title}}
								  </label> 
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Multiple Tag With Autocomplete Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'multipleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label> 
                                 <tags-input ng-model="externalContacts" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="title"   text="text" ng-blur="text=''">
                                    <auto-complete min-length="1" highlight-matched-text="true" source="searchPeople($query)"></auto-complete>
                                 </tags-input>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields">
							  <!----- For Single Tag Input Field----------->
                              <div class="form-group"  ng-show="data.type == 'singleTag'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <tags-input  name="tags" display-property="value" ng-model="data.value" ></tags-input>
                              </div>
							  </div>
							 <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Simple Text Input Field----------->
                              <div class="form-group" ng-show="data.symbol">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">{{data.symbol}}</span>
                                    <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}"  >
                                 </div>
                              </div>
							  </div>
							<div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Datepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'datePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <datepicker  date-format="yyyy-MM-dd"  button-prev='<i class="fa fa-arrow-circle-left"></i>'  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                    <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3"  />
                                 </datepicker>
                              </div>
							  </div>
							  <div class="col-lg-5 col-sm-5" ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Timepicker Input Field----------->
                              <div class="form-group" ng-show="data.type == 'timePicker'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <div class="input-group clockpicker"  clock-picker  data-autoclose="true" > 
                                    <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control">
                                    <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                    </span>
                                 </div>
                              </div>
							  </div>
						<div class="col-lg-5 col-sm-5"  ng-repeat="data in ctrl.columns_add_address_data[0].fields" >
							  <!----- For Textarea Input Field----------->
							  <div class="form-group"ng-show="data.type == 'longText'">
                                 <label for="{{data.identifier}}">{{data.title}}</label>
                                 <textarea class="form-control" name="{{data.identifier}}"ng-model="data.value"  id="{{data.identifier}}" >{{data.value}}</textarea>
                              </div>
							  
                           </div>
                           
						   
                        </div>
                   
					 
					 
                              <button   ng-click="add_address()" class="btn btn-success"> Update </button>
                  </form>
				  
										   
										   
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
           <!------------------------------------ Edit addresses ends -----------------------------------> 
		   
		   
		   
		   
		   
                                 </div>
                                 <div class="col-sm-12 " ng-controller="OtherController">
                                    <div class="other-controller">
                                       <dir-pagination-controls
                                          max-size="5"
                                          direction-links="true"
                                          boundry-links="true" on-page-change="pageChangeHandler(newPageNumber)">
                                       </dir-pagination-controls>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>

 
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/users.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>