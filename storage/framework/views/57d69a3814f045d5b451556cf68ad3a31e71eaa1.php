	<header id="topnav" class="navbar navbar-dark navbar-fixed-top clearfix" role="banner">

	 <?php $app_logo = @\App\Settings::where('key_title','app_logo')->first(['key_value'])->key_value;  ?>
	  
	                <span id="trigger-sidebar" class="toolbar-trigger" >
	                  	  <div class="navbar-header" style="height: 100%;" >
      <a class="navbar-brand" href="#" style="height: 50px;" >
	<img src="<?php echo env('APP_URL')."/logo/".@$app_logo;?>" class="img-responsive" style="width:auto;height:35px;"></a>
    </div>  
						 <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
					 
	                </span>
                                        
                    <ul class="nav navbar-nav toolbar pull-right">
                          
					 <!-- <li class="dropdown">
					         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <span><i class="fa fa-cog"></i></span>
                            </a>
                            <ul class="dropdown-menu" role="menu"> 									   
                            </ul>
                        </li> -->
						
						<li class="dropdown">
					         <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Add" role="button" aria-expanded="false">
                               <span><i class="fa fa-plus-circle"></i></span>
                            </a>
                             <ul class="dropdown-menu entities" role="menu">
							 <!-- <li><a href="<?php echo e(URL::to('product_get_form_basic')); ?>"><i class="fa fa-list"></i><span> Products Add Form</span></a></li>
                               	   <li><a href="<?php echo e(URL::to('user_forms')); ?>"><i class="fa fa-user"></i><span> Add User</span></a></li>  
                               	   <li><a href="<?php echo e(URL::to('order_add')); ?>"><i class="fa fa-truck"></i><span> Add order</span></a></li> ---> 

 <li>
						  <div class=" border" id="main"  >

    <div class=" title">
	   <h5>PRODUCTS</h5>
	</div>
	<div class="row">
		<div class="col-lg-12">
		   
		   <div class="row">
		   
		   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_products')): ?>  
		    <div class="col-lg-4 col-sm-4">
		     <a href="<?php echo e(URL::to('product_get_form_basic')); ?>">
		     <img src="<?php echo e(URL::to('admin/images/product.png')); ?>" class="img-responsive center-block">
			 <p class="text-center ">Add Products</p>
		    </a>	
          </div>
		  <?php endif; ?>
		  
		  
		    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_categories')): ?>  
		  <div class="col-lg-4 col-sm-4">
		     <a href="<?php echo e(URL::to('view_categories')); ?>">
		     <img src="<?php echo e(URL::to('admin/images/categories.png')); ?>" class="img-responsive center-block">
			 <p class="text-center">Add Categories</p>
		   </a>	
          </div>
		   <?php endif; ?>
		  
		  <?php $status = \App\FeaturesSettings::where('title','brand_ids')->first(['status'])->status;
				  if($status == '1')
				  {
				  ?>
				  
		 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_brands')): ?>  
		  <div class="col-lg-4 col-sm-4">
		      <a href=" <?php echo e(URL::to('brands')); ?>">
		     <img src="<?php echo e(URL::to('admin/images/brand.png')); ?>" class="img-responsive center-block">
			 <p class="text-center">Add Brands</p>
		   </a>	
          </div>
		  <?php endif; ?>
		  
		   <?php } ?>
		   
         </div>		  
		</div>
	  </div>
	<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_users')): ?>  
	 <div class=" title">
	   <h5>USERS</h5>
	 </div>
	  <div class="row">
		<div class="col-lg-12">
		   <div class="row">
		    <div class="col-lg-4 col-sm-4">
		     <a href="<?php echo e(URL::to('user_forms')); ?>">
		     <img src="<?php echo e(URL::to('admin/images/user.png')); ?>" class="img-responsive center-block">
			 <p class="text-center">Add Users</p>
		   </a>	
          </div>
		   
         </div>		  
		</div>
	  </div>
	  <?php endif; ?>
	  
	  
	  
	  
	  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_orders')): ?>  
	  <div class=" title">
	   <h5>ORDERS</h5>
	 </div>
	  <div class="row">
		<div class="col-lg-12">
		   <div class="row">
		    <div class="col-lg-4 col-sm-4">
		      <a href="<?php echo e(URL::to('order_add')); ?>">
		     <img src="<?php echo e(URL::to('admin/images/order.png')); ?>" class="img-responsive center-block">
			 <p class="text-center">Add Orders</p>
		   </a>	
          </div> 
         </div>		  
		</div>
	  </div>
		  <?php endif; ?>  
 
	  
	 </div> 
	 
							  </li>

							  
                            </ul>
                        </li> 
						
						
						 <li class="dropdown">
                            <a href="#" class="dropdown-toggle name" data-toggle="dropdown" title="Sign Out" role="button" aria-expanded="false">
                                <span class="user-name"><i class="fa fa-sign-out"></i></span><!--<span class="caret head"></span>-->
                            </a>

                            <ul class="dropdown-menu entities" role="menu">
                                <li>
                                    <a href="<?php echo e(url('/logout')); ?>"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                        
                                    </form>
                                </li>
                            </ul>
                        </li>
					   
                    </ul>  
 


	 </header>