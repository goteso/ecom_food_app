<?php $__env->startSection('title', 'User Profile' ); ?>
 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
   
   
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="text-right">
                     </div>
					 
					 <textarea id="res2"  style="display:none;"></textarea>
					 <input type="hidden" id="user_id" id="user_id" value="<?php echo @$_GET["user_id"];?>">
                     <div class="tab-content" >
                        <!--------------------------Angular App Starts ------------------------------------>
						
						<div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/images/89.svg')); ?>" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
      <div ng-app="mainApp">
         <div class="container-fluid"  ng-controller="profileController" ng-cloak>
   <div class="row" > 
	     <div class="col-sm-8 col-md-9 user-profile-data" >
		 
		 <div class="row"  > 
	      <div class="col-sm-12 col-md-6 col-lg-5"   >
		    <div class="card info"  ng-repeat="values in profileData.user_data" >
		     <img src="admin/images/user-placeholder.png" style="display:block;margin-right:0">
			 <div>
		       <h4>{{values.first_name}} {{values.last_name}} <a href="<?php echo e(URL::to('user_edit_form')); ?>/{{values.id}}"><i class="fa fa-edit"></i></a></h4>
			   <h5>{{values.email}}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		   <div class="col-sm-6 col-md-3"   >
		    <div class="card"  ng-repeat="values in profileData.user_data" >
			<div class="card-data">
		       <h4>{{values.orders_count}}</h4>
			   <h5>Total Orders</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"  ng-repeat="values in profileData.user_data" >
		   <div class="card-data">
		      <h4><?php echo env("CURRENCY_SYMBOL", "");?>{{values.total_revenues}}</h4>
			 <h5>Total Revenues</h5>
			 </div>
		   </div>
		   </div> 
		 
		 
		 </div>
		 </div>
		  <div class="col-sm-4 col-md-3 text-right" >
		    <a class="btn btn-create" href="<?php echo e(URL::to('order_add')); ?>">Create<br> Order</a>
		 </div> 
  </div>
   </div>  
   

	  <!--
	 Location: <input type="text" id="us2-address" style="width: 200px"/>

<div id="us2" style="width: 500px; height: 400px;"></div>				
Lat.: <input type="text" id="us2-lat"/>
Long.: <input type="text" id="us2-lon"/>
	 -->
	
	 <div ng-view></div>
	 </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>  

<script>
function updateControls(addressComponents) {
    $('#us5-street1').val(addressComponents.addressLine1);
    $('#us5-city').val(addressComponents.city);
    $('#us5-state').val(addressComponents.stateOrProvince);
    $('#us5-zip').val(addressComponents.postalCode);
    $('#us5-country').val(addressComponents.country);
}
$('#us5').locationpicker({
    location: {latitude: 42.00, longitude: -73.82480799999996},
    radius: 300,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        updateControls(addressComponents);
    },
    oninitialized: function(component) {
        var addressComponents = $(component).locationpicker('map').location.addressComponents;
        updateControls(addressComponents);
    }
});
</script>
<script>$('#us2').locationpicker({
enableAutocomplete: true,
    enableReverseGeocode: true,
  radius: 0,
  inputBinding: {
    latitudeInput: $('#us2-lat'),
    longitudeInput: $('#us2-lon'),
    radiusInput: $('#us2-radius'),
    locationNameInput: $('#us2-address')
  },
  onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
    console.log(currentLocation);  //latlon  
    updateControls(addressComponents); //Data
    }
});

function updateControls(addressComponents) {
  console.log(addressComponents);
}</script>

		<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/userProfile.js')); ?>"></script> 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>