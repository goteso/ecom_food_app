-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2018 at 02:41 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `category`, `created_at`, `updated_at`) VALUES
(1, 'view_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(2, 'add_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(3, 'edit_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(4, 'delete_users', 'web', 'users', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(5, 'view_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(6, 'add_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(7, 'edit_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(8, 'delete_roles', 'web', 'roles', '2018-01-11 00:45:06', '2018-01-11 00:45:06'),
(9, 'add_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(10, 'edit_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(11, 'view_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(12, 'delete_orders', 'web', 'orders', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(13, 'add_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(14, 'edit_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(15, 'view_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(16, 'delete_products', 'web', 'products', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(17, 'add_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(18, 'edit_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(19, 'view_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(20, 'delete_brands', 'web', 'brands', '2018-05-02 18:30:00', '2018-05-02 18:30:00'),
(21, 'add_categories', 'web', 'categories', NULL, NULL),
(22, 'edit_categories', 'web', 'categories', NULL, NULL),
(23, 'view_categories', 'web', 'categories', NULL, NULL),
(24, 'delete_categories', 'web', 'categories', NULL, NULL),
(25, 'add_faqs', 'web', 'faqs', NULL, NULL),
(26, 'edit_faqs', 'web', 'faqs', NULL, NULL),
(27, 'view_faqs', 'web', 'faqs', NULL, NULL),
(28, 'delete_faqs', 'web', 'faqs', NULL, NULL),
(29, 'add_transactions', 'web', 'transactions', NULL, NULL),
(30, 'edit_transactions', 'web', 'transactions', NULL, NULL),
(31, 'view_transactions', 'web', 'transactions', NULL, NULL),
(32, 'delete_transactions', 'web', 'transactions', NULL, NULL),
(33, 'view_features_settings', 'web', 'features_settings', NULL, NULL),
(34, 'view_reports', 'web', 'reports', NULL, NULL),
(35, 'driver_id', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(36, 'vendor_id', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(37, 'orderStatus', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(38, 'orderPaymentReceived', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(39, 'printInvoiceThermal', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(40, 'emailInvoice', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(41, 'printInvoice', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00'),
(42, 'applyDiscount', 'web', 'quick_links', '2018-05-20 18:30:00', '2018-05-20 18:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
